# RUMBA - Wechselarbeitsplatzverwaltung
# Copyright (C) 2021 Kreis Soest 

Dieses Programm erlaubt die Verwaltung von Wechselarbeitsplätzen in einer Web-Anwendung.
Intern wird die Funktion in 4 Instanzen bereitgestellt:
1. RUMBA Frontend (docker image)
2. RUMBA Backend (docker image)
3. Keycloak Service (nicht enthalten)
4. MySQL Datenbank Service (nicht enthalten)

Im Ordner "documentation" findet man die notwendige Dokumentation zur Inbetriebnahme der Software.

Eine gute Einführung in die Anwendung findet man unter "documentation/2022-10-26_Präsentation_RUMBA_V1_6.pdf".

Das gesammte Projekt (Frontend und Backend) kann über das Linux-Skript "start-rumba-bothends.sh" gestartet und über das Skript "stop-rumba-bothends.sh" gestoppt werden. 

Genaueres zum Frontend und Backend steht in den README.md Dateien im "frontend"- bzw. "backend"-Ordner.
