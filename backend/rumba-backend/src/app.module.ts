/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookingsModule } from './bookings/bookings.module';
import { EmployeesModule } from './employees/employees.module';
import { WorkspacesModule } from './workspaces/workspaces.module';
import { RoomsModule } from './rooms/rooms.module';
import { OccupationModule } from './occupation/occupation.module';
import { HomeofficeModule } from './homeoffice/homeoffice.module';
import {
  KeycloakConnectModule,
  RoleGuard,
  AuthGuard,
} from 'nest-keycloak-connect';
import { APP_GUARD } from '@nestjs/core';
import { DepartmentsModule } from './departments/departments.module';
import { AbsenceModule } from './absence/absence.module';
import { MapsModule } from './maps/maps.module';
import { MappingsModule } from './mappings/mappings.module';
import { OptionsModule } from './options/options.module';
import { MappingOptionsUserModule } from './mapping-options-user/mapping-options-user.module';
import { EquipmentModule } from './equipment/equipment.module';
import { MappingEquipmentWorkspaceModule } from './mapping-equipment-workspace/mapping-equipment-workspace.module';
import { WorkspacecolorsModule } from './workspacecolors/workspacecolors.module';
import { ManageEmployeesModule } from './manage-employees/manage-employees.module';
import { ManageWorkspacesModule } from './manage-workspaces/manage-workspaces.module';

@Module({
  imports: [
    KeycloakConnectModule.register('./keycloak.json' || process.env.KEYCLOAK_PATH_JSON),
    TypeOrmModule.forRoot(), HomeofficeModule, OccupationModule, BookingsModule, EmployeesModule, WorkspacesModule, RoomsModule, DepartmentsModule, AbsenceModule, MapsModule, MappingsModule, OptionsModule, MappingOptionsUserModule, EquipmentModule, MappingEquipmentWorkspaceModule, WorkspacecolorsModule, ManageEmployeesModule, ManageWorkspacesModule],
  controllers: [],
  providers: [
    // This adds a global level authentication guard,
    // you can also have it scoped
    // if you like.
    //
    // Will return a 401 unauthorized when it is unable to
    // verify the JWT token or Bearer header is missing.
    {
      provide: APP_GUARD,
      useClass: AuthGuard
    },

    // New in 1.1.0
    // This adds a global level role guard, which is permissive.
    // Used by `@Roles` decorator with the 
    // optional `@AllowAnyRole` decorator for allowing any
    // specified role passed.
    {
      provide: APP_GUARD,
      useClass: RoleGuard,
    },
  ],
})
export class AppModule {
  
}
