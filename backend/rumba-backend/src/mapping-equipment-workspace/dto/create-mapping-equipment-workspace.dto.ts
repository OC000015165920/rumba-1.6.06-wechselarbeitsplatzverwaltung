import { ApiProperty } from "@nestjs/swagger";

/**
 * This Dto is used to store data to for the creatin of new mapping between Euqipment and Workpaces
 */
export class CreateMappingEquipmentWorkspaceDto {
    /**id of the equipment */
    @ApiProperty()
    equipment_id: number;

    /**id of the workspace */
    @ApiProperty()
    workspace_id: number;
}
