import { PartialType } from '@nestjs/swagger';
import { CreateMappingEquipmentWorkspaceDto } from './create-mapping-equipment-workspace.dto';

/**
 * This Dto is not used.
 */
export class UpdateMappingEquipmentWorkspaceDto extends PartialType(CreateMappingEquipmentWorkspaceDto) {}
