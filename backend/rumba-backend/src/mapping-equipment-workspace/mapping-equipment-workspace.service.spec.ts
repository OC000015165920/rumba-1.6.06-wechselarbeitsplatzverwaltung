import { Test, TestingModule } from '@nestjs/testing';
import { MappingEquipmentWorkspaceService } from './mapping-equipment-workspace.service';

describe('MappingEquipmentWorkspaceService', () => {
  let service: MappingEquipmentWorkspaceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MappingEquipmentWorkspaceService],
    }).compile();

    service = module.get<MappingEquipmentWorkspaceService>(MappingEquipmentWorkspaceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
