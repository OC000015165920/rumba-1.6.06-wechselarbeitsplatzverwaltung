import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MappingEquipmentWorkspaceService } from './mapping-equipment-workspace.service';

/**
 * This controller is not used.
 */
@Controller('mapping-equipment-workspace')
export class MappingEquipmentWorkspaceController {
  /**
   * The constructor
   * @param mappingEquipmentWorkspaceService MappingEquipmentWorkspaceService
   */
  constructor(private readonly mappingEquipmentWorkspaceService: MappingEquipmentWorkspaceService) {}

}
