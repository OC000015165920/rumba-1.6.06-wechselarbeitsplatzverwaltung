import { Module } from '@nestjs/common';
import { MappingEquipmentWorkspaceService } from './mapping-equipment-workspace.service';
import { MappingEquipmentWorkspaceController } from './mapping-equipment-workspace.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MappingEquipmentWorkspace } from './entities/mapping-equipment-workspace.entity';

@Module({
  imports:[TypeOrmModule.forFeature([MappingEquipmentWorkspace])],
  controllers: [MappingEquipmentWorkspaceController],
  providers: [MappingEquipmentWorkspaceService],
  exports: [MappingEquipmentWorkspaceService]
})
export class MappingEquipmentWorkspaceModule {}
