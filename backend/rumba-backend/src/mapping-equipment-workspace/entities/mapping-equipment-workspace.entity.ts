import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

/**
 * This entity is used for mappings between euipment and workspaces.
 */
@Entity()
export class MappingEquipmentWorkspace {
    /**id of the mapping */
    @PrimaryGeneratedColumn()
    id: number;

    /**id of the equipment */
    @Column()
    equipment_id: number;

    /**id of the workspace */
    @Column()
    workspace_id: number;

    /** Date of Creation of the Employee */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    creationDate: Date;
}
