import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMappingEquipmentWorkspaceDto } from './dto/create-mapping-equipment-workspace.dto';
import { UpdateMappingEquipmentWorkspaceDto } from './dto/update-mapping-equipment-workspace.dto';
import { MappingEquipmentWorkspace } from './entities/mapping-equipment-workspace.entity';

/**
 * This service is used to handle mappings of Workspaces and equipment
 */
@Injectable()
export class MappingEquipmentWorkspaceService {
  /**
   * The construtor
   * @param mappingEquipmentWorkspace the implicite repository containing the euipment-workspace-mappings
   */
  constructor(@InjectRepository(MappingEquipmentWorkspace) private mappingEquipmentWorkspace: Repository<MappingEquipmentWorkspace>){

  }

  /**
   * This function returns all Mappings with the specified workspace
   * @param id number, the id of the workspace
   * @returns MappingEquipmentWorkspace[]
   */
  findAllForWorkspace(id: number){
    return this.mappingEquipmentWorkspace.find({
      where:{
        workspace_id: id
      }
    })
  }

  /**
   * This function create a new mapping withe the data from the Dto.
   * @param createmapping CreateMappingEquipmentWorkspaceDto
   * @returns Promise
   */
  createnewMapping(createmapping: CreateMappingEquipmentWorkspaceDto){
    return this.mappingEquipmentWorkspace.save(createmapping);
  }

  /**
   * This function deletes a specified mapping.
   * @param mappingID number, id if the mapping to be deleted
   * @returns Promise
   */
  delete(mappingID:number){
    return this.mappingEquipmentWorkspace.delete(mappingID);
  }
}
