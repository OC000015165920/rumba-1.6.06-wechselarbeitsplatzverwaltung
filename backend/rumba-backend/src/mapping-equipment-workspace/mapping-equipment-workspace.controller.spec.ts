import { Test, TestingModule } from '@nestjs/testing';
import { MappingEquipmentWorkspaceController } from './mapping-equipment-workspace.controller';
import { MappingEquipmentWorkspaceService } from './mapping-equipment-workspace.service';

describe('MappingEquipmentWorkspaceController', () => {
  let controller: MappingEquipmentWorkspaceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MappingEquipmentWorkspaceController],
      providers: [MappingEquipmentWorkspaceService],
    }).compile();

    controller = module.get<MappingEquipmentWorkspaceController>(MappingEquipmentWorkspaceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
