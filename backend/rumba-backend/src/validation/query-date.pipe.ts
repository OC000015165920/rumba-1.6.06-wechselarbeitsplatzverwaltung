/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';

@Injectable()
export class QueryDatePipe implements PipeTransform<string, number> {
  transform(value: string, metadata: ArgumentMetadata): number {
    const val = parseInt(value, 10);
    if (isNaN(val) || !this.validateQueryDate(val)) {
      throw new BadRequestException('Validation failed');
    }
    return val;
  }

  private validateQueryDate(dateAsNumber:number): boolean {
        //take the first four numbers and store them to year 
        const year  = Math.floor(dateAsNumber/10000)

        //take the fifth and sixth number and store it to month
        const month  = Math.floor(dateAsNumber/100)-year*100

        //take the last two numbers and store it to day
        const day = dateAsNumber-year*10000-month*100

        //create a Date object from the calculated values
        const bookingDate = new Date(month+'-'+day+'-'+year)
  
        //for comparison with the current day, create a Date object with 0:00 AM
        const nowDate = new Date().setHours(0,0,0,0)

        //calculate the difference between booking date and now (in milliseconds)
        const diff = bookingDate.valueOf() - nowDate.valueOf();

        //calculate amount of days from the difference
        const diffDays = Math.ceil(diff/(1000*3600*24))

        //check whether the day is in the future, but not more than 28 days
        return (diffDays >= -3 && diffDays <= 28); // for async validations you must return a Promise<boolean> here
  }
  
}