/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Booking } from "src/bookings/entities/booking.entity";
import { Employee } from "src/employees/entities/employee.entity";
import { Workspace } from "src/workspaces/entities/workspace.entity";
import { ViewEntity, ViewColumn, Connection} from "typeorm";

/**
 * This Class describes the View for Homeoffice Bookings
 */
@ViewEntity({ 
    expression: (connection: Connection) => connection.createQueryBuilder()
        .select("booking.booking_id", "booking_id")
        .addSelect("booking.employee_id", "employee_id")
        .addSelect("booking.booked_for_day", "booked_for_day")
        .addSelect("booking.morning", "morning")
        .addSelect("booking.afternoon", "afternoon")
        // .addSelect("booking.workspace_id", "workspace_id")
        // .addSelect("workspace.workspace_name", "workspace_name")
        .addSelect("employee.forename", "forename")
        .addSelect("employee.lastname", "lastname")
        .addSelect("employee.email_address", "email")
        .addSelect("employee.department", "department")
        .addSelect("booking.comment", "comment")
        .from(Booking, "booking")
        // .innerJoin(Workspace, "workspace", "workspace.workspace_id = booking.workspace_id")
        .innerJoin(Employee, "employee", "employee.employee_id = booking.employee_id")
        .where("booking.category = 2")
})
export class HomeofficeView {
    /** ID of the Booking */
    @ViewColumn()
    booking_id: number;
  
    /** Date of Booking */
    @ViewColumn()
    booked_for_day: number;

    /** id of the employee in homeoffice */
    @ViewColumn()
    employee_id: number;
  
    // @ViewColumn()
    // workspace_id: number;

    /** Name of the Workspace */
    // @ViewColumn()
    // workspace_name: string;
  
    /** Forename of the employee */
    @ViewColumn()
    forename: string;

    /** Lastname of the employee */
    @ViewColumn()
    lastname: string;

    /** email of the employee */
    @ViewColumn()
    email: string;

    /** Department of the employee */
    @ViewColumn()
    department: string; 
    
    /** Morning is booked */
    @ViewColumn()
    morning: boolean;

    /** Afternoon is booked */
    @ViewColumn()
    afternoon: boolean;

    /** Comment of the booking */
    @ViewColumn()
    comment: string;

    /** Boolean if the name is shown on the public map */
    anonymous: boolean;

}
