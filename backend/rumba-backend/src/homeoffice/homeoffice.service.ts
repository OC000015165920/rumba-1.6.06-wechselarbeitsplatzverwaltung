/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@nestjs/common';
import {HomeofficeView} from './entities/homeoffice-view.entity'
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OptionsService } from 'src/options/options.service';

/**
 * This service handles homeoffice bookings
 */
@Injectable()
export class HomeofficeService {
    /**
     * The constructor
     * @param homeofficeRepo the implicite repository containing the homeoffice bookings
     * @param optionsService OptionsService
     */
    constructor(@InjectRepository(HomeofficeView) private  homeofficeRepo: Repository<HomeofficeView>, private optionsService: OptionsService){

    }

    /**
     * This function finds and returns a homeoffice view by its booking id.
     * @param booking_id number, id of the booking
     * @returns HomeofficeView
     */
    async findOneById(booking_id: number): Promise<HomeofficeView> {
        try {
        return await this.homeofficeRepo.findOneOrFail({where:{booking_id: booking_id}}); //select * from employees where employee_id = is
        } catch (err) {
        throw err;
        }
    }

    /**
     * This function returns all homeoffice view entities for the specified day and department.
     * @param day number, date as a number
     * @param department string, name of the department
     * @returns HomeofficeView[], the homeoffice bookings
     */
    async findByDay(day: number, department: string): Promise<HomeofficeView[]> {
        try {
            let results =  await this.homeofficeRepo.find({where: {
                booked_for_day: day,
                department: department
            }})
        
            for (const result of results) {
                let options = await this.optionsService.findAllForEmployee(result.employee_id);
                if(options.length > 0){
                    for (const option of options) {
                        if(option.name == "anonymous"){
                            result.anonymous = true;
                        }
                        else{
                            result.anonymous = false;
                        }  
                    }
                }
                else{
                    result.anonymous = false;
                }
            }
            return results;
        } catch (err) {
        throw err;
        }
    }

    /**
     * This function returns all HomeofficeViews.
     * @returns HomeofficeView[]
     */
    async findAll(): Promise<HomeofficeView[]> {
        return await this.homeofficeRepo.find();
    }
}
