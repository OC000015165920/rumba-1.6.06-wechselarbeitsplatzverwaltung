/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Controller, Get, Param,ParseIntPipe} from '@nestjs/common';
import { HomeofficeService } from './homeoffice.service';
import { ApiTags, ApiOperation} from '@nestjs/swagger';
import { Unprotected } from 'nest-keycloak-connect';
import { QueryDatePipe } from 'src/validation/query-date.pipe';

/**
 * This Controller manages the Endpoints for Homeoffice Occupations
 */
@ApiTags('Homeoffice')
@Controller('homeoffice')
export class HomeofficeController {
  /**
   * The constructor 
   * @param homeofficeService HomeofficeService
   */
  constructor(private readonly homeofficeService: HomeofficeService) {}
  
  /**
   * This function returns all homeofficebookings of the specified department of te specified day.
   * @param day number, day for which to get the Homeofficebookings.
   * @param department string, name of the department
   * @returns HomeofficeView[]
   */
  @ApiOperation({ summary: 'Homeofficebuchungen pro Tag YYYYMMDD' })
  @Get(':day/:department')
  @Unprotected()
  findOccupations(@Param('day', new QueryDatePipe()) day: number, @Param('department') department: string) {
    return this.homeofficeService.findByDay(+day, department);
  }
}

