/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { PartialType } from '@nestjs/mapped-types';
import { CreateRoomDto } from './create-room.dto';
import { ApiProperty } from "@nestjs/swagger";
import {IsNotEmpty, Length} from 'class-validator';

/**
 * This Dto is used to store data to update a room entity.
 */
export class UpdateRoomDto extends PartialType(CreateRoomDto) {
    /**name of the room */
    @ApiProperty()
    @IsNotEmpty()
    @Length(5)
    room_name: string;

    /**name of the department of the room */
    @ApiProperty()
    department: string;

    /**description of the room */
    @ApiProperty()
    description: string;
}
