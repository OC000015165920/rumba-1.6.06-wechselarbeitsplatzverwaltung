/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Room } from './entities/room.entity';
import { CreateRoomDto } from './dto/create-room.dto';
import { UpdateRoomDto } from './dto/update-room.dto';

/**
 * THis service is used to handle rooms.
 */
@Injectable()
export class RoomsService {
  /**
   * The constructor
   * @param roomsRepo the implicite repository containing the rooms
   */
  constructor(@InjectRepository(Room) private roomsRepo: Repository<Room>){    
  }

  /**
   * This function create a neew room entity.
   * @param createRoomDto CreateRoomDto
   * @returns Promise
   */
  async create(createRoomDto: CreateRoomDto) {
    return await this.roomsRepo.save(createRoomDto);
  }

  /**
   * This function returns all rooms of the specified department.
   * @param department string, name of the department
   * @returns 
   */
  async findAll(department: string): Promise<Room[]>{
    return await this.roomsRepo.find({relations:['workspaces'], where: {department: department}});
  } 

  // async findAll(): Promise<OutputRoomDto[]>{
  //   const nestedRooms = await this.roomsRepo.find({relations:['workspaces']});
  //   const flatRooms:OutputRoomDto[] = [];
  //     for (const room of nestedRooms) {
  //      const flatRoom = new OutputRoomDto();
  //      flatRoom.room_name = room.room_name;
  //      flatRoom.workspace_name = room.workspaces[0].workspace_name;
  //      flatRoom.department = room.department;
  //      flatRoom.description = room.description;
  //      flatRooms.push(flatRoom);
  //     }
  //     return flatRooms;
  //  }

  /**
   * This function returns the room with the specified id if it exists.
   * @param id number, id of the room
   * @returns Room
   */
  async findOne(id: number) {
    return await this.roomsRepo.find({room_id: id});
  }

  /**
   * This function deletes the room entity of the specified id.
   * @param id number, id of the room
   */
  async remove(id: number) {
    await this.roomsRepo.delete({room_id: id});
  }

  /**
   * This function updates the room of the specified room id with the data from the Dto.
   * @param room_id number, id of the room
   * @param updateRoomDto UpdateRoomDto
   * @returns 
   */
  async update(room_id: number, updateRoomDto: UpdateRoomDto) {
    return await this.roomsRepo.update(room_id, updateRoomDto);
  }
}
