/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Workspace } from "../../workspaces/entities/workspace.entity";
import { Column, Entity, PrimaryGeneratedColumn, OneToMany, ManyToMany, JoinTable } from "typeorm";

/**
 * This entity is used to store room data.
 */
@Entity()
export class Room {
    /**id of the room */
    @PrimaryGeneratedColumn()
    room_id: number;

    /**name of the room */
    @Column()
    room_name: string;

    /**name of the department of the room */
    @Column()
    department: string;

    /**description of the room */
    @Column()
    description: string;

    /**date of creation of the workspace */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    creationDate: Date;

    /**all workspaces in the room */
    @OneToMany(() => Workspace, (workspace: Workspace) => workspace.room)
    public workspaces: Workspace[];    

    /**id of the building for other purposes than RUMBA */
    @Column({default: "0020"})
    building_id: string;
}
