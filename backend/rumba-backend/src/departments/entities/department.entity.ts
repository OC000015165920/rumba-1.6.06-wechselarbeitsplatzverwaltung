import { ConflictException } from "@nestjs/common";
import { Map } from "src/maps/entities/map.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

/**
 * Describes the Table of the Departments
 */
@Entity()
export class Department {
    /** Primary Key with Auto Increment */
    @PrimaryGeneratedColumn()
    id: number;

    /** Name of the Department */
    @Column()
    name: string;

    /** Description of the Department */
    @Column()
    description: string;

    /** The Number of Days how far users can book in the future */
    @Column()
    BookingInAdvanceDays: number;

    /** Describes the schema for the Users Names on the map */
    @Column()
    NamePatternOnMap: string;

    /**if the infocards in the department should be rotated as the workspaces */
    @Column()
    RotateInfocards: boolean;

    /** Date of Creation  */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    creationDate: Date;

    /** The Maps which belongs to the department */
    maps: Map[];

    /** Date of the last Modification */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    modificationDate: Date;

    /** Who did the last modification */
    @Column({default:''})
    modifiedBy: string;
    
    /** Who did the last modification */
    @Column({default:''})
    modifiedByUsername: string;

}
