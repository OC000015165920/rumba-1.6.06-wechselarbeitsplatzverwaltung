import { Module } from '@nestjs/common';
import { DepartmentsService } from './departments.service';
import { DepartmentsController } from './departments.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Department } from './entities/department.entity';
import { Map } from 'src/maps/entities/map.entity';
import { MapsModule } from 'src/maps/maps.module';
import { MapsService } from 'src/maps/maps.service';
import { EmployeesModule } from 'src/employees/employees.module';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { MappingsModule } from 'src/mappings/mappings.module';

@Module({
  controllers: [DepartmentsController],
  providers: [DepartmentsService],
  imports: [TypeOrmModule.forFeature([Department]),MapsModule, MappingsModule, EmployeesModule],
  exports:[DepartmentsService],
})
export class DepartmentsModule {}
