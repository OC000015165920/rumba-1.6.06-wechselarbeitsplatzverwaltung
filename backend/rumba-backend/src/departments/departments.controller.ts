import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Roles, Unprotected } from 'nest-keycloak-connect';
import { User } from 'src/employees/employee.decorator';
import { EmployeesService } from 'src/employees/employees.service';
import { DepartmentsService } from './departments.service';
import { CreateDepartmentDto } from './dto/create-department.dto';
import { updateDepartmentDTO } from './dto/update-department.dto';

/**
 * Controller for managing the Endpoints for Departments
 */
@Controller('departments')
@ApiTags('Departments')
export class DepartmentsController {
  /**
   * The constructor
   * @param departmentsService The Service where most of the logic and functions are
   */
  constructor(private readonly departmentsService: DepartmentsService,
    private employeesService: EmployeesService) {}

  /**
   * This function returns the department entity of the orgaunits given.
   * @param orgaunits is a string array which contains all the groups the user has in AD
   * --> The Frontend gets the roles from keycloak
   * @param user The Keycloak User
   * @returns a Department when there is a match between the orgaunits and the mapping table
   */
  @Post('/departmentByRole/')
  getDepartmentFromOrgaunit(@Body() orgaunits: string[],@User() user){
    let username = user.preferred_username;
    return this.departmentsService.getDepartmentFromRoles(orgaunits, username);
  }

  /**
   * 
   * @returns all Mappings orgaunit <-> department
   */
  // @Get('/allMappings')
  // getAllMappingsFromTable(){
  //   return this.departmentsService.getAllMappings();
  // }

  /**
   * This function returns the department entity of the given name.
   * @param name name of the department
   * @returns a department object when the given name was found
   */
  @Unprotected()
  @Get('/byname/:name')
  getDepartmentByName(@Param('name') name: string){
    return this.departmentsService.findOneByName(name);
  }

  /**
   * This function returns the department entity of the given name.
   * @param name name of the department
   * @returns a department object when the given name was found
   */
   @Unprotected()
   @Get('/getAll')
   getAllDepartments(){
     return this.departmentsService.findAll();
   }


  /**
   * This function updates the specified department object with the data given in the updateDepartmentDTO.
   * @param updateDepartment the updated Department Object
   * @param id the id of the department that should get updated
   * @param user the actual User from Keycloak
   * @returns the updated Department
   */
  @Patch(':id')
  @Roles({ roles: ['admin'] })
  async updateDepartment(@Body() updateDepartment: updateDepartmentDTO,@Param('id', new ParseIntPipe) id: number, @User() user){
    let adminuser = await this.employeesService.findEmployeeByUsername(user.preferred_username);
    let sameDepartment = this.employeesService.AdminAllowedForDepartment(updateDepartment.name, adminuser.department)
    if(sameDepartment){
      return this.departmentsService.update(id,updateDepartment);
    }

  }
}
