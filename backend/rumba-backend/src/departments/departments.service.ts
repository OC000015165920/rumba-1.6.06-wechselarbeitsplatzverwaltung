import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateDepartmentDto } from './dto/create-department.dto';
import { Department } from './entities/department.entity';
import { Map } from "src/maps/entities/map.entity";
import { MapsService } from 'src/maps/maps.service';
import { EmployeesService } from 'src/employees/employees.service';
import { MappingsService } from 'src/mappings/mappings.service';
import { updateDepartmentDTO } from './dto/update-department.dto';

/**
 * The Service for the Controller 
 */
@Injectable()
export class DepartmentsService {
  /**
   * The constructor
   * @param mapper the Connection to the mapping_department_orgaunit as a Repository
   * @param departmentRepo the connection to the department table as a Repository
   * @param department_map_Mapper the connection to the mapping_department_map as a Repository
   * @param mapsService The Service to manage maps
   * @param employeeService The Service to manage employees
   */
  constructor(@InjectRepository(Department) private departmentRepo: Repository<Department>,
  private mapsService: MapsService, private employeeService: EmployeesService,
  private mappingsService: MappingsService
  ){

  }
  /**
   * This function creates new department with the data from the Dto.
   * @param createDepartmentDto the department to get created
   * @returns the new created department
   */
  create(createDepartmentDto: CreateDepartmentDto) {
    return this.departmentRepo.save(createDepartmentDto);
  }

  /**
   * This function returns all the departments.
   * @returns returns all departments
   */
  async findAll() {
    let all = await this.departmentRepo.find();
    for(let dep of all){
      dep.maps = await this.mappingsService.getMapsForDepartment(dep.name);
    }
    return all;
  }

  /**
   * This function retruns the department with the given id if it exists.
   * @param id the id of the booking
   * @returns the booking with the given id
   */
  findOne(id: number) {
    return this.departmentRepo.findOneOrFail(id);
  }

  /**
   * This function updates the specified department with data from the Dto.
   * @param id Id of the Department that should get updated
   * @param updateDepartment the new Department Object
   * @returns the updated Department
   */
  update(id: number,updateDepartment: updateDepartmentDTO){
    return this.departmentRepo.update(id,updateDepartment);
  }

  /**
   * This function returns the department with the given name if it exists.
   * @param name name of the department which should be found
   * @returns a department
   */
  async findOneByName(name: string){
    try{
      let dep = await this.departmentRepo.findOneOrFail({
        where:{
          name: name
        }
      });
      dep.maps = await this.mappingsService.getMapsForDepartment(name);
      return dep;
    }
    catch (e) {
      console.log(e);
      return undefined;
    }
  }

  /**
   * This function returns all mappings between orgunit and department.
   * @returns Returns all Mappings for orgaunit <-> Depatment
   */
  getAllMappingsOrgaUnit_Dep(){
    return this.mappingsService.FindAllOrgaDepartmentMappings();
  }

  /**
   * This function returns the department of a user by evaluating its roles.
   * @param roles String Array of all roles comming from the frontend (which gets them from keycloak)
   * @param username Username of the keycloak user
   * @returns a single Department 
   */
  async getDepartmentFromRoles(roles: string[], username){
    const allMappings = await this.getAllMappingsOrgaUnit_Dep();
    const allDepartments = await this.findAll();
    for (const role of roles) {
      for (const mapping of allMappings) {
        if(role == mapping.orgaunit){
          let dep = allDepartments.find(d => d.name === mapping.department);
          dep.maps = await this.mappingsService.getMapsForDepartment(dep.name);

          // let user = await this.employeeService.findEmployeeByUsername(username);
          // console.log(user)
          // if(user.default_mapID != undefined){
          //   for (let i = 0; i < dep.maps.length; i++) {
          //     const element = dep.maps[i];
          //     if(element.id == user.default_mapID){
          //       //Dont change Items when it is already at the first position
          //       if(i>0){
          //         let old = dep.maps[0]
          //         dep.maps[0]=element
          //         dep.maps[i] = old
          //       }
          //     }
              
          //   }
          // }
          return dep;
          
        }
      }
    }
    return allDepartments.find(d => d.name === "Allgemein"); // Returns Allgemein when there is a error on loading the departments or when there is no result
  }


}
