import { ApiProperty, PartialType } from "@nestjs/swagger";
import { CreateDepartmentDto } from "./create-department.dto";

/**
 * This class is used to update department entities.
 */
export class updateDepartmentDTO extends PartialType(CreateDepartmentDto) {

    /** Additional Information */
    @ApiProperty()
    description: string;

    /** Describes the schema for the Users Names on the map */
    @ApiProperty()
    NamePatternOnMap: string;

    /**if the infocards in the department should be rotated as the workspaces */
    @ApiProperty()
    RotateInfocards: boolean;

    /** Date of the last modification for the employee */
    modificationDate: Date;
    /** Who did the last modification */
    modifiedBy: string;

    /** Username of the User who did the change */
    modifiedByUsername:string;    
}