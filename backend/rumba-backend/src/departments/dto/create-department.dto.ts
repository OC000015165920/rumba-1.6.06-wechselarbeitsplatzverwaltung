import { ApiProperty } from "@nestjs/swagger";
/**
 * This Class describes the DTO for creating a new Department
 */
export class CreateDepartmentDto {
    /** name of the Department */
    @ApiProperty()
    name: string;

    /** Additional Information */
    @ApiProperty()
    description: string;

    /** The Number of Days how far users can book in the future */
    @ApiProperty()
    BookingInAdvanceDays: number;

    /** Describes the schema for the Users Names on the map */
    @ApiProperty()
    NamePatternOnMap: string;

    /**if the infocards in the department should be rotated as the workspaces */
    @ApiProperty()
    RotateInfocards: boolean;
}
