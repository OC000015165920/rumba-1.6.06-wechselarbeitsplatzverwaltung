import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { WorkspacecolorsService } from './workspacecolors.service';
import { CreateWorkspacecolorDto } from './dto/create-workspacecolor.dto';
import { UpdateWorkspacecolorDto } from './dto/update-workspacecolor.dto';
import { ApiTags } from '@nestjs/swagger';
import { Roles, Unprotected } from 'nest-keycloak-connect';

/**
 * This class handles all the REST requests for Workspacecolors. Workspacecolor is used to give the workspaces a color on the map for example color=000000 is black. The diffrent colors can have different meanings which can be explained in description
 */
@ApiTags('WorkspaceColor')
@Controller('workspacecolors')
export class WorkspacecolorsController {
  /**
   * This function initializes the workspacecolorsService.
   * @param workspacecolorsService WorkspacecolorsService
   */
  constructor(private readonly workspacecolorsService: WorkspacecolorsService) {}

  /**
   * This function creates a new Workspacecolor entity.
   * @param createWorkspacecolorDto CreateWorkspacecolorDto, the data needed to create a new entity
   * @returns Workspacecolor
   */
  @Post()
  @Roles({roles: ['admin']})
  create(@Body() createWorkspacecolorDto: CreateWorkspacecolorDto) {
    return this.workspacecolorsService.create(createWorkspacecolorDto);
  }

  /**
   * This function returns all Workspacecolors of the given department. Is unprotected because it is needed on the publicpage.
   * @param department string, name of the department
   * @returns Workspacecolor[]
   */
  @Get('department/:department')
  @Unprotected()
  findAllByDepartment(@Param('department') department: string) {
    return this.workspacecolorsService.findAllByDepartment(department);
  }

  /**
   * This function returns the workspacecolor entity of the given id.
   * @param id number, id of the Workspacecolor
   * @returns Workspacecolor
   */
  @Get(':id')
  @Roles({roles: ['admin']})
  findOne(@Param('id') id: number) {
    return this.workspacecolorsService.findOne(id);
  }

  /**
   * This function returns the workspacecolor of the department of the given color
   * @param department string, name of the department
   * @param color string, rgb code of the color i.e. 000000 for black
   * @returns Workspacecolor
   */
  @Get('departmentColor/:department/:color')
  @Roles({roles: ['admin']})
  findOneByColorAndDepartment(@Param('department') department: string, @Param('color') color: string){
    return this.workspacecolorsService.findOneByColorAndDepartment(department, color);
  }

  /**
   * This function updates certain data of the Workspacecolor of the given id.
   * @param id number, id of the workspace
   * @param updateWorkspacecolorDto UpdateWorkspacecolor, data to update 
   * @returns Observable
   */
  @Patch(':id')
  @Roles({roles: ['admin']})
  update(@Param('id') id: number, @Body() updateWorkspacecolorDto: UpdateWorkspacecolorDto) {
    return this.workspacecolorsService.update(id, updateWorkspacecolorDto);
  }

  /**
   * This function removes the workspacecolor entity of the given id.
   * @param id number, id of the workspacecolor
   * @returns Observable
   */
  @Delete(':id')
  @Roles({roles: ['admin']})
  remove(@Param('id') id: number) {
    return this.workspacecolorsService.remove(id);
  }
}
