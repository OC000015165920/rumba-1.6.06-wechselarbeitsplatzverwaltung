import { Workspace } from "src/workspaces/entities/workspace.entity";
import { Column, Entity, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

/**
 * This entity is used to store a color for a workspace with additional data.
 */
@Entity()
export class Workspacecolor {
    /**id of the workspacecolor */
    @PrimaryGeneratedColumn()
    id: number;

    /**color of the workspacecolor*/
    @Column({default:'000000'})
    color: string;

    /**description of the workspacecolor */
    @Column({default:''})
    description: string;

    /**department for which the workspacecolor is */
    @Column()
    department: string;

}
