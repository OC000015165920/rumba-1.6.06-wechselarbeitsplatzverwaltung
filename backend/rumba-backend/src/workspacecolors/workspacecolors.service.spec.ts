import { Test, TestingModule } from '@nestjs/testing';
import { WorkspacecolorsService } from './workspacecolors.service';

describe('WorkspacecolorsService', () => {
  let service: WorkspacecolorsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WorkspacecolorsService],
    }).compile();

    service = module.get<WorkspacecolorsService>(WorkspacecolorsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
