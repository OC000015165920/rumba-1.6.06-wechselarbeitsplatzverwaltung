import { Module } from '@nestjs/common';
import { WorkspacecolorsService } from './workspacecolors.service';
import { WorkspacecolorsController } from './workspacecolors.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Workspacecolor } from './entities/workspacecolor.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Workspacecolor])],
  controllers: [WorkspacecolorsController],
  providers: [WorkspacecolorsService],
  exports: [WorkspacecolorsService]
})
export class WorkspacecolorsModule {}
