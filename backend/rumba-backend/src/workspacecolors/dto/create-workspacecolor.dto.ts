import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

/**
 * This interface is used to create a new Workspacecolor entity.
 */
export class CreateWorkspacecolorDto {
    /**color of the workspacecolor*/
    @ApiProperty()
    @IsNotEmpty()
    color: string;

    /**description of the workspacecolor */
    @ApiProperty()
    description: string;

    /**department for which the workspacecolor is */
    @ApiProperty()
    @IsNotEmpty()
    department: string;
}
