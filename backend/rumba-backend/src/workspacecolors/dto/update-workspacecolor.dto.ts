import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { CreateWorkspacecolorDto } from './create-workspacecolor.dto';

/**
 * This interface is used to update a Workspacecolor entity.
 */
export class UpdateWorkspacecolorDto extends PartialType(CreateWorkspacecolorDto) {
    /**description of the workspacecolor */
    @ApiProperty()
    description: string;
}
