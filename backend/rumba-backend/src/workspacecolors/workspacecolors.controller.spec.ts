import { Test, TestingModule } from '@nestjs/testing';
import { WorkspacecolorsController } from './workspacecolors.controller';
import { WorkspacecolorsService } from './workspacecolors.service';

describe('WorkspacecolorsController', () => {
  let controller: WorkspacecolorsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WorkspacecolorsController],
      providers: [WorkspacecolorsService],
    }).compile();

    controller = module.get<WorkspacecolorsController>(WorkspacecolorsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
