import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateWorkspacecolorDto } from './dto/create-workspacecolor.dto';
import { UpdateWorkspacecolorDto } from './dto/update-workspacecolor.dto';
import { Workspacecolor } from './entities/workspacecolor.entity';

/**
 * This class is used to handle the workspacecolor entities.
 */
@Injectable()
export class WorkspacecolorsService {

  /**
   * Constructs the Service (called from nestJS framework)
   * @param repo the implicite repository containing the workspacecolors
   */
  constructor(@InjectRepository(Workspacecolor) private repo: Repository<Workspacecolor>){

  }

  /**
   * This function creates a new Workspacecolor entity.
   * @param createWorkspacecolorDto CreateWorkspacecolorDTO, Object to store the data needed
   * @returns Workspacecolor, the newly created Workspacecolor
   */
  create(createWorkspacecolorDto: CreateWorkspacecolorDto) {
    return this.repo.save(createWorkspacecolorDto);
  }

  /**
   * This function returns all Workspacecolors of the specified department.
   * @param department string, name of the department
   * @returns Workspacecolor[]
   */
  findAllByDepartment(department: string): Promise<Workspacecolor[]> {
    return this.repo.find({where:{department: department}});   
  }

  /**
   * This functions returns the Workspacecolor of the given id.
   * @param id number, id of the Workspacecolor
   * @returns Workspacecolor
   */
  findOne(id: number) {
    return this.repo.findOne({where:{id: id}});
  }

  /**
   * This function returns the Workspacecolor of the given department and color if it exists.
   * @param department string, name of the department
   * @param color string, the rgb code of the color i.e. 000000 for black
   * @returns Workspacecolor
   */
  findOneByColorAndDepartment(department: string, color: string){
    return this.repo.findOneOrFail({where:{department: department, color: color}});
  }

  /**
   * This function updates the workspacecolor entity of the given id.
   * @param id number, id of the workspacecolor to be updated
   * @param updateWorkspacecolorDto UpdateWorkspacecolorDto
   * @returns Observable
   */
  update(id: number, updateWorkspacecolorDto: UpdateWorkspacecolorDto) {
    return this.repo.update(id, updateWorkspacecolorDto);
  }

  /**
   * Thsis function removes the Workspacecolor entity of the given id from the database
   * @param id number, id of the Workspacecolor to be removed
   * @returns Observable
   */
  remove(id: number) {
    return this.repo.delete(id);
  }
}
