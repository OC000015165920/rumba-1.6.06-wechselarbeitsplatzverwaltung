/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

/**
 * This function initializes the backend
 */
async function bootstrap() {
  const app = await NestFactory.create(AppModule)//, {logger: false});

  const config = new DocumentBuilder()
    .setTitle('RUMBA API - Arbeitsplatzverwaltung Kreis Soest')
    .setDescription('Backend für die Arbeitsplatzverwaltung des Kreises Soest (V1.6). <br> ACHTUNG: EINSATZ NUR IM INTRANET, kein sicheres Backend.')
    .setVersion('V1.6')
    .addTag('RUMBA')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/', app, document);
  
  app.enableCors(); //Why don't we use Same Origin Policy? KH
  app.useGlobalPipes(new ValidationPipe({disableErrorMessages: false, transform:true})); //all incoming requests will use an in-built validation pipe
  await app.listen(3000);
}
bootstrap();
