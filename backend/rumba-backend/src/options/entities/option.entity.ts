import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

/**
 * This entity is used to store options or roles (not admin) for employees.
 */
@Entity()
export class Option {
    /** The Primary Key */
    @PrimaryGeneratedColumn()
    id: number;

    /** name of the option */
    @Column()
    name: string;

    /** Date of Creation of the Employee */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    creationDate: Date;

    /** Date of the last Modification */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    modificationDate: Date;

    /** Who did the last modification */
    @Column({default:''})
    modifiedBy: string;
    
    /** Who did the last modification */
    @Column({default:''})
    modifiedByUsername: string;
    
}
