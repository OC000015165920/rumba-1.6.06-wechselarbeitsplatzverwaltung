/**
 * This Dto is used to store all neccessary data of the option for the frontend.
 */
export class returnOptionDTO {
    /**id of the option */
    options_id: number;
    /**name of the option */
    name: string;
}