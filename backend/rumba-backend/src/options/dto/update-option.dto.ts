import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateOptionDto } from './create-option.dto';

/**
 * This Dto is used to store data to update an option.
 */
export class UpdateOptionDto extends PartialType(CreateOptionDto) {
    /**name of the option */
    @ApiProperty()
    name: string;
}
