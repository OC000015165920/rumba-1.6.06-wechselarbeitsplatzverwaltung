import { Module } from '@nestjs/common';
import { OptionsService } from './options.service';
import { OptionsController } from './options.controller';
import { MappingOptionsUserModule } from 'src/mapping-options-user/mapping-options-user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Option } from './entities/option.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Option]),MappingOptionsUserModule],
  exports:[OptionsService],
  controllers: [OptionsController],
  providers: [OptionsService]
})
export class OptionsModule {}
