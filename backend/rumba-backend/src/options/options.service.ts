import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateMappingOptionsUserDto } from 'src/mapping-options-user/dto/create-mapping-options-user.dto';
import { MappingOptionsUserService } from 'src/mapping-options-user/mapping-options-user.service';
import { Repository } from 'typeorm';
import { CreateOptionDto } from './dto/create-option.dto';
import { returnOptionDTO } from './dto/return-option.dto';
import { UpdateOptionDto } from './dto/update-option.dto';
import { Option } from './entities/option.entity';

/**
 * This service handles options for employees
 */
@Injectable()
export class OptionsService {
  /**
   * The constructor
   * @param optionsRepo the implicite repository containing the options
   * @param mappingOptionsUserService MappingOptionsUserService
   */
  constructor(@InjectRepository(Option) private optionsRepo: Repository<Option>,
    private readonly mappingOptionsUserService: MappingOptionsUserService){

  } 

  /**
   * This function returns all options.
   * @returns ReturnOptionDTO[]
   */
  async findAll(){
    let allOptions = await this.optionsRepo.find();
    let result: returnOptionDTO[] = []
    for (const option of allOptions) {
      let newResult: returnOptionDTO = {
        options_id:option.id,
        name: option.name
      }
      result.push(newResult)
    }

    return result;

  }

  /**
   * This function returns all options of the specified employee.
   * @param userid number, id of the employee
   * @returns ReturnOptionDTO[]
   */
  async findAllForEmployee(userid: number){
    let allMappingsForEmployee = await this.mappingOptionsUserService.findAllForEmployee(userid);
    let allOptions = await this.findAll();

    let result: returnOptionDTO[] =[]
    for (const option of allOptions) {
      for (const mapping of allMappingsForEmployee) {
        if(option.options_id == mapping.options_id){

          result.push(option);
          
        }
      }
    }

    return result;
  }


  /**
   * This function creates a new Mapping with data of the Dto.
   * @param newmapping CreateMappingOptionsUserDto
   * @returns Promise
   */
  createNewMapping(newmapping: CreateMappingOptionsUserDto){
    return this.mappingOptionsUserService.createnewMapping(newmapping);
  }

  /**
   * This function deletes a mapping of the employee with the option.
   * @param employee_id number, id of the employee
   * @param option_id number, id of the option
   * @returns Promise
   */
  async deleteMapping(employee_id:number, option_id: number){
    let mappingsForUser = await this.mappingOptionsUserService.findAllForEmployee(employee_id);
    for (const mapping of mappingsForUser) {
      if(mapping.options_id == option_id){
        return this.mappingOptionsUserService.delete(mapping.id);
      }
    }

  }
}
