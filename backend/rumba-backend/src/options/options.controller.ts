import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe } from '@nestjs/common';
import { OptionsService } from './options.service';
import { CreateOptionDto } from './dto/create-option.dto';
import { UpdateOptionDto } from './dto/update-option.dto';
import { Roles, Unprotected } from 'nest-keycloak-connect';
import { MappingOptionsUserService } from 'src/mapping-options-user/mapping-options-user.service';
import { ApiTags } from '@nestjs/swagger';
import { CreateMappingOptionsUserDto } from 'src/mapping-options-user/dto/create-mapping-options-user.dto';

/**
 * This controller is used to handle options
 */
@ApiTags('Options')
@Controller('options')
export class OptionsController {
  /**
   * The constructor
   * @param optionsService OptionsService
   */
  constructor(private readonly optionsService: OptionsService) {}

  /**
   * This function returns all Options of the specified employee
   * @param employee_id number, id of the employee
   * @returns ReturnOptionDTO[]
   */
  @Get(':employee_id')
  @Roles({ roles: ['admin'] })
  // @Unprotected()
  findAllForEmployee(@Param('employee_id', new ParseIntPipe) employee_id: number){
    return this.optionsService.findAllForEmployee(employee_id)
  }

  /**
   * This function returns all options
   * @returns ReturnOptionDTO[]
   */
  @Get()
  @Roles({ roles: ['admin'] })
  findAllOptions(){
    return this.optionsService.findAll();
  }

  /**
   * This function create a new Option for an Employee by creating a mapping. (as admin)
   * @param createMappingOptionsUserDTO CreateMappingOptionsUserDto
   * @returns Promise
   */
  @Post('/admin/giveEmployeeOption')
  @Roles({ roles: ['admin'] })
  // @Unprotected()
  giveEmployeeRoleAsAdmin(@Body() createMappingOptionsUserDTO: CreateMappingOptionsUserDto){
    return this.optionsService.createNewMapping(createMappingOptionsUserDTO)
  }

  /**
   * This function removes an option from an employee by deleting the mapping. (as admin)
   * @param option_id number, id of the option
   * @param employee_id number, id of the employee
   * @returns Promise
   */
  @Delete('/admin/removeOptionFromEmployee/:employee_id/:option_id')
  @Roles({ roles: ['admin'] })
  // @Unprotected()
  deleteMappingAsAdmin(@Param('option_id', new ParseIntPipe) option_id: number,@Param('employee_id', new ParseIntPipe) employee_id: number){
    return this.optionsService.deleteMapping(employee_id,option_id);
  }
  
  /**
   * This function create a new Option for an Employee by creating a mapping.
   * @param createMappingOptionsUserDTO CreateMappingOptionsUserDto
   * @returns Promise
   */
  @Post('/giveEmployeeOption')
  // @Unprotected()
  giveEmployeeRole(@Body() createMappingOptionsUserDTO: CreateMappingOptionsUserDto){
    return this.optionsService.createNewMapping(createMappingOptionsUserDTO)
  }

  /**
   * This function removes an option from an employee by deleting the mapping.
   * @param option_id number, id of the option
   * @param employee_id number, id of the employee
   * @returns Promise
   */
  @Delete('/removeOptionFromEmployee/:employee_id/:option_id')
  // @Unprotected()
  deleteMapping(@Param('option_id', new ParseIntPipe) option_id: number,@Param('employee_id', new ParseIntPipe) employee_id: number){
    return this.optionsService.deleteMapping(employee_id,option_id);
  }

}
