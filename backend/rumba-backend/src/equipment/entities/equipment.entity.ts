
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

/**
 * This entity is used to store equipment types.
 */
@Entity()
export class Equipment {
        /** The Primary Key */
        @PrimaryGeneratedColumn()
        id: number;
    
        /** name of the option */
        @Column()
        name: string;
    
        /** Date of Creation of the Employee */
        @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
        creationDate: Date;
    
        /** Date of the last Modification */
        @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
        modificationDate: Date;
    
        /** Who did the last modification */
        @Column({default:''})
        modifiedBy: string;
        
        /** Who did the last modification */
        @Column({default:''})
        modifiedByUsername: string;
}
