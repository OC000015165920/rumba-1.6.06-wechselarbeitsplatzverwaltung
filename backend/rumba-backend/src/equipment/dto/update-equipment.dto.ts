import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateEquipmentDto } from './create-equipment.dto';

/**
 * This Dto is used to store data to create a new euipment entity.
 */
export class UpdateEquipmentDto extends PartialType(CreateEquipmentDto) {
    /**name of the euipment */
    @ApiProperty()
    name: string;
}
