/**
 * This DTO is used to store the neccessary euipment data for the frontend
 */
export class ReturnEquipmentDTO {
    /**id of the euipment */
    equip_id: number;
    /**name of the equipment */
    name: string;
}