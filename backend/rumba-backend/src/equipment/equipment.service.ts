import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateMappingEquipmentWorkspaceDto } from 'src/mapping-equipment-workspace/dto/create-mapping-equipment-workspace.dto';
import { MappingEquipmentWorkspaceService } from 'src/mapping-equipment-workspace/mapping-equipment-workspace.service';
import { returnOptionDTO } from 'src/options/dto/return-option.dto';
import { Repository } from 'typeorm';
import { CreateEquipmentDto } from './dto/create-equipment.dto';
import { ReturnEquipmentDTO } from './dto/return-equipment.dto';
import { UpdateEquipmentDto } from './dto/update-equipment.dto';
import { Equipment } from './entities/equipment.entity';

/**
 * This service handles the equipment data.
 */
@Injectable()
export class EquipmentService {
  /**
   * The constructor
   * @param eqRepo the implicite repository containing the equipment
   * @param mappingEQWorkspaceService MappingEquipmentWorkspaceService
   */
  constructor(@InjectRepository(Equipment) private eqRepo: Repository<Equipment>, private mappingEQWorkspaceService: MappingEquipmentWorkspaceService){

  }
  // create(createEquipmentDto: CreateEquipmentDto) {
  //   return 'This action adds a new equipment';
  // }

  /**
   * This function returns all Equipment entities.
   * @returns ReturnEquipmentDTO[]
   */
  async findAll() {
    let allEquip = await this.eqRepo.find();
    let result: ReturnEquipmentDTO[] = []
    for (const equip of allEquip) {
      let newResult: ReturnEquipmentDTO = {
        equip_id:equip.id,
        name: equip.name
      }
      result.push(newResult)
    }

    return result;
  }

  /**
   * This function returns the equipment entity with the specified id if it exists.
   * @param id number, id of the equipment
   * @returns RetrunEquipmentDTO
   */
  findOne(id: number) {
    return this.eqRepo.findOneOrFail(id);
  }

  /**
   * This function returns an array with all Equipment of the specified workspace.
   * @param workspaceid number, id of the workspace
   * @returns ReturnEquipmentDTO
   */
  async findAllForWorkspace(workspaceid: number){
    let allMappingsForWorkspace = await this.mappingEQWorkspaceService.findAllForWorkspace(workspaceid);
    let allEquip = await this.findAll();

    let result: ReturnEquipmentDTO[] =[]
    for (const equip of allEquip) {
      for (const mapping of allMappingsForWorkspace) {
        if(equip.equip_id == mapping.equipment_id){
          let returnDTO: ReturnEquipmentDTO ={
            equip_id: equip.equip_id,
            name: equip.name
          }
          result.push(returnDTO);
        }
      }
    }

    return result;
  }

  /**
   * This function adds equipment to a workspace by creating a new mapping.
   * @param newmapping CreateMappingEquipmentWorkspaceDto
   * @returns Promise
   */
  createNewMapping(newmapping: CreateMappingEquipmentWorkspaceDto){
    return this.mappingEQWorkspaceService.createnewMapping(newmapping);
  }

  /**
   * This function removes an equipment fro ma workspace by deleting the mapping.
   * @param workspaceid number, id of the workspace
   * @param equipid number, id of the equipment
   * @returns Promise
   */
  async deleteMapping(workspaceid:number, equipid: number){
    let mappingsForWorkspace = await this.mappingEQWorkspaceService.findAllForWorkspace(workspaceid);
    for (const mapping of mappingsForWorkspace) {
      if(mapping.equipment_id == equipid){
        return this.mappingEQWorkspaceService.delete(mapping.id);
      }
    }

  }
}
