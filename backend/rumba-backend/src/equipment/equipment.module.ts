import { Module } from '@nestjs/common';
import { EquipmentService } from './equipment.service';
import { EquipmentController } from './equipment.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Equipment } from './entities/equipment.entity';
import { MappingEquipmentWorkspaceModule } from 'src/mapping-equipment-workspace/mapping-equipment-workspace.module';

@Module({
  imports:[TypeOrmModule.forFeature([Equipment]),MappingEquipmentWorkspaceModule],
  exports:[EquipmentService],
  controllers: [EquipmentController],
  providers: [EquipmentService]
})
export class EquipmentModule {}
