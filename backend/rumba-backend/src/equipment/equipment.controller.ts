import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe } from '@nestjs/common';
import { EquipmentService } from './equipment.service';
import { CreateEquipmentDto } from './dto/create-equipment.dto';
import { UpdateEquipmentDto } from './dto/update-equipment.dto';
import { Roles, Unprotected } from 'nest-keycloak-connect';
import { ApiTags } from '@nestjs/swagger';
import { CreateMappingEquipmentWorkspaceDto } from 'src/mapping-equipment-workspace/dto/create-mapping-equipment-workspace.dto';

/**
 * This controller handles the equipment.
 */
@ApiTags('Eqipment')
@Controller('equipment')
export class EquipmentController {
  /**
   * The constructor
   * @param equipmentService EquipmentService
   */
  constructor(private readonly equipmentService: EquipmentService) {}

  /**
   * This function returns all equipment entities.
   */
  @Get()
  @Roles({ roles: ['admin'] })
  findAll() {
    return this.equipmentService.findAll();
  }

  /**
   * This function tries to find a role by id and returns it if it finds it.
   * @param id number, id of the euipment 
   * @returns Equipment
   */
  @Get(':id')
  @Roles({ roles: ['admin'] })
  findOne(@Param('id') id: string) {
    return this.equipmentService.findOne(+id);
  }

  /**
   * This function returns an array with the equipment of the specified workspace.
   * @param workspaceid number, id of the workspace
   * @returns Equipment[]
   */
  @Get('forWorkspace/:workspace_id')
  @Roles({ roles: ['admin'] })
  findAllForWorkspace(@Param('workspace_id', new ParseIntPipe) workspaceid: number){
    return this.equipmentService.findAllForWorkspace(workspaceid);
  }

  /**
   * This function creates a new mapping with the data of the Dto.
   * @param createDTO CreateMappingEquipmentWorkspaceDto
   * @returns Promise
   */
  @Post('addEquipToWorkspace/')
  @Roles({ roles: ['admin'] })
  createNewMapping(@Body() createDTO: CreateMappingEquipmentWorkspaceDto){
    return this.equipmentService.createNewMapping(createDTO)
  }
  
  /**
   * This function deletes a mapping.
   * @param workspaceid number, id of the workspace
   * @param equipid number, id of the equipment
   * @returns Promise
   */
  @Delete('forworkspace/:workspace_id/:equipid')
  @Roles({ roles: ['admin'] })
  deleteMapping(@Param('workspace_id', new ParseIntPipe) workspaceid: number,@Param('equipid', new ParseIntPipe) equipid: number){
    return this.equipmentService.deleteMapping(workspaceid,equipid)
  }
}
