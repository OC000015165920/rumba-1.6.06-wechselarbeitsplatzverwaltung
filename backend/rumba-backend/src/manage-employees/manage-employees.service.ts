import { Injectable } from '@nestjs/common';
import { BookingsService } from 'src/bookings/bookings.service';
import { CreateEmployeeDto } from 'src/employees/dto/create-employee.dto';
import { UpdateEmployeeDto } from 'src/employees/dto/update-employee.dto';
import { EmployeesService } from 'src/employees/employees.service';
import { MappingOptionsUserService } from 'src/mapping-options-user/mapping-options-user.service';
import { WorkspacesService } from 'src/workspaces/workspaces.service';

/**
 * This service manages employees additional to the EmployeeService
 */
@Injectable()
export class ManageEmployeesService {
  /**
   * The constructor
   * @param bookingsService BookingsService
   * @param employeeService EmployeeService
   */
  constructor(private bookingsService: BookingsService, private employeeService: EmployeesService, private workspacesService: WorkspacesService, private mappingOptionsUserModule: MappingOptionsUserService){

  }


  /**
     * Removes the employee with the given ID from the repository and anonymizes his/her bookings
     * @param employee_id the unique ID of the employee as a number
     * @return void
     */
  async remove(employee_id: number) {
    await this.bookingsService.findAllBookingsByUser(employee_id).then(result => {
      for(let booking of result){
        this.bookingsService.anonymizeBooking(booking.booking_id);
      }
      this.workspacesService.releaseWorkspace(employee_id);
      this.mappingOptionsUserModule.findAllForEmployee(employee_id).then(result => {
        for(let option of result){
          this.mappingOptionsUserModule.delete(option.id);
        }
        return this.employeeService.remove(employee_id); 
      })
      
    }); 
    
  }

  /**
   * This Function checks if the user from the DTO is already existing or if it has to be created
   * Reason behind this fnction:
   * We imported all the User from the AD into keycloak. But we needed an additionaly table for userobjects to save individual settings
   * To make life easier and not creating each user one by one, the user will be created automaticly if it is not existing
   * @param user UserDTO to be found or created
   * @returns a user object
   */
  async findOrCreateUser(user: CreateEmployeeDto){
    console.log("user existiert: ",await this.employeeService.isUserExisting(user.username))
    if(!(await this.employeeService.isUserExisting(user.username))){
      let newUser = await this.employeeService.create(user); 
         
      return newUser; 
    }else{
      let User = await this.employeeService.findEmployeeByUsername(user.username);
      if(user.forename!=User.forename||user.lastname!=User.lastname||user.department!=User.department){
        if(user.department!=User.department){
          let bookings = await this.bookingsService.findAllBookingsByUser(User.employee_id)
          for(let booking of bookings){
            if(booking.booked_for_day>=this.getNumberFromDate(new Date(Date.now()))){
              this.bookingsService.remove(booking.booking_id);
            }
          }
        }
        let newUser: UpdateEmployeeDto = {
          forename: user.forename,
          lastname: user.lastname,
          department: User.department,
          default_mapID: User.default_mapID, 
          email_address: User.email_address,
          modificationDate: User.modificationDate,
          modifiedBy: User.modifiedBy,
          modifiedByUsername: User.modifiedByUsername,
        }
        if(user.department!=User.department){
          newUser.department = user.department;
          newUser.default_mapID = -1;
        }
        return await this.employeeService.updateEmployee(User.employee_id, newUser);
      }
      return User;
    }
  }

  /**
  * Utility to get a number from the given Date object in the format YYYYMMDD
  * @param date the Date object
  * @returns a number presentation for the given Date object
  */
  private getNumberFromDate(date: Date): number{
    let dateAsNumber =
      (date.getFullYear()) +
      (date.getMonth() > 8 ? (+date.getMonth() + 1).toString() : '0' + (+date.getMonth() + 1).toString()) +
      (date.getDate() > 9 ? date.getDate() : '0' + date.getDate());
    return Number(dateAsNumber);
  }
}
