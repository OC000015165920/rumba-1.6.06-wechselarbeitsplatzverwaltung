import { PartialType } from '@nestjs/mapped-types';
import { CreateManageEmployeeDto } from './create-manage-employee.dto';

/**
 * This Dto is not used.
 */
export class UpdateManageEmployeeDto extends PartialType(CreateManageEmployeeDto) {}
