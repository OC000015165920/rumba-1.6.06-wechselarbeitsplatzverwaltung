import { Module } from '@nestjs/common';
import { ManageEmployeesService } from './manage-employees.service';
import { ManageEmployeesController } from './manage-employees.controller';
import { EmployeesModule } from 'src/employees/employees.module';
import { BookingsModule } from 'src/bookings/bookings.module';
import { WorkspacesModule } from 'src/workspaces/workspaces.module';
import { MappingOptionsUserModule } from 'src/mapping-options-user/mapping-options-user.module';

@Module({
  imports: [EmployeesModule, BookingsModule, WorkspacesModule, MappingOptionsUserModule],
  controllers: [ManageEmployeesController],
  providers: [ManageEmployeesService]
})
export class ManageEmployeesModule {}
