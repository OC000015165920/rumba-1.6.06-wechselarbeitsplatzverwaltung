import { Test, TestingModule } from '@nestjs/testing';
import { ManageEmployeesController } from './manage-employees.controller';
import { ManageEmployeesService } from './manage-employees.service';

describe('ManageEmployeesController', () => {
  let controller: ManageEmployeesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ManageEmployeesController],
      providers: [ManageEmployeesService],
    }).compile();

    controller = module.get<ManageEmployeesController>(ManageEmployeesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
