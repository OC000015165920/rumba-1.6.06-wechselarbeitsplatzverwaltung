import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe } from '@nestjs/common';
import { ManageEmployeesService } from './manage-employees.service';
import { Roles } from 'nest-keycloak-connect';
import { User } from 'src/employees/employee.decorator';
import { EmployeesService } from 'src/employees/employees.service';
import { CreateEmployeeDto } from 'src/employees/dto/create-employee.dto';

/**
 * This controller handles employee managing requests which cannot be done by the EmployeesController
 */
@Controller('manage-employees')
export class ManageEmployeesController {
  /**
   * The constructor
   * @param manageEmployeesService ManageEmployeeService
   * @param employeesService EmployeeService
   */
  constructor(private readonly manageEmployeesService: ManageEmployeesService, private readonly employeesService: EmployeesService) {}

  /**
    * This function deletes an employee entity.
    * @param id Id of the User that should get deleted
    * @param user the actual User from Keycloak
    */
   @Delete(':id')
   @Roles({ roles: ['admin'] })
   async deleteEmployee(@Param('id', new ParseIntPipe) id: number, @User() user){
    console.log(user);
    let admin = await this.employeesService.findEmployeeByUsername(user.preferred_username)
    let userToDelete = await this.employeesService.findOne(id);

    let sameDepartment = this.employeesService.AdminAllowedForDepartment(userToDelete.department, admin.department)

    if(sameDepartment){
      this.manageEmployeesService.remove(id);
    }
   }

  /**
   * This Function does 2 things:
   * --> If the user is already existing it returns the user object or if the user is not existing it creates a new User and returns the created Object
   * @param user the user
   * @returns a user Object
   */
   @Post('/GetOrCreateUser')
   async getorcreateuser(@Body() user: CreateEmployeeDto){
 
     return await this.manageEmployeesService.findOrCreateUser(user);
   }
}
