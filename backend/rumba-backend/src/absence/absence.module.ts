import { Module } from '@nestjs/common';
import { AbsenceService } from './absence.service';
import { AbsenceController } from './absence.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Absence } from './entities/absence.view.entity';
import { MappingOptionsUserModule } from 'src/mapping-options-user/mapping-options-user.module';
import { OptionsModule } from 'src/options/options.module';

@Module({
  imports:[TypeOrmModule.forFeature([Absence]), OptionsModule],
  controllers: [AbsenceController],
  providers: [AbsenceService]
})
export class AbsenceModule {}
