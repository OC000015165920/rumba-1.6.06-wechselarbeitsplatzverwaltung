import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MappingOptionsUser } from 'src/mapping-options-user/entities/mapping-options-user.entity';
import { MappingOptionsUserService } from 'src/mapping-options-user/mapping-options-user.service';
import { OptionsService } from 'src/options/options.service';
import { Repository } from 'typeorm';
import { Absence } from './entities/absence.view.entity';

/**
 * The Absence Service manages the background functions for the controller
 */
@Injectable()
export class AbsenceService {
    /**
     * The constructor
     * @param absenceRepo the interface to connect to the database
     */
  constructor(@InjectRepository(Absence) private absenceRepo: Repository<Absence>, private optionsService: OptionsService){}

  /**
   * This function returns a absence Entry for the given booking_id
   * @param booking_id the booking_id which for which the absence Entry is requested
   * @returns a absence Entry
   */
  async findOneById(booking_id: number): Promise<Absence> {
    try {
    return await this.absenceRepo.findOneOrFail({where:{booking_id: booking_id}}); //select * from employees where employee_id = is
    } catch (err) {
    throw err;
    }
}

/**
 * This function returns all absence bookings for the specified department on the specified day.
 * @param day The requested day
 * @param department the name of the department for which the results are for
 * @returns a Dataset (Array) of Absence Entries
 */
async findByDay(day: number, department: string): Promise<Absence[]> {
    let results =  await this.absenceRepo.find({where: {
        booked_for_day: day,
        department: department
    }})

    for (const result of results) {
        let options = await this.optionsService.findAllForEmployee(result.employee_id);
        if(options.length > 0){
            for (const option of options) {
                if(option.name == "anonymous"){
                    result.anonymous = true;
                }
                else{
                    result.anonymous = false;
                }  
            }
        }
        else{
            result.anonymous = false;
        }
    }
    return results;
}

/**
 * This function returns all Absence bookings.
 * @returns Returns all absence Entries
 */
async findAll(): Promise<Absence[]> {
    return await this.absenceRepo.find();
}
}
