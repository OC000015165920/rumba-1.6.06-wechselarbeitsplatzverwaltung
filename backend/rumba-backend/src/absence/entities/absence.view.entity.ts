/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Booking } from "src/bookings/entities/booking.entity";
import { Employee } from "src/employees/entities/employee.entity";
import { ViewEntity, ViewColumn, Connection} from "typeorm";

/**
 * This View Entity describes the absence Booking of a users and contains important information for the booking and user
 */
@ViewEntity({ 
    expression: (connection: Connection) => connection.createQueryBuilder()
        .select("booking.booking_id", "booking_id")
        .addSelect("booking.employee_id", "employee_id")
        .addSelect("booking.booked_for_day", "booked_for_day")
        .addSelect("booking.morning", "morning")
        .addSelect("booking.afternoon", "afternoon")
        .addSelect("employee.forename", "forename")
        .addSelect("employee.lastname", "lastname")
        .addSelect("employee.email_address", "email")
        .addSelect("employee.department", "department")
        .addSelect("booking.comment", "comment")
        .from(Booking, "booking")
        .innerJoin(Employee, "employee", "employee.employee_id = booking.employee_id")
        .where("booking.category = 3"),
        name: "absence_view"
})
export class Absence {
    /**ID of the booking */
    @ViewColumn()
    booking_id: number;
  
    /**Date of Booking in format yyyyMMdd */
    @ViewColumn()
    booked_for_day: number;
    
    /**forename of the user who has booked */
    @ViewColumn()
    forename: string;

    /**forename of the user who has booked */
    @ViewColumn()
    lastname: string;

    /** email adress of the user who has booked */
    @ViewColumn()
    email: string;

    /** department of the user who has booked */
    @ViewColumn()
    department: string;
    
    /**the id of the employee who has booked */
    @ViewColumn()
    employee_id: number;

    /**If this is true, the workspace is booked for morning */
    @ViewColumn()
    morning: boolean;

    /**If this is true, the workspace is booked for afternoon */
    @ViewColumn()
    afternoon: boolean;

    /** The Comment of the booking, the users can type anything */
    @ViewColumn()
    comment: string;

    /** Boolean if the name is shown on the public map */
    anonymous: boolean;
    
}
