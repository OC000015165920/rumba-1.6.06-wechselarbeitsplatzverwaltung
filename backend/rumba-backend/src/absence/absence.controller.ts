import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Unprotected } from 'nest-keycloak-connect';
import { QueryDatePipe } from 'src/validation/query-date.pipe';
import { AbsenceService } from './absence.service';

/**
 * This Controller manages the endpoints for Absence Bookings
 */
@ApiTags('Absence')
@Controller('absence')
export class AbsenceController {
  constructor(private readonly absenceService: AbsenceService) {}

  /**
   * The function is not protected because it is used in the public view in the frontend
   * @param day The requested day
   * @param department the name of the department for which the results are for
   * @returns returns the requested Result of AbsenceBookings
   */
  @ApiOperation({ summary: 'Absence Buchungen pro Tag YYYYMMDD' })
  @Get(':day/:department')
  @Unprotected()
  findOccupations(@Param('day', new QueryDatePipe()) day: number, @Param('department') department: string) {
    return this.absenceService.findByDay(+day, department);
  }
}
