import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MappingsService } from './mappings.service';
import { CreateMappingDto } from './dto/create-mapping.dto';
import { UpdateMappingDto } from './dto/update-mapping.dto';

/**
 * This controller is not used.
 */
@Controller('mappings')
export class MappingsController {
  /**
   * The constructor
   * @param mappingsService MappingsService
   */
  constructor(private readonly mappingsService: MappingsService) {}

}
