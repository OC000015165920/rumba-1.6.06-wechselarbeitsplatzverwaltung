import { PartialType } from '@nestjs/swagger';
import { CreateMappingDto } from './create-mapping.dto';

/**
 * This Dto is not used.
 */
export class UpdateMappingDto extends PartialType(CreateMappingDto) {}
