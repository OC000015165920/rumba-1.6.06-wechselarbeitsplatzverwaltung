import { Injectable } from '@nestjs/common';
import { CreateMappingDto } from './dto/create-mapping.dto';
import { UpdateMappingDto } from './dto/update-mapping.dto';
import { Map } from "src/maps/entities/map.entity";
import { InjectRepository } from '@nestjs/typeorm';
import { mapping_department_orgaunit } from './entities/mapping_department_orgaunit.entity';
import { Repository } from 'typeorm';
import { mapping_department_map } from './entities/mapping_Department_Map.entity';
import { MapsService } from 'src/maps/maps.service';

/**
 * This service handles the mappings of orgaunits and departments and of maps and departments
 */
@Injectable()
export class MappingsService {

  /**
   * The constructor
   * @param orgaDep_Mapper the implicite repository containing the orga unit and department mappings
   * @param mapDep_Mapper the implicite repository containing the map and department mappings
   * @param mapsService MapsService
   */
  constructor(@InjectRepository(mapping_department_orgaunit) private orgaDep_Mapper: Repository<mapping_department_orgaunit>,
  @InjectRepository(mapping_department_map) private mapDep_Mapper: Repository<mapping_department_map>,
  private mapsService: MapsService){
    
  }

  /**
   * This function returns all maps of th especified department
   * @param department Name of the Department
   * @returns All Maps from the mapping map <-> department with the name of the department
   */
     async getMapsForDepartment(department: string){
      let foundMappings = await this.mapDep_Mapper.find({where:{department: department}})
  
      let maps: Map[] =[]
      for (const mapping of foundMappings) {
        maps.push(await this.mapsService.findOne(mapping.mapID))
      }
  
      return maps;
    }
  
    /**
     * This function returns the department of the specified map.
     * @param mapID ID of the Map
     * @returns All Departments, which belongs to the mapID
     */
    async getDepartmentsForMap(mapID: number){
      let foundMappings = await this.mapDep_Mapper.find({where:{mapID: mapID}});
  
      let results: string[]
      for (const result of foundMappings) {
        results.push(result.department);
      }
      return results;
    }

    /**
     * This function returns all mappings.
     * @returns All Mappings
     */
    async FindAllOrgaDepartmentMappings(){
      return this.orgaDep_Mapper.find();
    }
}
