import { Module } from '@nestjs/common';
import { MappingsService } from './mappings.service';
import { MappingsController } from './mappings.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { mapping_department_orgaunit } from './entities/mapping_department_orgaunit.entity';
import { mapping_department_map } from './entities/mapping_Department_Map.entity';
import { MapsModule } from 'src/maps/maps.module';

@Module({
  imports:[TypeOrmModule.forFeature([mapping_department_orgaunit, mapping_department_map]), MapsModule],
  controllers: [MappingsController],
  providers: [MappingsService],
  exports:[MappingsService]
})
export class MappingsModule {}
