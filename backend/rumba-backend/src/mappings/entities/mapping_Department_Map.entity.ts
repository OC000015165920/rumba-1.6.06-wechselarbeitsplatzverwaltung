import { Department } from "src/departments/entities/department.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

/**
 * Describes the mapping which map be
 */
@Entity()
export class mapping_department_map{
    /** Primary Key with auto increment */
    @PrimaryGeneratedColumn()
    id: number;

    /** Name of the Department */
    @Column()
    department: string;

    /** ID of the Map*/
    @Column()
    mapID: number;

}