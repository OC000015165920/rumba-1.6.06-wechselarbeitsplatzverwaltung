import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

/**
 * Describes the Mapping from orgaunit (import from AD) with the department from RUMBA
 */
@Entity()
export class mapping_department_orgaunit{
    /** Primary Key with auto increment */
    @PrimaryGeneratedColumn()
    id: number;

   /** Name of the Department */
    @Column()
    department: string;

    /** 
     * The name of the orgaunit which should get mapped with the right department
     * e.g. in the Active Directory the group of the user is named '011002' but we want to map it to
     * department 10, so we can add both here and RUMBA will map the orgaunit to the department
     */
    @Column()
    orgaunit: string;

    
}