import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMapDto } from './dto/create-map.dto';
import { UpdateMapDto } from './dto/update-map.dto';
import { Map } from './entities/map.entity';

/**
 * This service handles the map entities.
 */
@Injectable()
export class MapsService {

  /**
   * The constructor
   * @param mapRepo the implicite repository containing the maps
   */
  constructor(@InjectRepository(Map) private mapRepo: Repository<Map>){

  }

  /**
   * This function is not used
   * @returns 'This action adds a new map'
   */
  create(createMapDto: CreateMapDto) {
    return 'This action adds a new map';
  }

  /**
   * This function is not used
   * @returns `This action returns all maps`
   */
  findAll() {
    return `This action returns all maps`;
  }

  /**
   * This function returns the map entity with the specified id if it exists.
   * @returns Map
   */
  findOne(id: number) {
    return this.mapRepo.findOneOrFail(id);
  }

  /**
   * This function is not used.
   * @param id number, id of the map to be updated
   * @param updateMapDto UpdateMapDto
   * @returns `This action updates a #${id} map`
   */
  update(id: number, updateMapDto: UpdateMapDto) {
    return `This action updates a #${id} map`;
  }

  /**
   * This function is not used.
   * @param id number, id of the map to be deleted
   * @returns `This action removes a #${id} map`
   */
  remove(id: number) {
    return `This action removes a #${id} map`;
  }
}
