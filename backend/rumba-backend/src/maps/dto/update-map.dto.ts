import { PartialType } from '@nestjs/swagger';
import { CreateMapDto } from './create-map.dto';

/**
 * This Dto is not used.
 */
export class UpdateMapDto extends PartialType(CreateMapDto) {}
