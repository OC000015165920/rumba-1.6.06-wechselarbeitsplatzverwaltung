import { Department } from "src/departments/entities/department.entity";
import { Column, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

/**
 * This entity is used to store map data
 */
@Entity()
export class Map {
    /** id of the map */
    @PrimaryGeneratedColumn()
    id: number;

    /** name of the map */
    @Column()
    name: string;

    /**the filename of the map image */
    @Column()
    imageName: string;

    /**the date if the creation of the entity */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    creationDate: Date;

}
