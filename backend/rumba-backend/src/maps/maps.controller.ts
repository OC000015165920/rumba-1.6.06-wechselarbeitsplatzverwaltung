import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MapsService } from './maps.service';
import { CreateMapDto } from './dto/create-map.dto';
import { UpdateMapDto } from './dto/update-map.dto';

/**
 * This controller is not used
 */
@Controller('maps')
export class MapsController {
  /**
   * The constructor
   * @param mapsService MapService
   */
  constructor(private readonly mapsService: MapsService) {}

}
