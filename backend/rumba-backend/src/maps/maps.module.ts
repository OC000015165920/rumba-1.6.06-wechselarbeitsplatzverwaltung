import { Module } from '@nestjs/common';
import { MapsService } from './maps.service';
import { MapsController } from './maps.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Map } from './entities/map.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Map])],
  controllers: [MapsController],
  providers: [MapsService, Map],
  exports:[MapsService]
})
export class MapsModule {}
