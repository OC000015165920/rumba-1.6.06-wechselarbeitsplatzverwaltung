/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OptionsModule } from 'src/options/options.module';
import { OccupiedWorkspace } from './entities/OccupiedWorkspace.entity';
import { OccupationController } from './occupation.controller';
import { OccupationService } from './occupation.service';

@Module({
  imports:[TypeOrmModule.forFeature([OccupiedWorkspace]), OptionsModule],
  exports:[TypeOrmModule, OccupationService],
  controllers: [OccupationController],
  providers: [OccupationService]
})
export class OccupationModule {}
