/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Controller, Get, Param,ParseIntPipe} from '@nestjs/common';
import { OccupationService } from './occupation.service';
import { ApiTags, ApiOperation} from '@nestjs/swagger';
import { Unprotected } from 'nest-keycloak-connect';
import { QueryDatePipe } from 'src/validation/query-date.pipe';
import { MappingOptionsUserService } from 'src/mapping-options-user/mapping-options-user.service';
import { OptionsService } from 'src/options/options.service';

/**
 * This controller handles occupations
 */
@ApiTags('Occupation')
@Controller('occupation')
export class OccupationController {
  /**
   * The constructor
   * @param occupationService OccupationService
   * @param optionsService OptionsService
   */
  constructor(private readonly occupationService: OccupationService, private readonly optionsService: OptionsService) {}

  /**
   * This function returns all occupations for the specified workspace.
   * @param workspaceid number, id of the workspace
   * @returns OccupiedWorkspace[]
   */
  @Get('forWorkspace/:workspaceid')
  findOccupationsForWorkspace(@Param('workspaceid', ParseIntPipe) workspaceid: number){
    return this.occupationService.findOccupationsForWorkspace(workspaceid);
  }

  /**
   * This function returns all occupations for the specified day and department
   * @param day number, date as a number
   * @param department string, name of the department
   * @returns OccupiedWorkspace[]
   */
  @ApiOperation({ summary: 'Arbeitsplatzbuchungen pro Tag YYYYMMDD' })
  @Get(':day/:department')
  @Unprotected()
  async findOccupations(@Param('day', new QueryDatePipe()) day: number, @Param('department') department: string) {
    let occupations = await this.occupationService.findByDay(day, department);

    for(let o of occupations){
      let options = await this.optionsService.findAllForEmployee(o.employee_id);
      o.first_aid=false;
      o.employee_anonymous=false;
      for(let op of options){
        if(op.name=="Ersthelfer"){
          o.first_aid=true;
        }else if(op.name=="anonymous"){
          o.employee_anonymous=true;
        }
      }
    }
    return occupations;
  }

  /**
   * This function returns all occupations for the specified day, department and map
   * @param day number, date as a number
   * @param department string, name of the department
   * @param mapID number, id of the map
   * @returns OccupiedWorkspace[]
   */
  @ApiOperation({ summary: 'Arbeitsplatzbuchungen pro Tag YYYYMMDD' })
  @Get('formap/:day/:department/:mapID')
  @Unprotected()
  async findOccupationsForMapForDay(@Param('day', new QueryDatePipe()) day: number, @Param('department') department: string, @Param('mapID', new ParseIntPipe()) mapID: number) {
    let occupations = await this.occupationService.findByDayAndForMap(day, mapID);

    for(let o of occupations){
      let options = await this.optionsService.findAllForEmployee(o.employee_id);
      o.first_aid=false;
      o.employee_anonymous=false;
      for(let op of options){
        if(op.name=="Ersthelfer"){
          o.first_aid=true;
        }else if(op.name=="anonymous"){
          o.employee_anonymous=true;
        }
      }
    }
    return occupations;
  }
}
