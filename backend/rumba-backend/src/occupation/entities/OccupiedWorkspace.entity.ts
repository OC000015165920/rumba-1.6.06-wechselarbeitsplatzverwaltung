/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Booking } from "src/bookings/entities/booking.entity";
import { Employee } from "src/employees/entities/employee.entity";
import { Workspace } from "src/workspaces/entities/workspace.entity";
import { ViewEntity, ViewColumn, Connection} from "typeorm";

/**
 * This entity is used to store data of occupied workspaces.
 */
@ViewEntity({ 
    expression: (connection: Connection) => connection.createQueryBuilder()
        .select("booking.booking_id", "booking_id")
        .addSelect("booking.employee_id", "employee_id")
        .addSelect("booking.booked_for_day", "booked_for_day")
        .addSelect("booking.workspace_id", "workspace_id")
        .addSelect("booking.comment", "comment")
        .addSelect("booking.morning", "morning")
        .addSelect("booking.afternoon", "afternoon")
        .addSelect("workspace.workspace_name", "workspace_name")
        .addSelect("workspace.mapID", "workspace_mapID")
        .addSelect("workspace.x", "workspace_x")
        .addSelect("workspace.y", "workspace_y")
        .addSelect("workspace.rotation", "workspace_rotation")
        .addSelect("workspace.scale", "workspace_scale")
        .addSelect("workspace.bookableWhenHO", "workspace_bookableWhenHO")
        .addSelect("employee.forename", "forename")
        .addSelect("employee.lastname", "lastname")
        .addSelect("employee.email_address", "email")
        .addSelect("employee.department", "department")
        .from(Booking, "booking")
        .innerJoin(Workspace, "workspace", "workspace.workspace_id = booking.workspace_id")
        .innerJoin(Employee, "employee", "employee.employee_id = booking.employee_id")
        .where("booking.category = 1")
})
export class OccupiedWorkspace {
    /**id of the booking  of the occupation */
    @ViewColumn()
    booking_id: number;
  
    /**for which day the occupations is */
    @ViewColumn()
    booked_for_day: number;

    /**the id of the employee for whom th eoccupation is. */
    @ViewColumn()
    employee_id: number;
  
    /**the id of the workspace for which the occupation is */
    @ViewColumn()
    workspace_id: number;

    /**the id if the map on which the workspace */
    @ViewColumn()
    workspace_mapID: number;

    /**the name of the workspace */
    @ViewColumn()
    workspace_name: string;
    
    /**the forename of the employee */
    @ViewColumn()
    forename: string;

    /**the lastname of the employee */
    @ViewColumn()
    lastname: string;
  
    /**the name of the department in which the workspace and employee are */
    @ViewColumn()
    department: string;

    /**email adress of the employee */
    @ViewColumn()
    email: string;

    /**comment of the booking */
    @ViewColumn()
    comment:string;

    /**if the occupation is for the morning */
    @ViewColumn()
    morning:boolean;
    
    /**if th eoccupation is for th eafternoon */
    @ViewColumn()
    afternoon:boolean;

    /**if the employee is a designated first aid helper */
    first_aid: boolean = false;
 
    /**if the employee wants to be anonymous on the public page */
    employee_anonymous: boolean = false;

    /**the x ccordinate of the workspace on the map */
    @ViewColumn()
    workspace_x: number;

    /**the y coordinate of the workspace on the map */
    @ViewColumn()
    workspace_y: number;

    /**if the workspace should be bookable when the employee which the workspace is reserved for (not neccessaryly) is in homeoffice or absence */
    @ViewColumn()
    workspace_bookableWhenHO:boolean;

    /**the rotation of the workspace on the map */
    @ViewColumn()
    workspace_rotation: number;

    /**the scale of the workspace on the map */
    @ViewColumn()
    workspace_scale: number;
}
