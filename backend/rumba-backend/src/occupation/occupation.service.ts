/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@nestjs/common';
import {OccupiedWorkspace} from './entities/OccupiedWorkspace.entity'
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Not, Repository } from 'typeorm';

/**
 * This service handles occupations.
 */
@Injectable()
export class OccupationService {
    /**
     * The constructor
     * @param occupiedRepo the implicite repository containing the occupations
     */
    constructor(@InjectRepository(OccupiedWorkspace) private  occupiedRepo: Repository<OccupiedWorkspace>){

    }
    
    /**
     * This function returns all occupied workspaces for a day and department
     * @param day number, day for which the occupations are
     * @param department string, name of the department
     * @returns OccupiedWorkspace[]
     */
    async findByDay(day: number, department: string): Promise<OccupiedWorkspace[]> {
        try {
        //    const occupied = await this.occupiedRepo.query(`select * from occupied_workspace WHERE booked_for_day = ${day} AND department = ${department}`);
           return this.occupiedRepo.find({where:{booked_for_day: day, department: department, employee_id: Not(1)}});
        } catch (err) {
        throw err;
        }
    }

    /**
     * This function returns all occupied workspaces for a day and department on the specified map.
     * @param day number, day for which the occupations are
     * @param mapID number, id of the map
     * @returns OccupiedWorkspace[]
     */
    async findByDayAndForMap(day: number, mapID: number): Promise<OccupiedWorkspace[]> {

        try {
           return this.occupiedRepo.find({where:{booked_for_day: day, workspace_mapID: mapID}});
        } catch (err) {
        throw err;
        }
    }

    
    /**
     * This function tries to find an occuppied workspace by day and workspace_id and returns it or throws an error if it cannot find it.
     * @param day 
     * @param daysNumber 
     * @param workspace_id 
     * @returns 
     */
    async findByWorkspaceAndDay(day: number, daysNumber: number, workspace_id: number){
        try {
            const occupied = await this.occupiedRepo.query(`select * from occupied_workspace WHERE booked_for_day = ${day} AND workspace_id = ${workspace_id} AND employee_id IS NOT NULL` );
            return occupied;
        }
        catch(err){
            throw err;
        }
    }

    /**
     * This function returns the Occupations for the workspace in a range of dates
     * @param day start day
     * @param daysCount days to count 
     * @param workspace_id workspace to check occupation
     * @returns a list of occupations for this workspace
     */
    async findByWorkspaceAndTimespan(day: number, daysCount: number, workspace_id: number){
        let occupations: OccupiedWorkspace[] =[]

        const allOccupation = await this.occupiedRepo.find({where:{workspace_id:workspace_id, employee_id: Not(1)}});
        const DATELIST = this.getDayList(day,daysCount);
        for (const date of DATELIST) { 
            occupations.push(allOccupation.find(x => x.booked_for_day === date))
        }
        //This function filters the null elements which we recieved if the workspaces is not occupied
        return occupations.filter((x): x is OccupiedWorkspace => x !== undefined)
    }

    /**
     * This function returns all occupations for the specified workspace.
     * @param workspace_id number, id of the workspace
     * @returns Occupations[]
     */
    async findOccupationsForWorkspace(workspace_id: number){
        let occupations: OccupiedWorkspace[] =[]
    
        const allOccupationForWorkspace = await this.occupiedRepo.find({where:{workspace_id:workspace_id, employee_id: Not(1)}});
        console.log(allOccupationForWorkspace)
        let today = new Date()
        const DATELIST = this.getDayList(this.getNumberFromDate(today),28);
        
        for (const date of DATELIST) {
            occupations.push(allOccupationForWorkspace.find(x => x.booked_for_day === date))
        }
        return occupations.filter((x): x is OccupiedWorkspace => x !== undefined)

    }

    /**
     * This function returns an array with days days as a number from the startday.
     * @param startday number, date as a number
     * @param days number, number of days
     * @returns number[], the dates as nubers
     */
    private getDayList(startday: number, days: number):number[]{
        let datesAsNumber: number[] =[]
        let date = this.getDateFromNumber(startday)  

        for (let i = 0; i < days; i++) {
            let newDate = this.addDaysOnDate(date,i)
            datesAsNumber.push(this.getNumberFromDate(newDate))
        }

        return datesAsNumber;
    }

    /**
     * This function converts a number to a date.
     * @param dateAsNumber number, the date as a number
     * @returns Date, the date
     */
    private getDateFromNumber(dateAsNumber: number): Date{
        var year = Number(dateAsNumber.toString().substring(0,4));
        var month = Number(dateAsNumber.toString().substring(4,6))-1;
        var day = Number(dateAsNumber.toString().substring(6,8));
        return new Date(year,month,day,0,0,0,0)
    }

    /**
     * This function adds days on a date and returns the new date.
     * @param date Date, the date on which to add days
     * @param daysToAdd number, number of days to add
     * @returns Date, the new date
     */
    private addDaysOnDate(date: Date, daysToAdd: number){
        let newDate = new Date(date);
        return new Date(newDate.setDate(date.getDate()+daysToAdd));
    }

    /**
     * This function converts a date to a number.
     * @param dateAsNumber Date, the date as a Date
     * @returns number, the date
     */
    private getNumberFromDate(date: Date): number{
        let dateAsNumber =
          (date.getFullYear()) +
          (date.getMonth() > 8 ? (+date.getMonth() + 1).toString() : '0' + (+date.getMonth() + 1).toString()) +
          (date.getDate() > 9 ? date.getDate() : '0' + date.getDate());
        return Number(dateAsNumber);
    }
}
