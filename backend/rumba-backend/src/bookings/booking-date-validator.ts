/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';

/**
 * This calls contains the validation method for the booking_data property
 *
 */
@ValidatorConstraint({ name: 'allowedBookingDate', async: true})
export class AllowedBookingDate implements ValidatorConstraintInterface {

  /**
   * This function checks if booking on the booking date is valid.
   * @param bookingDateNumber the value of booking date of type number, expected syntax is YYYYMMDD
   * @param args the validation arguments given by the frame work
   * @returns true, if the value is valid, not in the past and not more than 28 days in the future, false else
   */
  async validate(bookingDateNumber: number, args: ValidationArguments) {
    
    //take the first four numbers and store them to year 
    const year  = Math.floor(bookingDateNumber/10000)

    //take the fifth and sixth number and store it to month
    const month  = Math.floor(bookingDateNumber/100)-year*100

    //take the last two numbers and store it to day
    const day = bookingDateNumber-year*10000-month*100
 
    //create a Date object from the calculated values
    const bookingDate = new Date(month+'-'+day+'-'+year)
    
    //for comparison with the current day, create a Date object with 0:00 AM
    const nowDate = new Date().setHours(0,0,0,0)

    //calculate the difference between booking date and now (in milliseconds)
    const diff = bookingDate.valueOf() - nowDate.valueOf();

    //calculate amount of days from the difference
    const diffDays = Math.ceil(diff/(1000*3600*24))

    //check whether the day is in the future, but not more than 28 days
    return await (diffDays >= 0 && diffDays <= 28); // for async validations you must return a Promise<boolean> here
  }

  /**
   * This function returns a defaultmessage. 
   * @returns an error message saying the booking_day is not valid
   */
  defaultMessage() {
    return 'Buchungen für die Vergangenheit und länger als 28 Tage in die Zukunft sind nicht erlaubt.';
  }
}