import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

/**
 * Describes the table 
 */
@Entity()
export class bookingCategory {
    /**the id of the bookingcategory */
    @PrimaryGeneratedColumn()
    id: number;

    /**the name of the category */
    @Column()
    category: string;
}