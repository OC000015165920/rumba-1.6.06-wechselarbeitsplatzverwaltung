/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { createConnection, getConnection, getRepository } from "typeorm";
import { Booking } from './entities/booking.entity'

/**
 * implements a small test bed to access a test data base
 */
beforeEach(() => {
    return createConnection({
        type: "mysql",
        host: "mariadb",
        port: 3306,
        username: "root",
        password: "example",
        database: "bookingtest",
        entities: [Booking],
        synchronize: true,
        logging: true, 
    });
});

/**
 * implements the actions after the test is  passed
 */
afterEach(() => {
    let conn = getConnection();
    return conn.close();
});

/**
 * run the test
 */
test("store booking 1 and fetch it", async () => {
    const my_booking_date = 20211007
    const my_employee_id = 5
    const my_workspace_id = 1

    await getRepository(Booking).insert({
        workspace_id: my_workspace_id,
        booked_for_day: my_booking_date, 
        booked_at: new Date(),
        employee_id: my_employee_id,
        active: true
    });
    let joe = await getRepository(Booking).findOne({
        where: {
            id: my_workspace_id
        }
    });
    expect(joe.booked_for_day).toBe(my_booking_date);
    expect(joe.employee_id).toBe(my_employee_id);

    //TODO: add additional tests...
});


