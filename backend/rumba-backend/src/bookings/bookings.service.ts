/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, MoreThan, Not, Repository } from 'typeorm';
import { CreateBookingDto } from 'src/bookings/dto/create-booking.dto';
import { Booking } from 'src/bookings/entities/booking.entity';

/**
 * The class which implements the central service for the Booking module
 *
 */
@Injectable()
export class BookingsService {
  /**
   * Constructs the Service (called from nestJS framework)
   * @param bookingsRepo the implicite repository containing the bookings
   */
  constructor(@InjectRepository(Booking) private bookingsRepo: Repository<Booking>){
    
  }

  /**
   * Get all active bookings made by the given user and return it
   * @param employee_id the employee_id specifying the user for which the bookings should be returned
   * @returns a list of bookings as an array of Booking entities, including workspace attributes
   */
  async findBookingsByUser(employee_id:number): Promise<Booking[]> {
    const bookings = await this.bookingsRepo.find({relations: ['workspace'],where: {employee_id: employee_id, booked_for_day: MoreThan(this.calcDateDaysAgo(14))} });
    return bookings;
  }

  /**
   * Get all bookings made by the given user and return it
   * @param employee_id the employee_id specifying the user for which the bookings should be returned
   * @returns a list of bookings as an array of Booking entities, including workspace attributes
   */
   async findAllBookingsByUser(employee_id:number): Promise<Booking[]> {
    const bookings = await this.bookingsRepo.find({relations: ['workspace'],where: {employee_id: employee_id} });
    return bookings;
  }

  /**
   * Get all bookings made by the given workspace without anonymous bookings and return it
   * @param workspace_id the employee_id specifying the workspace for which the bookings should be returned
   * @returns a list of bookings as an array of Booking entities, including workspace attributes
   */
   async findBookingsByWorkspace(workspace_id:number): Promise<Booking[]> {
    const bookings = await this.bookingsRepo.find({relations: ['workspace'],where: {workspace_id: workspace_id, employee_id: Not(1)} });
    return bookings;
  }

  /**
   * Get all bookings made by the given workspace and return it
   * @param workspace_id the workspace_id specifying the workspace for which the bookings should be returned
   * @returns a list of bookings as an array of Booking entities, including workspace attributes
   */
   async findBookingsByWorkspaceWithAnonymousEmployee(workspace_id:number): Promise<Booking[]> {
    const bookings = await this.bookingsRepo.find({relations: ['workspace'],where: {workspace_id: workspace_id} });
    return bookings;
  }

  /**
   * Creates a new booking object in the repository
   * @param createBookingDto describes the data for the new booking object
   * @throws an exception, if something goes wrong
   */
  async create(createBookingDto: CreateBookingDto) {
    try {
      this.bookingsRepo.save(createBookingDto);
    } catch (err) {
      throw err;
    }
  }

  /**
   * Retrieves all bookings from the repo where booked_for_day not longer than 14 days ago
   * @returns all bookings as a promis of an array of booking entities 
   */
  async findAll(department: string): Promise<Booking[]> {
    return await this.bookingsRepo.find({where: {department: department,booked_for_day: MoreThan(this.calcDateDaysAgo(14)), employee_id: Not(1)} });
  }

  /**
   * Retrieves the booking with the given ID if booking booked_for_day not longer than 14 days ago
   * @param id the unique ID of the booking as a number
   * @returns the related booking object as a Promise
   */
  async findOne(id: number): Promise<Booking> {
    return await this.bookingsRepo.findOneOrFail({booking_id: id, booked_for_day: MoreThan(this.calcDateDaysAgo(14)), employee_id: Not(1)});
  }

  /**
   * Removes the booking with the given ID from the repository
   * @param id the unique ID of the booking as a number
   */
  async remove(id: number) {
    this.bookingsRepo.delete(id);
  }

  /**
   * Updates the related booking with the given object (currently not used)
   * @param id the unique ID of the booking as a number
   * @param updateBookingDto the booking dto with the changed values
   */
  // async update(id: number, updateBookingDto: UpdateBookingDto) {
  //   this.bookingsRepo.createQueryBuilder().update(Booking).set({'booked_for_day':updateBookingDto.booked_for_day, 'employee_id': updateBookingDto.employee_id, 'booked_at': updateBookingDto.booked_at, 'workspace_id':updateBookingDto.workspace_id}).where('id = :id', {id:id}).execute();
  // }

  /**
   * Retrieves the bookings for the given date YYYYMMDD if date is not longer than 14 days ago
   * @param dateAsNumber 
   * @returns an array of Bookings as a Promise
   */
  async findBookingsForDate(dateAsNumber: number): Promise<Booking[]>{
    if (dateAsNumber > this.calcDateDaysAgo(14)) {
      return await this.bookingsRepo.find({where: {booked_for_day:dateAsNumber, employee_id: 1}});
    }
      return undefined;
  }
  

    /**
   * Retrieves the bookings for the given date YYYYMMDD if date is not longer than 14 days ago
   * @param dateAsNumber 
   * @returns an array of Bookings as a Promise
   */
     async findBookingsForDateAndDepartment(dateAsNumber: number, department: string): Promise<Booking[]>{
      if (dateAsNumber > this.calcDateDaysAgo(14)) {
        return await this.bookingsRepo.find({where: {booked_for_day:dateAsNumber, department: department, employee_id: Not(1)}});
      }
        return undefined;
    }
  /**
   * Subtracts a given number of days from today
   * @param numberOfDays 
   * @returns integer date  YYYYMMDD
   */
  private calcDateDaysAgo(numberOfDays: number) {
    const today = new Date().setHours(0,0,0,0);
    const numDays = numberOfDays*1000*3600*24
    const dateAgo = new Date(today-numDays)
    const month = (dateAgo.getMonth()+1)*100 //bug in ts date function
    return dateAgo.getFullYear()*10000+month+dateAgo.getDate();
  }

  /**
   * This function checks if a booking for the day (morning and/or afternoon) is allowed.
   * @param employee_id number, id of the employee
   * @param booked_for_day number, day for which the booking is
   * @param morning boolean, if the booking is for the morning
   * @param afternoon boolean, if the booking is for the afternoon
   * @returns boolean, if the booking is allowed
   */
  async isAllowedToCreateBooking(employee_id: number, booked_for_day: number, morning:boolean, afternoon:boolean){
    let bookings = await this.findBookingsByUser(employee_id);
    let result = true;
    //check each booking
    for (const booking of bookings) { 
      //check if there is a booking for same day and if morning or afternoon is false, so the user is able too create a booking
      if(booking.booked_for_day == booked_for_day && (booking.morning == false || booking.afternoon == false)){
        // User wants to create booking for morning, but morning is already booked
        if(booking.morning == true && morning == true){
          result = false;
        }
        // User wants to create booking for afternoon, but afternoon is already booked
        if(booking.afternoon == true && afternoon == true){
          result = false;
        }
      }
    }
    return result;
  }

  /**
   * This function anonymizes a specified booking.
   * @param booking_id number, id of the booking to anonymize
   */
  //pk, 2023/01/13: because bookings.Repo.update works asynchronously, the method itself is asynchronous!
  async anonymizeBooking(booking_id: number){
    //pk, 2023/01/13: return the promise to allow caller to wait for the result
    return this.bookingsRepo.update(booking_id, {employee_id: 1});
  }

  /**
   * This function anonymizes a specified bookings workspace.
   * @param booking_id number, id of the booking to anonymize
   */
  //pk, 2023/01/13: because bookings.Repo.update works asynchronously, the method itself is asynchronous!
  async anonymizeBookingWorkspace(booking_id: number){
    //pk, 2023/01/13: return the promise to allow caller to wait for the result
    return this.bookingsRepo.update(booking_id, {workspace_id: null});
  }
}
