/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import { Controller, Get, Post, Body, Param, Delete, ParseIntPipe, UseGuards, UseInterceptors, ClassSerializerInterceptor, HttpException, HttpStatus } from '@nestjs/common';
import { BookingsService } from 'src/bookings/bookings.service';
import { CreateBookingDto } from 'src/bookings/dto/create-booking.dto';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { Roles} from 'nest-keycloak-connect';
import { Booking } from './entities/booking.entity';
import { QueryDatePipe } from 'src/validation/query-date.pipe';
import { EmployeesService } from 'src/employees/employees.service';
import { User } from 'src/employees/employee.decorator';
import { MongoRepository } from 'typeorm';
import { WorkspacesService } from 'src/workspaces/workspaces.service';

/**
 * This class implements the controller for the bookings module
 */
@ApiTags('Booking')
@Controller('bookings')
export class BookingsController {
  /**
   * The constructor
   * @param bookingsService BookingsService
   * @param employeeService EmployeesService
   * @param workspaceService WorkspacesService
   */
  constructor(private readonly bookingsService: BookingsService, private readonly employeeService: EmployeesService, private readonly workspaceService: WorkspacesService) {}

    /**
   * GET function to find all bookings by department (only for role "admin")
   * 
   * @returns all booking objects as an array of Booking entities
   */
     @ApiOperation({ summary: 'Aufruf liefert alle Arbeitsplatzbuchungen als unverschachtelte JSON mit workspace_id '+
     'und employee_id, aber ohne Detailinformationen zu Arbeitsplatzen und Räumen.' })
      @Get('admin/:department')
      @Roles({ roles: ['admin'] })
      async findAll(@Param('department') department: string, @User() adminuser): Promise<Booking[]> {
        
        // when this is true, the admin is allowed to get All Bookings
        let result = await this.employeeService.findOneByDepartmentAndKeycloakId(department, adminuser.sub); 

        if (result) {          
          return this.bookingsService.findAll(department);
        }
      }


  /**
   * GET function to find all bookings for a given employee
   * 
   * @param employee_id the employee_id for which the bookings should be returned
   * @returns all bookings for the given employee_id as an array of Booking entities
   * @todo if role = user, check if user.id from token = employee_id parameter, because users are allowed to retrieve their bookings, only
   */
  @ApiOperation({ summary: 'Liefert alle Buchungen eines Employee mit Arbeitsplatzinformationen.' })
  @Get('foruser/admin/:department/:employee_id')
  @Roles({ roles: ['admin'] })
  async findBookingsByUserAdmin(@Param('employee_id', ParseIntPipe) employee_id: number, @Param('department') department:string, @User() adminuser): Promise<Booking[]>  {
    

    let user = await this.employeeService.findOne(employee_id);
    let admin = await this.employeeService.findEmployeeByUsername(adminuser.preferred_username)
    //Compare admin-department with department from the userobject with the given emplyoee_id
    if(user && admin){
      if(admin.department == user.department){
        console.log("department gleich")
        let result = await this.employeeService.findOneByDepartmentAndKeycloakId(department, adminuser.sub);
        if (result) {
          return this.bookingsService.findBookingsByUser(+employee_id);
        }
      }
    }


  }

    /**
   * GET function to find all bookings for a given employee
   * 
   * @param employee_id the employee_id for which the bookings should be returned
   * @returns all bookings for the given employee_id as an array of Booking entities
   * @todo if role = user, check if user.id from token = employee_id parameter, because users are allowed to retrieve their bookings, only
   */
     @ApiOperation({ summary: 'Liefert alle Buchungen eines Employee mit Arbeitsplatzinformationen.' })
     @Get('/user/:employee_id') 
     findBookingsByUser(@Param('employee_id', ParseIntPipe) employee_id: number, @User() user): Promise<Booking[]>  {
      const keycloak_id = user.sub
      const result = this.employeeService.findEmployeeByEmployeeIdAndKeycloakId(employee_id, keycloak_id)
      if (result) {
        return this.bookingsService.findBookingsByUser(+employee_id);
      }
     }

     
  
   /**
   * GET function to find all bookings by department for a given day (admin only)
   * @param day the day for which the bookings should be returned (as a number of format 'YYYYMMDD')
   * @returns all bookings for the department for the given day as an array of Booking entities
   */
    @ApiOperation({ summary: 'Liefert alle Buchungen für den angegeben Tag' })
    @Get('/forday/admin/:department/:day')
    @Roles({ roles: ['admin'] })
    async findBookingsForDayByDepartment(@Param('day', new QueryDatePipe()) day: number, @Param('department') department: string, @User() adminuser): Promise<Booking[]>{
      
      let result = await this.employeeService.findOneByDepartmentAndKeycloakId(department, adminuser.sub);
      // pk, 20240528, #20240116-0046: return bookings for common offices even if the user is not admin user for "Allgemein"
      if (result || department == 'Allgemein') {
        return this.bookingsService.findBookingsForDateAndDepartment(day, department);
      }
    }

  /**
   * CREATE function to create a booking for a given day/user/workplace specified by CreateBookingDto
   * @param createBookingDto the DTO to describe all needed values of a booking
   * @returns void
   * @todo Currently, there is no guard on this 
   */
  @ApiOperation({ summary: 'Bucht einen Arbeitsplatz für einen bestimmten Tag, dessen Datum in der Form YYYYMMDD '+
                          'angegeben sein muss. Eine gültige Buchung hat die Eigenschaft booked=true.' })
  @Post('/admin')
  @Roles({ roles: ['admin'] })
  async createAdmin(@Body() createBookingDto: CreateBookingDto, @User() adminuser) {
    const dep = createBookingDto.department;
    const employeeBooking = createBookingDto.employee_id;
    //check whether keycloak_id of admin matches department of admin
    const adminDepartment = await this.employeeService.findOneByDepartmentAndKeycloakId(dep, adminuser.sub);
    //check whether employee_id of booking matches department
    const employeeDepartment = await this.employeeService.findEmployeeByEmployeeIdAndDepartment(employeeBooking, dep);
    if (adminDepartment && employeeDepartment) {
      return await this.bookingsService.create(createBookingDto);
    }
  }

   /**
   * CREATE function to create a booking for a given day/user/workplace specified by CreateBookingDto
   * 
   * @param createBookingDto the DTO to describe all needed values of a booking
   * @returns void
   * @todo Currently, there is no guard on this 
   */
    @ApiOperation({ summary: 'Bucht einen Arbeitsplatz für einen bestimmten Tag, dessen Datum in der Form YYYYMMDD '+
    'angegeben sein muss. Eine gültige Buchung hat die Eigenschaft booked=true.' })
    @Post('/user')
    async createUser(@Body() createBookingDto: CreateBookingDto, @User() user) {
      const employee_id = createBookingDto.employee_id;
      let workspace;
      if(createBookingDto.workspace_id){
        workspace = await this.workspaceService.findOne(createBookingDto.workspace_id);
      }else{
        workspace = undefined;
      }
      const result = await this.employeeService.findEmployeeByEmployeeIdAndKeycloakId(employee_id, user.sub)
      const employee = await this.employeeService.findEmployeeByUsername(user.preferred_username)
      const userAllowedToBook = await this.bookingsService.isAllowedToCreateBooking(employee_id,createBookingDto.booked_for_day, createBookingDto.morning, createBookingDto.afternoon)
      if (result && userAllowedToBook && employee.department == createBookingDto.department ) {
        if(workspace){
          if(workspace.department == createBookingDto.department){
            return await this.bookingsService.create(createBookingDto);
          }else{
            throw  new HttpException('Unauthorized Booking', HttpStatus.BAD_REQUEST); 
          }
        }
        return await this.bookingsService.create(createBookingDto);
      }
      else{
        throw  new HttpException('Already Booked', HttpStatus.BAD_REQUEST); 
      }
      
    }

   /**
   * CREATE function to create a  booking for a public workspace for a given day/user specified by CreateBookingDto
   * 
   * @param createBookingDto the DTO to describe all needed values of a booking
   * @returns void
   * @todo Currently, there is no guard on this 
   */
    @ApiOperation({ summary: 'Bucht einen abteilungsunabhängigen Arbeitsplatz für einen bestimmten Tag, dessen Datum in der Form YYYYMMDD '+
    'angegeben sein muss. Eine gültige Buchung hat die Eigenschaft booked=true.' })
    @Post('/foralluser')
    async createForAllUser(@Body() createBookingDto: CreateBookingDto, @User() user) {
      const employee_id = createBookingDto.employee_id;
      let workspace;
      if(createBookingDto.workspace_id){
        workspace = await this.workspaceService.findOne(createBookingDto.workspace_id);
      }else{
        workspace = undefined;
      }
      const result = await this.employeeService.findEmployeeByEmployeeIdAndKeycloakId(employee_id, user.sub)
      const userAllowedToBook = await this.bookingsService.isAllowedToCreateBooking(employee_id,createBookingDto.booked_for_day, createBookingDto.morning, createBookingDto.afternoon)
      if (result && userAllowedToBook && createBookingDto.department == "Allgemein") {
        if(workspace){
          if(workspace.department == createBookingDto.department){
            return await this.bookingsService.create(createBookingDto);
          }else{
            throw  new HttpException('Unauthorized Booking', HttpStatus.BAD_REQUEST); 
          }
        }
        return await this.bookingsService.create(createBookingDto);
      }
      else{
        throw  new HttpException('Already Booked', HttpStatus.BAD_REQUEST); 
      }
      
    }

  /**
   * DELETE function to delete the booking with the given ID
   * !!!! NICHT MANDANTENFÄHIG !!!!
   * @param id id the booking ID to be deleted
   * @returns void
   * @todo Currently, there is no guard on this
   */
  @ApiOperation({ summary: 'Löscht eine einzelne der booking_id entsprechende Buchung.' })
  @Delete('/admin/:id')
  @Roles({roles: ['admin']})
  async removeAdmin(@Param('id', ParseIntPipe) id: string, @User() adminuser ) {
    // get department of booking specified by id
    const dep = (await this.bookingsService.findOne(+id)).department;
    const booking = await this.bookingsService.findOne(+id)


    if (dep) { 
      //check whether admin for that department exists in database
      let result = await this.employeeService.findOneByDepartmentAndKeycloakId(dep, adminuser.sub);
      if (result) {//delete booking
        //Need to check if the adminuser has the same department as the bookings department
        let admin = await this.employeeService.findEmployeeByUsername(adminuser.preferred_username)
        if(admin){
          if(admin.department == booking.department){
            return await this.bookingsService.remove(+id);
          }
        }

      }else if(dep=="Allgemein"){ //for bookings in the public department
        //Need to check if the adminuser has the same department as the bookings employees department
        let admin = await this.employeeService.findEmployeeByUsername(adminuser.preferred_username)
        if(admin){
          if(await this.employeeService.findEmployeeByEmployeeIdAndDepartment(booking.employee_id, admin.department)){
            return await this.bookingsService.remove(+id);
          }
        }
      }
    }
  }

    /**
   * DELETE function to delete the booking with the given ID
   * !!!! NICHT MANDANTENFÄHIG !!!!
   * @param id id the booking ID to be deleted
   * @returns void
   * @todo Currently, there is no guard on this
   */
     @ApiOperation({ summary: 'Löscht eine einzelne der booking_id entsprechende Buchung.' })
     @Delete('/user/:id')
     async removeUser(@Param('id', ParseIntPipe) id: string,@User() user) {
       //check if booking.employee is current user
       let userEmployee = await this.employeeService.findEmployeeByUsername(user.preferred_username);
       let booking = await this.bookingsService.findOne(+id);
       if(userEmployee.employee_id == booking.employee_id){
        return await this.bookingsService.remove(+id);
       }
       throw  new HttpException('Forbidden', HttpStatus.FORBIDDEN); 

     }
}
