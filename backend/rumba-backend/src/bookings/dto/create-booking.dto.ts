/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { ApiProperty, ApiResponseProperty } from "@nestjs/swagger";
import {isBoolean, IsBoolean, isBooleanString, IsDateString, IsNumber, isString, Validate} from 'class-validator';
import { AllowedBookingDate } from "../booking-date-validator";

/**
 * This class describes the DTO for creation of a workplace booking
 */
export class CreateBookingDto {

    /**
     * reference to the workplace which is booked
     * (has to be a number)
     */
    @ApiProperty()
    // @IsNumber()
    workspace_id?: number;
 
    /**
     * Contains the day the new booking is made for
     * (Format: YYYYMMDD, checked by AllowedBookingDate()) 
     */
    @ApiProperty()
    @IsNumber()
    @Validate(AllowedBookingDate)
    booked_for_day: number;

    /**
     * contains the timestamp the booking was made
     */
    @ApiProperty()
    @IsDateString()
    booked_at: Date;
    
    /** The department, where the booking belongs to */
    @ApiProperty()
    department: string;

    /**indicates if the workspace is booked for morning */
    @ApiProperty({default: 1})
    morning: boolean;

    /**indicates if the workspace is booked for afternoon */
    @ApiProperty({default: 1})
    afternoon: boolean;

    /** the comment for the booking - the user can type anything */
    @ApiProperty({default: ""})
    comment: string;
    
    /**
     * Reference to the employee the booking was made for
     */
    @ApiProperty() 
    @IsNumber() 
    employee_id: number;

    /**
     * Indicates, whether the booking is "active"
     * @deprecated currently not used (bookings are deleted completely)
     */
    @ApiProperty({default:true})
    @IsBoolean()
    active: boolean;

    /**the booking category comes from the table 'booking_category' and indicates if the user has booked for homeoffice, absence or for a workspace */
    @ApiProperty()
    category: number;

    /**show who did the booking - user, admin or system or whatever */
    @ApiProperty()
    booked_by: string;
}
