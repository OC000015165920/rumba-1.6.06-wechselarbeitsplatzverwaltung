import { PartialType } from '@nestjs/mapped-types';
import { CreateManageWorkspaceDto } from './create-manage-workspace.dto';

/**
 * This dto is not used.
 */

export class UpdateManageWorkspaceDto extends PartialType(CreateManageWorkspaceDto) {}
