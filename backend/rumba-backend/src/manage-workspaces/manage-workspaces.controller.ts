import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe } from '@nestjs/common';
import { ManageWorkspacesService } from './manage-workspaces.service';
import { Roles } from 'nest-keycloak-connect';
import { User } from 'src/employees/employee.decorator';
import { EmployeesService } from 'src/employees/employees.service';
import { WorkspacesService } from 'src/workspaces/workspaces.service';

/**
 * This controller handles workspaces when other Services are needed
 */
@Controller('manage-workspaces')
export class ManageWorkspacesController {

  /**
   * The constructor
   * @param manageWorkspacesService ManageWorkspacesService
   */
  constructor(private readonly manageWorkspacesService: ManageWorkspacesService, private readonly employeesService: EmployeesService, private readonly workspacesService: WorkspacesService) {}

  /**
   * This function deletes a workspace entity.
   * @param id number, id of the Workspace to be deleted
   */
  @Delete(':id')
  @Roles({ roles: ['admin']})
  async deleteWorkspace(@Param('id', new ParseIntPipe) id: number, @User() user) {
    let admin = await this.employeesService.findEmployeeByUsername(user.preferred_username)
    let workspaceToDelete = await this.workspacesService.findOne(id);

    let sameDepartment = this.employeesService.AdminAllowedForDepartment(workspaceToDelete.department, admin.department)
 
    if(sameDepartment){
      //pk, 2023/01/17: bug fix for delete workspace - wait for end of remove function before returning "OK"
      //                otherwise, the next GET request would return the workspace which should be deleted...
      await this.manageWorkspacesService.remove(id);
      //console.log("sameDepartment: true")
    }
  }
}
