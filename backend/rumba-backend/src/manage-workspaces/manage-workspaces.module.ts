import { Module } from '@nestjs/common';
import { ManageWorkspacesService } from './manage-workspaces.service';
import { ManageWorkspacesController } from './manage-workspaces.controller';
import { WorkspacesModule } from 'src/workspaces/workspaces.module';
import { BookingsModule } from 'src/bookings/bookings.module';
import { EmployeesModule } from 'src/employees/employees.module';

@Module({
  imports: [WorkspacesModule, BookingsModule, EmployeesModule],
  controllers: [ManageWorkspacesController],
  providers: [ManageWorkspacesService]
})
export class ManageWorkspacesModule {}
