import { Test, TestingModule } from '@nestjs/testing';
import { ManageWorkspacesService } from './manage-workspaces.service';

describe('ManageWorkspacesService', () => {
  let service: ManageWorkspacesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ManageWorkspacesService],
    }).compile();

    service = module.get<ManageWorkspacesService>(ManageWorkspacesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
