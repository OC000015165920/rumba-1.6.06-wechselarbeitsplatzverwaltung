import { Injectable } from '@nestjs/common';
import { BookingsService } from 'src/bookings/bookings.service';
import { WorkspacesService } from 'src/workspaces/workspaces.service';
import { CreateManageWorkspaceDto } from './dto/create-manage-workspace.dto';
import { UpdateManageWorkspaceDto } from './dto/update-manage-workspace.dto';

/**
 * This service handles the workspaces when other services are needed in the controller.
 */
@Injectable()
export class ManageWorkspacesService {
  /**
   * The constructor
   * @param bookingsService BookingsService
   * @param workspacesService WorkspacesService
   */
  constructor(private bookingsService: BookingsService, private workspacesService: WorkspacesService){

  }

  /**
   * This function removes the specified workspace and removes itself as the foreign key from all bookings
   * @param workspace_id number, id of the workspace to be removed
   */
  async remove(workspace_id: number) {
    //pk, 2023/01/17: bug fix for delete workspace - wait for end of the whole procedure, otherwise the return is too early
    let result = await this.bookingsService.findBookingsByWorkspaceWithAnonymousEmployee(workspace_id);

    if (result) {
      //pk, 2023/01/13, reduce console output: console.log(result);
      let temp = null;
      for(let booking of result){
        //pk, 2023/01/13: wait for each replacement before calling the remove function,
        //                otherwise, the remove could case a REFERENCE error and the workspace wouldn't be deleted
        temp = await this.bookingsService.anonymizeBookingWorkspace(booking.booking_id);
        //jh, 2023/01/16: bug fix: anonymize bookings to allow the user to book again
        temp = await this.bookingsService.anonymizeBooking(booking.booking_id);
      }
      //pk, 2023/01/13, log result of remove function if it fails
      try {
        temp = await this.workspacesService.remove(workspace_id);
        //console.log("ManageWorkspaceService.remove(",workspace_id,") returns ",temp);
      }
      catch (e) {
        console.error("Caught exception: ", e);
      }
      //console.log("remove(",workspace_id,") done");
      return temp;
    };
    
  }
}
