import { Test, TestingModule } from '@nestjs/testing';
import { ManageWorkspacesController } from './manage-workspaces.controller';
import { ManageWorkspacesService } from './manage-workspaces.service';

describe('ManageWorkspacesController', () => {
  let controller: ManageWorkspacesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ManageWorkspacesController],
      providers: [ManageWorkspacesService],
    }).compile();

    controller = module.get<ManageWorkspacesController>(ManageWorkspacesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
