/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { ReturnEquipmentDTO } from "src/equipment/dto/return-equipment.dto";
import { Equipment } from "src/equipment/entities/equipment.entity";
import { Workspacecolor } from "src/workspacecolors/entities/workspacecolor.entity";
import { Column, Entity, PrimaryGeneratedColumn, OneToMany, ManyToOne, JoinColumn} from "typeorm";
import {Booking}  from  '../../bookings/entities/booking.entity';
import { Room } from "../../rooms/entities/room.entity";

/**
 * This entity is used to store information of workspaces
 */
@Entity()
export class Workspace {
    /**id of the workspace */
    @PrimaryGeneratedColumn()
    workspace_id: number;

    /**name of the workspace */
    @Column()
    workspace_name: string;

    /**department name of the workspace */
    @Column()
    department: string;

    /**id of the map on which the workspace is */
    @Column()
    mapID: number;

    /**id of the room of the workspace */
    @Column()
    room_id: number; //foreign key

    /**x coordinate of the workspace on the map */
    @Column({default:0})
    x: number; //rechtswert*0.8

    /**y coordinate of the workspace on the map */
    @Column({default:0})
    y: number; //hochwert*0.8

    /**rotation of the workspace on the map in degrees */
    @Column({default:0})
    rotation: number;

    /**the scale of the workspace on the map */
    @Column({default:1.0})
    scale: number;

    /**id of the employee for which the workspace is reserved */
    @Column({nullable: true})
    employee_id: number; 

    /**if the workspace should be bookable when the employee is in homeoffice or absence */
    @Column({default: true})
    bookableWhenHO: boolean;

    /**id of the workspacecolor */
    @Column()
    color_id: number;

    /**the equipment of the workspace */
    equipment: ReturnEquipmentDTO[];
    
    /**the workspacecolor object of the workspace */
    color: Workspacecolor;

    /**all bookings for the workspace */
    @OneToMany(() => Booking, (booking: Booking) => booking.workspace)
    public bookings: Booking[];

    /** the room object of the workspace */
    @ManyToOne(() => Room, (room: Room) => room.workspaces)
    @JoinColumn({name: 'room_id'})
    room: Room;


    /**the date of the creation of the workspace object */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    creationDate: Date;

    /** Date of the last Modification */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    modificationDate: Date;

    /** Who did the last modification */
    @Column({default:''})
    modifiedBy: string;    

    /** State of the Workspace, it can be disabled, so no one can book this workspace */
    @Column({default: true})
    active: boolean;
}

