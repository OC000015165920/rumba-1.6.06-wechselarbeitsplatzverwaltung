/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { PartialType } from '@nestjs/mapped-types';
import { CreateWorkspaceDto } from './create-workspace.dto';
import { ApiProperty } from "@nestjs/swagger";
import {IsNotEmpty, IsNumberString, Length} from 'class-validator';

/**
 * This Dto is used to store data to update Workspaces
 */
export class UpdateWorkspaceDto extends PartialType(CreateWorkspaceDto) {
    /**name of the workspace */
    @ApiProperty()
    @IsNotEmpty()
    @Length(5)
    workspace_name: string;

    /**x coordinate of the workspace on the map */
    @ApiProperty()
    @IsNumberString()
    x: number;

    /**y coordinate of the workspace on the map */
    @ApiProperty()
    @IsNumberString()
    y: number;

    /**scale of th eworkspace on the map */
    @ApiProperty({default:1})
    scale: number;

    /**rotation of the workspace on the map */
    @ApiProperty({default: 0})
    rotation: number;

    /**id of the map of the workspace */
    @ApiProperty()
    mapID: number;

    /**if the workspace is active (bookable at all right now) */
    @ApiProperty()
    active:boolean;

    /**id of the employee for which the workspace is reserved (optional) */
    @ApiProperty({nullable:true})
    employee_id: number;

    /**if the workspace should be bookable when the employee which it is reserved for is in homeoffice or absence */
    @ApiProperty({default:true})
    bookableWhenHO: boolean;

    /**id of the workspacecolor */
    @ApiProperty({nullable:true})
    color_id: number;

    /**date of the last modification */
    modificationDate: Date;
    /**who modified it last */
    modifiedBy: string;

}

/**
 * This dto is used block or unblock workspaces
 */
export class blockWorkspaceDto extends PartialType(CreateWorkspaceDto) {
    /**if the workspace is active (bookable at all right now) */
    @ApiProperty()
    active:boolean;
}
