/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post } from '@nestjs/common';
import { WorkspacesService } from './workspaces.service';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { Roles, Unprotected } from 'nest-keycloak-connect';
import { CreateWorkspaceDto } from './dto/create-workspace.dto';
import { blockWorkspaceDto, UpdateWorkspaceDto } from './dto/update-workspace.dto';
import { EmployeesService } from 'src/employees/employees.service';
import { User } from 'src/employees/employee.decorator';

/**
 * This controller is used to handle workspaces
 */
@ApiTags('Workspace')
@Controller('workspaces')
export class WorkspacesController {
  /**
   * The constructor
   * @param workspacesService WorkspaceService
   * @param employeeService EmployeeService
   */
  constructor(private readonly workspacesService: WorkspacesService, private employeeService: EmployeesService) {}

  /**
   * This function returns all workspaces for the specified department.
   * @param department string, name of the department
   * @returns Workspace[]
   */
  @ApiOperation({ summary: 'Gibt eine Liste aller Workspaces mit allen Attributen aus.' })
  @Get(':department')
  @Unprotected()
  findAll(@Param('department') department: string) {
    return this.workspacesService.findAll(department);
  }

  /**
   * This function activates or deactivates the specified workspace depending on the information in active.
   * @param active blockWorkspaceDto, information to update 
   * @param id number, id of the workspace to be updated
   * @param adminuser @User(), the actual User from Keycloak
   * @returns Promise<UpdateResult>
   */
  @Patch('clear/:id')
  @Roles({ roles: ['admin']})
  async blockWorkspace(@Body() active: blockWorkspaceDto, @Param('id', ParseIntPipe) id: number, @User() adminuser){
    console.log("WorkspaceController.blockWorkspace(",active, ", ",id)
    let workspace = await this.workspacesService.findOne(id);
    let user = await this.employeeService.findEmployeeByUsername(adminuser.preferred_username);
    let userFound = this.employeeService.findEmployeeByEmployeeIdAndKeycloakId(user.employee_id, adminuser.sub)
    
    if(userFound){
      let sameDepartment = this.employeeService.AdminAllowedForDepartment(workspace.department, user.department)
      if(sameDepartment){
        return this.workspacesService.block(id,active);
      }
    }

  }


  /**
   * This function returns all workspaces for the specified department on the specified map.
   * @param department string, name of the department
   * @param mapID number, id of the map
   * @returns Workspace[]
   */
  @ApiOperation({ summary: 'Gibt eine Liste aller Workspaces mit allen Attributen aus.' })
  @Get('formap/:department/:mapID')
  @Unprotected()
  findAllForMap(@Param('department') department: string,@Param('mapID') mapID: number) {
    return this.workspacesService.findAllForMapAndDepartment(department, mapID);
  }

  /**
   * This function returns all unoccupied workspaces for the specified department on the specified day.
   * @param day number, date as a number
   * @param department 
   * @returns 
   */
  @ApiOperation({ summary: 'UNSICHERE OPERATION, mindestens anfällig für SQL Injection. Liefert eine Liste mit freien Workspaces inklusive Attributen für einen bestimmten Tag, wobei der Tag im Format YYYYMMDD angegeben werden muss. Keine Informationen zu BOOKING oder EMPLOYEE.' })
  @Get('free/:day/:department')
  @Roles({ roles: ['admin'] })
  findUnoccupiedWorkspaces(@Param('day', ParseIntPipe) day: number,@Param('department') department: string){
    return this.workspacesService.findUnoccupiedWorkspaces(+day, department);
  }

  /**
   * This function creates a new workspace with the data from the Dto.
   * @param createDTO The new Workspace to be created
   * @param adminuser the actual User from Keycloak
   * @returns The created Workspace
   */
  @Post()
  @Roles({ roles: ['admin'] })
  async createNewWorkspace(@Body() createDTO: CreateWorkspaceDto, @User() adminuser){
    let user = await this.employeeService.findEmployeeByUsername(adminuser.preferred_username);
    let userFound = this.employeeService.findEmployeeByEmployeeIdAndKeycloakId(user.employee_id, adminuser.sub)
    if(userFound){
      let sameDepartment = this.employeeService.AdminAllowedForDepartment(createDTO.department, user.department)
      if(sameDepartment){
        return this.workspacesService.create(createDTO);
      }
    }

  }

  /**
   * Thsi function updates the specified workspace with the data from the Dto.
   * @param updateDTO updated Workspace Object
   * @param id ID of the Workspace to get updated
   * @param adminuser the actual User from Keycloak
   * @returns the updated Workspace Object
   */
  @Patch(':id')
  @Roles({ roles: ['admin'] })
  async updateWorkspace(@Body() updateDTO: UpdateWorkspaceDto,@Param('id', ParseIntPipe) id: number, @User() adminuser){
    let workspace = await this.workspacesService.findOne(id);
    let user = await this.employeeService.findEmployeeByUsername(adminuser.preferred_username);
    let userFound = this.employeeService.findEmployeeByEmployeeIdAndKeycloakId(user.employee_id, adminuser.sub)
    if(userFound){
      let sameDepartment = this.employeeService.AdminAllowedForDepartment(workspace.department, user.department)
      if(sameDepartment){
        return this.workspacesService.update(id,updateDTO);
      }
    }

  }

}
