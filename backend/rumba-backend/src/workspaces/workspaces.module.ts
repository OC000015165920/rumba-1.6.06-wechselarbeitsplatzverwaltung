/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Module } from '@nestjs/common';
import { WorkspacesService } from './workspaces.service';
import { WorkspacesController } from './workspaces.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Workspace } from './entities/workspace.entity';
import { Booking } from 'src/bookings/entities/booking.entity';
import { OccupationModule } from 'src/occupation/occupation.module';
import { OccupationService } from 'src/occupation/occupation.service';
import { MapsModule } from 'src/maps/maps.module';
import { DepartmentsModule } from 'src/departments/departments.module';
import { EmployeesModule } from 'src/employees/employees.module';
import { Workspacecolor } from 'src/workspacecolors/entities/workspacecolor.entity';
import { WorkspacecolorsService } from 'src/workspacecolors/workspacecolors.service';
import { WorkspacecolorsModule } from 'src/workspacecolors/workspacecolors.module';
import { EquipmentModule } from 'src/equipment/equipment.module';

@Module({
  imports:[TypeOrmModule.forFeature([Workspace, Booking]),OccupationModule, MapsModule, DepartmentsModule, EmployeesModule, WorkspacecolorsModule, EquipmentModule],
  exports: [WorkspacesService],
  controllers: [WorkspacesController],
  providers: [WorkspacesService, OccupationModule],
})
export class WorkspacesModule {}
