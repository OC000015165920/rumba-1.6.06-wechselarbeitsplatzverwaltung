/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Injectable} from '@nestjs/common';
import { InjectRepository} from '@nestjs/typeorm';
import { DepartmentsService } from 'src/departments/departments.service';
import { EquipmentService } from 'src/equipment/equipment.service';
import { MapsService } from 'src/maps/maps.service';
import { OccupationService } from 'src/occupation/occupation.service';
import { WorkspacecolorsService } from 'src/workspacecolors/workspacecolors.service';
import { Repository } from 'typeorm';
import { CreateWorkspaceDto } from './dto/create-workspace.dto';
import { blockWorkspaceDto, UpdateWorkspaceDto } from './dto/update-workspace.dto';
import { Workspace } from './entities/workspace.entity';

/**
 * This service handles Workspaces.
 */
@Injectable()
export class WorkspacesService {
  /**
   * The constructor
   * @param workspaceRepo the implicite repository containing the workspaces
   * @param occupationService OccupationService
   * @param mapService MapService
   * @param departmentService DepartmentService
   * @param workspacecolorService WorkspacecolorService
   * @param equipmentService EquipmentService
   */
    constructor(@InjectRepository(Workspace) private workspaceRepo: Repository<Workspace>, private occupationService: OccupationService, private mapService: MapsService,
    private departmentService:DepartmentsService, private readonly workspacecolorService: WorkspacecolorsService, private readonly equipmentService: EquipmentService){
  }

  /**
   * This function creates a new workspace with data of the Dto.
   * @param createWorkspaceDto the Workspace that should be created
   * @returns the created Workspace
   */
  create(createWorkspaceDto: CreateWorkspaceDto) {
    return this.workspaceRepo.save(createWorkspaceDto);
  }

  /**
   * This function returs the workspace with the specified name if it exists.
   * @param workspacename string, name of the workspace
   * @returns Workspace
   */
  findWorkspaceByName(workspacename:string){
    return this.workspaceRepo.findOneOrFail({
      where:{
        workspace_name: workspacename
      }
    })
  }

  /**
   * This function return all Workspaces for the specified department and map.
   * @param department string, name of the department
   * @param mapID number, id of the map
   * @returns Workspace[]
   */
  async findAllForMapAndDepartment(department: string,mapID: number){
    let temp = await this.workspaceRepo.find({where:{department: department, mapID: mapID}});
    for(let w of temp){
      w.color = (await this.workspacecolorService.findOne(w.color_id));
      w.equipment = await this.equipmentService.findAllForWorkspace(w.workspace_id);
    }
    return temp;
  }

  /**
   * This function returns all unoccupied workspaces of the specified department on the specified day.
   * @param day number, date as a number
   * @param department string, name of the department
   * @returns Workspace[]
   */
  async findUnoccupiedWorkspaces(day:number, department: string) {  
    const allOccupations = await this.occupationService.findByDay(day, department) //Get all the occupations for tday
    const allWorkspaces = await this.findAll(department); // get all workspaces

    //Compare all workspaces with all occupations and find (with some) the workspaces which are not in the result of allOccupations
    const unoccupied = allWorkspaces.filter(x => !allOccupations.some(occu => occu.workspace_id == x.workspace_id && occu.morning && occu.afternoon))

    // const unoccupied = this.workspaceRepo.query(`select * from workspace As a WHERE a.workspace_id NOT IN (Select b.workspace_id From booking As b WHERE b.booked_for_day = '${day}') AND a.department = '${department}' `);
    return unoccupied;
  } 

  /**
   * This function returns all workspaces of the specified department.
   * @param department string, name of the department
   * @returns Workspace[]
   */
  async findAll(department: string) {
    let temp = await this.workspaceRepo.find({where:{department: department}});
    for(let w of temp){
      w.color = (await this.workspacecolorService.findOne(w.color_id));
      w.equipment = await this.equipmentService.findAllForWorkspace(w.workspace_id);
    }
    return temp;
  }

  /**
   * This function returns the workspace with the specified id if it exists.
   * @param id number, id of the Workspace
   * @returns Workspace
   */
  async findOne(id: number) {
    let temp = await this.workspaceRepo.findOneOrFail({workspace_id: id});
    temp.color = (await this.workspacecolorService.findOne(temp.color_id));
    temp.equipment = await this.equipmentService.findAllForWorkspace(temp.workspace_id);
    return temp;
  }

  /**
   * This function updates the specified workspace with the data from the Dto.
   * @param workspace_id the Workspace ID that should get updated
   * @param updateWorkspaceDto the updatedWorkspace Object
   * @returns the update result as a promise
   */
  async update(workspace_id: number, updateWorkspaceDto: UpdateWorkspaceDto) {
    let returnValue = null;
    let temp = await this.workspaceRepo.find({workspace_name: updateWorkspaceDto.workspace_name, mapID: updateWorkspaceDto.mapID})
    // pk, 2023/01/13: we have to support two cases:
    //                  1) if the name of the workspace has been changed, the length of the returned array is 0
    //                  2) if other values are changed, the length is 1 and the workspace names have to match
    if (temp.length == 0 || (temp.length == 1 && updateWorkspaceDto.workspace_name == temp[0].workspace_name)) {
      if (temp.length == 1) {
        //as expected, only 1 entry found in repository
        console.log("WorkspaceService.update(",workspace_id,"): found workspace ",temp[0]);
      }
      updateWorkspaceDto.modificationDate = new Date();
      updateWorkspaceDto.modifiedBy = "admin"
      if(!updateWorkspaceDto.employee_id){
        this.workspaceRepo.update(workspace_id, {employee_id: null});
      }
      
      if(String(updateWorkspaceDto.employee_id)=="null"){
        updateWorkspaceDto.employee_id==null;
      }
      
      returnValue = this.workspaceRepo.update(workspace_id, updateWorkspaceDto);
    }
    else {
      console.log("WorkspaceService.update(",workspace_id,"): unexpected amount of entries found ",temp.length)
    }

    return returnValue;
  }

  /**
   * This function deletes the workspace with the specified id.
   * @param workspace_id ID of the worksapce that should get deleted
   * @returns the delete Results
   */
  async remove(workspace_id: number) {
    return this.workspaceRepo.delete(workspace_id);
  }

  /**
   * This function cancels the reservations of all workspaces for the specified employee
   * @param employee_id number, id of the employee 
   */
  async releaseWorkspace(employee_id){
    let temp = await this.workspaceRepo.find({where: {employee_id: employee_id}});
    for(let workspace of temp){
      this.workspaceRepo.update(workspace.workspace_id, {employee_id: null});
    }
  }

  /**
   * This function activates or deactivates the specified workspace depending on the information in the Dto.
   * @param workspace_id number, id of the workspace
   * @param active blockWorkspaceDto, information to update
   * @returns Promise<UpdateResult>
   */
  async block(workspace_id: number, active: blockWorkspaceDto){
    return this.workspaceRepo.update(workspace_id, active);
  }
}
