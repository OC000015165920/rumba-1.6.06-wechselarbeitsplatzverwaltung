import { Module } from '@nestjs/common';
import { MappingOptionsUserService } from './mapping-options-user.service';
import { MappingOptionsUserController } from './mapping-options-user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MappingOptionsUser } from './entities/mapping-options-user.entity';

@Module({
  imports:[TypeOrmModule.forFeature([MappingOptionsUser])],
  controllers: [MappingOptionsUserController],
  providers: [MappingOptionsUserService],
  exports:[MappingOptionsUserService],
})
export class MappingOptionsUserModule {}
