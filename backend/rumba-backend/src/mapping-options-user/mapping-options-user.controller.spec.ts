import { Test, TestingModule } from '@nestjs/testing';
import { MappingOptionsUserController } from './mapping-options-user.controller';
import { MappingOptionsUserService } from './mapping-options-user.service';

describe('MappingOptionsUserController', () => {
  let controller: MappingOptionsUserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MappingOptionsUserController],
      providers: [MappingOptionsUserService],
    }).compile();

    controller = module.get<MappingOptionsUserController>(MappingOptionsUserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
