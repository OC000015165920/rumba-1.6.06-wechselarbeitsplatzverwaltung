import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe, UnprocessableEntityException } from '@nestjs/common';
import { MappingOptionsUserService } from './mapping-options-user.service';
import { CreateMappingOptionsUserDto } from './dto/create-mapping-options-user.dto';
import { UpdateMappingOptionsUserDto } from './dto/update-mapping-options-user.dto';
import { Unprotected } from 'nest-keycloak-connect';


/**
 * This controller is not used.
 */
@Controller('mapping-options-user')
export class MappingOptionsUserController {
  /**
   * The constructor
   * @param mappingOptionsUserService MappingOptionsUserService
   */
  constructor(private readonly mappingOptionsUserService: MappingOptionsUserService) {}

  // @Get('employee_id')
  // @Unprotected()
  // findAllForEmployee(@Param('employee_id', new ParseIntPipe) employee_id: number){
  //   return this.mappingOptionsUserService.findAllForEmployee(employee_id)
  // }
}
