import { PartialType } from '@nestjs/swagger';
import { CreateMappingOptionsUserDto } from './create-mapping-options-user.dto';

/**
 * This Dto is not used.
 */
export class UpdateMappingOptionsUserDto extends PartialType(CreateMappingOptionsUserDto) {}
