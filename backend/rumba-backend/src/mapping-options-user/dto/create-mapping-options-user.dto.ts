import { ApiProperty } from "@nestjs/swagger";

/**
 * This Dto is used to store data for the creation of mappings between Options and Employees
 */
export class CreateMappingOptionsUserDto {
    /**id of the option */
    @ApiProperty()
    options_id: number;
    /**id of the employee */
    @ApiProperty()
    employee_id: number;
}
