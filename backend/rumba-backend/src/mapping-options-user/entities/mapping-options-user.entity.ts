import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

/**
 * This entity is used for mappings between optionsand employees.
 */
@Entity()
export class MappingOptionsUser {
    /**the id of the mapping */
    @PrimaryGeneratedColumn()
    id: number;

    /**id of the option */
    @Column()
    options_id: number;

    /**id of the employee */
    @Column()
    employee_id: number;

    /** Date of Creation of the Employee */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    creationDate: Date;
}
