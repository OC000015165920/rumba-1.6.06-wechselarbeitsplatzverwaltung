import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMappingOptionsUserDto } from './dto/create-mapping-options-user.dto';
import { UpdateMappingOptionsUserDto } from './dto/update-mapping-options-user.dto';
import { MappingOptionsUser } from './entities/mapping-options-user.entity';

/**
 * Thi service handles the mappings for options and employees
 */
@Injectable()
export class MappingOptionsUserService {
  /**
   * The constructor
   * @param mappingOptionsUserRepo the implicite repository containing the mappings of options and employees
   */
  constructor(@InjectRepository(MappingOptionsUser) private mappingOptionsUserRepo: Repository<MappingOptionsUser>){

  }

  /**
   * This function returns all mappings with the specified employee
   * @param id number, id of the employee
   * @returns MappingOptionsUser[]
   */
  findAllForEmployee(id: number){
    return this.mappingOptionsUserRepo.find({
      where:{
        employee_id: id
      }
    })
  }

  /**
   * This function creates a new mapping with the data of the Dto
   * @param createmapping CreateMappingOptionsUserDto
   * @returns Promise
   */
  createnewMapping(createmapping: CreateMappingOptionsUserDto){
    return this.mappingOptionsUserRepo.save(createmapping);
  }

  /**
   * THis function deletes the specified mapping
   * @param id number, id of the mapping
   * @returns Promise
   */
  delete(id:number){
    return this.mappingOptionsUserRepo.delete(id);
  }
}
