import { Test, TestingModule } from '@nestjs/testing';
import { MappingOptionsUserService } from './mapping-options-user.service';

describe('MappingOptionsUserService', () => {
  let service: MappingOptionsUserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MappingOptionsUserService],
    }).compile();

    service = module.get<MappingOptionsUserService>(MappingOptionsUserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
