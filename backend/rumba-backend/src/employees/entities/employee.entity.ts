/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Column, Entity, PrimaryGeneratedColumn, OneToMany} from "typeorm";
import {Booking}  from  '../../bookings/entities/booking.entity';

/**
 * This Class describes the table of the employees
 */
@Entity()
export class Employee {
    /** ID of the Employee as a primary Key with auto increment */
    @PrimaryGeneratedColumn()
    employee_id: number;

    /** ID from the Keycloak User */
    @Column()
    keycloak_id: string;

    /** The Username of the employee */
    @Column()
    username: string;

    /** The Forename of the employee */
    @Column()
    forename: string;

    /** The lastname of the employee */
    @Column()
    lastname: string;

    /** The department of the employee */
    @Column()
    department: string;

    /** email of the employee */
    @Column()
    email_address: string

    /** ID of the default Map. 
     * The default map will be on the first index at the maps Array, so it will show up first 
    */
    @Column({default: -1})
    default_mapID: number;

    /** Date of Creation of the Employee */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    creationDate: Date;

    /** Date of the last Modification */
    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
    modificationDate: Date;

    /** Who did the last modification */
    @Column({default:''})
    modifiedBy: string;
    
    /** Who did the last modification */
    @Column({default:''})
    modifiedByUsername: string;
    
    /**
     * Is no Column in the Table but is needed to create a relation which user belongs to which booking
     */
    @OneToMany(() => Booking, (booking: Booking) => booking.employee)
    public bookings: Booking[];
}
