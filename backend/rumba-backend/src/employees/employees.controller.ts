/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import { Controller, Get, Post, Body,  Param, Patch, ParseIntPipe, Delete} from '@nestjs/common';
import { EmployeesService } from 'src/employees/employees.service';
import { CreateEmployeeDto } from 'src/employees//dto/create-employee.dto';
import { Employee } from 'src/employees/entities/employee.entity';
import { ApiTags, ApiOperation} from '@nestjs/swagger';
import { Roles, Unprotected } from 'nest-keycloak-connect';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { User } from './employee.decorator';
import { DepartmentsService } from 'src/departments/departments.service';


/**
 * This class implements the controller for the bookings module
 */
@ApiTags('Employee')
@Controller('employees')
export class EmployeesController {

  /**
   * The constructor
   * @param employeesService EmployeesService
   */
  constructor(private readonly employeesService: EmployeesService) {}
  
  /**
   * GET function for all employees (only for role "admin")
   * 
   * @returns an array of Employee entities as a promise
   */
  @ApiOperation({ summary: 'Gibt eine Liste aller Employees mit allen Attributen aus.' })
  @Get(':department')
  @Roles({ roles: ['admin'] })
  async findAll(@Param('department') department: string): Promise<Employee[]> {
    return await this.employeesService.findAll(department);
  }

  /**
   * GET function for a specific employee by id
   * 
   * @returns an Employee entitie as a promise
   */
   @ApiOperation({ summary: 'Gibt den Employee mit der angegebenen ID mit allen Attributen aus.' })
   @Get('byID/:employee_id')
   @Unprotected()
   async findOne(@Param('employee_id') employee_id: number): Promise<Employee> {
    return await this.employeesService.findOne(employee_id);
  }

  /**
   * This function updates the specified employee entity with the data of the UpdateEmployeeDto.
   * @param updatedEmployee the Object of the updated User
   * @param employee_id the user ID which should be updated
   * @param user the keycloak user object - to verify the user can only change his own preferences
   * @returns the updated Employee Object
   */
  @Patch(':employee_id')
  async updateEmployee(@Body() updatedEmployee: UpdateEmployeeDto, @Param('employee_id', new ParseIntPipe) employee_id: number, @User() user){
    const result = this.employeesService.findEmployeeByEmployeeIdAndKeycloakId(employee_id, user.sub)
    if(result){
      return this.employeesService.updateEmployee(employee_id,updatedEmployee);
    }
  }

  /**
   * This function updates the specified employee entity with the data of the UpdateEmployeeDto (as admin).
   * @param updatedEmployee the Object of the updated User
   * @param employee_id the user ID which should be updated
   * @param user the keycloak user object - to verify the user can only change his own preferences
   * @returns the updated Employee Object
   */
   @Roles({ roles: ['admin'] })
   @Patch('admin/:employee_id')
   async updateEmployeeAsAdmin(@Body() updatedEmployee: UpdateEmployeeDto, @Param('employee_id', new ParseIntPipe) employee_id: number, @User() user){
       //check whether admin for that department exists in database
       let userWithDepartmentExisting = await this.employeesService.findEmployeeByEmployeeIdAndDepartment(employee_id, updatedEmployee.department);
       if (userWithDepartmentExisting) {
         //Need to check if the adminuser has the same department as the bookingsdepartment
         let admin = await this.employeesService.findEmployeeByUsername(user.preferred_username)
         if(admin){
           if(admin.department == updatedEmployee.department){
            return this.employeesService.updateEmployeeAsAdmin(employee_id,updatedEmployee,user.preferred_username);
           }
         } 
     }
   }

   

}
