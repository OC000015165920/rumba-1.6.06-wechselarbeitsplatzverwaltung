/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsNumberString, MinLength, MaxLength } from 'class-validator';

/**
 * This Class describes the DTO with the fields that are needed to create a new employee
 */
export class CreateEmployeeDto {

    /** Username of the Employee */
    @ApiProperty()
    @IsNotEmpty()
    @MinLength(4)
    // @MaxLength(8)
    username: string;

    /** Forename of the employee */
    @ApiProperty()
    @IsNotEmpty()
    @MaxLength(50)
    forename: string;

    /** lastname of the employee */
    @ApiProperty()
    @IsNotEmpty()
    @MaxLength(50)
    lastname: string;

    /** department of the employee */
    @ApiProperty()
    @IsNotEmpty()
    // @MaxLength(6)
    department: string;

    /** email of the employee */
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email_address: string;

    // /** ID of the default Map */
    // @ApiProperty()
    // @IsNotEmpty()
    // default_mapID: number;

    /** Is the Employee able to help at accidents? */
    @ApiProperty({default:false})
    @IsNotEmpty()
    first_aid: boolean;

    /** ID from the Keycloak User */
    @ApiProperty()
    keycloak_id: string;


}
