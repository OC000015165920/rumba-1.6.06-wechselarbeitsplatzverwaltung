/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { CreateEmployeeDto } from './create-employee.dto';
import { IsEmail, IsNotEmpty, isNumber, IsNumberString, Length, MaxLength } from 'class-validator';

/**
 * Describes the Class of the DTO which fields are needed to update an existing employee
 */
export class UpdateEmployeeDto extends PartialType(CreateEmployeeDto) {
    /** Forename of the employee */
    @ApiProperty()
    @IsNotEmpty()
    @MaxLength(50)
    forename: string;

    /** lastname of the employee */
    @ApiProperty()
    @IsNotEmpty()
    @MaxLength(50)
    lastname: string;

    /** department of the employee */
    @ApiProperty()
    @IsNotEmpty()
    // @MaxLength(6)
    department: string;

    /** ID of the default Map */
    @ApiProperty()
    @IsNotEmpty()
    default_mapID: number;

    /** email of the employee */
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email_address: string;

    /** Date of the last modification for the employee */
    modificationDate: Date;
    /** Who did the last modification */
    modifiedBy: string;

    /** Username of the User who did the change */
    modifiedByUsername:string;

}




