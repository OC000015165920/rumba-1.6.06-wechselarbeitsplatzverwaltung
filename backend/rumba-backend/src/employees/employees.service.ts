/*
RUMBA Backend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm'; 
import { Repository } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';

/**
 * The class which implements the central service for the Employee module
 *
 */
@Injectable()
export class EmployeesService {
  /**
   * Constructs the Service (called from nestJS framework)
   * @param employeeRepo the implicite repository containing the employees
   */
  constructor(@InjectRepository(Employee) public employeeRepo: Repository<Employee>,
  // @InjectRepository(Department) public depReo: Repository<Department>,
    ){

  }

  /**
   * This function creates a new Employee with the given data.
   * @param createEmployeeDto DTO that describes the new employee
   * @returns the created employee
   */
  async create(createEmployeeDto: CreateEmployeeDto): Promise<Employee> {
    const employee = await this.employeeRepo.save(createEmployeeDto);
    return employee;
  }

  /**
   * Get all employees registered   *
   * @returns a list of employees as an array of Employee entities
   */
  async findAll(department: string): Promise<Employee[]> {
    return await this.employeeRepo.find({where:{department: department}});   
  }

  /**
   * Retrieves employee specified by username
   * @param username the username specifying the user 
   * @returns Employee entity
   */
  async findEmployeeByUsername(username: string): Promise<Employee>{
      return await this.employeeRepo.findOne({where:{username: username}});
  }

  /**
   * This funtion returns an employee with the specified id if there is one in the repository.
   * @param id id of the booking
   * @returns a booking with the given ID
   */
  async findOne(id: number){
    return await this.employeeRepo.findOneOrFail(id);
  }

  /**
   * Retrieves employee specified by username
   * @param employee_id the employee_id specifying the user
   * @param keycloak_id the keycloak_id unique for the user 
   * @returns Employee entity
   */
   async findEmployeeByEmployeeIdAndKeycloakId(employee_id: number, keycloak_id:string): Promise<boolean>{
    return (await this.employeeRepo.find({where:{employee_id: employee_id, keycloak_id:keycloak_id}})).length==1;
  }

  /**
   * Retrieves employee specified by username
   * @param employee_id the employee_id specifying the user
   * @param keycloak_id the keycloak_id unique for the user 
   * @returns Employee entity
   */
     async findEmployeeByEmployeeIdAndDepartment(employee_id: number, department:string): Promise<boolean>{
      return (await this.employeeRepo.find({where:{employee_id: employee_id, department:department}})).length==1;
    }

    /**
     * This function checks if the Admin is allowed as an admin for the department.
     * @param department Name of the Department
     * @param AdminDepartment Name of the Department which belongs to the Admin User
     * @returns a boolean if it is the same or not
     */
    AdminAllowedForDepartment(department: string, AdminDepartment: string){
      if(department == AdminDepartment){
        return true;
      }
      return false;
    }

    /**
     * This function checks if a user with the specified username exists in the repository.
     * @param username the username to check, if it is existing 
     * @returns a boolean if the username was found
     */
  async isUserExisting(username: string){
    const userlist = await this.employeeRepo.find();
    for (const user of userlist) {
      if(user.username == username) {
        return true;
      }
    }
    return false;
  }

  /**
   * Retrieves employee specified by employee_id and username
   * @param employee_id
   * @param username
   * @returns Employee entity as promise
   */
   async findOneByIdAndKeycloakId(employee_id: number, keycloak_id: string): Promise<boolean> {
    try {
      return (await this.employeeRepo.find({where:{employee_id: employee_id, keycloak_id: keycloak_id}})).length==1; //select * from employees where employee_id = is
    } catch (err) {
      throw err;
    }
  }

  /**
   * Removes the employee with the given ID from the repository
   * @param employee_id the unique ID of the employee as a number
   * @return void
   */
  async remove(employee_id: number) {
    return await this.employeeRepo.delete(employee_id); 
  }

  /**
   * Returns a boolean wether a employee with the given department and keycloak_id is found in the employee Table
   * 
   * @param department the given department Name in the url
   * @param keycloak_id the keycloak_id in from the token
   * @returns boolean
   */
     async findOneByDepartmentAndKeycloakId(department: string, keycloak_id: string): Promise<Boolean> {
      
      return await (await this.employeeRepo.find({where:{department: department, keycloak_id: keycloak_id}})).length == 1;
    }


    /**
     * This function updates the specified employee with the data from the updateEmployeeDto.
     * @param employee_id the unique ID of the employee as a number
     * @param updated_employee the updated Employee Object
     * @returns the updated Employee
     */
    async updateEmployee(employee_id: number, updated_employee: UpdateEmployeeDto){
      updated_employee.modificationDate = new Date();
      updated_employee.modifiedBy = "user"
      await this.employeeRepo.update(employee_id,updated_employee);
      return await this.findOne(employee_id)
    }

    /**
     * updates a Employee as Admin 
     * @param employee_id the unique ID of the employee as a number
     * @param updated_employee the updated Employee Object
     * @returns the updated Employee
    */
    async updateEmployeeAsAdmin(employee_id: number, updated_employee: UpdateEmployeeDto, username: string){
      updated_employee.modificationDate = new Date();
      updated_employee.modifiedBy = "admin"
      updated_employee.modifiedByUsername = username;
      await this.employeeRepo.update(employee_id,updated_employee);
      return await this.findOne(employee_id)
    }
}
