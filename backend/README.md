
#
# RUMBA Backend
# Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Security Information
**The backend runs on a nodeJS platform with version 16.17.0 LTS.
The CORS (Cross-Origin Resource Sharing) is enabled inside the backend!

Although there are validation algorithms active for the REST API, some 
resources could be attacked, e.g. by SQL Injection. Our recommendation is
to run this version in intranet context, only!**


# Backend for Production

Since we do not need an addtitional Database we only have to create the docker-image for the Backend.
The Image can be created with the following command:

docker build -t rumba-backend -f Dockerfile.prod --build-arg APP_NAME=rumba-backend .

**Note**: The dot at the end of the comment is needed.

**Note**: the APP_Name Parameter has to be the same as the Folder in the nestjs Folder. In this case it is 'rumba-backend'. So in case you change the name, you have to change the name here aswell

**Note**: the file 'production.docker-compose.yml' is a template of the docker-compose file which can start the productive RUMBA Backend. A '.env'-File has to be created which contains the needed Parameters. More Information can be found in the Document 'Betriebsdokumentation'



# Backend in Development-Mode

The RUMBA backend service consists of 2 Docker images, one for Nestjs (inside the source folder nestjs), and one for a database (MariaDb) with the folders mariadb_data, mariadb_config and db-dump.
The nestjs folder contains of the application folder rumba-backend and the neccessary files to build the application.
## build procedure

To build the project and start the project:
1. copy ".env.old" to ".env" and change it according to your needs (see below)
2. run the commande "docker-compose build"
3. run the command "docker-compose up"
4. run the command "cd nestjs; docker cp nestjs_dev:/work/node_modules rumba-backend" (only needed initially)

If everythink worked fine, you should be able to see the Swagger UI on "localhost:$NESTJS_PORT"

## enviroment variables
There are different enviroment variables for docker in the file .env.
* COMPOSE_PROJECT_NAME=TOCHANGE: change this variable to your username when you call docker ps

Enviroment variables for MariaDB:
* MARIADB_VERSION=10.6
* MYSQL_ROOT_PASSWORD=example
* MYSQL_DATABASE=test            name of the database
* MYSQL_USER=karl
* MYSQL_PASSWORD=karl

Enviroment variables for nestJS:
* NESTJS_PORT=3000: port to listen for nestjs on localhost
* NESTJS_DEBUG_PORT=9229: port to listen for debugger in VSC, change the port in configurations too!
* COMPODOC_PORT=8090: port to listen for the compodoc documentation in localhost, changed from default port 8080
* APP_NAME=app-name

MariaDB and Adminer:
* ADMINER_PORT=7080: port to listen to adminer to modify mairaDB
* MARIA_PORT=3306


### change Node version
It is only possibly to change the Node Version inside the Dockerfile. 

## debug nestjs with VSC

start the application with *docker-compose up*. Go to "Run and Debug" and activate **Docker: Attach to Node**. Debug as usual.

## nestjs unittests
Check if the docker container is running with *docker ps*. The name of the docker container is in the docker-compose.yml file and is nestjs_dev_YOUR-USERNAME.
Go inside the docker container nestjs_dev_YOUR-USERNAME with *docker exec -it nestjs_dev_YOUR-USERNAME bash*.
Run *npm test*.

## change name of the project

1. Change the enviroment vairable APP_NAME in ".env" file
2. Modify the name in package.json file (Replace all occurrences of the old name)
3. Rename Project folder name.
4. Delete the files inside node_modules folder from your project directory, but not the .gitkeep file.
5. run npm install and ng serve -> is done while building and starting docker container


## use compodoc

Check if the docker container is running with docker ps. The name of the docker container is in the docker-compose.yml file and is nestjs_dev_YOUR-USERNAME.
Check if the file tsconfig.doc.json is in the nestjs/app-name folder. 
Go inside the docker container nestjs_dev with *docker exec -it nestjs_dev_YOUR-USERNAME bash*.
Create the compodoc files with *npm run compodoc:build* or serve the compodoc documentation to the port COMPODOC_PORT with the bash command *npm run compodoc:serve*. 

## mariaDB

The adminer can be used to inspect the mariadb from the port 8080. The server is mariadb, the user root and the passwort example. You can change the passwort in the .env file.

# IMPORTANT
Create your own .env file from the .env.old file (copy the .env.old file and call it .env).
Change the COMPOSE_PROJECT_NAME in the .env file.
If other people are using the same Port in localhost please change the ports in the .env file!


# troubleshooting

**problem**: The nestjs application is not reachable on the citrix client.The docker-compose script is running and the docker images (docker ps) have no error or restarting notifications.
**solution**: Go to ports and map the port 3000 for nestjs / 8090 for compodoc.


**problem**: MariaDB restartes very often.
**solution**: Delete the content of the mariadb_data folder (not the .gitkeep) and call *docker-compose up --no-start*.

**problem**: MariaDB restarts every second due to DB inconsistency
**solution**: Stop backend containers, rename recovery.cnf_sav to recovery.cnf_sav
            Start the backend, MariaDB will be in read-only node_modules
            Export the data via Adminer Userinterface to an SQL file
            stop the backend
            delete MariaDB data (content of mariadb_data directory)
            rename recovery.cnf to recovery.cnf_sav
            start the backend with --no-start option to initialize the data base
            start the backend as usual (database will be empty)
            import the data from saved SQL file
