V1.06.14 (11.07.2024)
Filename: rumba-frontend_v1.06.14, rumba-backend_v1.06.14
Fixes:
-  20240709-0106: nodeJS Version auf 18.20.4 gebracht wegen gravierender Sicherheitslücken (höhere Versionen sind nicht mit Angular 16 kompatbel)

V1.06.13 (28.05.2024)
Filename: rumba-frontend_v1.06.13, rumba-backend_v1.06.13
Fixes:
-   20240528-0076: Grundriss wird nach Kartenumschaltung nicht aktualisiert
-   20240116-0046: Doppelbuchung in RUMBA möglich, falls Pufferbüros gebucht wurden (als Admin), Änderung im Backend

V1.06.12 (27.05.2024)
Filename: rumba-frontend_v1.06.12
Fixes:
-   20240111-0148: Probleme in Karten mit vielen Namen, wenn nach unten gescrolled wird (2. Versuch)
-   20240527-0114: RotateCards und NamePattern werden nicht berücksichtigt
-   20240527-0144: Kalender zeigt keine gültigen Daten an

V1.06.11 (27.05.2024)
Filename: rumba-frontend_v1.06.11
Fixes:
-   Angular core und cli von 15.2 auf 16.1.5 gebracht, da die Version 15 aus dem Wartungsfenster läuft
-   keycloak_angular auf Version 14 gesetzt, damit die Bibliothek mit Angular 16 kompatibel ist (unterstützt Keycloak von v18-v24)
-   nodeJS Version auf 18.20.2 gebracht wegen gravierender Sicherheitslücken (höhere Versionen sind nicht mit Angular 16 kompatbel)
-   Performance: oftmaliges Laden des Bildes vermieden (nicht im Admin-Bereich)
-   20240116-0046: Doppelbuchung in RUMBA möglich, falls Pufferbüros gebucht wurden (als Admin)
-   20240507-0152: Zurückspringen der Anzeige am Terminal nach 30s in den Startbildschirm
-   20230718-0098: Probleme im Adminbereich bei Halbtagsbuchungen
-   20240111-0148: Probleme in Karten mit vielen Namen, wenn nach unten gescrolled wird
                    
V1.06.10 (19.12.2023)
Filename: rumba-frontend_v1.06.10, rumba-backend_v1.06.10
Fixes:
-   nodeJS Version von 18 auf 20.10.0 gebracht wegen gravierender Sicherheitslücken


V1.06.09 (12.07.2023)
Filename: rumba-frontend_v1.06.09, rumba-backend_v1.06.09
Fixes:
-   nodeJS Version von 16.7.5 auf 18 wg. gravierender Sicherheitslücken erhöht (Ticket 20230601-0156)

V1.06.08 (01.06.2023)
Filename: rumba-frontend_v1.06.08, rumba-backend_v1.06.08
Fixes:
-   Buchungen auf für bestimmte Benutzer reservierte Schreibtische werden nicht angezeigt (Ticket 20230621-0134)

V1.06.07 (30.05.2023)
Filename: rumba-frontend_v1.06.07, rumba-backend_v1.06.07
Fixes:
-   Halbtagsbuchungen waren teilweise nicht löschbar, wenn 2 Halbtagsbuchungen von unterschiedlichen Usern auf dem selben Arbeitsplatz waren,
    da beim Löschversuch in einigen Fällen die falsche von den beiden versucht wurde zu löschen. Es wird nun die Halbtagsbuchung des Nutzers ausgewählt.
-   Positionierung der Infocards nimmt nun die Länge des Namens und andere Faktoren mit auf, woraus bei langen Namen die Positionierung sich deutlich verbessert.
-   Anzeige des Initialwertes zur Drehung der Infocards in den Abteilungen im Adminpanel gefixed

Features:
-   Neue Benutzer, die keine Keycloak Rolle haben, die in der map_department_orgaunit Tabelle ist, bekommen die Abteilung "Allgemein".
    So können sie zumindest die Pufferbüros buchen.
-   Anzeige von reservierten Arbeitsplätzen als Infocards. Reservierungen unterscheiden sich von Buchungen durch einen schwarzen Hintergrund.

V1.06.06 (31.03.2023)
Filename: rumba-frontend_v1.06.06, rumba-backend_v1.06.06
Fixes:
-   Ersthelfer bei 2 Halbtagsbuchungen auf denselben AP werden jetzt abhängig vom angezeigten Namen angezeigt.
-   Markierung der/s gesuchten Nutzer/in wird bei Wechsel auf anderen Tag zurückgesetzt.
-   Mehre-Tage-Buchungen funktionieren nun auch für den Administrator
-   Rückmeldung nach Buchung im Admin-Panel auf Snackbar umgestellt
-   Löschen unter "Eigene Buchungen" korrigiert, so dass beim Refresh keine Duplikate der verbleibenden Zeilen entstehen
-   Arbeitsplätze, die bereits halbtags belegt worden sind, sind nun wieder für die zweite Tageshälfte buchbar

Features:
-   Allgemeine abteilungsunabhängige APs, die für alle Nutzer buchbar sind, können als Abteilung "Allgemein" verwaltet werden. 
    (Optional, abhängig davon, ob es die Abteilung namens "Allgemein" in der DB gibt)
-   Im Terminalmodus werden nur die öffentlichen Seiten dargestellt, der Login-Button entfällt. 
    Erreichbar über die URL der Seite + "/public/terminal/terminal/".
-   Personen wechseln nach Änderung der Abteilungszugehörigkeit in Keycloak automatisch die RUMBA-Orgaunit (bei Anmeldung).
-   Drehung der Infocards wie die APs. (Optional pro Abteilung, Abteilungsadmin kann ein/ausstellen)
    Korrekte Darstellung des Schriftzugs unabhängig vom Drehwinkel des Schreibtischs dank der Idee des Kreis Warendorf.
-   Löschen von Home-Office/Abwesenheitsbuchungen per "x" auf der Buchungsseite.
-   Umstellung auf Keycloak 20.x

V1.06.05 (17.01.2023)
Filename: rumba-frontend_v1.06.05, rumba-backend_v1.06.05
Fixes:
-   Im Backend Korrektur für das Löschen von Workspaces. Die Anonymisierung umfasst nun auch die Benutzerbuchungen,
    und die Delete-Operation ist nun sychnron, so dass auch der folgende Refresh im Frontend die korrekten
    Daten zeigt
-   Im Frontend Korrektur für das Ändern von Arbeitsplätzen. Diese wurde abgelehnt, wenn nicht der Name modifiziert wurde.
    Zusätzlich eine kleine Korrektur beim Anlegen der Arbeitsplätze, so dass Karte und Farbe vorbesetzt sind, wenn nur eine
    Auswahl zur Verfügung steht.
Features:
-   keine

V1.06.04 (04.01.2023)
Filename: rumba-frontend_v1.06.04
Fixes:
-   Bei Vergabe eines doppelten Namens für einen Schreibtisch erfolgt nun eine Fehlermeldung
-   Für die Vorlesefunktion bei Sehbehinderten wurde die Beschriftung der Umschalter für Tabelle und Grafik verbessert
-   SQL-Scripte für Create und Update wurden verbessert
Features:
-   Die Seite "http://<rumba-frontend>/public" zeigt nun Schaltfläche für jede Abteilung, über die man zur jeweiligen Übersichtsseite gelangt. Diese Sicht kann z.B. für Auskunftsterminals oder für sichere Mobil-Verbindungen genutzt werden.

V1.06.03 (25.10.2022)
Filename: rumba-frontend_v1.06.03
Fixes:
-   Fehler in Arbeitsplätze erstellen und bearbeiten behoben
-   Anonymisierte Buchungen werden nicht mehr mit ans Frontend geschickt
-   Beim Löschen eines Arbeitsplatzes werden alle Buchungen auf diesen anonymisiert
-   Beim Löschen eines Benutzer werden alle Arbeitsplätze die auf ihn reserviert sind freigegeben
-   Größenanpassung der Karte verbessert

V1.06.02 (29.09.2022)
Filename: rumba-frontend_v1.06.02
Fixes:
-   Benutzer löschen funktiert auch wenn noch Buchungen für den Benutzer vorhanden sind
-   Rervierte Arbeitsplätze werde nun auch in Tabellendarstellung berücksichtigt
-   Nach Löschung der Buchung über "x" bleibt der Tag in der später Ansicht bestehen
-   Reservierte Schreibtische freigeben funktiert wieder
-   Sortierung von Tabellen angepasst
-   Vormittags/Nachmittags Switch in der Public page hinzugefügt
Designanpassungen
Features:
-   Halbtagsbuchungen werden auf der Karte anders dargestellt: Buchung von anderen rot, eigene blau, freie grün; Morgens oben, nachmittags unten; morgens


V1.06.01 (09.09.2022)
Images für Abt. 20 eingespielt!

V1.06.00 (29.8.2022)
Filename: rumba-frontend_v1.06.00
Features:
-   Suchfunktion für Nutzernamen
-   Umschaltung der Grundrisskarte
-   Markierung des eigenen Arbeitsplatzes in der Karten
-   Löschen von eigenen Buchungen in der Karten über "x" im Label
-   Verschiedene Arbeitsplatztypen und Ausstattungseigenschaften
-   Benutzereigenschaften: Default-Karte, anonyme Darstellung in der Übersichtskarte
-   Administrationsfuntionen: Benutzerverwaltung, Arbeitsplatzverwaltung, Einstellungen für Organisation


V1.05.00 (3.6.2022)
Filename: rumba-frontend_v1.05.00
Features: 
-   Mandantenfähigkeit (über URL und Prüfung auf AD-Gruppe)
-	Anmeldung über die AD-Kennung (gleiches Passwort wie für die Windows-Anmeldung)
-	Umschaltung zwischen Karten- und Tabellendarstellung
-	Halbtagsbuchung vormittags/nachmittags (bis/nach 12:30 Uhr)
-	Kommentarfunktion bei der Buchung
-	5 aufeinanderfolgende Tage in einem Dialog buchen
-	Zusätzliche Kategorie „Abwesend“ buchbar
-	Zurückblättern um 3 Tage
-	Im Adminbereich: Anlegen und Löschen von Benutzerbuchungen


V1.00.05 (27.1.2022)
Filename: rumba-frontend_v1.00.05
Files: docker-compose-frontend-18.yml docker-compose-frontend-11.yml rumba/src/assets/rooms_0411.png
Features: %
Fixes: 3
20220119-0070 RUMBA: Abt. 11 benutzt auch den Raum 2.050
20220119-xxxx RUMBA: Planungshorizont für Abt. 11 auf 28 Tage einstellen
20220120-0072 RUMBA: Planungshorizont für Abt. 18 auf 14 Tage einstellen

===========================================================================
V1.00.04 (14.1.2022)
Filename: rumba-frontend_v1.00.04
Files: rumba/src/assets/rooms_0411.png rumba/src/assets/rooms_0418.png
Features: %
Fixes:
202201xx-xxxx RUMBA: Grafiken für Abt. 11 und 18 verkleinern, um Srollen zu vermeiden

============================================================================
V1.00.03 (7.1.2022)
Initial Version
