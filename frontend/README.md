# RUMBA Frontend
# Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


## Production build

The docker image for the production build has a seperate Dockerfile: `Dockerfile.prod`

Build production docker image:
```bash
docker build -t rumba-frontend -f Dockerfile.prod --build-arg APP_NAME=rumba .
```

# Development build

first build the project and second start the project:
1. docker-compose build
2. docker-compose up -d 

## API configuration

The API url is configured in the `rumba/src/assets/config.json`

## enviroment variables
There are 3 different enviroment variables for docker in the file .env.
* COMPOSE_PROJECT_NAME=TOCHANGE: change this variable to your username!
* ANGULAR_PORT=4200: port to listen for the angular application in localhost
* ANGULAR_TEST_PORT=9876: port to run unittests for angular in localhost
* COMPODOC_PORT=8080: port to listen for the compodoc documentation in localhost
* APP_NAME=app-name

### change Node version
It is only possibly to change the Node Version inside the Dockerfile. 

## angular unittests
Check if the docker container is running with docker ps. The name of the docker container is in the docker-compose.yml file and is angular_dev.
Go inside the docker container angular_dev with docker exec -it angular_dev bash.
Run ng test

## change name of the project

1. Change the enviroment vairable APP_NAME in .evn
2. Modify Project name in angular.json file (Replace all occurrences of the old name in angular.json)
3. Rename Project folder name.
4. Delete node_modules folder from your project directory. -> node_modules only exist inside docker container, ignore this step
5. run npm install and ng serve -> is done while building and starting docker container


## use compodoc

Check if the docker container is running with docker ps. The name of the docker container is in the docker-compose.yml file and is angular_dev.
Go inside the docker container angular_dev with docker exec -it angular_dev bash.
Create the compodoc files with npm run compodoc:build or serve the compodoc documentation to the port COMPODOC_PORT with the bash command npm run compodoc:serve. 



<!-- The docker image can be startetd by using the docker-compose file. 
It has to bestarted by this command:

docker-compose -f <docker-compose-file> up -d -->

<!--OLD:
 The docker image allows the configuration of the production API via the `API_URL` enviroment variable:

Run production docker container:
```bash
docker run -e API_URL=https://localhost:1234 -p 8080:8080 rumba-frontend:latest
``` -->

