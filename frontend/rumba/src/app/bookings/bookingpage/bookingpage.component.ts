/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserStoreService } from 'src/app/shared/User/user-store.service';
import { StoreService } from 'src/app/shared/store-service.service';
import { Workspace } from 'src/app/shared/Workspace/workspace';
import { WorkspacestoreService } from 'src/app/shared/Workspace/workspacestore.service';
import { DatePipe } from '@angular/common';
import { ConfirmDialogModel } from 'src/app/shared/Dialog/dialog.component';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { occupationDTO } from 'src/app/shared/Occupation/occupation.dto';
import { OccupationService } from 'src/app/shared/Occupation/occupation.service';
import { homeofficeDTO } from 'src/app/shared/Homeoffice/homeoffice.dto';
import { HomeofficeStoreService } from 'src/app/shared/Homeoffice/homeoffice-store.service';
import { RoomStoreService } from 'src/app/shared/rooms/room-store.service';
import { Booking } from 'src/app/shared/bookings/booking';
import { BookingsStoreService } from 'src/app/shared/bookings/bookings-store.service';
import { CreateBookingDTO } from 'src/app/shared/bookings/CreateBookingDTO';
import { bookingCategories } from 'src/app/shared/bookings/bookingCategories.enum';
import { absenceDTO } from 'src/app/shared/absence/absence.dto';
import { AbsenceStoreService } from 'src/app/shared/absence/absence-store.service';
import { ConfirmDialogModelOnlyHalfday, DialogbookingOnlyhalfdayComponent } from 'src/app/shared/dialogbooking-onlyhalfday/dialogbooking-onlyhalfday.component';
import { workspaceState } from 'src/app/shared/Workspace/WorkspaceState';
import { WorkspaceWithBookingFromTable } from 'src/app/shared/booking-table/booking-table.component';
import { ConfirmDialogModelMultiDay, DialogbookingWithMultiDayComponent } from 'src/app/shared/dialogbooking-with-multi-day/dialogbooking-with-multi-day.component';
import { dialogResultMultiDay } from 'src/app/shared/dialogbooking-with-multi-day/dialogresult-multi-day';
import { departmentDTO } from 'src/app/shared/Department/Department.dto';
import { tempUser } from 'src/app/shared/search/search.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Workspacecolor } from 'src/app/shared/Workspace/WorkspacecolorDTOs';

/**const number = 0 */
export const MORNING_WS: number = 0;
/**const number = 1 */
export const AFTERNOON_WS: number = 1;
/**const string = "Arbeitsplatz" */
export const CATEGORY_WS: string = "Arbeitsplatz";
/**const string = "Mobiles Arbeiten" */
export const CATEGORY_HO: string = "Mobiles Arbeiten";
/**const string = "Abwesend" */
export const CATEGORY_AB: string = "Abwesend";
/**const string = "Unbekannt" */
export const CATEGORY_UN: string = "Unbekannt";
/**const number = 1 */
export const CATEGORY_NO_WS: number = 1;
/**const number = 2 */
export const CATEGORY_NO_HO: number = 2;
/**const number = 3 */
export const CATEGORY_NO_AB: number = 3;
/**const number = 0 */
export const CATEGORY_NO_UN: number = 0;

/**
 * This component handles the whole booking stuff
 */
@Component({
  selector: 'app-bookingpage',
  templateUrl: './bookingpage.component.html',
  styleUrls: ['./bookingpage.component.scss']
})


/**
 * This class handles the whole booking stuff
 */
export class BookingpageComponent implements OnInit {

  /**defines the event handle for the workspace map*/
  @Input() workspaceClick = new EventEmitter<{event: Workspace, dates: Date[]}>();
  // user?: User;    // contains the current user data

  // data store
  /**occupied workspaces for the selected date and map*/
  occupationsForDay?: occupationDTO[];
  /**occupied workspaces for the selected date and map*/
  occupationsForDayAllMaps?: occupationDTO[];
  /**contains the name of the image to show the floor plan */
  floorplanName: string = '';
  /** all active workspaces */
  allActiveWorkspaces?: Workspace[];
   /**the workspacecolors for the department */
   workspacecolors: Workspacecolor[]=[];
  /**list of users booked for HomeOffice */
  homeofficeUser?: homeofficeDTO[];
  /**list of users booked for Absence */
  absenceUser?: absenceDTO[];
  // allRooms?: Room[];
  /**list of bookings from the user */
  bookingsByUser?: Booking[];

  //control parameters
  /**list of 2 elements for booked workspaces for morning and afternoon. Empty string for no booking */
  bookedWorkspaces: string[] = ["",""];


  // showTableInsteadOfMap = false //indicates whether the map view or the table view if shown
  /**indicates whether the user has booked a workspace for the whole day */
  wholeDayBooked = false;
  /**indicates whether the user has a booking for the morning of the recent day */
  morningBooked = false;
  /** indicates whether the user has a booking for the afternoon of the recent day */
  afternoonBooked = false;

  /**date of today as typeof Date */
  today: Date = new Date(Date.now());
  /**the selected data for the booking in milliseconds  */
  selectedDate: Date = new Date(this.today);
  /**contains a text representation for the selcted data, e.g. "heute", "morgen", "übermorgen", "am ..." */
  selectedDateText?: string = 'heute';
  /** text for the toggle to change between table and map */
  toggleMapTableText = "Tabelle anzeigen";
  /**text for the toggle to change between morning and afternoon */
  toggleMorningAfternoonText = "Nachmittag anzeigen";

  /**Date for checking if booking is avaliable or it is too far in the future */
  maxBookingDate = new Date();
  /**Bool for enabling + Button for Date increment */
  incrementDateDisabled = false;
  /**Bool for enabling + Button for Date decrement */
  decrementDateDisabled = false;
  /**indicates whether the date picker widget is needed */
  bookForAnydate = false;

  //timer
  //timerToMoveToStartPage = 600;  // time in seconds to move to overview page after the booking
  /**storage for time subscription */
  subscription: any;

  /**workspace = 1, homeoffice = 2, absence = 3 */
  bookingCategories = bookingCategories;

  /**all maps for the user */
  maps: {id:number, name:string, imageName:string, public?:boolean}[]=[];
  /**selected map */
  selected?: {id:number, name:string, imageName:string, public?:boolean}={id:0, name:"", imageName:""};

  /**if the searched employees must be updated */
  change: boolean = false;
  /**the occupation of the searched for user */
  searchedOccupation?: occupationDTO;
  /**the homeoffice entry of the searched for user */
  searchedHomeoffice?: homeofficeDTO;
  /**the absence entry of the searched for user */
  searchedAbsence?: absenceDTO;
  /**localdepartment */
  department: departmentDTO;

/**
 * initialises the services
 * @param userstoreservice Service which contains the most recent user data (created by dependency injection)
 * @param bookingsstoreservice Service which contains the most recent bookings (created by dependency injection)
 * @param router
 * @param storeService Service which contains the most recent common data (created by dependency injection)
 * @param workplaceService Service which contains the most recent workspace data (created by dependency injection)
 * @param activatedRoute
 */
  constructor(private userstoreservice: UserStoreService, private bookingsstoreservice: BookingsStoreService,
    private router: Router, public storeService: StoreService, private workspaceService: WorkspacestoreService,
    private activatedRoute: ActivatedRoute, private dialog: MatDialog, private occupationsService: OccupationService,
    private homeofficestoreService: HomeofficeStoreService, private roomStoreService:RoomStoreService, private absenceStoreService:AbsenceStoreService, private _snackBar: MatSnackBar) {

      if(storeService.localdepartment){
        this.maps = storeService.localdepartment.maps;
        for(let map of this.maps){
          map.public = false;
        }
        if(storeService.publicdepartment){
          this.maps = this.maps.concat(storeService.publicdepartment.maps);
          for(let map of this.maps){
            if(map.public==undefined){
              map.public = true;
            }
          }
        }

        if(this.maps.length>0){
          this.selected = this.maps[0];
          for(let map of this.maps){
            if(map.id==this.storeService.localuser.default_mapID){
              this.selected = map;
            }
          }
        }
        if(this.selected){
          storeService.selectedMap=this.selected;
        }
        this.department = storeService.localdepartment;
      }else{
        this.department = {
          id:-1,
          name: "",
          description: "",
          BookingInAdvanceDays: -1,
          NamePatternOnMap: "",
          maps: [],
          RotateInfocards: false,
        }
      }
   }

   /**
    * just initzialize the internal fields
    */
  ngOnInit(): void {
    // console.log("BookingPageComponent.ngOnInit() called")
    this.intitialize(true);
    if(this.storeService.localdepartment){
      this.maxBookingDate =  this.storeService.addDaysOnDate(this.today, this.storeService.localdepartment.BookingInAdvanceDays)
    }

    if(localStorage.getItem('showTableInsteadOfMap') == 'true'){
      this.storeService.showTableInsteadOfMap= true;
      this.toggleMapTableText = "Karte anzeigen"
    }

    this.today = new Date(Date.now());


    // loads all data again in an 120 second interval
    let interval = setInterval(() => {
      this.intitialize(false);
    }, 120000);
    //this.setUserBookedForDay(); pk, 2024/05/14: seems to make no sense, because this is too early (network requests are pending)
    //this.storeService.showTableInsteadOfMap=false; pk, 2024/05/14: seems to make no sense, becuase setting is changed some lines above...

  }

  /**
   * Initializes the internal fields
   * @param reloadParameters Indicates whether the parameters itself should be loaded again
   */
  intitialize(reloadParameters: boolean){
    //Reset Arrays

    console.log("initialize("+reloadParameters+") called")
    this.occupationsForDay = [];
    this.occupationsForDayAllMaps =[];
    this.allActiveWorkspaces =[];

    //Load Content
     //pk, 2021/09/16: simplify coding
    if(reloadParameters) {
      this.loadParameter();
    }
    // pk, 2022/05/26: initialize values after date has been changed
    this.morningBooked = false;
    this.afternoonBooked = false;
    this.wholeDayBooked = false;
    this.bookedWorkspaces = ["", ""];
    this.storeService.localuser.morningBooked = false;    // TODO: we should avoid this amount of booleans
    this.storeService.localuser.afternoonBooked = false;


    this.searchedAbsence=undefined;
    this.searchedHomeoffice=undefined;
    this.searchedOccupation=undefined;

    this.homeofficeUser=[];

    //console.log("initialize("+reloadParameters+"), date = "+this.selectedDate);

    // load values from server for the desired date
    this.loadOccupiedWorkspaces();

    this.loadWorkspacecolordescriptions();
    this.loadAllWorkspaces();

    this.loadHomeofficeForDay();
    // this.loadAllRooms();
    this.loadAllAbsence();

   console.log("initialize(): done")

  }

  /**
   * This function refreshes the map when the selected map changes.
   */
  refresh(){
    console.log ("refresh() called")
    if(this.selected){
      this.storeService.selectedMap=this.selected;
    }
    this.intitialize(true);
    console.log ("refresh() done")
  }

  /**
   * This function sets the parameters morningBooked, afternoonBooked and wholeDayBooked.
   */
  /* pk, 2024/05/14, function not needed, settings are done during handling of the network requests
  setUserBookedForDay(){
    if(this.occupationsForDayAllMaps){
      for(let booking of this.occupationsForDayAllMaps){
        if(booking.employee_id==this.storeService.localuser.employee_id){
          if(booking.afternoon){
            this.afternoonBooked=true;
            this.storeService.localuser.afternoonBooked = true;
            if(booking.morning){
              this.wholeDayBooked=true;

            }
          }
          if(booking.morning){
            this.morningBooked=true;
            this.storeService.localuser.morningBooked = true;
          }
        }
      }
    }
    if(this.absenceUser){
      for(let booking of this.absenceUser){
        if(booking.employee_id==this.storeService.localuser.employee_id){
          if(booking.afternoon){
            this.afternoonBooked=true;
            this.storeService.localuser.afternoonBooked = true;
            if(booking.morning){
              this.wholeDayBooked=true;

            }
          }
          if(booking.morning){
            this.morningBooked=true;
            this.storeService.localuser.morningBooked = true;
          }
        }

      }
    }
    if(this.homeofficeUser){
      for(let booking of this.homeofficeUser){
        if(booking.employee_id==this.storeService.localuser.employee_id){
          if(booking.afternoon){
            this.afternoonBooked=true;
            this.storeService.localuser.afternoonBooked = true;
            if(booking.morning){
              this.wholeDayBooked=true;

            }
          }
          if(booking.morning){
            this.morningBooked=true;
            this.storeService.localuser.morningBooked = true;
          }
        }

      }
    }
    console.log("setUserBookedForDay() done, wholeDayBooked = ", this.wholeDayBooked);
    //TODO this.intitialize(true);
  }*/

  /**
   * Sets all neccessary fields if the current date is changed
   * @param newDate a Date object which specifies the new selected day
   *
   */
  setSelectedDate(newDate: Date) {
    //console.log("setSelectedDate() called with date=" + newDate);
    this.selectedDate = newDate;
    this.storeService.selectedDate = newDate;

    // create a text with pattern "am xx.xx.xxxx"

    const newDateText =  this.selectedDate.toLocaleDateString();

    if ( newDateText != null ) {
        const weekday = this.storeService.getWeekDayFromDate(newDate);

        // pk, 2021/10/08: comma added to text
        this.selectedDateText = 'am ' + weekday+", den " + newDateText;

        if (newDate === this.today) {
            this.selectedDateText = 'heute, den ' + newDateText +', ';
        }
        else {
          const tomorrow = this.storeService.addDaysOnDate(this.today,1);

          if (newDate === tomorrow) {

            this.selectedDateText = 'morgen, den ' + newDateText +', ';
          }
        }

    }
    else {
      console.error("setSelectedDate(): bad format!");
    }
    //console.log("setSelectedDate() returns with text=" + this.selectedDateText);
  }

  /**
   * determine important parameter from context (browser's local store and activeRoute)
   * 2. the day to book for (today, tomorrow, or anydate)
  */
  loadParameter(){
    //Check the URL for the Parameter to know when to book
    let segments = this.activatedRoute.snapshot.url;
    if(segments.length >0)
    {
      let bookfor = segments[segments.length -1].path; //gets the string of the last segments e.g. today, or tomorrow
      //console.log(bookfor);
      switch(bookfor){
        case 'today':

          this.bookForAnydate = false;
          this.setSelectedDate(this.today);
          break;
        case 'tomorrow':

          this.bookForAnydate = false;
          this.setSelectedDate(this.storeService.addDaysOnDate(this.today,1));   // This Line sets the actual date +1 day
          this.storeService.showOnlyMorningBookings=true;
          this.storeService.showOnlyAfternoonBookings=false;
          break;
        case 'anydate':

          this.setSelectedDate(this.storeService.addDaysOnDate(this.today,2));
          this.bookForAnydate = true;  // with BookForAnydate = true, the input datepicker shows up and the user can choose a date

          this.storeService.showOnlyMorningBookings=true;
          this.storeService.showOnlyAfternoonBookings=false;
          break;
        default:
          console.error('Ungültige URL in bookingpage!');
      }
    }
    else {
      console.error('Leere URL in bookingpage!');
    }

  }

  /**
   * The function is called from data picker's OK button and shall change the internal values accordingly
   * @param event the event structure from date picker, we assume the date information is in event.target.value !!
   */
  setDateFromEvent(event: any){
    // store the new date value (in milliseconds)
    let nextday = new Date(event.target.value);
    if(this.isBookingdateValidInAdvance(nextday)&&this.isBookingdateValidNotPast(nextday)){
    let nextday = new Date(event.target.value);
    this.setSelectedDate(nextday);
    // initialize the internal data without initializing the parameters
    this.intitialize(false);
    }else{
      if(this.isBookingdateValidNotPast(nextday)){
        this.setSelectedDate(this.maxBookingDate);
      }else{
        this.setSelectedDate(this.today);
      }
    }
  }

 /**
   * The function is called from '+' button and shall add one day to the current selection, then change the internal values accordingly
    */
  onIncrementCurrentDay(){
    // calculate timevalue for the next day and store it in selectedDate
    let nextday = this.storeService.addDaysOnDate(this.selectedDate,1);
    this.incrementDateDisabled = !this.isBookingdateValidInAdvance(nextday);
    this.decrementDateDisabled = !this.isBookingdateValidNotPast(nextday);
    if(this.isBookingdateValidInAdvance(nextday)){
      this.setSelectedDate(nextday);
      // initialize the internal data without initializing the parameters
      this.intitialize(false);
    }
    else
      alert("Datum ungültig");

  }

 /**
   * The function is called from '-' button and shall substract one day to the current selection, then change the internal values accordingly
    */
  onDecrementCurrentDay(){
    // calculate timevalue for the former day and store it in selectedDate
    let nextday = this.storeService.addDaysOnDate(this.selectedDate,-1);
    this.decrementDateDisabled = !this.isBookingdateValidNotPast(nextday);
    this.incrementDateDisabled = !this.isBookingdateValidInAdvance(nextday);

    if(this.isBookingdateValidNotPast(nextday)){
      this.setSelectedDate(nextday);
      // initialize the internal data without initializing the parameters
      this.intitialize(false);
    }
    else
      alert("Datum ungültig");


  }

  /**
   * The function loads the available workspaces for the day specified in this.selectedDate, and stores prepares the infocards array
  */
  loadAllWorkspaces(){
      if(this.storeService.localdepartment&&this.selected){
        if(this.selected.public==false){
          this.workspaceService.getAllForMap(this.storeService.localdepartment.name, this.selected?.id).subscribe(result =>{
            //pk, 20240517: show only active workdesks
            this.allActiveWorkspaces = result.filter(x => x.active ==true);
          })
        }
        else {//if map is from the public department
          this.workspaceService.getAllForMap(this.storeService.publicDepartmentName, this.selected?.id).subscribe(result => {
            //pk, 20240517: show only active workdesks
            this.allActiveWorkspaces = result.filter(x => x.active ==true);
          })
        }

      }
    }
  /**
   * This function loads all Workspacecolors for the department.
   */
  async loadWorkspacecolordescriptions(){
    //console.log("loadWorkspacecolordescriptions() called, department =", this.department);
    if(this.workspaceService&&this.department){
      await this.workspaceService.getAllWorkspacecolorsForDepartment(this.department.name).subscribe(result => {
        this.workspacecolors = result;
        //console.log("getAllWorkspacecolorsForDepartment() returned, count =", this.workspacecolors.length);
      });
    }
    //console.log("loadWorkspacecolordescriptions() done");
  }

  /**
   * A helper method to adjust global booking indicators according to the given values (from user's occupation list)
   * @param morning true, if the user has a booking for the morning time
   * @param afternoon true, if the user has a booking for the afternoon
   * @param workspace_name contains the workspace name to be shown (workspace, "Mobiles Arbeiten" or "Abwesend")
   */
  adjustBookingIndicators(morning: boolean, afternoon: boolean, workspace_name: string) {
    //console.log("adjustBookingIndicators("+this.morningBooked+","+this.afternoonBooked+",workspace_name+) called");
    if(morning){
      this.morningBooked = true;
      this.storeService.localuser.morningBooked = true;
      this.bookedWorkspaces[MORNING_WS] = workspace_name;
    }
    if(afternoon) {
      this.afternoonBooked = true;
      this.storeService.localuser.afternoonBooked = true;
      this.bookedWorkspaces[AFTERNOON_WS] = workspace_name;
      if (morning) {
          // the whole day is booked by the user, simplify messages
          this.wholeDayBooked = true;
      }
    }
   // console.log("adjustBookingIndicators(): return, wholeDayBooked = " + this.wholeDayBooked);
  }
  /**
   * Method to retrieve all workspace reservations from backend and store them for presenting
   */
  loadOccupiedWorkspaces(){

    if(this.storeService.localdepartment&&this.selected){
      this.occupationsService.getOccupationsForDay(this.selectedDate, this.storeService.localdepartment.name).subscribe(result =>{
        // store the object for presenting
        this.occupationsForDayAllMaps = result;

        //get the occupations from the public department
        this.occupationsService.getOccupationsForDayOnMap(this.selectedDate, this.storeService.publicDepartmentName, this.storeService.selectedMap.id).subscribe(result =>{
          // pk, 2023/03/30: bug fix to see the occupation from others on the public maps, too
          //                NOTE: the concat function returns a new array, therefore the return value has to be assigned!!!
          this.occupationsForDayAllMaps = this.occupationsForDayAllMaps?.concat(result);

          if (this.occupationsForDayAllMaps) {
            this.occupationsForDay = [];
            // check if the current user has an occupation
            for (const element of this.occupationsForDayAllMaps) {

              if (element.workspace_mapID == this.storeService.selectedMap.id) {
                this.occupationsForDay.push(element);
                //console.log("loadOccupiedWorkspaces(), element pushed: ", element);
              }
              // pk, 2021/10/08: use correct field name for id comparison
              if(element.employee_id === this.storeService.localuser.employee_id){
                this.adjustBookingIndicators(element.morning, element.afternoon, element.workspace_name);
                console.log(`Workplace ${this.bookedWorkspaces} vom user ${this.storeService.localuser.employee_id} für diesen Tag gebucht: `, this.storeService.getFormatedDate(this.selectedDate));
              }
            }
          }

        });
      });
    }

  }



  /**
   * Method to retrieve all homeoffice reservations from backend and store them for presenting
   */
  loadHomeofficeForDay(){

    // pk, 2022/05/24: improvement - if statement should cover all actions in this function...
    if(this.storeService.localdepartment) {
     this.homeofficestoreService.getHomeofficeForDay(this.selectedDate, this.storeService.localdepartment.name).subscribe(result =>{
      this.homeofficeUser = result.sort((a,b) => (a.lastname > b.lastname)?1:-1);
      for (const element of this.homeofficeUser) {
        if(element.employee_id === this.storeService.localuser.employee_id){
          this.adjustBookingIndicators(element.morning, element.afternoon,"Mobiles Arbeiten");
          if (this.wholeDayBooked) {
            console.log(`Arbeitsplatz ${this.bookedWorkspaces} vom user ${this.storeService.localuser.employee_id} für diesen Tag gebucht: `, this.storeService.getFormatedDate(this.selectedDate));
          }
          else {
            if (this.morningBooked) {
              console.log(`Arbeitsplatz ${this.bookedWorkspaces} vom user ${this.storeService.localuser.employee_id} vormittags gebucht:`, this.storeService.getFormatedDate(this.selectedDate));
            }
            else {
              if (this.afternoonBooked) {
                console.log(`Arbeitsplatz ${this.bookedWorkspaces} vom user ${this.storeService.localuser.employee_id} vormittags gebucht: `, this.storeService.getFormatedDate(this.selectedDate));
              }
              else {
                console.error("Ups, something went wrong with this booking: ",element);
              }
            }
          }
        }
      }
    })
   }
  }

  /**
   * Loads all absence bookings for the selected date
   */
  loadAllAbsence(){
    // pk, 2022/05/24: improvement - if statement should cover all actions in this function...
    if(this.storeService.localdepartment) {

     this.absenceStoreService.getAbsenceForDate(this.selectedDate, this.storeService.localdepartment.name).subscribe(result =>{
      // console.log("Alle Abwesenheiten:", result)
      this.absenceUser = result.sort((a,b) => (a.lastname > b.lastname)?1:-1);
      for (const element of this.absenceUser) {
        if(element.employee_id === this.storeService.localuser.employee_id){
          this.adjustBookingIndicators(element.morning, element.afternoon,"Abwesend");

          console.log(`Abwesenheit vom user ${this.storeService.localuser.employee_id} für diesen Tag gebucht: `, this.storeService.getFormatedDate(this.selectedDate));
        }
      }
     })
    }
  }
  /**
   * Get the welcome text for the user
   * @returns a string with the text to show
   */
  getWelcomeText(): string {
    //console.log("getWelcomeText() called, wholeDayBooked = " + this.wholeDayBooked)
    let returnValue = "";
    if (this.wholeDayBooked) {
      returnValue = "Du hast " + this.selectedDateText + " für den gesamten Tag '"+this.bookedWorkspaces[MORNING_WS]+"' gebucht!";
    }
    else {
      if (this.morningBooked) {
        returnValue =  "Der Arbeitsplatz '"+this.bookedWorkspaces[MORNING_WS]+"' steht " + this.selectedDateText +  " vormittags für Dich bereit";
        if (!this.afternoonBooked) {
          returnValue += ", wo möchtest Du nachmittags arbeiten?"
        }
      }
      if (this.afternoonBooked) {
        if (returnValue != "") {
          returnValue += ", der Arbeitsplatz '"+this.bookedWorkspaces[AFTERNOON_WS]+"' steht nachmittags für Dich bereit";
        }
        else {
          returnValue = "Der Arbeitsplatz '"+this.bookedWorkspaces[AFTERNOON_WS]+"' steht " + this.selectedDateText +  " nachmittags für Dich bereit";
        }

        if (!this.morningBooked) {
          returnValue += ", wo möchtest Du vormittags arbeiten?"
        }
        else {
          returnValue += "!"
        }
      }
      if (!this.morningBooked && !this.afternoonBooked) {
        returnValue = "Hallo "+this.storeService.localuser.forename+", wo möchtest Du "+this.selectedDateText+" arbeiten?"
        if(this.storeService.showOnlyMorningBookings){
          returnValue += " Ansicht vormittags"
        }else{
          returnValue += " Ansicht nachmittags"
        }
      }
    }
    //console.log("getWelcomeText() returns " + returnValue);
    return returnValue
  }
  /**
   * The function is called from the UI map if a workspace is clicked
   * @param workspaceFromEvent Workspace which is clicked
  */
  onWorkspaceClick(workspaceFromEvent: Workspace) {
    if(!this.wholeDayBooked && workspaceFromEvent.active==true && this.storeService.localdepartment){

      if(workspaceFromEvent.employee_id && workspaceFromEvent.employee_id!= this.storeService.localuser.employee_id){
        let homeoffice: homeofficeDTO[]=[];
        let absence: absenceDTO[]=[];
        let temp = false;
        let day: {morning:boolean, afternoon:boolean} = {morning: false, afternoon: false};
        this.homeofficestoreService.getHomeofficeForDay(this.selectedDate, this.storeService.localdepartment.name).subscribe(result => {
          homeoffice = result;
          for(let home of homeoffice){
            if(home.employee_id==workspaceFromEvent.employee_id){
              console.log(home)
              day={morning:home.morning, afternoon: home.afternoon}
              temp = true;
            }
          }
          if(this.storeService.localdepartment){
            this.absenceStoreService.getAbsenceForDate(this.selectedDate, this.storeService.localdepartment.name).subscribe(result => {
              absence = result;
              for(let ab of absence){
                if(ab.employee_id==workspaceFromEvent.employee_id){
                  if(ab.morning){
                    day.morning=true;
                  }
                  if(ab.afternoon){
                    day.afternoon=true;
                  }
                  temp = true;
                }
              }
              if(temp){
                if(workspaceFromEvent.bookableWhenHO){
                  this.occupationsService.getOccupationsForWorkspace(workspaceFromEvent.workspace_id).subscribe(occupations =>{
                    let nobooking = false;
                    if(occupations){
                      for(let occupation of occupations){
                        if(occupation.booked_for_day == this.storeService.getNumberFromDate(this.selectedDate)){
                          if(occupation.morning && occupation.afternoon){
                            nobooking = true;
                          }
                        }
                      }
                    }
                    if(!nobooking){
                      console.log(day)
                      if(day.morning&&day.afternoon){
                        this.opendialogForReservedWorkspace(workspaceFromEvent);
                      }else if(day.morning){
                        this.opendialogForReservedWorkspaceHalfDay("morning", workspaceFromEvent);
                      }else{
                        this.opendialogForReservedWorkspaceHalfDay("afternoon", workspaceFromEvent);
                      }

                    }
                    else{
                      this.openSnackBar("Dieser Arbeitsplatz ist schon gebucht.");
                    }

                  });
                }else{
                  this.openSnackBar("Dieser Arbeitsplatz ist reserviert.");
                }

              }else{
                this.openSnackBar("Dieser Arbeitsplatz ist reserviert.");
              }
            })
          }

        })

      }else{
        this.occupationsService.getOccupationsForWorkspace(workspaceFromEvent.workspace_id).subscribe(occupations =>{
          let nobooking = false;
          if(occupations){
            for(let occupation of occupations){
              if(occupation.booked_for_day == this.storeService.getNumberFromDate(this.selectedDate)){
                if(occupation.morning && occupation.afternoon){
                  nobooking = true;
                }
              }
            }
          }
          if(!nobooking){
            this.opendialog(bookingCategories.workspace, occupations,workspaceFromEvent)
          }else{
            this.openSnackBar("Dieser Arbeitsplatz ist schon gebucht.");
          }

        });
      }

    }else{
      let message = "";
      if(this.wholeDayBooked){
        message+="Du hast für diesen Tag bereits gebucht. \n" ; //Zeilenumbruch mit \n funktioniert nicht
      }
      if(workspaceFromEvent.active==false){
        message+="Dieser Arbeitsplatz ist zur Zeit gesperrt. \n"; //Zeilenumbruch mit \n funktioniert nicht
      }
      if(!this.storeService.localdepartment){
        message+="Es ist keine Organisationseinheit hinterlegt. \n"; //Zeilenumbruch mit \n funktioniert nicht
      }
      this.openSnackBar(message);
    }

  }

  /**
   * Handling the checkbox click for half day booking
   * @param occupation the recent occupationDTO
   */
  onHalfDayBookingClick(event: {workspace_id: number, halfday: string, occupation?:occupationDTO}){
    if(!event.occupation){
      this.opendialogForReservedWorkspaceHalfDay(event.halfday, this.allActiveWorkspaces?.find((e)=> { return e.workspace_id == event.workspace_id}));
    }
    if(this.allActiveWorkspaces&&!this.wholeDayBooked && event.occupation){
      for (const workspace of this.allActiveWorkspaces) {
        if(workspace.workspace_id == event.occupation.workspace_id){
          this.occupationsService.getOccupationsForWorkspace(workspace.workspace_id).subscribe(occupations =>{
            this.opendialog(bookingCategories.workspace, occupations,workspace)
          })
        }
      }
    }

  }

  /**
   * Gets the occupation status for a given workspace
   * @param workspace the workspace to be checked
   * @param occupations an array of occupations to check against
   * @returns an instance of workapceState containing the booking status
   */
  getWorkspaceState(workspace:Workspace, occupations: occupationDTO[]): workspaceState {
    let occu = occupations.find(x => x.workspace_id == workspace.workspace_id)
    if(occu){
      let wsState: workspaceState = {
        morningBooked: occu.morning,
        afternoonBooked: occu.afternoon
      }
      return wsState;
    }
    let wsState: workspaceState = {
      morningBooked: false,
      afternoonBooked: false
    }
    return wsState;

  }

/**
   * The function is called from the UI if the "Mobiles Arbeiten" button is clicked
  */
  onHomeofficeClick() {

    if(!this.wholeDayBooked){

      // start booking process only if there is no current booking
      this.opendialog(bookingCategories.homeoffice)
    }
  }

  /**
   * The function is called from the UI if the "Abwesenheit" button is clicked
  */
  onAbsenceClick(){

    if(!this.wholeDayBooked){
      this.opendialog(bookingCategories.absence);
    }

  }

  /**
   * A halper function to create a confirm dialog
   * @param message the message to be shown
   * @returns a reference to a MatDialogconfig instance which can be used to open the dialog
   */
  createConfirmDialog(message:string): MatDialogConfig {
    const dialogConfig = new MatDialogConfig();
    const dialogData = new ConfirmDialogModel('Bestätigung', message);
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = dialogData;
    return dialogConfig;

  }

  /**
   *  Opens the dialog for a halfday booking
   * @param message the message shown to the user
   * @param morningBooked indicator for morning booking
   * @param afternoonBooked indicator for morning booking
   * @returns the reference to the dialogconfig data
   */
  createConfirmDialogHalfDay(message:string, morningBooked: boolean, afternoonBooked: boolean): MatDialogConfig {
    const dialogConfig = new MatDialogConfig();
    const dialogData = new ConfirmDialogModelOnlyHalfday('Bestätigung', message,morningBooked, afternoonBooked);
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = dialogData;
    return dialogConfig;

  }

  /**
   * Opens the dialog for a multiday booking
   * @param message the message shown to the user
   * @param occupations an array of occupations
   * @param workspace the name of the workspace, or undefined
   * @returns the reference to the dialogconfig data
   */
  createConfirmDialogMultiDay(message:string, occupations?: occupationDTO[], workspace?:Workspace): MatDialogConfig {
    const dialogConfig = new MatDialogConfig();
    const dialogData = new ConfirmDialogModelMultiDay('Bestätigung',message,this.selectedDate, this.morningBooked, this.afternoonBooked,occupations,workspace);
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    console.log(dialogData);
    dialogConfig.data = dialogData;
    return dialogConfig;

  }

  /**
   * Opens the dialog for a booking
   * @param bookingcategory the type of booking category
   * @param occupations an array of accupations
   * @param workspace the name of the workspace, or undefined
   */
  opendialog(bookingcategory: bookingCategories, occupations?:occupationDTO[], workspace?: Workspace){
    let datePipe = new DatePipe('en-US');
    let selectedDay = datePipe.transform(this.selectedDate, 'dd.MM.yyyy');

    //content of Dialog
    let message;
    if(workspace){
      message = 'Soll der Arbeitsplatz (' +workspace.workspace_name +') für den '+ selectedDay +' gebucht werden?';
    }
    else{
      message = 'Soll der Arbeitsplatz für den '+ selectedDay +' gebucht werden?';
    }

    let dialogConfig = this.createConfirmDialogMultiDay(message,occupations,workspace);
    if(!this.storeService.isDialogOpen){
      const dialogRef = this.dialog.open(DialogbookingWithMultiDayComponent, dialogConfig);
      this.storeService.isDialogOpen=true;

      dialogRef.afterClosed().subscribe(result => {
        this.storeService.isDialogOpen=false;
        if (result) {
          this.startBookingProcess(result,bookingcategory,workspace);
          this.change=true;
          this.change=false;
          this.refreshMap(1)
        }
      });
    }

  }

  /**
   * Opens the dialog for a booking
   * @param bookingcategory the type of booking category
   * @param occupations an array of accupations
   * @param workspace the name of the workspace, or undefined
   */
   opendialogForReservedWorkspace(workspace?: Workspace){
    let datePipe = new DatePipe('en-US');
    let selectedDay = datePipe.transform(this.selectedDate, 'dd.MM.yyyy');

    //content of Dialog
    let message;
    if(workspace){
      message = 'Soll der Arbeitsplatz (' +workspace.workspace_name +') für den '+ selectedDay +' gebucht werden?';
    }
    else{
      message = 'Soll der Arbeitsplatz für den '+ selectedDay +' gebucht werden?';
    }

    let dialogConfig = this.createConfirmDialogMultiDay(message);
    if(!this.storeService.isDialogOpen){
      const dialogRef = this.dialog.open(DialogbookingWithMultiDayComponent, dialogConfig);
      this.storeService.isDialogOpen=true;

      dialogRef.afterClosed().subscribe(result => {
        this.storeService.isDialogOpen=false;
        if (result) {
          console.log(result);
          this.startBookingProcess(result,1,workspace);
          this.change=true;
          this.change=false;
          this.refreshMap(1)
        }
      });
    }

  }

  /**
   * Opens the dialog for a booking
   * @param bookingcategory the type of booking category
   * @param occupations an array of accupations
   * @param workspace the name of the workspace, or undefined
   */
   opendialogForReservedWorkspaceHalfDay(halfday: string, workspace?: Workspace){
    let datePipe = new DatePipe('en-US');
    let selectedDay = datePipe.transform(this.selectedDate, 'dd.MM.yyyy');

    //content of Dialog
    let message;
    if(workspace){
      message = 'Soll der Arbeitsplatz (' +workspace.workspace_name +') für den '+ selectedDay +' gebucht werden?';
    }
    else{
      message = 'Soll der Arbeitsplatz für den '+ selectedDay +' gebucht werden?';
    }

    let dialogConfig;
    if(halfday=="morning"){
      dialogConfig = this.createConfirmDialogHalfDay(message, false, true);
    }else{
      dialogConfig = this.createConfirmDialogHalfDay(message, true, false);
    }

    if(!this.storeService.isDialogOpen){
      const dialogRef = this.dialog.open(DialogbookingOnlyhalfdayComponent, dialogConfig);
      this.storeService.isDialogOpen=true;

      dialogRef.afterClosed().subscribe(result => {
        this.storeService.isDialogOpen=false;
        if (result) {
          console.log(result);
          this.startBookingProcess([result],1,workspace);
          this.change=true;
          this.change=false;
          this.refreshMap(1)
        }
      });
    }

  }

  /**
   * check if the Workspace is already occupied
   */
  isWorkspaceAlreadyOccupied(workspace: Workspace):boolean{
    let isOccupied = false;
    if(this.occupationsForDay){
      this.occupationsForDay.forEach(element => {
        if(element.workspace_name == workspace.workspace_name){
          isOccupied = true;
        }
      });
      return isOccupied;
    }
    return false;

  }

  /**
   * The functions checks if the User has bookings for the day
   * @param workspace workspace which should be booked
   * @param dates selected Dates to book the workspace wfor
   * @returns a Promise to a boolean indicating whether the user has bookings for the recent day
   */
  async hasUserBookingsForDates(dates: Date[]):Promise<boolean>{
    //Check if the localuser object is valid
    if(this.storeService.localuser){
      const result = await this.bookingsstoreservice.getBookingsForUserAsPromise(this.storeService.localuser.employee_id);
        for (const booking of result) {
          for (const date of dates) {
            if(booking.booked_for_day === this.storeService.getNumberFromDate(date)){
              alert("An folgendem Tag besteht bereits eine Buchung: " + this.storeService.getFormatedDate(date))
              return true;
            }
          }
        }
        return false;
    }
    return false;
  }

  /**
   * The function executes the booking process for the selected workspace and date
   * @param workspace the workspace clicked in GUI
  */
   startBookingProcess(dialogResult: dialogResultMultiDay[], bookingCategory:bookingCategories, workspace?:Workspace){
     console.log("Was gebucht werden soll",dialogResult)
    if(bookingCategory == bookingCategories.workspace){
      if(workspace){
        for (const booking of dialogResult) {
           this.createBooking(booking,bookingCategory,workspace)
        }
      }
    }
    else if(bookingCategory == bookingCategories.homeoffice || bookingCategory == bookingCategories.absence){
      for (const booking of dialogResult) {
        console.log("                 ", dialogResult)
        this.createBooking(booking,bookingCategory)
     }
    }

  }

  /**
   * The function to execute the booking request to the backend
   * @param dialogResult the date value from the dialog
   * @param bookingcategory the booking category
   * @param workspace the name of the workspace, or empty
   */
  createBooking(dialogResult:dialogResultMultiDay, bookingcategory: bookingCategories, workspace?: Workspace){
    if(this.storeService.localuser){
      let newBooking: CreateBookingDTO;

      //Check if the Date is set or if we need to take the selectedDate
      let date = this.selectedDate;
      if(dialogResult.date){
        date = dialogResult.date
      }
      console.log(dialogResult)
      newBooking ={
        workspace_id: workspace?.workspace_id,
        employee_id: this.storeService.localuser.employee_id,
        booked_at: new Date(Date.now()),
        booked_for_day: this.storeService.getNumberFromDate(date),
        active: true,
        morning: dialogResult.morning,
        afternoon:dialogResult.afternoon,
        comment: dialogResult.note,
        category: bookingcategory,
        department: this.storeService.localuser.department,
        booked_by: "user"
      };
      //console.log("New booking: ", newBooking);

      if(this.selected?.public==true){//if the selected map is from the public department
        newBooking.department = this.storeService.publicDepartmentName;
        this.bookingsstoreservice.createPublic(newBooking).subscribe(() => {
          this.intitialize(false);//Initialize everything again, so the lists get updated
        });
      }else{
        this.bookingsstoreservice.create(newBooking).subscribe(() =>{
          this.intitialize(false); //Initialize everything again, so the lists get updated
        });
      }


    }

  }

  /**
   * This Function checks if the date is not higher than today + bookingdaysinadvance
   * @param date Selected Date
   */
  isBookingdateValidInAdvance(date: Date): boolean{
    if(date <= this.maxBookingDate){
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * This Function checks if the date is not older than today
   * @param date Selected Date
   * @returns boolean true if date is not in the past
   */
  isBookingdateValidNotPast(date: Date): boolean{

      // pk, 2022/05/24: bug fix to allow current date as valid day
      //    use the getNumberFromDate method of the store service to ignore hours, minutes
      //    and seconds for the comparison
      if (this.storeService.getNumberFromDate(date) >= this.storeService.getNumberFromDate(this.today)) {
        return true;
      }
      else {
        return false;
      }
    }

  /**
   * Called when the user presses the "buchen" button in the table
   * @param result an instance of WorkspaceWithBookingFromTable containing the workspace and the booking times
   */

  async BookingFromTable(result: WorkspaceWithBookingFromTable){
      /**this.occupationsService.getOccupationsForWorkspace(result.workspace.workspace_id).subscribe(occupations =>{

        if(!result.workspace.employee_id){
          this.opendialog(bookingCategories.workspace, occupations,result.workspace)
        }

      })*/
      if(!this.wholeDayBooked && result.workspace.active==true && this.storeService.localdepartment){

        if(result.workspace.employee_id && result.workspace.employee_id!= this.storeService.localuser.employee_id){
          let homeoffice: homeofficeDTO[]=[];
          let absence: absenceDTO[]=[];
          let temp = false;
          let day: {morning:boolean, afternoon:boolean} = {morning: false, afternoon: false};
          this.homeofficestoreService.getHomeofficeForDay(this.selectedDate, this.storeService.localdepartment.name).subscribe(tempresult => {
            homeoffice = tempresult;
            for(let home of homeoffice){
              if(home.employee_id==result.workspace.employee_id){
                console.log(home)
                day={morning:home.morning, afternoon: home.afternoon}
                temp = true;
              }
            }
            if(this.storeService.localdepartment){
              this.absenceStoreService.getAbsenceForDate(this.selectedDate, this.storeService.localdepartment.name).subscribe(tempresult => {
                absence = tempresult;
                for(let ab of absence){
                  if(ab.employee_id==result.workspace.employee_id){
                    if(ab.morning){
                      day.morning=true;
                    }
                    if(ab.afternoon){
                      day.afternoon=true;
                    }
                    temp = true;
                  }
                }
                if(temp){
                  if(result.workspace.bookableWhenHO){
                    this.occupationsService.getOccupationsForWorkspace(result.workspace.workspace_id).subscribe(occupations =>{
                      let nobooking = false;
                      if(occupations){
                        for(let occupation of occupations){
                          if(occupation.booked_for_day == this.storeService.getNumberFromDate(this.selectedDate)){
                            nobooking = true;
                          }
                        }
                      }
                      if(!nobooking){
                        console.log(day)
                        if(day.morning&&day.afternoon){
                          this.opendialogForReservedWorkspace(result.workspace);
                        }else if(day.morning){
                          this.opendialogForReservedWorkspaceHalfDay("morning", result.workspace);
                        }else{
                          this.opendialogForReservedWorkspaceHalfDay("afternoon", result.workspace);
                        }

                      }else{
                        this.openSnackBar("Dieser Arbeitsplatz ist schon gebucht.");
                      }

                    });
                  }else{
                    this.openSnackBar("Dieser Arbeitsplatz ist reserviert.");
                  }

                }else{
                  this.openSnackBar("Dieser Arbeitsplatz ist reserviert.");
                }
              })
            }

          })

        }else{
          this.occupationsService.getOccupationsForWorkspace(result.workspace.workspace_id).subscribe(occupations =>{
            let nobooking = false;
            if(occupations){
              for(let occupation of occupations){
                if(occupation.booked_for_day == this.storeService.getNumberFromDate(this.selectedDate)){
                  nobooking = true;
                }
              }
            }
            if(!nobooking){
              this.opendialog(bookingCategories.workspace, occupations,result.workspace)
            }else{
              this.openSnackBar("Dieser Arbeitsplatz ist schon gebucht.");
            }

          });
        }

      }else{
        let message = "";
        if(this.wholeDayBooked){
          message+="Du hast für diesen Tag bereits gebucht. \n" ; //Zeilenumbruch mit \n funktioniert nicht
        }
        if(result.workspace.active==false){
          message+="Dieser Arbeitsplatz ist zur Zeit gesperrt. \n"; //Zeilenumbruch mit \n funktioniert nicht
        }
        if(!this.storeService.localdepartment){
          message+="Es ist keine Organisationseinheit hinterlegt. \n"; //Zeilenumbruch mit \n funktioniert nicht
        }
        this.openSnackBar(message);
      }

  }

  /**
   * Called when the user switches between map view and table view
   */
  onToggleChange() {
      this.storeService.showTableInsteadOfMap = !this.storeService.showTableInsteadOfMap
      if(this.storeService.showTableInsteadOfMap){
        localStorage.setItem('showTableInsteadOfMap','true')
        this.toggleMapTableText = "Karte anzeigen"
      }
      else{
        localStorage.setItem('showTableInsteadOfMap','false')
        this.toggleMapTableText = "Tabelle anzeigen"
      }

  }

  /**
   * This funtion switches between map and table when called with the one not selected right now.
   * @param value boolean, true for table and false for map
   */
  onToggleChangeButton(value: boolean) {
    this.storeService.showTableInsteadOfMap = Boolean(value);
    if(this.storeService.showTableInsteadOfMap){
      localStorage.setItem('showTableInsteadOfMap','true')
      this.toggleMapTableText = "Karte anzeigen"
    }
    else{
      localStorage.setItem('showTableInsteadOfMap','false')
      this.toggleMapTableText = "Tabelle anzeigen"
    }
    this.intitialize(false);
}

  /**
   * Called when the user switches between morning's and afternoon's view on the map
   */
  onToggleChangeMorningAfternoon(){
      this.storeService.showOnlyAfternoonBookings = !this.storeService.showOnlyAfternoonBookings;
      this.storeService.showOnlyMorningBookings =!this.storeService.showOnlyMorningBookings;

      if(this.storeService.showOnlyAfternoonBookings){
        this.toggleMorningAfternoonText = "Vormittag anzeigen"
      }
      else{
        this.toggleMorningAfternoonText = "Nachmittag anzeigen"
      }

      this.intitialize(false);
  }

  /**
   * The function checks if an entry in Homeoffice or Absence should be shown depending on halfdaybookings and if only morning or afternoon should be shown.
   */
  showEntry(homeofficeData?:homeofficeDTO, absenceData?: absenceDTO){
    let returnValue = false;
    if(homeofficeData){
      if(homeofficeData.morning == this.storeService.showOnlyMorningBookings || homeofficeData.afternoon == this.storeService.showOnlyAfternoonBookings ){
        returnValue = true;
      }
    }

    if(absenceData){
      if(absenceData.morning == this.storeService.showOnlyMorningBookings || absenceData.afternoon == this.storeService.showOnlyAfternoonBookings ){
        returnValue = true;
      }
    }

    return returnValue
  }

  /**
   * The function reloads the component to refresh to map due to changed data
   * @param event number
   */
  refreshMap(event: number){
    console.log(event);
    if(event){
      let time = this.selectedDate;
      this.intitialize(false);
      this.selectedDate = time;
    }
  }

  /**
   * This function finds the occupation/homeoffice/absenc booking of the searched for user and highlights him/her and changes the map if necessary.
   * @param event the user which was searched for
   */
  searchUser(event: tempUser){
    console.log(event.employee_id)
    this.searchedAbsence = undefined;
    this.searchedHomeoffice = undefined;
    this.searchedOccupation = undefined;
    //to store all occupations not only on the selected map
    let occupations: occupationDTO[]=[];
    if(this.storeService.localdepartment){
      this.occupationsService.getOccupationsForDay(this.selectedDate, this.storeService.localdepartment?.name).subscribe(result => {
        occupations = result;
        if(occupations&&this.absenceUser&&this.homeofficeUser){
          for(let occupation of occupations){
            //if the searched user is in occupations set the searchedOccupation on the searched one
            if(event.employee_id===occupation.employee_id){
              this.searchedOccupation = occupation;
              //change map

              for(let map of this.maps){
                if(map.id==occupation.workspace_mapID){
                  this.storeService.selectedMap=map;
                  this.selected=map;
                  this.refresh();
                  console.log(this.selected);
                }
              }

            }
          }
          for(let absence of this.absenceUser){
            //if the searched user is in absence set the searchedAbsence on the searched one
            if(event.employee_id==absence.employee_id){
              this.searchedAbsence = absence;
            }
          }
          for(let homeoffice of this.homeofficeUser){
            //if the searched user is in homeoffice set the searchedHomeoffice on the searched one
            if(event.employee_id==homeoffice.employee_id){
              this.searchedHomeoffice = homeoffice;
            }
          }
        }
      });
    }

    //this.loadData();
  }

  /**
   * This function checks if the homeoffice booking is the one with the user searched for.
   * @param homeoffice the homeoffice to be checked
   * @returns boolean if the entry is the one searched
   */
  searchHomeoffice(homeoffice: homeofficeDTO){
    if(homeoffice===this.searchedHomeoffice){
      return true;
    }
    return false;
  }

  /**
   * This function checks if the absence booking is the one with the user searched for.
   * @param homeoffice the absence to be checked
   * @returns boolean if the entry is the one searched
   */
  searchAbsence(absence: absenceDTO){
    if(absence===this.searchedAbsence){
      return true;
    }
    return false;
  }

  onDeleteHO(event: any){
    this.refreshMap(event);
  }

  onDeleteAbsence(event: any){
    this.refreshMap(event);
  }

  /**
   * This function returns an Array of the workspaces (IDs) that are reserved but free due to bookings and when they are free.
   * @returns {workspace_id: number, morning: boolean, afternoon: boolean}[], an Array of the workspaces (IDs) that are reserved but free due to bookings and when they are free.
   */
  getFreeReservedWorkspaces(){
    let list:{workspace_id: number, morning: boolean, afternoon: boolean}[] = [];
    if(this.allActiveWorkspaces){
      for(let w of this.allActiveWorkspaces){
        if(w.employee_id && w.bookableWhenHO){
          this.homeofficeUser?.find((e)=> {
            if(e.employee_id==w.employee_id){
              let temp = list.findIndex((e)=>{return e.workspace_id == w.workspace_id});
              if(temp>=0){
                if(e.morning){
                  list[temp].morning=true;
                }
                if(e.afternoon){
                  list[temp].afternoon=true;
                }
              }else{
                list.push({workspace_id: w.workspace_id, morning: e.morning, afternoon: e.afternoon})
              }
            }
          })
          this.absenceUser?.find((e)=> {
            if(e.employee_id==w.employee_id){
              let temp = list.findIndex((e)=>{return e.workspace_id == w.workspace_id});
              if(temp>=0){
                if(e.morning){
                  list[temp].morning=true;
                }
                if(e.afternoon){
                  list[temp].afternoon=true;
                }
              }else{
                list.push({workspace_id: w.workspace_id, morning: e.morning, afternoon: e.afternoon})
              }
            }
          })
          this.occupationsForDayAllMaps?.find((e)=> {
            if(e.employee_id==w.employee_id){
              let temp = list.findIndex((e)=>{return e.workspace_id == w.workspace_id});
              if(temp>=0 && list[temp].workspace_id!=e.workspace_id){
                if(e.morning){
                  list[temp].morning=true;
                }
                if(e.afternoon){
                  list[temp].afternoon=true;
                }
              }else{
                list.push({workspace_id: w.workspace_id, morning: e.morning, afternoon: e.afternoon})
              }
            }
          })
        }
      }
    }
    return list;
  }

  /**
   * This functions is used to send a toast message for changing user attributes and disappears after 5 seconds.
   * @param input string, the message
   */
   openSnackBar(input: string) {
    this._snackBar.open(input, "" ,{
      duration: 5000
    });
  }

}
