/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { BookingsRoutingModule } from './bookings-routing.module';
import { SharedModule } from '../shared/shared.module';
//import { StoreModule } from '@ngrx/store';

import { BookingpageComponent } from './bookingpage/bookingpage.component';
import { AngularMaterialModule } from '../angular-material.module';
import { MatNativeDateModule } from '@angular/material/core';
//import * from ./store/BookingService.reducer;


@NgModule({
  declarations: [
    BookingpageComponent,

  ],
  imports: [
    CommonModule,
    BookingsRoutingModule,
    SharedModule,
    AngularMaterialModule,
    MatNativeDateModule,
    //StoreModule.forFeature(fromBookings.bookingsFeatureKey, fromBooks.reducer),
  ],
  exports: [
    
  ]
})
export class BookingsModule { }
