/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from 'src/app/shared/store-service.service';
import { DialogComponent, ConfirmDialogModel } from 'src/app/shared/Dialog/dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { Sort } from '@angular/material/sort';
import { Booking } from 'src/app/shared/bookings/booking';
import { BookingsStoreService } from 'src/app/shared/bookings/bookings-store.service';
import { bookingCategories } from 'src/app/shared/bookings/bookingCategories.enum';
import { CATEGORY_AB, CATEGORY_HO, CATEGORY_NO_AB, CATEGORY_NO_HO, CATEGORY_NO_WS, CATEGORY_WS } from 'src/app/bookings/bookingpage/bookingpage.component';

/**This interface is used for the table to represent bookings */
export interface TableBookings {
  /**id of the booking */
  booking_id: number,
  /**date for which the booking is */
  booked_for_day: Date,
  /**the booked workspace */
  workspace: string,
  /**if the booking is deletable */
  deletable: boolean,
  /**if the booking is checked */
  booking_checked: boolean,
  /**category of the booking */
  category: string,
  /**if morning is booked */
  morning: boolean,
  /**if afternoon is booked */
  afternoon: boolean
}

/**
 * This component manages bookings for the user.
 */
@Component({
  selector: 'app-manage-bookings',
  templateUrl: './manage-bookings.component.html',
  styleUrls: ['./manage-bookings.component.scss']
})

export class ManageBookingsComponent implements OnInit {

  /**the table */
  @ViewChild (MatTable) table!: MatTable<any>;

  /**is not used */
  showFutureBookings = true;
  /**is not used */
  showPastBookings = false;
  /**data for the table */
  dataSource: TableBookings[] = [];

  /**saves all future bookings (from the user) */
  futureBookings: Booking[] =[];
  /**saves all past bookings (from the user) */
  pastBookings: Booking[] =[];
  /**seves all bookings (from the user) */
  allBookings: Booking[] = [];
  /**if a booking was deleted */
  bookingDeleted = false;
  /**a string representing the deleted booking (only date because you can't book multiple on the same day) */
  deletedBooking = '';
  /**the column displayed in the table */
  displayedColumns: string[] = ['booking_checked', 'booking_id', 'booked_for_day', 'category', 'workspace', 'occupied', 'deleteAction' ];
  /**is not used */
  weekday ="";
  /**is always false */
  checked = false;

  /**
   * This constructor initilizes the component with the parameters below
   * @param router Router for navigation
   * @param bs BookingsStoreService Service which contains the most recent booking data (created by dependency injection)
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   * @param dialog MatDialog for dialogs
   */
  constructor(private router: Router,private bs: BookingsStoreService, public storeService: StoreService, private dialog: MatDialog) { }

  /**
   * The function loads all bookings for the localuser and sets bookingDeleted to false.
   */
  ngOnInit(): void {
    if(this.storeService.localuser.employee_id) {
      this.loadAllBookings(this.storeService.localuser.employee_id);
    }
    this.bookingDeleted = false;
  }

  /**
   * The function sets futureBookings, pastBookings, allBookings and dataSource to an empty array.
   */
  initialize() {
    this.futureBookings = [];
    this.pastBookings = [];
    this.allBookings = [];
    this.dataSource = [];

  }

  /**
   * The function loads all bookings for the specified user to allBookings.
   */
  loadAllBookings(userID: number){
    console.log(`loadAllBookings(${userID})`);

    // clean former array content
    this.initialize();
    this.bs.getBookingsForUser(userID).subscribe(result =>{
      this.allBookings = result;
      this.loadPastAndFutureBookings();
      // pk, 2023/03/28, update the table synchronously to the result
      if (this.table) {
        //console.log('render rows');
        this.table.renderRows();
      }
    });
  }

  /**
   * The function is empty.
   */
  sortData(event:Sort) {
    return;
  }

  /**
   * The function is empty.
   */
  applyFilter(event: Event) {
    return;
  }

  /**
   * deletes Booking referenced by booking_id
   * @param booking_id Unique ID to indentify Booking
   */
 deleteBooking(booking_id: number){
   console.log(`deleteBooking called for booking ID ${booking_id}`);
    this.bs.deleteBooking(booking_id).subscribe( result =>{
      this.allBookings.forEach(element => {
        if(element.booking_id == booking_id) {
          this.deletedBooking = element.booked_for_day.toString(); //booked_for_day expected in Format YYYYMMDD
          this.deletedBooking = this.deletedBooking.slice(6,8) + '.' + this.deletedBooking.slice(4,6) + '.' + this.deletedBooking.slice(0,4)
        }
      })
      console.log(`deleteBooking(${booking_id} returned: ${result}`);
      // reload the existing bookings to update the view
      if(this.storeService.localuser.employee_id) {
        this.loadAllBookings(this.storeService.localuser.employee_id);
      }
    });
  }

  /**
   * The function navigates to the landingpage.
   */
  goToLandingPage(){
    this.router.navigate(['landingpage']);
  }

  /**
   * The function navigates to the startpage.
   */
  goToStart(){
    this.router.navigate(['']);
  }

  /**
   * The function loads all past and future bookings into the attributes pastBookings and FutureBookings and updates the table.
   */
  loadPastAndFutureBookings(){
    // pk, 2023/03/28: initialize data store, otherwise entries are duplicated virtually in case of multi deletion
    this.dataSource = [];
    this.futureBookings = [];
    this.pastBookings = [];

    this.allBookings.forEach(element => {

      //console.log(`Datum für Bookingsaufteilung: ${element.booked_for_day}, Date.now=${Date.now()}`);
      // pk, 2021/09/29: show bookings of today, too
      if(element.booked_for_day < this.storeService.getNumberFromDate(new Date(Date.now()))) {
        this.pastBookings.push(element);
      }
      else{
        this.futureBookings.push(element);
      }
    });

    // console.log(`past Bookings: ${this.pastBookings}`);
    //console.log("future Bookings: before sort" + this.FutureBookings);

    // sort the array to have the newest entry on top
    this.futureBookings.sort((a,b) => (a.booked_for_day>b.booked_for_day)||(a.booked_for_day==b.booked_for_day&&a.afternoon)?1:-1);


    // console.log("future Bookings: after sort",this.futureBookings);
    this.futureBookings.forEach(element => {
      let workspacename =""
      let category =""
      switch(element.category){
        case CATEGORY_NO_WS: {
          workspacename = element.workspace.workspace_name
          category = CATEGORY_WS;
        }break;
        case CATEGORY_NO_HO: {
          category = CATEGORY_HO;
        }break;
        case CATEGORY_NO_AB: {
          category = CATEGORY_AB;
        }break;
      }
      // pk, 2022/05/27: added morning/afternoon booking to table
      let morningBooked = element.morning;
      let afternoonBooked = element.afternoon;

      let tableRow: TableBookings = {
        booking_id: element.booking_id,
        // booked_for_day: this.bs.convertInternalDateToHumanForm(element.booked_for_day.toString()),
        booked_for_day: new Date(this.storeService.getDateFromNumber(element.booked_for_day)),
        workspace: workspacename,
        deletable: true,
        booking_checked: this.checked,
        category: category,
        morning: morningBooked,
        afternoon: afternoonBooked
      }
      //console.log(`workspace: ${element.workspace}`);
      this.dataSource.push(tableRow);

    })
    console.log("Table Datasource_ ", this.dataSource)
    this.table.renderRows();
  }

  /**
   * The function opens a deleting dialog for the given table booking.
   * @param element TableBookings
   */
  onDeleteButton(element: TableBookings): void {
    console.log(`onDeleteButton() called for ${element.booking_id}`)
    this.openDeleteDialog(element.booking_id);
  }

  /**
   * Opens Dialog to Confirm Deletion of Booking
   * @param booking_id Unique ID to indentify Booking
   */
  openDeleteDialog(booking_id: number) {
    const dialogConfig = new MatDialogConfig();
    var date:string = '';
    this.allBookings.forEach(element => {
      if(element.booking_id == booking_id) {
        date = element.booked_for_day.toString(); //booked_for_day expected in Format YYYYMMDD
        date = date.slice(6,8) + '.' + date.slice(4,6) + '.' + date.slice(0,4)
      }
    })
    //content of Dialog
    const message = 'Soll die Buchung vom ' + date + ' wirklich gelöscht werden?';
    const dialogData = new ConfirmDialogModel('Bestätigung', message);

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = dialogData;

    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.bookingDeleted = result; //for the html file to show deletion message

      if (result) {
        //actual deletion function
        this.deleteBooking(booking_id);
      }
    });
  }

  /**
   * The function opens a deleting dialog for multiple bookings and deletes them after confirmation.
   */
  onDeleteMultipleBookings(){
    const dialogConfig = new MatDialogConfig();
    const message ='Sollen die ausgewählten Buchungen wirklich gelöscht werden?';
    const dialogData = new ConfirmDialogModel("Bestätigung", message);

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = dialogData;
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig)

    dialogRef.afterClosed().subscribe(result =>{
      if(result){
        for (const booking of this.dataSource) {
          if(booking.booking_checked){
            this.deleteBooking(booking.booking_id);
          }
        }
        //this.loadAllBookings(this.storeService.localuser.employee_id)
      }

    })

  }

}
