/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { RouterTestingModule } from '@angular/router/testing';
import { KeycloakService } from 'keycloak-angular';
import { AppConfigTestingService } from 'src/app/config/app-config-test.service';
import { AppConfigService } from 'src/app/config/app-config.service';
import { ManageBookingsComponent } from './manage-bookings.component';

describe('ManageBookingsComponent', () => {
  let component: ManageBookingsComponent;
  let fixture: ComponentFixture<ManageBookingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageBookingsComponent ],
      imports: [HttpClientTestingModule, RouterTestingModule],
      //pk, 2022/03/21: insert neccessary external providers to avoid KARMA errors
      providers: [ 
        { provide: AppConfigService, useClass: AppConfigTestingService },
        { provide: KeycloakService, useValue: {} },
        { provide: MatDialog, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBookingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /**
   * Just checks whether the component could be created 
   */
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * checks whether bookings could be "loaded"
   */
  it('should load all bookings', () => {
    expect(component.loadAllBookings).toBeTruthy();
  });
});
