/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
import { Booking } from 'src/app/shared/bookings/booking';

/**
 * This component represents a booking.
 */
@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {

  /**The Booking object */
  @Input() booking?: Booking;
  /**a event emitter to delete the booking */
  @Output() deleteEvent = new EventEmitter<number>();

  /**booking date as Date object */
  bookingDate?: Date;

  /**
   * The constructor is empty.
   */
  constructor() { 

  }

  /**
   * The function sets bookingDate to booked_for_day from booking if booking is not undefined.
   */
  ngOnInit(): void {
    if(this.booking){
      console.log("geladene booking in profil:", this.booking)
      this.bookingDate = this.getDateFromNumber(this.booking?.booked_for_day);
      console.log(this.bookingDate);
    }

  }

  /**
   * The function emits a deleteEvent event with the booking id if booking is not undefined.
   */
  deleteBooking(){
    this.deleteEvent.emit(this.booking?.booking_id);
  }

  /**
   * The function converts a number in format yyyyMMdd to a Date object.
   */
  getDateFromNumber(dateAsNumber: number): Date{
    var year = Number(dateAsNumber.toString().substring(0,4));
    var month = Number(dateAsNumber.toString().substring(4,6))-1;
    var day = Number(dateAsNumber.toString().substring(6,8));
    
    //console.log("Datum:"+day + "."+month + "."+year );
    return new Date(year,month,day,0,0,0,0)
  }



}
