import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Map } from 'src/app/shared/map/Map'
import { StoreService } from 'src/app/shared/store-service.service';
import { LocalUser } from 'src/app/shared/User/localuser';
import { UpdateEmployeeDto } from 'src/app/shared/User/updateEmployeeDTO';
import { User } from 'src/app/shared/User/user';
import { UserStoreService } from 'src/app/shared/User/user-store.service';

/**
 * This component is used to change user settings.
 */
@Component({
  selector: 'settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  /**all maps for the user */
  maps: Map[]=[];
  /**selected map */
  selectedMap: Map={id:0, name:"", imageName:""};
  /**selected anonymous value */
  selectedAnonymous: boolean = false;

  /**
   * This constructor sets maps and selected.
   * @param storeService StoreService
   * @param userStoreService UserStoreService
   * @param _snackBar MatSnackBar
   */
  constructor(private storeService: StoreService, private userStoreService: UserStoreService, private _snackBar: MatSnackBar) {
    if(storeService.localdepartment){
      console.log(storeService.localdepartment);
      this.maps = storeService.localdepartment.maps;
      if(this.maps.length>0){
        this.selectedMap = this.maps[0];
      }
      if(this.selectedMap){
        this.storeService.selectedMap=this.selectedMap;
      }
    }
    if(this.storeService.localuser.employee_id){
      console.log(this.storeService.localuser.employee_id);
    }
    this.userStoreService.allOptionsForEmployee(this.storeService.localuser.employee_id).subscribe(result => {
      console.log(result);
      for(const option of result){
        if(option.name=="anonymous"){
          this.selectedAnonymous=true;
        }
      }
    });
  }

  /**
   * This function does nothing, but is necessary to implement OnInit
   */
   ngOnInit(): void {
  }

  /**
   * This function is called from the "update" button in the settings dialog
   * and changes the the selected changed attributes of the user.
   */

  changeUser(){
    //get the user object of the localuser first
    let user: User = this.storeService.getLoginUser();

      //store the changes for userfeedback
      let changes:{attribute: string, newValue: string}[]=[];
      if(user.default_mapID!=this.selectedMap.id){
        changes.push({attribute: "Defaultkarte", newValue: this.selectedMap.name});
      }
      let tempanonymous=false;
      this.userStoreService.allOptionsForEmployee(this.storeService.localuser.employee_id).subscribe(result => {
        console.log(result);
        for(const option of result){
          if(option.name=="anonymous"){
            tempanonymous=true;
          }
        }
        if(tempanonymous!=this.selectedAnonymous){
          if(this.selectedAnonymous){
            changes.push({attribute: "Anonym", newValue:"Anonym"});
            this.userStoreService.addOptionForEmployee({options_id: 1, employee_id: user.employee_id}).subscribe(result=>{
              console.log(result);
            });
          }else{
            changes.push({attribute: "Anonym", newValue:"Nicht Anonym"});
            this.userStoreService.deleteOptionForEmployee(user.employee_id, 1).subscribe(result=>{
              console.log(result);
            });
          }
        }
        //convert the input on selectedAnonymous to boolean
        let anontemp:boolean;
        if(this.selectedAnonymous){
          anontemp=true;
        }else{
          anontemp=false;
        }
        //new user data
        let temp: UpdateEmployeeDto = {
          forename: user.forename,
          lastname: user.lastname,
          department: this.storeService.localuser.department,
          default_mapID: this.selectedMap.id,
          email_address: user.email_address,
        }
        console.log(temp);
        let tmp = this.userStoreService.updateUser(user.employee_id, temp).subscribe(result => {

          if(this.selectedMap){
            //user feedback
            this.openSnackBar(changes);
          }
        });
      });
  }

  /**
   * This function changes the selectedAnonymous attribute to the opposite.
   */
  toggleChangeAnonymous(){
    this.selectedAnonymous = !this.selectedAnonymous;
  }

  /**
   * This functions is used to send a toast message for changing user attributes and disappears after 5 seconds.
   * @param input {string,string}, pairs of attributes and new values
   */
  openSnackBar(input: {attribute: string, newValue: string}[]) {
    let output="";
    for(let i of input){
      output = output + i.attribute + " wurde auf " + i.newValue + " gesetzt. "
    }
    this._snackBar.open(output, "" ,{
      duration: 5000
    });
  }
}
