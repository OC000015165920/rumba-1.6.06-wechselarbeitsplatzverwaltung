/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ManageBookingsComponent } from './manage-bookings/manage-bookings.component';
import { BookingComponent } from './booking/booking.component';

import { ReactiveFormsModule } from '@angular/forms';

import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';

import { AngularMaterialModule } from '../angular-material.module';
import { SharedModule } from '../shared/shared.module';

import { SettingsComponent } from './settings/settings.component';


@NgModule({
  declarations: [
    ManageBookingsComponent,
    BookingComponent,
    SettingsComponent,
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ReactiveFormsModule,
    //MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AngularMaterialModule,
    SharedModule,
    //MatProgressSpinnerModule
  ]
})
export class ProfileModule { }
