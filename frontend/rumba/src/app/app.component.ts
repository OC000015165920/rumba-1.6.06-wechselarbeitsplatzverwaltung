/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { formatDate } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { interval, timer } from 'rxjs';
import { StoreService } from './shared/store-service.service';

/**
 * This component is the base for the app.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  /**title of the app */
  title = 'RUMBA - Arbeitsplatzplanung meets New Work';
  /**if the user logged in has admin rights */
  userHasAdminRights = false;
  
  /**
   * This constructor sets some attributes for the storeService.
   * @param ss Service which contains the most recent common data (created by dependency injection)
   * @param router Router for navigation
   * @param keycloakService Keycloakservice
   */
  constructor(public ss:StoreService, private router: Router, private keycloakService: KeycloakService, private route: ActivatedRoute) { 
    this.route.paramMap.subscribe(params => {
      
      if(params.get("terminal")){
        this.ss.terminal = true;
      }
    });
    if(this.ss.getNumberFromDate(this.ss.selectedDate) != this.ss.getNumberFromDate(new Date())){
      this.ss.time = new Date();
      let timeAsString = this.ss.time.toLocaleString('de-DE', { hour: 'numeric', minute:'numeric', hour12: false })
      if(this.ss.switchMorningAfternoonTime > timeAsString){
        this.ss.showOnlyMorningBookings = true;
        this.ss.showOnlyAfternoonBookings = false;
      }
      else{
        this.ss.showOnlyMorningBookings = false;
        this.ss.showOnlyAfternoonBookings = true;
      }
    }


  }

  /**this function is empty */
  test(){
    
  }

  /**
   * This function sets some attributs on the storeService and potentially reloads the window.
   */
  ngOnInit(): void {
    let oldMorningOnlyShowing = this.ss.showOnlyMorningBookings
      this.ss.switchMorningAfternoonSubscription = interval(60000).subscribe(x => {
        this.ss.time = new Date();
        let timeAsString = this.ss.time.toLocaleString('de-DE', { hour: 'numeric', minute:'numeric', hour12: false })

        if(this.ss.switchMorningAfternoonTime > timeAsString){
          oldMorningOnlyShowing = this.ss.showOnlyMorningBookings
          this.ss.showOnlyMorningBookings = true;
          this.ss.showOnlyAfternoonBookings = false
        }
        else{
          oldMorningOnlyShowing = this.ss.showOnlyMorningBookings
          this.ss.showOnlyMorningBookings = false;
          this.ss.showOnlyAfternoonBookings = true;
        }
        
        if(oldMorningOnlyShowing != this.ss.showOnlyMorningBookings){
          this.ss.switchMorningAfternoonSubscription.unsubscribe();
          window.location.reload()
        }
        // console.log("showoldmorning: " + oldMorningOnlyShowing + " this.ss.showonlymorningbookins:" + this.ss.showOnlyMorningBookings )

      })
  

  }

  /**
   * The function navigates the website to the bookingpage of today.
   */
  onLoginClick(){
    this.router.navigate(['bookings/roompage/today']);
  }

  /**
   * The function logs the user out.
   */
  async onLogoutClick(){
    this.ss.onLogout();

  }

  /**
   * The function navigates the website to the publiclandingpage of the local department.
   */
  onLogoClicked(){
    if(this.ss.localdepartment){
      if(this.ss.localuser){
        this.router.navigate(['bookings/roompage/today'])
      }else{
        this.router.navigate(['public/'+this.ss.localdepartment.name])
      }
      
    }else{
      if(this.ss.terminal){
        this.router.navigate(['public/terminal/terminal/'])
      }else{
        this.router.navigate(['public/'])
      }
    }
  }


  
}
