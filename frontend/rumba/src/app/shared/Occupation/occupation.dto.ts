/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This interface is used to store data for workspace bookings
 */
export interface occupationDTO{   
    /**id of the booking */
    booking_id: number;
    /**day which the booking is for */
    booked_for_day: number;
    /**id of the workspace */
    workspace_id: number;
    /**name of the workspace */
    workspace_name: string;
    /**id of the employee */
    employee_id: number;
    /**forename of the employee */
    forename: string;
    /**lastname of the employee */
    lastname: string;
    /**orga unit of the employee */
    orga_unit: string;
    /**email of the employee */
    email: string;
    /**if the employee is a designated first aid helper */
    first_aid: boolean;
    /**booking comment (optional "") */
    comment: string;
    /**if the booking is for the morning */
    morning: boolean;
    /**if the booking is for the afternoon */
    afternoon: boolean;
    /**x coordinate for point 1 of the workspace on the map */
    workspace_x: number,
    /**y coordinate for point 1 of the workspace on the map */
    workspace_y: number,
    /**scale of the workspace */
    workspace_scale: number;
    /**rotation of the workspace on the map */
    workspace_rotation: number;
    /**if the workspace is bookable when the assigned employee is in homeoffice or absence */
    workspace_bookableWhenHO: boolean;
    /**id of the map where the workspace of the occupation is */
    workspace_mapID: number;
    /**if the employee of the occupations wants to be anonymous on the public page */
    employee_anonymous: number;
}