/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StoreService } from '../store-service.service';
import { WorkspacestoreService } from '../Workspace/workspacestore.service';
import { occupationDTO } from './occupation.dto';

/**
 * This component provides the recent occupations data from the backend.
 */
@Injectable({
  providedIn: 'root'
})
export class OccupationService {

  /**base url for the REST requests */
  api = this.storeService.getBaseURL()+'occupation/';

  /**
   * This constructor intilizes the component with the parameters below.
   * @param storeService Service which contains the most recent common data (created by dependency injection)
   * @param http HttpClient for REST requests
   * @param workspaceService Service which contains the most recent workspace data (created by dependency injection)
   */
  constructor(private storeService: StoreService, private http: HttpClient, private workspaceService: WorkspacestoreService) { }

  /**
   * The function returns all occupations for the specified date and department.
   * @param day Date
   * @param department string, department name
   * @returns occupationDTO[]
   */
  getOccupationsForDay(day: Date, department: string){
    const dayAsNumber = this.storeService.getNumberFromDate(day);
    return this.http.get<occupationDTO[]>(this.api + dayAsNumber + "/" + department);
  }

  /**
   * The function returns all occupations for the specified date, department and map.
   * @param day Date
   * @param department string, department name
   * @param map_id number, id of the specified map
   * @returns occupationDTO[]
   */
  getOccupationsForDayOnMap(day: Date, department: string, map_id: number){
    const dayAsNumber = this.storeService.getNumberFromDate(day);
    return this.http.get<occupationDTO[]>(this.api + "formap/" + dayAsNumber + "/" + department + "/" + map_id);
  }

  /**
   * The function returns all occupations for the specified time period and workspace.
   * @param day Date, start date of the period
   * @param daysCount number, how many days long the time period is
   * @param workspace_id number, id of the workspace
   * @returns occupationDTO[]
   */
  getOccupationsForWorkspacePeriod(day: Date, daysCount: number, workspace_id: number):Observable<occupationDTO[]>{
    const dayAsNumber = this.storeService.getNumberFromDate(day);
    return this.http.get<occupationDTO[]>(this.api + dayAsNumber + "/" + daysCount + "/" + workspace_id);
  }

  /**
   * The function returns all occupations for the specified workspace.
   * @param workspace_id number, id of the workspace
   * @returns occupationDTO[]
   */
  getOccupationsForWorkspace(workspace_id: number):Observable<occupationDTO[]>{
    return this.http.get<occupationDTO[]>(this.api + "forWorkspace/" + workspace_id);
  }
}
