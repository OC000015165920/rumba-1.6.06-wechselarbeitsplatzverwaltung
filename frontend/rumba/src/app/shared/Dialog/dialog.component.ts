/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import { Component, Inject, OnInit } from '@angular/core';

/**
 * This component is a base for all confirmation dialogs.
 */
@Component({
    selector: 'app-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss']
})

export class DialogComponent implements OnInit {

    /**title of the dialog */
    title:string;
    /**description of the dialog */
    description:string;
    /**if there should be a way to close the dialog without confirmation */
    showCancelButton: boolean = true;

    /**
   * The constructor initilizes the attributes desription, title and showCancelButton with the corresponding data.
   * @param dialogRef MatDialogRef<DialogComponent> to reference an open or closed dialog
   * @param data ConformDialogModel @Inject(MAT_DIALOG-DATA)
   */
    constructor(
        private dialogRef: MatDialogRef<DialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data:ConfirmDialogModel ) {
        
        this.description = data.description;
        this.title = data.title;
        if (data.showCancelButton != undefined) {
            this.showCancelButton = data.showCancelButton;
        }
        
    }

    /**
   * The function is empty.
   */
    ngOnInit() {
        
    }

    /**
   * The function closes the dialog with the result true.
   */
    save() {
        this.dialogRef.close(true);
    }

    /**
   * The function closes the dialog without results.
   */
    close() {
        this.dialogRef.close(false);
    }
}

/**This class is used as an interface for storing data of a Confirmdialog (title, description, showCancelButton) */
export class ConfirmDialogModel {

    /**
     * This constructor initilizes the class with the parameters below
     * @param title string, title of the dialog
     * @param description string, description of the dialog
     * @param showCancelButton boolean, if the dialog can be canceled
     */
    constructor(public title: string, public description: string,
        public showCancelButton?: boolean) {
    }
  }