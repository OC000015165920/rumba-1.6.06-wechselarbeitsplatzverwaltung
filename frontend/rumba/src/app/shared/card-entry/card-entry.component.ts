import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { absenceDTO } from '../absence/absence.dto';
import { BookingsStoreService } from '../bookings/bookings-store.service';
import { ConfirmDialogModel, DialogComponent } from '../Dialog/dialog.component';
import { homeofficeDTO } from '../Homeoffice/homeoffice.dto';
import { StoreService } from '../store-service.service';
import { UserStoreService } from '../User/user-store.service';

/**
 * This component represents homeoffice and absence entries
 */
@Component({
  selector: 'app-card-entry',
  templateUrl: './card-entry.component.html',
  styleUrls: ['./card-entry.component.scss']
})
export class CardEntryComponent implements OnInit {
  /**if the card is on public page */
  @Input() isPublic?: boolean;
  /**get the homeoffice bookings for this card as input */
  @Input() homeofficeData?: homeofficeDTO;
  /**get the absence bookings for this day as input */
  @Input() absenceData?: absenceDTO;
  /**if the user of this homeoffice booking was searched for */
  @Input() searchedHomeoffice?: boolean;  
  /**if the user of this absence booking was searched for */
  @Input() searchedAbsence?: boolean;
  /**the comment from the booking if it exists */
  tooltipText: string ="";
  /**if entry is by logged in user */
  user: boolean=false;

  
  /**emits event delete_click if the delete cross is clicked */
  @Output() delete_click = new EventEmitter<number>();

  /**
   * This constructor initilizes the parameters below
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   */
  constructor(private storeService: StoreService, private dialog: MatDialog, private bs: BookingsStoreService) {
    if((storeService.localuser.employee_id==this.homeofficeData?.employee_id)||(storeService.localuser.employee_id==this.absenceData?.employee_id)){
      this.user=true;
    }
  }

  /**
   * The function sets the tooltipText to the comment in the input data and adds the user class to the entry if the employee of the card is the localuser.
   */
  ngOnInit(): void {
    if(this.homeofficeData){
      this.tooltipText = this.homeofficeData.comment;
      if((this.storeService.localuser.employee_id==this.homeofficeData?.employee_id)){
        this.user=true;
      }
    }

    if(this.absenceData){
      this.tooltipText = this.absenceData.comment;
      if((this.storeService.localuser.employee_id==this.absenceData?.employee_id)){
        this.user=true;
      }
    }

  }

  /**
   * The function checks if a comment image should be shown because there is a comment.
   * @returns boolean, true if there is a comment
   */
  showCommentImage(): boolean{
    let returnValue = false;
    if(this.homeofficeData){
      if(this.homeofficeData.comment != ""){
        returnValue = true;
      }
    }
    if(this.absenceData){
      if(this.absenceData.comment != ""){
        returnValue = true;
      }
    }
    return returnValue
  }

  /**
   * The function returns the name of the booking (homeoffice or absence).
   * @returns string, name in format forename lastname
   */
  showName():string{
    let name ="";
    if(this.homeofficeData){
      name = this.homeofficeData.forename  + " " + this.homeofficeData.lastname
    }
    if(this.absenceData){
      name = this.absenceData.forename  + " " + this.absenceData.lastname
    }

    if(this.isPublic){
      if(this.homeofficeData){
        if(this.homeofficeData.anonymous){
          name = "N. N.";
        }
      }
      if(this.absenceData){
        if(this.absenceData.anonymous){
          name = "N. N.";
        }
      }
    }

    return name;
  }

  /**
   * The function checks if the card is for the localuser
   */
   isOwnCard(){
    if(this.homeofficeData){
      if(this.homeofficeData.employee_id==this.storeService.localuser.employee_id){
        return true;
      }
    }
    if(this.absenceData){
      if(this.absenceData.employee_id==this.storeService.localuser.employee_id){
        return true;
      }
    }
    return false;
  }

  /**
   * The function opens a dialog to delete the booking the infocard represents
   */
   openDeleteDialog() {
    const dialogConfig = new MatDialogConfig();
    //content of Dialog
    const message = 'Soll die Buchung wirklich gelöscht werden?';
    const dialogData = new ConfirmDialogModel('Bestätigung', message);

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = dialogData;
    
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    this.storeService.isDialogOpen=true;

    dialogRef.afterClosed().subscribe(result => {
      
      this.storeService.isDialogOpen=false;
      if (result) {
        //actual deletion function
        let temp: number|undefined;
        if(this.homeofficeData){
          temp = this.homeofficeData.booking_id;
        }
        if(this.absenceData){
          temp = this.absenceData.booking_id;
        }
        if(temp){
          this.deleteBooking(temp);
        }
      }
    }); 
  }   

  /**
   * The function deletes the booking the infocard represents
   */
   deleteBooking(booking_id: number){
    console.log(`deleteBooking called for booking ID ${booking_id}`);
     this.bs.deleteBooking(booking_id).subscribe( result =>{

     this.delete_click.emit(booking_id);
     });
   }
}
