import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StoreService } from '../store-service.service';
import { absenceDTO } from './absence.dto';

/**
 * This class provides the most recent absence data
 */
@Injectable({
  providedIn: 'root'
})
export class AbsenceStoreService {

  /**base url for REST requests */
  api = this.storeService.getBaseURL() + 'absence/';

  /**
   * This constructor initilizes the class with the parameters below
   * @param http HttpClient for REST request
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   */
  constructor(private http:HttpClient, private storeService: StoreService) { }

  /**
   * The function gets all absence bookings for the specified date and department.
   * @param day Date
   * @param department string, name of the department
   * @returns absenceDTO[]
   */
  getAbsenceForDate(day: Date, department: string):Observable<absenceDTO[]>{
    return this.http.get<absenceDTO[]>(this.api + this.storeService.getNumberFromDate(day) + "/" + department);
  }
}
