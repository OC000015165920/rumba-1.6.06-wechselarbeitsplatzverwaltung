import { TestBed } from '@angular/core/testing';

import { AbsenceStoreService } from './absence-store.service';

describe('AbsenceStoreService', () => {
  let service: AbsenceStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AbsenceStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
