import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { mapperDepartmentOrgaunit } from './mapper_department_orgaunit.dto';
import { StoreService } from '../store-service.service';
import { departmentDTO, updateDepartmentDTO } from './Department.dto';

/**
 * This component provides the recent department data from the backend.
 */
@Injectable({
  providedIn: 'root'
})
export class DepartmentstoreService {

  /**url for REST requests */
  url = this.ss.getBaseURL() + "departments/";

  /**
   * This constructor initilizes the component with the parameters below
   * @param ss StoreService Service which contains the most recent common data (created by dependency injection)
   * @param http HttpClient for REST requests
   */
  constructor(public ss: StoreService, private http: HttpClient) { }

  // getAllDepartments():Observable<departmentDTO[]>{
  //   return this.http.get<departmentDTO[]>(this.url);
  // }

  /**
   * The function returns the department as a departmentDTO.
   * @param name string
   * @returns Promis<departmentDTO>
   */
  async getDepartmentByName(name: string):Promise<departmentDTO>{
    return await this.http.get<departmentDTO>(this.url + "byname/"+ name).toPromise();
  }

  /**
   * The function returns the department specified by the roles as a departmentDTO.
   * @param roles string[]
   * @returns Promis<departmentDTO>
   */
  async getDepartmentByRoles(roles: string[]):Promise<departmentDTO>{
    return await this.http.post<departmentDTO>(this.url+"departmentByRole", roles).toPromise();
  }

  getAllDepartments(){
    return this.http.get<departmentDTO[]>(this.url + "getAll");
  }

  /**
   * This function updates the department settings.
   * @param department_id number, the id of the department to update
   * @param updatedDepartment updateDepartmentDTO, the data on which to update
   * @returns Observable<departmentDTO>
   */
  updateDepartment(department_id: number, updatedDepartment: updateDepartmentDTO){
    return this.http.patch<departmentDTO>(this.url + department_id, updatedDepartment);
  }
}
