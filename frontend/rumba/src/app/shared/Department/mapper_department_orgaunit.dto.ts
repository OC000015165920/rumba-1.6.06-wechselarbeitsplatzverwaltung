/**
 * This interface maps an orgaunit to a department
 */
export interface mapperDepartmentOrgaunit{
    /**the mapping id */
    id: number;
    /**the department name */
    department: string;
    /**the orgaunit name */
    orgaunit: string;
}