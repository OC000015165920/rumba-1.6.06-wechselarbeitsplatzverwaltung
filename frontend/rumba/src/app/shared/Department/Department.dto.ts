// export class departmentDTO{
//     name: string ="";
//     description: string="";
//     BookingInAdvanceDays: string="";
//     NamePatternOnMap: string="";
//     FloorplanName: string="";
// }
import {Map} from 'src/app/shared/map/Map'
/**
 * This interface represents a department
 */
export interface departmentDTO{
    /**id of the department */
    id: number;
    /**name of the department */
    name: string;
    /**description of the department */
    description: string;
    /**how many day in advance booking is allowed in the department */
    BookingInAdvanceDays: number;
    /**in which name pattern the names are shown in the map */
    NamePatternOnMap: string;
    /**the maps used by the department */
    maps: Map[];
    /**if the infocards should be rotated like the workspaces */
    RotateInfocards:boolean;
}

/**
 * This interface is used to save data for updating the department
 */
 export interface updateDepartmentDTO{
    /**name of the department */
    name: string;
    /**description of the department */
    description: string;
    /**in which name pattern the names are shown in the map */
    NamePatternOnMap: string;
    /**if the infocards should be rotated like the workspaces */
    RotateInfocards: boolean;
}
