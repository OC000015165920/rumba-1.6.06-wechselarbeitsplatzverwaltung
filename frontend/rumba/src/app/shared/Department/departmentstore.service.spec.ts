import { TestBed } from '@angular/core/testing';

import { DepartmentstoreService } from './departmentstore.service';

describe('DepartmentstoreService', () => {
  let service: DepartmentstoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DepartmentstoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
