import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogbookingOnlyhalfdayComponent } from './dialogbooking-onlyhalfday.component';

describe('DialogbookingOnlyhalfdayComponent', () => {
  let component: DialogbookingOnlyhalfdayComponent;
  let fixture: ComponentFixture<DialogbookingOnlyhalfdayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogbookingOnlyhalfdayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogbookingOnlyhalfdayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
