import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { dialogbookingresult } from '../dialogbooking/dialogbookingresult';

/**
 * This component handles booking dialogs for halfday bookings
 */
@Component({
  selector: 'app-dialogbooking-onlyhalfday',
  templateUrl: './dialogbooking-onlyhalfday.component.html',
  styleUrls: ['./dialogbooking-onlyhalfday.component.scss']
})
export class DialogbookingOnlyhalfdayComponent implements OnInit {

  /**title of the dialog */
  title:string;
  /**desription of the dialog */
  description:string;
  /**is not used */
  halfday: boolean = false;
  /**if morning is selected to be booked */
  morning: boolean = false;
  /**if afternoon is selcted to be booked */
  afternoon: boolean = false;
  /**optional note to attach to a booking */
  note: string ="";

  /**is not used */
  selected: string ="";

  /**
   * The constructor initilizes the attributes desription, title and showCancelButton with the corresponding data, sets halfday to true if exatly one of morning or afternoon is already booked in data and sets morning/afternoon to !data.morning/afternoonAlreadyBooked.
   * @param dialogRef MatDialogRef<DialogbookingOnlyHalfdayComponent> to reference an open or closed dialog
   * @param data ConformDialogModelOnlyHalfday @Inject(MAT_DIALOG-DATA)
   */
  constructor(
      private dialogRef: MatDialogRef<DialogbookingOnlyhalfdayComponent>,
      @Inject(MAT_DIALOG_DATA) public data:ConfirmDialogModelOnlyHalfday ) {
      
      this.description = data.description;
      this.title = data.title;
      if((data.morningAlreadyBooked && !data.afternoonAlreadyBooked) || (!data.morningAlreadyBooked && data.afternoonAlreadyBooked) ){
        this.halfday =true;
      }
      this.morning = !data.morningAlreadyBooked
      this.afternoon = !data.afternoonAlreadyBooked
  }

  /**
   * The function is empty.
   */
  ngOnInit() {
      
  }

  /**
   * The function closes the dialog with the results morning, afternoon and note.
   */
  save() {
      //creating a object which will be returned
      let result: dialogbookingresult ={
        morning: this.morning,
        afternoon: this.afternoon,
        note: this.note
      }
      this.dialogRef.close(result);
  }

  /**
   * The function closes the dialog without results.
   */
  close() {
      this.dialogRef.close();
  }

  /**
   * This function checks which button is selected and sets the morning and afternoon variables
   * @param event is the value of the selected radio-button
   */
  changeRadio(event: string ){
    if(event === "nachmittags"){
      this.morning = false;
      this.afternoon = true;
    }
    else if(event ==="vormittags"){
      this.morning = true;
      this.afternoon = false;
    }

    // console.log("vormittags: " + this.morning +" nachmittags:  " + this.afternoon)
  }
}

/**This class is used as an interface for storing data of a Confirmdialog for half days (title, description, morningAlreadyBooked, afternoonAlreadyBooked) */
export class ConfirmDialogModelOnlyHalfday {

  /**
   * This constructor initilizes the class with the parameters below
   * @param title string, title of the dialog
   * @param description string, description of the dialog
   * @param morningAlreadyBooked boolean, if the morning was already booked
   * @param afternoonAlreadyBooked boolean, if the afternoon was already booked
   */
  constructor(public title: string, public description: string, public morningAlreadyBooked: boolean, public afternoonAlreadyBooked: boolean) {
  }
}
