/**This interface is used to store if morning and/or afternoon is booked */
export interface AlreadyBooked{
    /**if morning is booked */
    morning: boolean,
    /**if afternoon is booked */
    afternoon: boolean
}