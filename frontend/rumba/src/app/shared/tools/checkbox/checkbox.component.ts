import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

/**
 * This component is an enhanced checkbox.
 */
@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {

  /**get the box color as input */
  @Input() boxColor?: string;
  /**get the checked color as input */
  @Input() checkedColor?: string;

  /**get if the checkbox is checked as input */
  @Input() checked?: boolean;
  /**get if the workspace the checkbox represents is already booked at the date and halfday the checkbox represents as input */
  @Input() alreadyBooked?:boolean;

  /**get if the checkbox is disabled as input */
  @Input() disabled = false;

  /**emits event change when checkbox is checked or unchecked */
  @Output() change = new EventEmitter<boolean>();
  /**a message why the checkbox is disabled */
  tooltipText = "";

  /**
   * This constructor is empty.
   */
  constructor() { }

  /**
   * This function sets tooltipText when initiating the component. "Du hast eine Buchung" when disabled. "Der Platz ist bereits belegt" when already booked. 
  */
  ngOnInit(): void {
    if(this.disabled){
      this.tooltipText = "Du hast bereits eine Buchung"
    }
    else if(this.alreadyBooked){
      this.tooltipText = "Der Platz ist bereits belegt";

    }
  }

  /**
   * The function flips the checked attributs value and emits a change event with the checked value if alreadyBooked and disabled are both false.
   */
  onChange(){
    if(!this.alreadyBooked && !this.disabled){
      this.checked = !this.checked
      this.change.emit(this.checked)
    }


 
  }

  /**
   * The function checks if the checkbox is selectable by returning false if either already booked or disabled is true and true if both are false.
   * @returns boolean, true if checkbox is selectable
   */
  selectable():boolean{
    let returnValue = true;
    if(this.alreadyBooked || this.disabled){
      returnValue = false;
    }
    return returnValue
      
  }

  /**
   * The function returns 'gray' if disabled is true, if not then it returns the boxColor
   * @returns string, boxColor if not disabled else 'gray'
   */
  boxColorOrDisabled(): string{
    
    if(this.boxColor){
      if(this.disabled){
      return 'gray'
        }
        else{
          return this.boxColor
        }

  }
  return ''
  }

  /**
   * The function returns 'gray' if disabled is true, if not then it returns the checkedColor
   * @returns string, checkedColor if not disabled else 'gray'
   */
  checkedColorOrDisabled():string{
    if(this.checkedColor){
      if(this.disabled){
        return 'gray'
      }
      else{
        return this.checkedColor
      }
    }
    return ''
  }
}
