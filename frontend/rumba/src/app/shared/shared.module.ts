/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapComponent } from './map/map.component';
import { AngularMaterialModule } from '../angular-material.module';
import { DialogbookingComponent } from './dialogbooking/dialogbooking.component';
import { InfocardComponent } from './map/infocard/infocard.component';
import { DialogbookingOnlyhalfdayComponent } from './dialogbooking-onlyhalfday/dialogbooking-onlyhalfday.component';
import { StateComponent } from './state/state.component';
import { BookingTableComponent } from './booking-table/booking-table.component';
import { DialogbookingWithMultiDayComponent } from './dialogbooking-with-multi-day/dialogbooking-with-multi-day.component';
import { StateSelectHalfdayComponent } from './dialogbooking-with-multi-day/state-select-halfday/state-select-halfday.component';
import { CheckboxComponent } from './tools/checkbox/checkbox.component';
import { HomeofficeStoreService } from './Homeoffice/homeoffice-store.service';
import { CardEntryComponent } from './card-entry/card-entry.component';
import { SearchComponent } from './search/search.component';
import { ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [MapComponent, DialogbookingComponent, InfocardComponent, DialogbookingOnlyhalfdayComponent, StateComponent, BookingTableComponent,
     DialogbookingWithMultiDayComponent, StateSelectHalfdayComponent,CheckboxComponent, CardEntryComponent, SearchComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ],
  exports:[MapComponent, BookingTableComponent,CheckboxComponent,StateComponent, CardEntryComponent, SearchComponent]

})
export class SharedModule { }
