import {Component, Input, Output, OnInit, EventEmitter, OnChanges} from '@angular/core';
import {UntypedFormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { AbsenceStoreService } from '../absence/absence-store.service';
import { HomeofficeStoreService } from '../Homeoffice/homeoffice-store.service';
import { OccupationService } from '../Occupation/occupation.service';
import { User } from '../User/user';
import { UserStoreService } from '../User/user-store.service';

/**This interface is used as user object with minimal information */
export interface tempUser{
  /**id of the user */
  employee_id: number;
  /**forename of the user */
  forename: string;
  /**lastname of the user */
  lastname: string;
}

/**
 * This component is used to search for an employee.
 */
@Component({
  selector: 'app-search',
  templateUrl: 'search.component.html',
  styleUrls: ['search.component.scss'],
})
export class SearchComponent implements OnInit, OnChanges {
  /**if search is on public page */
  @Input() isPublic?: boolean;
  /**the date on which search is used */
  @Input() date?: Date;
  /**the department in which search is used */
  @Input() department?: string;
  /**if it should refresh the options */
  @Input() change?: boolean=false;
  /**FormControl for the input field */
  myControl = new UntypedFormControl('');
  /**the users which can be found */
  options: tempUser[]=[];
  /**the filtered options */
  filteredOptions?: Observable<string[]>;
  /**event emitter for onClick when a user is clicked */
  @Output() onClick = new EventEmitter<tempUser>();

  /**
   * This constructor initializes the parameters below: 
   * @param occupationService  OccupationService
   * @param homeOfficeStoreService HomeofficeStoreService
   * @param absenceStoreService AbsenceStoreService
   * @param userStoreService UserStorService
   */
  constructor(private occupationService: OccupationService, private homeOfficeStoreService: HomeofficeStoreService, private absenceStoreService: AbsenceStoreService, private userStoreService: UserStoreService){
    
  }

  /**
   * This functions is empty.
   */
  ngOnInit() {
    /*
    if(this.date&&this.department){
      
      if(this.date&&this.department){
        //gets the employees from occupations
        await this.occupationService.getOccupationsForDay(this.date, this.department).subscribe(result =>{
          for(let i of result){
            if(!i.employee_anonymous||!this.isPublic){//not anonymous users on publicpage
              this.options.push({employee_id: i.employee_id, forename: i.forename, lastname: i.lastname});
            }
          }
        });
        //gets the empoyees from homeoffice
        await this.homeOfficeStoreService.getHomeofficeForDay(this.date, this.department).subscribe(result =>{
          for(let i of result){
            if(!i.anonymous||!this.isPublic){//not anonymous users on publicpage
              this.options.push({employee_id: i.employee_id, forename: i.forename, lastname: i.lastname});
            }
          }
        });
        //gets the employees from absence  
        await this.absenceStoreService.getAbsenceForDate(this.date, this.department).subscribe(result =>{
          console.log(result);
          for(let i of result){
            if(!i.anonymous||!this.isPublic){//not anonymous users on publicpage
              this.options.push({employee_id: i.employee_id, forename: i.forename, lastname: i.lastname});
            }
          }
        });
        console.log(this.options);

        //applies the filter
        this.filteredOptions = await this.myControl.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value || '')),
        );
      } 
    
    }*/
    
    
  }

  /**
   * This function is triggered when changes are made to the input data and updates the options and filter.
   */
  ngOnChanges(){
    this.options=[]
    if(this.date&&this.department){
      
      if(this.date&&this.department){
        //gets the employees from occupations
        this.occupationService.getOccupationsForDay(this.date, this.department).subscribe(result =>{
          for(let i of result){
            if(!i.employee_anonymous||!this.isPublic){//not anonymous users on publicpage
              this.options.push({employee_id: i.employee_id, forename: i.forename, lastname: i.lastname});
            }
          }
        });
        //gets the empoyees from homeoffice
        this.homeOfficeStoreService.getHomeofficeForDay(this.date, this.department).subscribe(result =>{
          for(let i of result){
            if(!i.anonymous||!this.isPublic){//not anonymous users on publicpage
              this.options.push({employee_id: i.employee_id, forename: i.forename, lastname: i.lastname});
            }
          }
        });
        //gets the employees from absence  
        this.absenceStoreService.getAbsenceForDate(this.date, this.department).subscribe(result =>{
          for(let i of result){
            if(!i.anonymous||!this.isPublic){//not anonymous users on publicpage
              this.options.push({employee_id: i.employee_id, forename: i.forename, lastname: i.lastname});
            }
          }
        });

        //applies the filter
        this.filteredOptions = this.myControl.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value || '')),
        );
      } 
    
    }
    
    
  }

  /**
   * This function filters the options and returns all options where the value matches a part of the forename + lastname of the Users.
   * @param value string, the value to match
   * @returns 
   */
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    if(this.options){
      let temp: string[] = [];

      for(let i of this.options){
        temp.push(i.forename + " " + i.lastname)
      }
      return temp.filter(option => option.toLowerCase().includes(filterValue));
    }else{
      return [];
    }
  }

  /**
   * This functions emits an onClick event with the User object of the user which the name was selected.
   * @param event any, the selected name
   */
  onSelect(event: any){
    console.log(event)
    this.onClick.emit(this.getUserFromName(event));
  }

  /**
   * This funtions gets the User object for the the name.
   * @param name string, name of the User
   * @returns User, user to get
   */
  getUserFromName(name: string):tempUser{
    for(let i of this.options){
      if(i.forename + " " + i.lastname==name){
        console.log(i)
        return i;
      }
    }
    return this.options[0];
  }
}