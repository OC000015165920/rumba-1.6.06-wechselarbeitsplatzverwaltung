/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StoreService } from '../store-service.service';
import { createUserDTO } from './createUserDTO';
import { User } from './user';
import { Map } from '../map/Map'
import { UpdateEmployeeDto } from './updateEmployeeDTO';
import { optionDTO } from './optionDTO';

/**
 * This injectable handles the recent user data
 */
@Injectable({
  providedIn: 'root'
})
export class UserStoreService {

  /**  
   * pk, 2021/09/01: centralize API definition in StoreService */
  api = this.ss.getBaseURL() + 'employees';
  /**api but for options */
  optionsapi = this.ss.getBaseURL() + "options"; 

  /**
   * This constructor initilizes the class with the parameters below
   * @param http HttpClient for REST requests
   * @param ss Service which contains the most recent common data (created by dependency injection)
   */
  constructor(public http:HttpClient, public ss:StoreService) { 
  }

  /**
   * The function returns all users of the specified department.
   * @param department string, department name
   * @returns User[]
   */
  getall(department: string):Observable<User[]> {
    return this.http.get<User[]>(this.api + "/"+department);
  }

  /**
   * This function returns the User with the specified ID.
   * @param id number, Id of the User to get
   * @returns Observable<User>
   */
  getOneByID(id: number): Observable<User>{
    return this.http.get<User>(this.api+'/byID/'+id);
  }

  // getOneByUsername(username: string): Observable<User>{
  //   return this.http.get<User>(this.api+'/me/'+ username);
  // }

  /**
   * The function either returns the user if it is found in the database or creates the user in the database and then returns the user.
   * @param user createUserDTO, user to find/create
   * @returns Promise<User>
   */
  async findOrCreateUser(user: createUserDTO):Promise<User>{
    return await this.http.post<User>(this.ss.getBaseURL() + 'manage-employees' + '/GetOrCreateUser', user).toPromise();
  }

  /**
   * The function creates the user in the database and then returns the user.
   * @param newUser createUserDTO, user to create
   * @returns Observable<User>
   */
  createNewUser(newUser: createUserDTO):Observable<User>{
    return this.http.post<User>(this.api,newUser);
  }

  /**
   * This function updates user data.
   * @param user_id number, id of the user to be updated
   * @param updateEmployeeDto UpdateEmployeeDto, the new data
   * @returns Observable<User>, observable of the updated user
   */
  updateUser(user_id:number, updateEmployeeDto: UpdateEmployeeDto):Observable<User>{
    console.log(this.api + "/" + user_id);
    return this.http.patch<User>(this.api + "/" + user_id, updateEmployeeDto); 
  }

  /**
   * This function updates user data but as an admin.
   * @param user_id number, id of the user to be updated
   * @param updateEmployeeDto UpdateEmployeeDto, the new data
   * @returns Observable<User>, observable of the updated user
   */
  updateUserAsAdmin(user_id:number, updateEmployeeDto: UpdateEmployeeDto):Observable<User>{
    return this.http.patch<User>(this.api + "/admin/" + user_id, updateEmployeeDto); 
  }
  
  /**
   * This functions returns all available options.
   * @returns optionsDTO[], allOptions for the user to possibly have
   */
  allOptions(){
    return this.http.get<optionDTO[]>(this.optionsapi);
  }

  /**
   * This function returns all options the user already has.
   * @param employee_id number, id of the employee
   * @returns optionDTO[], all options of the user
   */
  allOptionsForEmployee(employee_id: number){
    return this.http.get<optionDTO[]>(this.optionsapi + "/" + employee_id);
  }

  /**
   * This functions adds an new option to the employee
   * @param option {options_id: number, employee_id: number}, information for the option
   * @returns Observable
   */
  addOptionForEmployeeAsAdmin(option: any):Observable<optionDTO>{
    return this.http.post<optionDTO>(this.optionsapi + "/admin/giveEmployeeOption", option);
  }

  /**
   * This function deletes an option of the user.
   * @param employee_id number, id of the employee
   * @param id number, id of the option
   * @returns Observable
   */
  deleteOptionForEmployeeAsAdmin(employee_id: number, id: number){
    return this.http.delete<optionDTO[]>(this.optionsapi + "/admin/removeOptionFromEmployee/" + employee_id + "/" + id);
  }

  /**
   * This functions adds an new option to the employee
   * @param option {options_id: number, employee_id: number}, information for the option
   * @returns Observable
   */
   addOptionForEmployee(option: any):Observable<optionDTO>{
    return this.http.post<optionDTO>(this.optionsapi + "/giveEmployeeOption", option);
  }

  /**
   * This function deletes an option of the user.
   * @param employee_id number, id of the employee
   * @param id number, id of the option
   * @returns Observable
   */
  deleteOptionForEmployee(employee_id: number, id: number){
    return this.http.delete<optionDTO[]>(this.optionsapi + "/removeOptionFromEmployee/" + employee_id + "/" + id);
  }

  /**
   * This function deletes an employee.
   * @param employee_id number, id of the employee
   * @returns Observable
   */
  deleteEmployee(employee_id: number){
    return this.http.delete<User>(this.ss.getBaseURL() + 'manage-employees' + "/" + employee_id);
  }
}
