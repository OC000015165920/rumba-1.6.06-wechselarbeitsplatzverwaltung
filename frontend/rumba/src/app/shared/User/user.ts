/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This interface is used to store user data
 */
export interface User {
  /**id of the employee */
  employee_id: number,
  /**username of the employee */
  username: string,
  /**forename of the employee */
  forename: string,
  /**lastname of the employee */
  lastname: string,
  /**orga unit of the employee */
  orga_unit: string,
  /**email address of the employee */
  email_address: string,
  /**if the user is a designated first aid helper*/
  first_aid: boolean,
  /**keycloak id of the employee */
  keycloak_id: string,
  /**id of the map selected first for the user */
  default_mapID: number,
  /**if the users name should apear on public page */
  anonymous: boolean,
}
