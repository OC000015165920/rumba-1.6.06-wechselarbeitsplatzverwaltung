import { User } from './user';

 /**
 * This Class is a Container for all important Information for / from the currently logged in User.
 * With this class we do not need to have multiple spreaded variables.
 */
export class LocalUser implements User {
    /**forename of the localuser */
    forename: string ="";
    /**lastname of the localuser */
    lastname: string="";
    /**a2-kennung of the localuser */
    username: string="";
    /**department of the localuser */
    department: string="";
    /**orgaunit of the localuser */
    orga_unit: string="";
    /**if the user is a designated first aid helper*/
    first_aid: boolean=false;
    /**if localuser is logged in */
    loggedIn: boolean = false;
    /**role of the localuser */
    role: string ="";
    /**employeeID of the localuser */
    employee_id: number =0;
    /**token of the localuser */
    token: string="";
    /**email of the localuser */
    email_address: string ="";
    /**keycloak_id of the localuser */
    keycloak_id: string ="";
    /**if localuser has a booking in the morning */
    morningBooked: boolean = false;
    /**if localuser has a booking in the afternoon */
    afternoonBooked: boolean = false;
    /**if the users name should apear on public page */
    anonymous: boolean = false;
    /**default map id of the user */
    default_mapID: number = 0;
}