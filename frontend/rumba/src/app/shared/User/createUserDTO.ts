/**
 * This interface is used for creating users
 */
export interface createUserDTO{
    /**username of the user */
    username: string;
    /**forename of the user*/
    forename: string;
    /**lastname of the user */
    lastname: string;
    /**department of the user*/
    department: string;
    /**email of the user*/
    email_address: string;
    /**if the user is a designated first aid helper*/
    first_aid: boolean;
    /**keycloak id of the user*/
    keycloak_id: string;
}