/**
 * This interface is used to store options of user roles data.
 */
export interface optionDTO{
    /**id of the option */
    options_id:number;
    /**name of the option */
    name:string;
}