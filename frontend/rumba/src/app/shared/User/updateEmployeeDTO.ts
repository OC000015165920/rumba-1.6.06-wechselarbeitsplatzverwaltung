/**
 * This interface is used to store User data to send it to the backend to update the User data there.
 */
export interface UpdateEmployeeDto {
    /**forename of the user */
    forename: string;
    /**lastname of the user */
    lastname: string;
    /**department of the user */
    department: string;
    /**default map of the user */
    default_mapID: number;
    /**email address of the user */
    email_address: string;
    /**if the user is a designated first aid helper*/
}
