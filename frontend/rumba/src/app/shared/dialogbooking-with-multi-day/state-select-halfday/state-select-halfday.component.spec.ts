import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StateSelectHalfdayComponent } from './state-select-halfday.component';

describe('StateSelectHalfdayComponent', () => {
  let component: StateSelectHalfdayComponent;
  let fixture: ComponentFixture<StateSelectHalfdayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StateSelectHalfdayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StateSelectHalfdayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
