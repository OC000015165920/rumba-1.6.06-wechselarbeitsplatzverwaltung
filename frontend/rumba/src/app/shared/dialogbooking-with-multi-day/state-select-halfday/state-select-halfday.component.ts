import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Booking } from '../../bookings/booking';
import { StoreService } from '../../store-service.service';
import { TableObject } from '../dialogbooking-with-multi-day.component';

/**
 * This component is a special checkbox to represent the halfdays (morning and afternoon) in multi day booking
 */
@Component({
  selector: 'app-state-select-halfday',
  templateUrl: './state-select-halfday.component.html',
  styleUrls: ['./state-select-halfday.component.scss']
})
export class StateSelectHalfdayComponent implements OnInit {

  /**table object for this checkbox which contains information about the date and morning aud afternoon booking as input */
  @Input() to?: TableObject
  /**all user bookings as input */
  @Input() userBookings?: Booking[];
  /**the default checkbox color */
  checkboxBoxColor = "#74c071";
  /**the default color of the checking */
  checkboxCheckedColor = "#f16363"
  /**the color of the checkbox if it is disabled */
  checkboxdisabledColor = "#000000"
  /**the color of the checking if the checkbox is disabled */
  checkboxBoxDisabledColor ="#f16363"

  /**if the workspace is already booked in the morning */
  WorkspaceBookedAtMorning: boolean = false;
  /**if the workspace is already booked in the afternoon */
  WorkspaceBookedAtAfternoon: boolean = false
  
  
  /**if the user already has a booking in the morning */
  UserBookedAtMorning: boolean = false
  /**if the user already has a booking in the morning */
  UserBookedAtAfternoon: boolean = false

  /**emits event change on the checking or unchecking of the checkbox */
  @Output() change = new EventEmitter<TableObject>();

  /**
   * The constructor initilizes the component with the parameters below
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   */
  constructor(public storeService:StoreService) { }

  /**
   * This function initialises the component with the input data
   */
  ngOnInit(): void {
    if(this.to && this.userBookings){
      this.WorkspaceBookedAtMorning = this.to.morning;
      this.WorkspaceBookedAtAfternoon = this.to.afternoon

      for (const booking of this.userBookings) {
        if(booking.morning){
          this.UserBookedAtMorning = true;
        }
        if(booking.afternoon){
          this.UserBookedAtAfternoon = true;
        }
      }
    }

    if(this.userBookings){
      console.log("state-select - userbookings:" , this.userBookings)
    }
  }

  /**
   * This function updates the data for the selection of afternoon
   * @param boolean if checkbox afternoon is checked
   */
  onAfternoonChange(afternoonChecked: boolean){
    if(this.to){
      this.WorkspaceBookedAtAfternoon= afternoonChecked
      this.to.selected.afternoon = !this.to.selected.afternoon;
      this.onUpdate()
    }
    
  }

  /**
   * This function updates the data for the selection of morning
   * @param boolean if checkbox morning is checked
   */
  onMorningChange(morningChecked: boolean){
    if(this.to){
      this.WorkspaceBookedAtMorning = morningChecked
      this.to.selected.morning = !this.to.selected.morning;
      this.onUpdate()
    }
  

  }

  /**
   * This function updates the to and emits a change so that the dialogbookingwithmultidaycomponent can update as well
   */
  onUpdate(){
    if(this.to){


      var newTO = { ...this.to};  //SPREAD Operator to make a new object
      newTO.morning = this.WorkspaceBookedAtMorning;
      newTO.afternoon = this.WorkspaceBookedAtAfternoon;
      console.log("newTO",newTO)
      console.log("oldTO",this.to)
      this.change.emit(newTO)
    }

  }

}
