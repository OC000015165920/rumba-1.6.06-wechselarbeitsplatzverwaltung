import { Workspace } from "../Workspace/workspace";

/**This interface is used to store the results of a multi day booking dialog */
export interface dialogResultMultiDay{
    /**date of booking */
    date?: Date,
    /**if morning was booked */
    morning: boolean,
    /**if afternoon was booked */
    afternoon: boolean,
    /**comment of the booking (optional "") */
    note: string
}