import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { Booking } from '../bookings/booking';
import { BookingsStoreService } from '../bookings/bookings-store.service';
import { occupationDTO } from '../Occupation/occupation.dto';
import { StoreService } from '../store-service.service';
import { User } from '../User/user';
import { Workspace } from '../Workspace/workspace';
import { dialogResultMultiDay } from './dialogresult-multi-day';

/**
 * This interface is used for the table for multi day booking to represent a day each for the workspace
 */
export interface TableObject{
  /**id to differentiate them */
  id: number;
  /**the date to book if selected */
  date: Date
  /**if the workspace has a booking on that day in the morning */
  morning: boolean
  /**if the workspace has a booking on that day in the afternoon */
  afternoon: boolean
  /**the employee who has booked the workspace in the morning for this date (possibly undefined) */
  morning_employee?:string
  /**the employee who has booked the workspace in the afternoon for this date (possibly undefined) */
  afternoon_employee?: string
  /**selections in the table */
  selected: {
    morning: boolean;
    afternoon: boolean;
    selected: boolean;
  }
}

/**
 * This component handles dialogs with multiday bookings
 */
@Component({
  selector: 'app-dialogbooking-with-multi-day',
  templateUrl: './dialogbooking-with-multi-day.component.html',
  styleUrls: ['./dialogbooking-with-multi-day.component.scss']
})
export class DialogbookingWithMultiDayComponent implements OnInit {
  /**title of the dialog */
  title:string;
  /**description of the dialog */
  description:string;
  /**if halfday booking is checked*/
  halfday: boolean = false;
  /**if the user already has a booking for the morning of the selected day */
  userMorningAlreadyBooked: boolean = false;
  /**if the user already has a booking for the afternoon of the selected day */
  userAfternoonAlreadyBooked: boolean = false;
  /**if the workspace is already booked for the morning of the selected day */
  workspaceMorningAlreadyBooked: boolean = false;
  /**if the workspace is already booked for the afternoon of the selected day */
  workspaceAfternoonAlreadyBooked: boolean = false;

  /**if morning is to be booked*/
  bookForMorning = false;
  /**if afternoon is to be booked */
  bookForAfternoon = false;

  /**if radio button for morning/afternoon is on morning */
  checkRadioButtonMorning = false;
  /**if radio button for morning/afternoon is on afternoon */
  checkRadioButtonAfternoon = false;

  /**latest bookable date (28 days from today for now) */
  maxDate = this.storeService.addDaysOnDate(new Date(),28);
  /**selected date */
  selectedDate: Date;

  /**optional not for the booking */
  note: string ="";
  /**maximal char length for the note 255 */
  maxNoteChars = 255;

  /**is not used */
  selected: string ="";
  /**the workspace for which the booking dialog is */
  workspace?: Workspace

  /**if the dialog is set to multi day booking */
  multidayBooking: boolean = false;
  /**if the dialog is set to single day booking */
  singleDayBooking: boolean = true;
  /**if the halfday button should be disabled */
  disableHalfDayButtons = false;

  /**saves the occupations for the workspace */
  occupations?: occupationDTO[];
  /**columns to be displayed in the table for multidaybooking */
  displayedColumns: string[] = ['select','date','status'];
  /**the table data */
  tableObjects: TableObject[] =[];
  /**the table data as a MatTableSource */
  TableDataSource = new MatTableDataSource<TableObject>();

  /**saves all bookings from the user */
  userBookings?: Booking[];

  // pk, 2023/03/28, fix wrong behavior for admin booking by adding user context
  selectedUser?: User;

  /**
   * The constructor initilizes the attributes desription, title, occupations, workspace, userMorningBookedAlready, userAfternoonBookedAlready, selectedDate, workspaceMorningAlreadyBooked, workspaceAfternoonAlreadyBooked, halfday, disableHalfDayButtons, checkRadioButtonAfternoon, bookForMorning, bookForAfternoon with the corresponding data.
   * @param dialogRef MatDialogRef<DialogbookingMultidayComponent> to reference an open or closed dialog
   * @param data ConformDialogModelMultiday @Inject(MAT_DIALOG-DATA)
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   * @param bookingStoreService BookingsStoreService Service which contains the most recent bokking data (created by dependency injection)
   */
  constructor(
      private dialogRef: MatDialogRef<DialogbookingWithMultiDayComponent>,
      @Inject(MAT_DIALOG_DATA) public data:ConfirmDialogModelMultiDay, public storeService:StoreService, private bookingStoreService: BookingsStoreService ) {
      this.description = data.description;
      this.title = data.title;
      this.occupations = data.upCommingOccupations;
      this.workspace = data.workspace
      this.userMorningAlreadyBooked = data.morningBooked
      this.userAfternoonAlreadyBooked = data.afternoonBooked
      this.selectedDate = data.date;
      this.selectedUser = data.user;

      console.log(this.userMorningAlreadyBooked, this.userAfternoonAlreadyBooked);


      //Check if the workspace is already occupied at the selected date
      if(this.occupations && this.workspace){
        let thisOccupations = this.occupations.filter(x => x.booked_for_day == this.storeService.getNumberFromDate(this.storeService.selectedDate) &&
        x.workspace_id == this.workspace?.workspace_id );
        for (const occupation of thisOccupations) {
          if(occupation.morning){
            this.workspaceMorningAlreadyBooked = true;
          }
          if(occupation.afternoon){
            this.workspaceAfternoonAlreadyBooked = true;
          }
        }



      }
      if(this.forceHalfDayBooking()){
          this.halfday =true;
          this.disableHalfDayButtons = true;

          // RadioButtonAfternoon
          if(this.workspaceMorningAlreadyBooked){
            this.checkRadioButtonAfternoon = true;
            this.bookForAfternoon = true;
          }
          if(this.userMorningAlreadyBooked && !this.workspaceAfternoonAlreadyBooked){
            this.checkRadioButtonAfternoon = true;
            this.bookForAfternoon = true;
          }

          //RadioButtonMorning
          if(this.workspaceAfternoonAlreadyBooked){
            this.checkRadioButtonMorning = true;
            this.bookForMorning = true;
          }
          if(this.userAfternoonAlreadyBooked && !this.workspaceMorningAlreadyBooked){
            this.checkRadioButtonMorning = true;
            this.bookForMorning = true;
          }
        }
        else{
          this.checkRadioButtonMorning = true;
          this.bookForMorning = true;
      }
      this.loadTableData();
  }

  /**loads all user bookings */
  ngOnInit() {
      this.loadUserBookings();
  }

  /**
   * This function checks if there should be a Checkbox at the start of the table row for the TableObject
   * @param to is the value of the TableObject in the table for multiDayBooking
   * @returns boolean: true for a possible booking for the TableObject
   */
  showCheckbox(to: TableObject){
    let showCheckbox = true;

    if(this.userBookings){
      for (const booking of this.userBookings) {
        if(booking.booked_for_day == this.storeService.getNumberFromDate(to.date)){
          if(booking.morning && booking.afternoon){
            showCheckbox = false;
          }
        }
      }
    }

    if(to.morning && to.afternoon){
      showCheckbox = false;
    }

    return showCheckbox
  }

  /**
   * This function checks if a half day booking should be forced by checking if either the User has booked half the day already or the Workspace has been booked for half the day
   * @returns true if half day booking has to be forced
   */
  forceHalfDayBooking():boolean{
    let returnValue = false;
    //If the workspace is already booked today
    if(this.workspaceAfternoonAlreadyBooked || this.workspaceMorningAlreadyBooked){
      return true;
    }

    if(this.userMorningAlreadyBooked || this.userAfternoonAlreadyBooked){
      return true;
    }


    return returnValue
  }

  /**loads all user bookings to userBookings */
  async loadUserBookings(){
    // pk, 2023/03/28, bug fix für multi-booking in admin area: consider selectedUser from calling component instead of logged in user
    if (this.selectedUser) {
      // if the user is set from calling component, take this ID to read the bookings
      console.log("DialogBookingWithMultiDayComponent.loadUserBookings(): employee_id = ", this.selectedUser.employee_id);
      this.userBookings = await this.bookingStoreService.getBookingsForUserAsPromise(this.selectedUser.employee_id);
    }
    else
    // if the user is not set from calling component, take the logged-in user from storeService
    if(this.storeService.localdepartment){
      this.userBookings = await this.bookingStoreService.getBookingsForUserAsPromise(this.storeService.localuser.employee_id);
    }
  }

  /**
   * This function returns all bookings by the logged in user for the date which is put in
   * @param date the date for which you want the UserBookings
   * @returns Booking[], an array of bookings by the user for the specified date
   */
  getUserBookingsForDay(date: Date): Booking[]{
    if(this.userBookings){
      return this.userBookings.filter(x => x.booked_for_day == this.storeService.getNumberFromDate(date));
    }
    return []
  }

  /**
   * This function checks if saving is allowed by checking if it is either single day booking or there is something selected in multi day booking.
   * @returns boolean, true if saving is allowed
   */
  savingAllowed():boolean{
    let returnValue = false;
    if(this.singleDayBooking){
      returnValue = true;
    }
    else{
      for (const to of this.tableObjects) {

        if(to.selected.selected){ // Check if each at least 1 element is selected
          if(to.selected.morning){
            to.morning=true;
          }else{
            to.morning=false;
          }
          if(to.selected.afternoon){
            to.afternoon=true;
          }else{
            to.afternoon=false;
          }
          if(to.morning || to.afternoon){
            returnValue = true;
          }
          else if(!to.morning && !to.afternoon){
            returnValue = false;
          }

        }
      }
    }


    return returnValue
  }

  /**
   * This function saves the bookings of bookingdialog and makes the requests to the backend for the bookings
   */
  save() {
    if(this.savingAllowed()){
      //creating a object which will be returned
      let results: dialogResultMultiDay[] =[];
      //OneDay Booking:
      if(this.singleDayBooking){
        if(!this.halfday){
          this.bookForMorning = true;
          this.bookForAfternoon = true;
        }
        let result: dialogResultMultiDay ={
          morning: this.bookForMorning,
          afternoon: this.bookForAfternoon,
          note: this.note
        }
        results.push(result);
      }
      //Multi-Day Booking
      else if(this.multidayBooking){
        for (const to of this.tableObjects) {
          if(to.selected.selected){
            var result: dialogResultMultiDay = {
              date: to.date,
              morning: to.morning,
              afternoon: to.afternoon,
              note: this.note
            }
            results.push(result)
          }
        }
      }
      this.dialogRef.close(results);
    }
    else{
      this.storeService.openAlertDialog("Bitte mindestens einen Tag auswählen und Vormittag oder Nachmittag", "Fehler")
    }

  }

  /**
   * This function closes the Dialog
   */
  close() {
      this.dialogRef.close();
  }

  /**
   * This function converts a date to a String in the format 'dd.MM.yyyy'
   * @param date Date for which you want the test
   */
  getDateTextForTable(date: Date){
    let datePipe = new DatePipe('en-US');
    let newDateText = datePipe.transform(date, 'dd.MM.yyyy');
    if ( newDateText != null ) {
      const weekday = this.storeService.getWeekDayFromDate(date);
      return weekday+ ',den ' + newDateText
    }
    return ""
  }

  /**
   * This function checks which button is selected and sets the morning and afternoon variables
   * @param event is the value of the selected radio-button
   */
  changeRadio(event: string ){
    if(event === "nachmittags"){
      this.bookForMorning = false;
      this.bookForAfternoon = true;
    }
    else if(event ==="vormittags"){
      this.bookForMorning = true;
      this.bookForAfternoon = false;
    }
  }

  /**
   * This function checks which button is selected and sets the single and multi day booking variables
   * @param event is the value of the selected radio-button
   */
  changeRadioSingleOrMulti(event: string ){
    if(event === "single"){
      this.singleDayBooking = true;
      this.multidayBooking = false;
    }
    else if(event ==="multi"){
      this.singleDayBooking = false;
      this.multidayBooking = true;
    }
  }

  /**
   * This function is triggered by selecting/deselecting the checkbox at the start of the row and selects/deselects the checkboxes in the same row for morning and afternoon
   * @param to is the value of the TableObject in the table for multiDayBooking
   */
  onChangeSelection(to: TableObject){
    to.selected.selected = !to.selected.selected
    let userBookingsForDay = this.getUserBookingsForDay(to.date);
    if(to.selected.selected){
      to.selected.afternoon=true;
      to.selected.morning=true;
      for(let booking of userBookingsForDay){
        if(booking.morning){
          to.selected.morning=false;
        }
        if(booking.afternoon){
          to.selected.afternoon=false;
        }
      }
    }else{
      to.selected.afternoon=false;
      to.selected.morning=false;
    }
  }

  /**
   * This function loads the data for the multi day booking table. There are rows for all bookable working days in the next 7 days starting with the selected day.
   * The data includes if the workspace is booked (also emoloyee who booked) for any of those days or the user has bookings on that day as well as the date.
   */
  loadTableData(){
    this.TableDataSource = new MatTableDataSource<TableObject>();
    const DATELIST = this.getDayList(this.storeService.getNumberFromDate(this.selectedDate),7);
    let id =0;

    for (const date of DATELIST) {
      let morning = false;
      let afternoon = false;
      let selected = {morning: false, afternoon: false, selected: false};
      if(this.occupations){
        let tempOccupation = this.occupations.find(x => x.booked_for_day == date);
        if(tempOccupation){
          if(tempOccupation.morning)
            morning = tempOccupation.morning
          if(tempOccupation.afternoon)
              afternoon = tempOccupation.afternoon
        }
      }

      // if(date == this.storeService.getNumberFromDate(new Date())){ //if date == today
      //   selected = true;
      // }


      let to: TableObject = {
        date: this.storeService.getDateFromNumber(date),
        morning: morning,
        afternoon: afternoon,
        selected: selected,
        id: id
      }
      this.tableObjects.push(to);
      id++;
    }
    this.TableDataSource = new MatTableDataSource(this.tableObjects);

  }

  /**
   * This function returns an array with days that can be booked from the startday to the day which is days-1 after the start day.
   * @param startday the day from which on you want the bookable days
   * @param days how many days you want to get if all whould be bookable
   * @returns number[], dates as numbers
   */
  getDayList(startday: number, days: number):number[]{
    let datesAsNumber: number[] =[]
    let date = this.storeService.getDateFromNumber(startday)

    for (let i = 0; i < days; i++) {
        let newDate = this.storeService.addDaysOnDate(date,i)
        if(newDate.getDay() != 0 && newDate.getDay() != 6){
          if(newDate < this.maxDate){
            datesAsNumber.push(this.storeService.getNumberFromDate(newDate))
          }

        }
    }
    return datesAsNumber;
  }

  /**
   * This function is triggered by selecting/deselecting either a morning or afternoon checkbox and adjusts the table data acordingly
   * @param toFromChange is the value of the TableObject in the table for multiDayBooking where one the checkboxes morning or afternoon was changed
   */
  onCheckboxChange(toFromChange: TableObject){
      let temp = this.tableObjects.find(x => x.id == toFromChange.id)
      if(temp){
        if(this.userBookings){
          let userBooking = this.userBookings.filter(x => x.booked_for_day == this.storeService.getNumberFromDate(toFromChange.date));
          console.log(userBooking);
          for(const uBooking of userBooking){
            if(uBooking.morning && !toFromChange.selected.afternoon || uBooking.afternoon && !toFromChange.selected.morning){
              temp.selected.selected = false;
              toFromChange.selected.selected = false;
            }else{
              temp.selected.selected = true;
              toFromChange.selected.selected = true;
            }
          }
          if(userBooking.length>1){
            temp.selected.selected = false;
            toFromChange.selected.selected = false;
          }
          if(userBooking.length==0){
            if(!toFromChange.selected.morning && !toFromChange.selected.afternoon){
              temp.selected.selected = false;
              toFromChange.selected.selected = false;
            }
            else{
              temp.selected.selected = true;
              toFromChange.selected.selected = true;
            }
          }
        }else if(!toFromChange.selected.morning && !toFromChange.selected.afternoon){
          temp.selected.selected = false;
          toFromChange.selected.selected = false;
        }
        else{
          temp.selected.selected = true;
          toFromChange.selected.selected = true;
        }

        let index = this.tableObjects.indexOf(temp)
        this.tableObjects[index] = toFromChange;

      }
      console.log(this.tableObjects)
  }

  /**
   * This function checks if the buttons on single day booking should be disabled
   * @returns boolean, true if half a day is booked by the user on the selected day
   */
  disableSingleDayButtons(){
    if(this.userMorningAlreadyBooked || this.userAfternoonAlreadyBooked){
      this.halfday = true;
      return true;
    }
    return false;
  }

  /**
   * This function changes the note attribut if there is a comment input which does not exceed the char limit.
   * @param to is the value of the TableObject in the table for multiDayBooking
   */
  onCommentInput(value: string){
    if(value.length <= this.maxNoteChars){
      this.note = value
    }

  }

  /**
   * This function checks if it should check the radiobutton morning on the half day booking
   * @returns boolean, true if half day booking is forced and user has not booked for the morning yet
   */
  checkRadioButtonMorningOnHalfDay():boolean{
    let returnValue = false;
    if(this.forceHalfDayBooking()){
      if(this.userAfternoonAlreadyBooked && !this.userMorningAlreadyBooked){
        returnValue = true;
      }
      else if(!this.userMorningAlreadyBooked && !this.userMorningAlreadyBooked){
        returnValue = true;
      }
    }
    return returnValue
  }

  /**
   * This function checks if it should check the radiobutton afternoon on the half day booking
   * @returns boolean, true if half day booking is forced and user has not booked for the afternoon yet
   */
  checkRadioButtonAfternoonOnHalfDay():boolean{
    let returnValue = false;
    if(this.forceHalfDayBooking()){
      if(this.userMorningAlreadyBooked && !this.workspaceAfternoonAlreadyBooked){
        returnValue = true;
      }
      else if(!this.userAfternoonAlreadyBooked && !this.workspaceAfternoonAlreadyBooked){
        returnValue = true;
      }
    }
    return returnValue
  }

}

/**This class is used as an interface for storing data of a Confirmdialog for multi day booking(title, description, date, morningBooked, afternoonBooked, upCommingOccupations, workspace) */
export class ConfirmDialogModelMultiDay {

  /**
   * This constructor initilizes the class with the parameters below
   * @param title string, title of the dialog
   * @param description string, description of the dialog
   * @param date Date, start date of the multi days
   * @param morningBooked boolean, if in the morning of date the user already has a booking
   * @param afternoonBooked boolean, if in the afternoon of date the user already has a booking
   * @param upCommingOccupations occupationDTO[], list of upcomming occupations for the workspace
   * @param workspace Workspace, the workspace to be booked
   * @param user the user where the booking should be made for
   */
  constructor(public title: string, public description: string, public date:Date, public morningBooked: boolean, public afternoonBooked: boolean,
    public upCommingOccupations?: occupationDTO[], public workspace?: Workspace, public user?: User) {
  }
}
