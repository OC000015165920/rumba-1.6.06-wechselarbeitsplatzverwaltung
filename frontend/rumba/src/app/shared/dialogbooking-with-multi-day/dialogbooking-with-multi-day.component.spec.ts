import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogbookingWithMultiDayComponent } from './dialogbooking-with-multi-day.component';

describe('DialogbookingWithMultiDayComponent', () => {
  let component: DialogbookingWithMultiDayComponent;
  let fixture: ComponentFixture<DialogbookingWithMultiDayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogbookingWithMultiDayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogbookingWithMultiDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
