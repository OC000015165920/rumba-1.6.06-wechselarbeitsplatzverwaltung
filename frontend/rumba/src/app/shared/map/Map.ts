/**This interface is used to store map information */
export interface Map{
    /**id of the map */
    id:number;
    /**name of the map */
    name:string;
    /**filename of the image of the map */
    imageName:string;
}