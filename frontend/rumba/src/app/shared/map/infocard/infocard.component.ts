/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { BookingsStoreService } from '../../bookings/bookings-store.service';
import { departmentDTO } from '../../Department/Department.dto';
import { ConfirmDialogModel, DialogComponent } from '../../Dialog/dialog.component';
import { occupationDTO } from '../../Occupation/occupation.dto';
import { StoreService } from '../../store-service.service';
import { User } from '../../User/user';
import { UserStoreService } from '../../User/user-store.service';
import { Workspace } from '../../Workspace/workspace';

/**
 * This component represents a booking on the map.
 */
@Component({
  selector: 'app-infocard',
  templateUrl: './infocard.component.html',
  styleUrls: ['./infocard.component.scss']
})
export class InfocardComponent implements OnInit {
  /**rotation of the infocard */
  @Input() rotation?: number;
  /**if the infocard is in publiclandingpage */
  @Input() isPublic?:boolean;
  /**if the employee of the occupation was searched for */
  @Input() searched?: boolean;
  /**booking_id of the booking as input*/
  @Input() booking_id?: number;
  /**occupations as input*/
  @Input() occupations?: occupationDTO[];
  /**html offset to the left of the map */
  @Input() offsetLeft?: number;
  /**html offset to the top of the map */
  @Input() offsetTop?: number;
  /**Workspace of the infocard */
  @Input() workspace?: Workspace;
  /**ID of the employee the workspace of the infocard is reserved for */
  @Input() employee_id?: number;
  /**an object to see when a reserved workspace is bookable */
  @Input() freeReserved?: {workspace_id: number, morning: boolean, afternoon: boolean};
  /**the User object belonging to employee_id */
  employee?: User;
  /**the occupation of the booking */
  currentOccupation?:occupationDTO;
  /**department of the booking */
  department?: departmentDTO;
  /**department of the booking as input from the public page*/
  @Input() departmentFromPublicPage?: departmentDTO
  /**comment of the booking if it exists */
  tooltipText="";
  /**emits event infocard_click if the card is clicked */
  @Output() infocard_click = new EventEmitter<{workspace_id: number, halfday: string, occupation?:occupationDTO}>();
  /**emits event delete_click if the delete cross is clicked */
  @Output() delete_click = new EventEmitter<number>();

  /** INCREMENT for Infocard length for FIRST AID ICON */
  FIRST_AID_INCREMENT = 3

  /** INCREMENT for Infocard length for COMMENT ICON */
  COMMENT_INCREMENT = 3

  /** INCREMENT for Infocard length for DELETE ICON */
  DELETE_INCREMENT = 3

  /** Interval for rotation degree should be between 0 and 90 or 270 to 359 */
  SEMI_CIRCLE_DEGREE = 180
  QUARTER_CIRCLE_DEGREE = 89

  /**if the booking should be visible */
  showBooking = false;

  /**if morning for this date and workspace was booked by the booking represented here */
  morningBooked = false;
  /**if afternoon for this date and workspace was booked by the booking represented here */
  afternoonBooked = false;
  /**window width for positioning */
  @Input() windowWidth?: number;

  /**calculated x coordinate for html positioning */
  calculatedX = 0;

  /**calculated y coordinate for html positioning */
  calculatedY = 0;

  /**default image width */
  DEFAULT_IMAGE_WIDTH = 936;

  /**  */
  image_scale_factor = 1

  /**scale for style */
  scaleXY = "scale(1)"

  /** length of the displayed string  */
  displayedNameLengthOverExpected = 0;


  /**listens for the window to be resized */
  @HostListener('window:resize', ['$event'])
  /**adjusts the hitboxes and map when the window is resized */
  onResize(){
     this.adjustNameLengthOverExpected();
  }

  /**listens for the window to be resized */
  @HostListener('window:load', ['$event'])
  /**adjusts the hitboxes and map when the window is resized */
  onLoad(){
     this.adjustNameLengthOverExpected();
  }

  /**
   * This constructor initilizes the component with the parameters below.
   * @param storeService reference to the global service handling common objecs, e.g. the current user
   * @param dialog MatDialog for dialogs
   * @param bs BookingsStoreService which contains the most recent booking data (created by dependency injection)
   */
  constructor(public storeService:StoreService, private dialog: MatDialog, private bs: BookingsStoreService, private userstoreService: UserStoreService) {

  }

  /**
   * Initialization of the component
   */
  ngOnInit(): void {

    // pk, 2022/06/01: Attention: this is needed for public dashboard page - don't remove it!!!
    if(this.departmentFromPublicPage){
      this.department = this.departmentFromPublicPage
    }

    if(this.storeService.localdepartment){
      this.department = this.storeService.localdepartment
    }
    if(this.occupations){
      for (const occupation of this.occupations) {
        if(occupation.morning){
          this.morningBooked = true
        }
        if(occupation.afternoon){
          this.afternoonBooked = true
        }

        if((occupation.morning && this.storeService.showOnlyMorningBookings) || (occupation.afternoon && this.storeService.showOnlyAfternoonBookings)){
          this.showBooking = true;
          this.tooltipText =`Kommentar:\n`+ occupation.comment;
        }

        if((occupation.morning&&!occupation.afternoon)||(!occupation.morning&&occupation.afternoon)){
          if(occupation.morning&&this.storeService.showOnlyMorningBookings){
            this.currentOccupation = occupation;
          }else if(occupation.afternoon&&this.storeService.showOnlyAfternoonBookings){
            this.currentOccupation = occupation;
          }
        }else{
          this.currentOccupation = occupation;
        }
      }
    }
    // pk, 2023/03/21, correction for label rotation to make it readable in any case (input by Kreis Warendorf)
    // od, 2022/12/12: rotation
    if (this.rotation) {
      if (this.rotation > this.SEMI_CIRCLE_DEGREE) {
        this.rotation = this.rotation - this.SEMI_CIRCLE_DEGREE; //
      }
      if (this.rotation > this.QUARTER_CIRCLE_DEGREE) {
        this.rotation = this.rotation + this.SEMI_CIRCLE_DEGREE;
      }
    }
    if(this.employee_id){
      this.getEmployeeByID(this.employee_id)

    }


    this.adjustCoordinates();
    setTimeout(()=>{this.adjustNameLengthOverExpected()},100)
  }

  /**
   * This function adjusts the x-coordinate of the infocard on display by calculating (estimating) the length of the infocard by the NameLength.
   */
  adjustNameLengthOverExpected(){

    // pk, 2023/03/21, correct Infocard placement by calculating the label length
    if (this.currentOccupation && this.department) {
      // calculate the lenght of the label in characters for correction of the calculated x position of the label
      // (I don't know why this works quite good, as well as why it is needed to correct the Inforcard position)

      this.displayedNameLengthOverExpected = this.getDisplayedName(this.currentOccupation.forename, this.currentOccupation.lastname, this.department?.NamePatternOnMap).length
      if (this.currentOccupation.first_aid) {
        this.displayedNameLengthOverExpected += this.FIRST_AID_INCREMENT;
      }
      if (this.currentOccupation.comment) {
        this.displayedNameLengthOverExpected += this.COMMENT_INCREMENT;
      }
      if (!this.isPublic&&this.isOwnWorkspace()){
        this.displayedNameLengthOverExpected += this.DELETE_INCREMENT;
      }


      var temp = document.getElementById("currentOccupationEmployeeID" + this.currentOccupation.employee_id);

      if(temp){
        this.displayedNameLengthOverExpected = ((this.displayedNameLengthOverExpected * parseFloat(window.getComputedStyle(temp).fontSize) /1.9)-50)/2;
      }

    }else if(this.department && this.employee){
      var temp = document.getElementById("reservationEmployeeID" + this.employee.employee_id);

      if(temp?.classList.contains("MorningBookedAfternoonReserved") && this.currentOccupation){
        this.displayedNameLengthOverExpected = this.getDisplayedName(this.currentOccupation.forename, this.currentOccupation.lastname, this.department?.NamePatternOnMap).length
      }else if(temp?.classList.contains("MorningUserAfternoonReserved") && this.currentOccupation){
        this.displayedNameLengthOverExpected = this.getDisplayedName(this.currentOccupation.forename, this.currentOccupation.lastname, this.department?.NamePatternOnMap).length
      }else if(temp?.classList.contains("AfternoonBookedMorningReserved") && this.currentOccupation){
        this.displayedNameLengthOverExpected = this.getDisplayedName(this.currentOccupation.forename, this.currentOccupation.lastname, this.department?.NamePatternOnMap).length
      }else if(temp?.classList.contains("AfternoonUserMorningReserved") && this.currentOccupation){
        this.displayedNameLengthOverExpected = this.getDisplayedName(this.currentOccupation.forename, this.currentOccupation.lastname, this.department?.NamePatternOnMap).length
      }else{
        this.displayedNameLengthOverExpected = this.getDisplayedName(this.employee.forename, this.employee.lastname, this.department?.NamePatternOnMap).length
      }

      if (this.employee.first_aid) {
        this.displayedNameLengthOverExpected += this.FIRST_AID_INCREMENT;
      }
      if (!this.isPublic&&this.isOwnWorkspace()){
        this.displayedNameLengthOverExpected += this.DELETE_INCREMENT;
      }




      if(temp){
        this.displayedNameLengthOverExpected = ((this.displayedNameLengthOverExpected * parseFloat(window.getComputedStyle(temp).fontSize) /1.9)-50)/2;
      }
    }
  }

  /**
   * Checks whether a free time slot of the workspace and a free timeslot of the current user matches
   * @returns true, if the Infocard should be hoovable (cursor changes, clickable)
   */

  hovable(): boolean {
    let returnValue = false;

    if (((!this.storeService.localuser.morningBooked && !this.morningBooked) ||
        (!this.storeService.localuser.afternoonBooked && !this.afternoonBooked)) && !this.employee_id){
          returnValue = true;

    }
    if(this.freeReserved && this.occupations?.length==0){
      if(this.freeReserved.morning!=this.freeReserved.afternoon){
        returnValue = true;
      }
    }
    //console.log("hovable() returns "+returnValue);
    return returnValue;
  }

  /**
   * Implements the "onclick" action on the Infocard
   * The action is available only if there is a free slot for workspace combined with user's bookings for that day
   * @param occupation the reference to the occupation which is currently handled by the infocard
   */
  onClick(occupation?: occupationDTO){
      if(this.hovable()) {
        if(occupation){
          let string = "";
          if(occupation.morning){
            string = "morning"
          }else if(occupation.afternoon){
            string = "afternoon"
          }
          this.infocard_click.emit({workspace_id: occupation.workspace_id, halfday: string, occupation:occupation});
        }else if(this.freeReserved){
          let string = "";
          if(this.freeReserved.morning){
            string = "morning"
          }else if(this.freeReserved.afternoon){
            string = "afternoon"
          }
          this.infocard_click.emit({workspace_id: this.freeReserved?.workspace_id, halfday: string});
        }

      }
  }

   /**
   *
   * @param forename forename from a user
   * @param lastname lastname from a user
   * @returns This Function returns the shown Name on the Map as a string. It is formatted as the Pattern in the config.json
   * If there is no valid Pattern, the default Output is forename and lastname
   */
  getDisplayedName(forename: string, lastname: string, NamePatternForMap: string): string{
    if((this.occupations?.length==2)||(this.currentOccupation?.morning && this.storeService.showOnlyMorningBookings || this.currentOccupation?.afternoon && this.storeService.showOnlyAfternoonBookings) || this.employee){
      if(!this.currentOccupation?.employee_anonymous||(this.isPublic==false)){
        if(this.storeService.showOnlyMorningBookings && this.occupations){
          for(let occupation of this.occupations){
            if(occupation.morning){
              forename = occupation.forename;
              lastname = occupation.lastname;
            }
          }
        }
        if(this.storeService.showOnlyAfternoonBookings && this.occupations){
          for(let occupation of this.occupations){
            if(occupation.afternoon){
              forename = occupation.forename;
              lastname = occupation.lastname;
            }
          }
        }
        if(NamePatternForMap == "forename lastname"){
          return forename + " " + lastname;
        }
        else if(NamePatternForMap == "forename, lastname"){
          return forename + ", " + lastname;
        }
        // pk, 2021/12/16: new pattern added (forename without comma)
        else if(NamePatternForMap == "forename l."){
          return forename + " " + lastname.substr(0,1)+".";
        }
        else if(NamePatternForMap == "forename, l."){
          return forename + ", " + lastname.substr(0,1)+".";
        }
        else if(NamePatternForMap == "f. lastname"){
          return forename.substr(0,1) + ". " + lastname;
        }
        else if(NamePatternForMap == "lastname forename"){
          return lastname + " " + forename;
        }
        else if(NamePatternForMap == "lastname, forename"){
          return lastname + ", " + forename;
        }
        else if(NamePatternForMap == "lastname, f."){
          return lastname + ", " + forename.substr(0,1)+".";
        }else{
          return forename + " " + lastname;
        }

      }
      return "N. N.";
    }else{
      if(this.employee && this.occupations?.length==0){
        if(NamePatternForMap == "forename lastname"){
          return forename + " " + lastname;
        }
        else if(NamePatternForMap == "forename, lastname"){
          return forename + ", " + lastname;
        }
        // pk, 2021/12/16: new pattern added (forename without comma)
        else if(NamePatternForMap == "forename l."){
          return forename + " " + lastname.substr(0,1)+".";
        }
        else if(NamePatternForMap == "forename, l."){
          return forename + ", " + lastname.substr(0,1)+".";
        }
        else if(NamePatternForMap == "f. lastname"){
          return forename.substr(0,1) + ". " + lastname;
        }
        else if(NamePatternForMap == "lastname forename"){
          return lastname + " " + forename;
        }
        else if(NamePatternForMap == "lastname, forename"){
          return lastname + ", " + forename;
        }
        else if(NamePatternForMap == "lastname, f."){
          return lastname + ", " + forename.substr(0,1)+".";
        }else{
          return forename + " " + lastname;
        }
      }
      return "_____";
    }
  }

  /**
   * This function checks if there are 2 seperate bookings on the infocard.
   * @returns boolean, true if there are 2 seperate bookings on the infocard.
   */
  isTwoOccupations(){
    if(this.occupations){
      if(this.occupations.find((e) => { if(this.occupations){ return e.employee_id != this.occupations[0].employee_id}else{ return false}})){
        return true;
      }
    }
    return false;
  }

  /**
   * This function returns a string with the css classes for the infocard depending on the bookings it represents.
   * @returns string of the css classes
   */
  giveClass():string{
    let temp = "";
    if(this.employee_id && ((this.occupations && this.occupations.length==0)||this.freeReserved)){
      if(!this.freeReserved){
        temp = "ReservedFullDay"
      }else{
        if(this.freeReserved.afternoon){
          if(this.isOwnWorkspaceAfternoon()){
            if(this.storeService.showOnlyMorningBookings){
              temp = "MorningReservedAfternoonUser";
            }else{
              temp = "AfternoonUserMorningReserved";
            }
          }else if(this.afternoonBooked){
            if(this.storeService.showOnlyMorningBookings){
              temp = "MorningReservedAfternoonBooked";
            }else{
              temp = "AfternoonBookedMorningReserved";
            }
          }else{
            if(this.storeService.showOnlyMorningBookings){
              temp = "MorningReservedAfternoonFree";
            }else{
              temp = "AfternoonFreeMorningReserved";
            }
          }
        }else{
          if(this.isOwnWorkspaceMorning()){
            if(this.storeService.showOnlyMorningBookings){
              temp = "MorningUserAfternoonReserved";
            }else{
              temp = "AfternoonReservedMorningUser";
            }
          }else if(this.morningBooked){
            if(this.storeService.showOnlyMorningBookings){
              temp = "MorningBookedAfternoonReserved";
            }else{
              temp = "AfternoonReservedMorningBooked";
            }
          }else{
            if(this.storeService.showOnlyMorningBookings){
              temp = "MorningFreeAfternoonReserved";
            }else{
              temp = "AfternoonReservedMorningFree";
            }
          }
        }
      }

    }else{
      if(this.isOwnWorkspace()){
        if(this.isOwnWorkspaceMorning()){
          if(this.isOwnWorkspaceAfternoon()){
            temp = "ownWorkspace";
          }else if(this.isTwoOccupations()){
            if(this.storeService.showOnlyMorningBookings){
              temp = "MorningWithAfternoonBookedUserMorning"
            }else{
              temp = "AfternoonWithMorningBookedUserMorning"
            }
          }else{
            if(this.storeService.showOnlyMorningBookings){
              temp = "ownWorkspaceMorning"
            }else{
              temp = "ownWorkspaceMorningAfternoonView"
            }
          }
        }else{
          if(this.isTwoOccupations()){
            console.log("test")
            if(this.storeService.showOnlyMorningBookings){
              temp = "MorningWithAfternoonBookedUserAfternoon"
            }else{
              temp = "AfternoonWithMorningBookedUserAfternoon"
            }
          }else{
            if(this.storeService.showOnlyMorningBookings){
              temp = "ownWorkspaceAfternoonMorningView"
            }else{
              temp = "ownWorkspaceAfternoon"
            }
          }
        }
      }else{
        if(this.isTwoOccupations()){
          if(this.storeService.showOnlyMorningBookings){
            temp = "MorningWithAfternoonBooked"
          }else{
            temp = "AfternoonWithMorningBooked"
          }
        }else{
          if(this.afternoonBooked&&this.morningBooked){
            temp = "CompleteDayBooked"
          }else{
            if(this.afternoonBooked){
              if(this.storeService.showOnlyMorningBookings){
                temp = "OnlyAfternoonBookedMorningView"
              }else{
                temp = "OnlyAfternoonBooked"
              }

            }else if(this.morningBooked){
              if(this.storeService.showOnlyAfternoonBookings){
                temp = "OnlyMorningBookedAfternoonView"
              }else{
                temp = "OnlyMorningBooked"
              }

            }
          }

        }

      }
    }

    if(this.hovable()){
      temp+=" hovable";
    }
    if(this.searched){
      temp+= " searched"
    }
    return temp;
  }


  /**
   * The function checks if the occupation is for the localuser
   */
  isOwnWorkspace(){
    if(this.occupations){
      for(let occupation of this.occupations){
        if(occupation.employee_id==this.storeService.localuser.employee_id){
          return true;
        }
      }
    }

    return false;
  }

  /**
   * This function checks if the morning part of the infocard is by the loggedin user.
   * @returns boolean, true if the morning part of the infocard is by the loggedin user.
   */
  isOwnWorkspaceMorning(){
    if(this.occupations){
      for(let occupation of this.occupations){
        if(occupation.employee_id==this.storeService.localuser.employee_id && occupation.morning){
          return true;
        }
      }
    }

    return false;
  }

  /**
   * This function checks if the afternoon part of the infocard is by the loggedin user.
   * @returns boolean, true if the afternoon part of the infocard is by the loggedin user.
   */
  isOwnWorkspaceAfternoon(){
    if(this.occupations){
      for(let occupation of this.occupations){
        if(occupation.employee_id==this.storeService.localuser.employee_id && occupation.afternoon){
          return true;
        }
      }
    }
    return false;
  }

  /**
   * The function opens a dialog to delete the booking the infocard represents
   */
  openDeleteDialog() {
    const dialogConfig = new MatDialogConfig();
    //content of Dialog
    const message = 'Soll die Buchung wirklich gelöscht werden?';
    const dialogData = new ConfirmDialogModel('Bestätigung', message);

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = dialogData;

    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    this.storeService.isDialogOpen=true;

    dialogRef.afterClosed().subscribe(result => {

      this.storeService.isDialogOpen=false;
      if (result) {
        //actual deletion function
        let temp = this.occupations?.find((e) => { return e.employee_id == this.storeService.localuser.employee_id })
        if(temp){
          this.deleteBooking(temp.booking_id);
        }else if(this.booking_id){
          this.deleteBooking(this.booking_id);
        }
      }
    });
  }

  /**
   * The function deletes the booking the infocard represents
   */
  deleteBooking(booking_id: number){
    console.log(`deleteBooking called for booking ID ${booking_id}`);
     this.bs.deleteBooking(booking_id).subscribe( result =>{

     this.delete_click.emit(booking_id);
     });
   }

  /**adjusts the coordinates for the resized map */
  adjustCoordinates(){
    if(this.currentOccupation){
      this.windowWidth = document.getElementById("map")?.clientWidth;
      this.calculatedX = this.calculateNewCoordinates(this.currentOccupation.workspace_x);
      this.calculatedY = this.calculateNewCoordinates(this.currentOccupation.workspace_y);
    }else if(this.employee_id && this.workspace){
      this.windowWidth = document.getElementById("map")?.clientWidth;
      this.calculatedX = this.calculateNewCoordinates(this.workspace.x);
      this.calculatedY = this.calculateNewCoordinates(this.workspace.y);
    }
  }

  /**calculates the the new coordinate for resized map and adjusts the scale of the card */
  calculateNewCoordinates(coordinate: number){
    let returnValue = coordinate;
    if(this.windowWidth){
      this.image_scale_factor = this.windowWidth/this.DEFAULT_IMAGE_WIDTH;
      this.scaleXY = 'scale(' + this.image_scale_factor + ')';
      //console.log(this.scaleXY)
      returnValue = coordinate*this.image_scale_factor;
    }
    else {
      console.error("windowWidth not set!!")
    }
    return returnValue;
  }

  /**
   * This function sets the employee parameter by getting it from the backend by its ID
   * @param employee_id number, ID of the employee
   */
  getEmployeeByID(employee_id: number){
    this.userstoreService.getOneByID(employee_id).subscribe(result =>{
      this.employee = result;
    });
  }
}
