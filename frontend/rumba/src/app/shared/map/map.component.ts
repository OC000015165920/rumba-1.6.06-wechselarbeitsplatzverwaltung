/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Component, EventEmitter, HostListener, Input, OnInit, Output, OnChanges, SimpleChanges } from '@angular/core';
import { Workspace } from 'src/app/shared/Workspace/workspace';
import { departmentDTO } from '../Department/Department.dto';
import { occupationDTO } from '../Occupation/occupation.dto';
import { StoreService } from '../store-service.service';
import { Workspacecolor } from '../Workspace/WorkspacecolorDTOs';
import { WorkspacestoreService } from '../Workspace/workspacestore.service';
import { Map } from './Map'



/**This interface represents the hitbox of a workspace */
export interface Hitbox{
  /**x1 of the hitbox */
  x1:number;
  /**x2 of the hitbox */
  x2:number;
  /**x3 of the hitbox */
  x3:number;
  /**x4 of the hitbox */
  x4:number;
  /**y1 of the hitbox */
  y1:number;
  /**y2 of the hitbox */
  y2:number;
  /**y3 of the hitbox */
  y3:number;
  /**y4 of the hitbox */
  y4:number;
  /**the workspace of the hitbox */
  workspace: Workspace;
}

/**
 * This component handles the map
 */
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})

export class MapComponent implements OnInit, OnChanges {
  /**if map is used on public page */
  @Input() isPublic?: boolean;
  /**the searched for users occupation */
  @Input() searched?: occupationDTO;
  /**the filename of the floor */
  @Input() map?: Map;
  /**all occupations for the date (for this department) as input */
  @Input() mapOccupations?: occupationDTO[];
  /**all workspaces (for this department) as input */
  @Input() allWorkspaces?: Workspace[];
  //pk, 2022/05/25: no longer needed: @Input() AllowBooking = false;
  /**if the user has already has a booking for this date in the morning */
  @Input() morningBooked = false;
  /**if the user has already has a booking for this date in the afternoon */
  @Input() afternoonBooked = false;
  /**the department the map represents */
  @Input() department?: departmentDTO;
  /**the workspacecolors for the department */
  @Input() workspacecolors?: Workspacecolor[];
  /**the reserved workspaces which are free with morning/afternoon */
  @Input() freeReservedWorkspaces?: {workspace_id: number, morning: boolean, afternoon: boolean}[];

  /**emits event workspace_click when clicked on workspace that is displayed as empty */
  @Output() workspace_click = new EventEmitter<Workspace>();

  /**emits event halfday_booking_click when clicked on workspace that is displayed as a half day booking */
  @Output() halfday_booking_click = new EventEmitter<{workspace_id: number, halfday: string, occupation?:occupationDTO}>();
  /**emits event onDelete when clicked on the delete icon on the booking of the user */
  @Output() onDelete = new EventEmitter<number>();



  /**Path for the floor Image. Value is set right after checking the department Value */
  ImagePath?: string;
  /**is not used */
  DisplayedName ="";
  /**workspace hitboxes */
  hitboxes: Hitbox[]=[];
  /**adjusted map width */
  mapWidth = 936;
  /**scale of the map */
  mapScale = 1;
  /**offset left of the map in the html */
  mapOffsetLeft?: number;
  /**offset top of the map in the html */
  mapOffsetTop?: number;

  /**if the legend is before or after the map in the html */
  legendBeforeMap: boolean=true;

  /**listens for the window to be resized */
  @HostListener('window:resize', ['$event'])
  /**adjusts the hitboxes and map when the window is resized */
  onResize(){
    //console.log("onResize() called");
     this.adjustHitboxes();
    //console.log("onResize() done");
  }

  /**listens for the window to be reloaded */
  @HostListener('window:load', ['$event'])
  /**adjusts the hitboxes and map when the window is resized */
  onLoad(){
    //console.log("onLoad() called");
    setTimeout(()=> {

      this.refreshGrafics();
      /* pk, 20240527, just a try to fix the "jumping label" symptom */
    }, 100)
    //console.log("onLoad() done");
  }


  /**
   * This constructor initilizes the component with the parameters below.
   * @param storeService Service which contains the most recent common data (created by dependency injection)
   */
  constructor(public storeService: StoreService, private workspacestoreService: WorkspacestoreService) {


   }

  /**
   * The function gives the ImagePath by setting it to "assets/" + department.FloorplanName. With that the map should be loaded on the html.
   */
  ngOnInit(): void {
    //console.log("ngOnInit() called");
    //console.log("workspacecolors.length = ", this.workspacecolors?.length);
    if(this.department){
      // pk, 2024-04-30: no idea what the t= parameter is used for, but it causes too much network trafic
      this.ImagePath = "assets/" + this.map?.imageName ; //TODO+ "?t=" + new Date().getTime();
    }
    this.refreshGrafics();
    //console.log("ngOnInit() done");
  }

  /**
   * This function is triggered when an input is changed and refreshes the map with the new input.
   * @param changes is not used
   */
  ngOnChanges(changes: SimpleChanges): void{
    //console.log("ngOnChanges() called, changes=",changes);
    if(changes){
      this.ngOnInit();
    }
    //console.log("ngOnChanges() done");
  }

  /**
   * common routine to refresh/rescale the map view
   */
  refreshGrafics(): void {

    //console.log("refreshGrafics() called")
    this.getOffset();

    this.adjustHitboxes();

    //console.log("refreshGrafics() done")
  }


  /**adjusts the hitboxes for the workspaces when the map is resized */
  adjustHitboxes(){

    this.adjustMapSize(); //pk, 2023/03/23: this call is needed, otherwise the workplaces are not located correctly

    this.mapScale = this.mapWidth/936;
    this.getOffset();
    // TODO: the use of the next statement is unclear
    //this.adjustMapSize();
  }

  /**
   * The function emits the workspace_click event
   */
  onWorkspace_click(event: Workspace){
    this.workspace_click.emit(event);
    console.log(event.workspace_name + "wurde geklickt");
  }

  /**
   * The function emits the halfday_booking_click event
   */
  onInfocard_click(event: {workspace_id: number, halfday: string, occupation?:occupationDTO}){
    this.halfday_booking_click.emit(event)
  }

  /**
   * The function returns the occupatins for the specified workspace
   * @param workspace_id number for the Workspace
   * @returns occupationDTO[], all occupations for the Workspace
   */
  getOccupationsForWorkspace(workspace_id: number): occupationDTO[]{
    if(this.mapOccupations){
      let occupations = this.mapOccupations.filter(x => x.workspace_id == workspace_id)
      return occupations;

    }
    return [];
  }

  /**
   * The function emits the onDelete event
   */
  delete_clicked(event: number){
    if(event){
      this.onDelete.emit(event);
    }
  }

  /**
   * This function sets the variables for offset for the displayed workspaces to the offset of the map
   */
  getOffset(){
    if(document.getElementById('map')){
      let mapOffsetLeftOld = this.mapOffsetLeft;
      let mapOffsetTopOld = this.mapOffsetLeft;

      //get the offset of the map
      this.mapOffsetLeft = document.getElementById('map')?.offsetLeft;
      this.mapOffsetTop = document.getElementById('map')?.offsetTop;
      //asign value if no value is given
      if(!this.mapOffsetLeft){
        this.mapOffsetLeft=0;
      }
      if(!this.mapOffsetTop){
        this.mapOffsetTop=0;
      }
      //adjust the offset for the change of the workspaces scale
      //pk, 2023/03/14: can't see the reason why this is needed
      this.mapOffsetLeft -= (1-this.mapScale)*25;
      this.mapOffsetTop -= (1-this.mapScale)*12,5;

      if (mapOffsetLeftOld != this.mapOffsetLeft) {
        console.log("getOffset() mapOffsetLeft changed from ",mapOffsetLeftOld, ", to ", this.mapOffsetLeft);
      }
      if (mapOffsetTopOld != this.mapOffsetTop) {
        //console.log("getOffset() mapOffsetTop changed from ",mapOffsetTopOld, ", to ", this.mapOffsetTop);
      }
    }
  }

  /**adjusts the map size to the window/screen height/width */
  adjustMapSize(){
    //console.log("adjustMapSize() called");
    const rect = document.getElementById("map")?.getBoundingClientRect();
    const box = document.getElementById("box")?.getBoundingClientRect(); //the div with the map and the legend inside

    let clientHeight = document.documentElement.clientHeight;

    if(box&&rect&&clientHeight){

      //console.log("rect.y=", rect.y, ", box.bottom=", box?.bottom)
      //console.log("box.width=", box?.width)
      if (rect.y > 0) {
        console.log("clientHeight =", clientHeight, ", rect.y = ", rect.y);
        console.log("box.bottom = ", box?.bottom, ", box.width = ", box?.width);
        if(clientHeight-rect.y<box?.bottom&&clientHeight<box?.width){
          //if the browser window is smaller in height than the box is in height or width
          this.mapWidth = clientHeight - rect.y;
          this.legendBeforeMap=true;
          console.log("adjustMapSize() 1 : mapWidth = ",this.mapWidth, ", legendBeforeMap = ", this.legendBeforeMap);
        }else{

          if(clientHeight-rect.y<box.width){
            //if the browser windows height is bigger than the box but the browser windows height minus the height of all above the map
            this.legendBeforeMap = false; //in this case the legend will be put beneath the map
            //pk, 20240527, don't change the width to avoid "jumping labels"
            //this.mapWidth = clientHeight - rect.y;
            console.log("adjustMapSize() 2: mapWidth = ",this.mapWidth, ", legendBeforeMap = ", this.legendBeforeMap);
          }else{
            //if the browser windows height is big enough to fit the whole map, the map width is set to the boxes width
            //pk, 20240527, don't change the width to avoid "jumping labels"
            //this.mapWidth=box?.width;
          }
        }
      }
      else {
        console.log("ignore box adjustment, just scrolling")
      }

    }
    //console.log("adjustMapSize() done: mapWidth = ",this.mapWidth, ", legendBeforeMap = ", this.legendBeforeMap);
  }

  /**
   * This function checks if the occupation is the one with the searched for employee
   * @param occupation The occupation to be checked
   * @returns boolean if the occupation is the one with the searched for employee
   */
  searchedWorkspace(occupation: occupationDTO | Workspace): boolean{
    if(occupation.workspace_id==this.searched?.workspace_id){
      return true;
    }else{
      return false;
    }
  }

  /**
   * This function returns all equipment of a workspace as a string.
   * @param workspace Workspace
   * @returns string, the Eqipment as a string
   */
  getEquipmentAsString(workspace: Workspace):string{
    let temp="";
    for(let e of workspace.equipment){
      temp += e.name + " ";
    }
    return temp;
  }

  /**
   * This function checks if the specified reserved workspace is free at least for a halfday
   * @param workspace_id number, ID of the workspace to check
   * @returns {workspace_id: number, morning: boolean, afternoon: boolean}, an object with the workspace ID and parameters for morning and afternoon if they are free. Returns undefined if it is not free
   */
  checkReservedFree(workspace_id: number){

    return this.freeReservedWorkspaces?.find((e) => {
      return e.workspace_id == workspace_id;
    })
  }

  /**
   * This function checks if the specified reserved workspace is free for a halfday
   * @param workspace_id number, ID of the workspace to check
   * @returns boolean, true if exactly half a day is free, false if not;
   */
  checkReservedFreeHalfDay(workspace_id: number){
    let temp = this.checkReservedFree(workspace_id);
    if(temp){
      return temp.afternoon != temp.morning;
    }else{
      return false;
    }
  }

  /**
   * This function returns the workspace object for a given ID
   * @param workspace_id number, ID of the workspace to return
   * @returns the workspace, if found, or undefined
   */
  getWorkspaceFromID(workspace_id: number) {
    let workspace = undefined;

    if (this.allWorkspaces) {
      workspace = this.allWorkspaces.find((e) => {
        return e.workspace_id == workspace_id;
      })
    }
    //console.log("MapComponent.getWorkspaceFromID(", workspace_id, ") returns ", workspace)
    return workspace;
  }

  /**
   * This function returns the employee_ID for a given workspace ID
   * @param workspace_id number, ID of the workspace to return
   * @returns the ID of the employee, for which the desk is reserved, or undefined (if the workspace cannot be found or the desk can be booked freely)
   */
  getEmployeeIDFromWorkspaceID(workspace_id: number) {
    let workspace = undefined;
    let employee_ID = undefined;
    if (this.allWorkspaces) {
      workspace = this.allWorkspaces.find((e) => {
        return e.workspace_id == workspace_id;
      })
    }

    //console.log("MapComponent.getEmployeeIDFromWorkspaceID(", workspace_id, ") found ", workspace);

    if (workspace) {
      employee_ID = workspace.employee_id;
    }
    //console.log("MapComponent.getEmployeeIDFromWorkspaceID(", workspace_id, ") returns ", employee_ID);

    return employee_ID;
  }
}

