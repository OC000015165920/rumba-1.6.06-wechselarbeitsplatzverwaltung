/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ObservedValueOf } from 'rxjs';
import { StoreService } from '../store-service.service';
import { homeofficeDTO } from './homeoffice.dto';

/**
 * This class provides the most recent homeoffice data
 */
@Injectable({
  providedIn: 'root'
})
export class HomeofficeStoreService {

  /**base url for REST requests */
  api = this.storeService.getBaseURL() + 'homeoffice/';

  /**
   * This constructor initilizes the class with the parameters below
   * @param storeService Storeservice Service which contains the most recent common data (created by dependency injection)
   * @param http HttpClient for REST requests
   */
  constructor(private storeService: StoreService, private http:HttpClient) { }

  /**
   * The function returns all homeoffice bookings for the specified date and department
   * @param day Date
   * @param department string, department name
   * @returns homeofficeDTO[], all homeoffice bookings for the day
   */
  getHomeofficeForDay(day: Date, department: string): Observable<homeofficeDTO[]>{
    const dayAsNumber = this.storeService.getNumberFromDate(day);
    return this.http.get<homeofficeDTO[]>(this.api + dayAsNumber + "/"+department);
  }
}
