/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**This interface is used for storing data for homeoffice bookings */
export interface homeofficeDTO{
    /**id of the booking */
    booking_id: number;
    /**id of the workspace */
    workspace_id: number;
    /**day which the booking is for */
    booked_for_day: number;
    /**id of the employee */
    employee_id: number;
    /**name of the workspace */
    workspace_name: string;
    /**forename of the employee */
    forename: string;
    /**lastname of the employee */
    lastname: string;
    /**email of the employee */
    email: string;
    /**department of the employee and workspace */
    department: string;
    /**if the booking is for in the morning */
    morning: boolean;
    /**if the booking is for in the afternoon */
    afternoon: boolean;
    /**booking comment (optional "") */
    comment: string;
    /**if the employee wants to be anonymous in public page */
    anonymous: boolean;
}