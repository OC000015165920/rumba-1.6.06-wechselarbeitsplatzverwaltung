import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { KeycloakService } from 'keycloak-angular';
import { AppConfigTestingService } from 'src/app/config/app-config-test.service';
import { AppConfigService } from 'src/app/config/app-config.service';

import { HomeofficeStoreService } from './homeoffice-store.service';

describe('HomeofficeService', () => {
  let service: HomeofficeStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
          { provide: AppConfigService, useClass: AppConfigTestingService },
          { provide: KeycloakService, useValue: {} },
      ]
    });
    service = TestBed.inject(HomeofficeStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
