/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BookingsModule } from 'src/app/bookings/bookings.module';
import { ConfirmDialogModelOnlyHalfday } from '../dialogbooking-onlyhalfday/dialogbooking-onlyhalfday.component';
import { AlreadyBooked} from '../dialogbooking-onlyhalfday/dialogbookingresult_halfday';
import { occupationDTO } from '../Occupation/occupation.dto';
import { Room } from '../rooms/room';
import { RoomStoreService } from '../rooms/room-store.service';
import { StoreService } from '../store-service.service';
import { Workspace } from '../Workspace/workspace';
import { workspaceState } from '../Workspace/WorkspaceState';

/**This interface is used for the booking table to represent workspaces */
export interface TableObject{
  /**name of the workspace */
  workspaceName: string
  /**id of the workspace */
  workspace: Workspace
  /**name of the room */
  roomName: string
  /**name of the employee that has booked the workspace (possibly "") */
  occupationName: string
  /**if the booking is for the morning */
  morning: boolean
  /**if the booking is for the afternoon */
  afternoon: boolean
  /**the name of the employee who has booked in the morning */
  morning_employee:string
  /**the name of the employee who has booked in the afternoon */
  afternoon_employee: string
}

/**This workspace is used to store data for output for the onBooking event */
export interface WorkspaceWithBookingFromTable{
  /**how the workspace is booked (morning, afternoon) */
  alreadyBooked: AlreadyBooked,
  /**the workspace */
  workspace: Workspace
}

/**
 * This component handles booking on the booking table.
 */
@Component({
  selector: 'app-booking-table',
  templateUrl: './booking-table.component.html',
  styleUrls: ['./booking-table.component.scss']
})
export class BookingTableComponent implements OnInit {
  /**get all the occupations relevant to the user as an input */
  @Input() occupations?: occupationDTO[];
  /**get all workspaces (from the department) as an input*/
  @Input() allWorkspaces?: Workspace[];
  /**get if the user has booked for the morning already as an input */
  @Input() morningBooked = false;
  /**get if the user has booked for the afternoon already as an input */
  @Input() afternoonBooked = false;

  /** emits event onBooking with data of the workspace and if the user has already booked for morning and/or afternoon */
  @Output() onBooking = new EventEmitter<WorkspaceWithBookingFromTable>();

  //Table
  /**columns diplayed in the table */
  displayedColumns =['roomName', 'workspaceName','occupied','occupationName','buchen']
  /** data for the table */
  tableObjects: TableObject[] =[]
  /** sorted data for the table */
  sortedTableObjects: TableObject[];
  /** data for the table as MatTableSource */
  dataSource = new MatTableDataSource<TableObject>();
  /** data store for room information */
  rooms: Room[] = []

  /**
   * The constructor initilizes sortedTableObjects with tableObjects
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   */
  constructor(private storeService: StoreService, private roomStoreService:RoomStoreService) {
    this.sortedTableObjects = this.tableObjects.slice();
  }

  /**
   * The function loads the table.
   */
  ngOnInit(): void {
    // pk, 2022/08/25, fix bug to show the configured room name correctly
    if (this.storeService.localdepartment){
      this.rooms = [];
      this.roomStoreService.getall(this.storeService.localdepartment.name).subscribe( result => {
        this.rooms = result;
        //console.log("rooms = "+result)

        this.loadTable();
      });
    }
    //this.userBookingState()
  }

  /**
   * The function loads the table.
   */
  ngOnChanges(){
    this.loadTable();
    //this.userBookingState()
  }

  /**
   * The function loads the table.
   */
  loadTable(){
    //console.log("loadtable()");
    this.dataSource = new MatTableDataSource<TableObject>();
    this.tableObjects = []
    
    if(this.occupations && this.allWorkspaces) {
      for (const workspace of this.allWorkspaces) {
        let tempTableObject = {} as TableObject; // create empty tableObject
        //Set Values from Workspace
        tempTableObject.workspace = workspace;
        tempTableObject.workspaceName = workspace.workspace_name;
        // pk, 2022/08/25, fix bug if room name doesnot correspond to workspace name
        //tempTableObject.roomName = workspace.workspace_name.substr(0,workspace.workspace_name.length-2)
        for(const tempRoom of this.rooms ) {
          if (tempRoom.room_id == workspace.room_id) {
            tempTableObject.roomName = tempRoom.room_name;
            break;
          }
        }
        //Get the right Occupations for this workspace. the result is an array, because we can get 2 occupations for one workspace per day
        let occupations = this.occupations.filter(x => x.workspace_id == workspace.workspace_id)
        let morning = false;
        let morning_employee ="";
        let afternoon = false;
        let afternoon_employee ="";
        if(occupations){
          for (const oc of occupations) {
            if(oc.morning){
              morning = true;
              morning_employee = oc.forename.substr(0,1) + ". " + oc.lastname
            }
            if(oc.afternoon){
              afternoon = true;
              afternoon_employee = oc.forename.substr(0,1) + ". " + oc.lastname
            }
          }
          tempTableObject.morning = morning;
          tempTableObject.afternoon = afternoon;
          tempTableObject.morning_employee = morning_employee;
          tempTableObject.afternoon_employee = afternoon_employee;
        }
        this.tableObjects.push(tempTableObject)
      }
    }
    this.dataSource = new MatTableDataSource(this.tableObjects);
  
  }

  /*
  userBookingState(){
    
    if(this.occupations){
      for (const oc of this.occupations) {
        if(oc.employee_id == this.storeService.localuser.employeeID){
          if(oc.morning) {
            console.log("user has morning booking");
            this.morningBooked = true;
          }
          if(oc.afternoon){
            console.log("user has morning booking");
            this.afternoonBooked = true;
          }
        }
      }
    }
    // console.log("userBookingState() return, Booked=["+this.morningBooked+","+this.afternoonBooked+"]");
  }
    */

  /**
   * The function emits a onBooking event with data for morning and afternoon booked for the specified workspace.
   */
  bookingWorkspace(workspace: Workspace){

    let alreadyBooked: AlreadyBooked ={
      morning: this.morningBooked,
      afternoon: this.afternoonBooked
    }

    let result: WorkspaceWithBookingFromTable = {
      alreadyBooked: alreadyBooked,
      workspace: workspace
    }
    this.onBooking.emit(result);

  }

  /**
   * The function returns the name(s) of the empolyee(s) who have booked the tableObject.
   * @param id number, user id
   * @returns Observable<Booking>
   */
  showNames(to: TableObject){
    if(to.morning && to.afternoon){
      if(to.morning_employee == to.afternoon_employee)
        return to.morning_employee
      else
        return to.morning_employee +" | " + to.afternoon_employee
    }
    else if(to.morning && !to.afternoon)
      return to.morning_employee
    else if(!to.morning && to.afternoon)
      return to.afternoon_employee
    return ""
  }

  /**
   * The function checks if a booking button should be shown by checking if a booking is possible for the to.
   * @param to TableObject
   * @returns boolean
   */
  showBookingButton(to:TableObject){
    let result = false;

    //Afternoon Booking
    if(!this.morningBooked && this.afternoonBooked){
      //console.log("showBookingButton(): afternoon booking by user exists");
      if(!to.morning ){
        //console.log("showBookingButton(): free slot for room in the morning");
        result = true;
      }
    }
    //Morning Booking
    else if(this.morningBooked && !this.afternoonBooked){
      if( !to.afternoon){
        //console.log("showBookingButton(): free slot for room in the afternoon");
        result = true;
      }
    }
    //All-Day Booking
    else if(this.morningBooked && this.afternoonBooked){
      //console.log("showBookingButton(): whole day booking by user");
      result = false;
    }
    //No Booking
    else if(!this.morningBooked && !this.afternoonBooked){
      //console.log("showBookingButton(): no booking by user");
      if((!to.morning || !to.afternoon)){
        //console.log("showBookingButton(): free slot for room");
        result = true;
      }
    }
    //console.log("showBookingButton(): returns "+result);
    return result;
  }

  /**
   * The function sorts the table by given sort method
   * @param sort Sort, sort method
   */
  sortData(sort: Sort) {
    const isAsc = sort.direction === 'asc';
    if(sort.active == "roomName"){
      if(isAsc){
        this.tableObjects.sort(x => {return x.roomName ? 1:-1})
        console.log(this.tableObjects)
      }
      else{
        this.tableObjects.sort(x => {return x.roomName ? -1:1})
      }
    }
    else if(sort.active == "workspaceName"){
      if(isAsc){
        this.tableObjects.sort(x => {return x.workspaceName ? 1:-1})
        console.log(this.tableObjects)
      }
      else{
        this.tableObjects.sort(x => {return x.workspaceName ? -1:1})
      }
    }
    //Error "cannot read property of undefined" when there is no occupation
    // else if(sort.active == "occupationName"){
    //   if(isAsc){
    //     this.tableObjects.sort(x => {return x.occupationName.substr(3, x.occupationName.length -3) ? 1:-1})
    //   }
    //   else{
    //     this.tableObjects.sort(x => {return x.occupationName.substr(3, x.occupationName.length -3) ? -1:1})
    //   }
    // }




    this.dataSource = new MatTableDataSource(this.tableObjects);
  }

  /**
   * The function checks if a is smaller or higher than b depending on the isAsc value. The function is applyable with strings and numbers 
   * @param a number | string
   * @param b number | string
   * @param isAsc boolean
   * @returns boolean, true if a<b && isAsc == true or b<a && isAsc == false
   */
  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}
