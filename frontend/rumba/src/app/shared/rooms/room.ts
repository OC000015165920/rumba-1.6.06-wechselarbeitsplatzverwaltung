/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This interface is used to store room data
 */
export interface Room {
  /**name of the room */
  room_name: string,
  /**id of the room */
  room_id: number,
  /**orga unit of the room */
  orga_unit: string;
  /**desription of the room  */
  description: string;
  /**if the room is reserved */
  active: boolean;
  /**till when the room is reserved */
  dateOfExpiry: Date;
  /**when the reservation starts */
  validFrom: Date;
}
