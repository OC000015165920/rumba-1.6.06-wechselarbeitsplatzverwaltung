/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Room } from './room'
import { Observable } from 'rxjs';
import { StoreService } from '../store-service.service';

/**
 * This injectable provides the recent data for rooms
 */
@Injectable({
  providedIn: 'root'
})
export class RoomStoreService {

  /** pk, 2021/09/01: centralize API definition in StoreService */
  api = this.ss.getBaseURL() + 'rooms';

  /**
   * This constructor initilizes the class with the parameters below
   * @param http HttpClient for REST requests
   * @param ss Service which contains the most recent common data (created by dependency injection)
   */
  constructor(public http:HttpClient, public ss:StoreService) { 
  }

  /**
   * The function returns all rooms for the specified department
   * @param department string, department name
   * @returns Room[]
   */
  getall(department: string):Observable<Room[]> {
    return this.http.get<Room[]>(this.api + "/" + department);
  }

  // getOneByID(id:number):Observable<Room>{
  //   return this.http.get<Room>(this.api+ '/'+ id);
  // }

  // getFreeRoomsForDay(day: number): Observable<Room[]>{
  //   return this.http.get<Room[]>(this.api + '/free/'+day);
  // }
  // getBookedRoomsForDay(day: number): Observable<Room[]>{
  //   console.log(this.api + '/booked/'+day);
  //   return this.http.get<Room[]>(this.api + '/booked/'+day);
  // }

}
