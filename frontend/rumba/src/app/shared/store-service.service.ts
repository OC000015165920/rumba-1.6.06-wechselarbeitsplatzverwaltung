/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { DatePipe, I18nSelectPipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { timer } from 'rxjs';
import { AppConfigService } from '../config/app-config.service';
import { departmentDTO } from './Department/Department.dto';
import { ConfirmDialogModel, DialogComponent } from './Dialog/dialog.component';
import { LocalUser } from './User/localuser';
import { Map } from './map/Map'
import { User } from './User/user';

/**
 * The class to handle generic data (e.g. settings from config file)
 */
@Injectable({
  providedIn: 'root'
})

/** pk, 2021/09/01: Service renamed from StoreServiceService to StoreService */

export class StoreService {

  /**the URL of the backend */
  private api= "http://docker-dev-01:3003/";
  /**seconds after the user gets logged out */
  private secondsTillAutoLogout = 1800;
  /** time left until auto logout --> =(secondsTillAutoLogout - interval) */
  timeTillLogOut =0;

  /**mapping from english weekday names to german ones */
  weekdayMap = {'Monday':'Montag', 'Tuesday':'Dienstag', 'Wednesday':'Mittwoch', 'Thursday':'Donnerstag', 'Friday':'Freitag' ,'Saturday':'Samstag', 'Sunday':'Sonntag'};

  /**the logged in user */
  localuser = new LocalUser();
  /**the department of the logged in user */
  localdepartment?: departmentDTO
  /**the public department where every user can book a workspace */
  publicdepartment?: departmentDTO;

  /**if login was started */
  loginStarted = false;
  /**logout subsription to log the user out automatically after some time */
  AutoLogOutSubscription: any;

  /**not used */
  switchMorningAfternoonSubscription: any;

  /**time to check if it is morning or afternoon (before after the switchMorningAfternoonTime) */
  time = new Date();
  /**if only morning bookings should be shown */
  showOnlyMorningBookings = false;
  /**if only afternoon bookings should be shown */
  showOnlyAfternoonBookings = false;
  /**the time where the system changes from morning to afternoon */
  switchMorningAfternoonTime = "12:30"

  /**indicates whether the map view or the table view if shown */
  showTableInsteadOfMap = false;
  /**selected date */
  selectedDate = new Date();
  /**selected map */
  selectedMap: {id:number, name:string, imageName:string, public?:boolean}={id:0,name:"",imageName:""};
  /**helper for dialog conflicts*/
  isDialogOpen: boolean=false;


  /**if RUMBA is used in terminal mode */
  terminal: boolean = false;
  /**if RUMBA is on the public page in terminal mode */
  terminalpublic: boolean = false;

  /** Name of the Department for e.g. Ausweichbüros or Abteilungsunabhängige Arbeitsplätze */
  publicDepartmentName = "Allgemein";

  /**
   * This constructor initilizes the component with the parameters belo
   * @param appConfigService the reference to the configuration service
   * @param router           the used router
   * @param keyCloakService  the reference to the service handling authentication and authorization
   */
  constructor(appConfigService: AppConfigService, public router: Router,
    private keyCloakService: KeycloakService, private dialog: MatDialog) {
      this.time = new Date();
      let timeAsString = this.time.toLocaleString('de-DE', { hour: 'numeric', minute:'numeric', hour12: false })
      if(this.switchMorningAfternoonTime <= timeAsString){
        this.showOnlyMorningBookings = false;
        this.showOnlyAfternoonBookings = true;
      }
      else{
        this.showOnlyMorningBookings = true;
        this.showOnlyAfternoonBookings = false;
      }

    // pk, 2021/10/07: check for undefined (in case of prod container) and set a default value
    if (this.secondsTillAutoLogout === undefined) {
      this.secondsTillAutoLogout = 300;   // default
    }
    //If there is already a subscription it will be stopped, so there is only one active subscription
    if(this.AutoLogOutSubscription)
      this.AutoLogOutSubscription.subscribe();

    //pk, 2022/05/31: correction to use setting from AppConfig
    this.api = appConfigService.get().apiUrl;
    // console.log('StoreService(): API = ', this.api);

    // console.log('StoreService(): secondsTillAutoLogout = ', this.secondsTillAutoLogout);
  }
  /**
   * returns the user logged in as a User object
   */
  getLoginUser(): User {
    let user = this.localuser;

    return user;
  }
  /**
   * Utility to get a Date object of the given RUMBA date
   * @param dateAsNumber the internal day of RUMBA booking, format YYYYMMDD
   * @returns a Date object for the given day
   */
  getDateFromNumber(dateAsNumber: number): Date{
    var year = Number(dateAsNumber.toString().substring(0,4));
    var month = Number(dateAsNumber.toString().substring(4,6))-1;
    var day = Number(dateAsNumber.toString().substring(6,8));
    return new Date(year,month,day,0,0,0,0)
  }

  /**
   * Utility to get a number from the given Date object in the format YYYYMMDD
   * @param date the Date object
   * @returns a number presentation for the given Date object
   */
  getNumberFromDate(date: Date): number{
    let dateAsNumber =
      (date.getFullYear()) +
      (date.getMonth() > 8 ? (+date.getMonth() + 1).toString() : '0' + (+date.getMonth() + 1).toString()) +
      (date.getDate() > 9 ? date.getDate() : '0' + date.getDate());
    return Number(dateAsNumber);
  }

  /**
   * Calculates a new date from a given Date and an offset of days
   * @param date the Date object to be taken as the base
   * @param daysToAdd the number of days to add
   * @returns a new Date object containing the sum of the two given values
   */
  addDaysOnDate(date: Date, daysToAdd: number){
    let newDate = new Date(date);
    // console.log("newdate:", new Date(newDate.setDate(date.getDate()+daysToAdd)))
    return new Date(newDate.setDate(date.getDate()+daysToAdd));
  }

  /**
   * Transforms the given date to a formatted string with the pattern "DD.MM.YYYY"
   * @param date
   * @returns a string with the given date value according to the pattern "DD.MM.YYYY"
   */
  getFormatedDate(date: Date): string{
    //console.log(date);
    let day = date.getDate().toString();
    let month = (date.getMonth()+1).toString()
    if(day.length==1)
      day='0'+day;
    if(month.length==1)
      month='0'+month;

    return day + "." + month +"."+ date.getFullYear().toString();
  }


  /**
   * This function returns the base url
   * @returns the base URL for backend access (e.g. "https://localhost:3000/")
   */
  getBaseURL(): string {
    //console.log("StoreServer.getBaseULR() returns"+this.api);
    return this.api;
  }

  /**
   * Starts the Timer to log out the User automaticly when the token is invalid or before
   */
  startAutoLogoutTimer(){
    if(!this.loginStarted){
      // console.log("startAutoLogoutTimer()");
      this.loginStarted = true;
      const source = timer(1000,1000); //sets the timer to an interval of 1 second
      this.AutoLogOutSubscription = source.subscribe(val =>{ //starts the timer
        // if(val >= this.secondsTillAutoLogout*0.8){
        //   alert("Sie werden gleich ausgeloggt");
        // }
        this.timeTillLogOut = this.secondsTillAutoLogout - val;
        if(val >= this.secondsTillAutoLogout){ //when the timer is higher than secondsTillAutoLogout then the timer will be stopped and loggedIn is set to false
            this.AutoLogOutSubscription.unsubscribe();
            console.log("startAutoLogoutTimer(): max. value reached => logout");
            const dialogRef = this.openConfirmDialog();
            dialogRef.afterClosed().subscribe(result => {
              this.onLogout();
            });
        }
      });
    }

  }

  /**
   * Show the message that the user is logged out
   */
  openConfirmDialog() {
      const dialogConfig = new MatDialogConfig();

      //content of Dialog
      const message = 'Die Login-Zeit ist abgelaufen, Sie werden nun automatisch ausgeloggt!';
      const dialogData = new ConfirmDialogModel('Bestätigung', message, false);

      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;

      dialogConfig.data = dialogData;

      const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
      return dialogRef;


    }
  /**
   * Implements the action after logout time has been reached
   */
  onLogout(){
    //this.keyCloakService.logout(location.origin);
    this.keyCloakService.logout(location.origin+"/public/"+this.localdepartment?.name);

  }

  /**
   * Retrieves the weekday from the given Date object
   * @param date a value of type Date
   * @returns the string presentation of the week day in German
   */
  getWeekDayFromDate(date: Date): string{
    let datePipe = new DatePipe('en-US');
    let weekday = datePipe.transform(date, 'EEEE');
    return I18nSelectPipe.prototype.transform(weekday,this.weekdayMap)
  }

  /**
   * The function opens an alert dialog.
   * @param message string
   * @param title string
   * @returns a referenz on the opened dialog
   */
  openAlertDialog(message: string, title: string) {
    const dialogConfig = new MatDialogConfig();

    //content of Dialog
    const dialogData = new ConfirmDialogModel(title, message, false);

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = dialogData;

    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    return dialogRef;


  }



}
