/**This interface is used to store the results of a booking dialog */
export interface dialogbookingresult{
    /**the comment of the booking (optional "") */
    note: string;
    /**if morning was booked */
    morning: boolean;
    /**if afternoon was booked */
    afternoon: boolean;
    /**the dates selected */
    selectedDates?: Date[];
    /**the date selected if one */
    date?: Date
}
