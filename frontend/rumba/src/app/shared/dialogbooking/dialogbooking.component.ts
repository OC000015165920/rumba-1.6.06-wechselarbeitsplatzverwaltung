import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { dialogbookingresult } from './dialogbookingresult';

/**
 * This component handles single full day bookings
 */
@Component({
  selector: 'app-dialogbooking',
  templateUrl: './dialogbooking.component.html',
  styleUrls: ['./dialogbooking.component.scss']
})
export class DialogbookingComponent implements OnInit {

  /**title of the dialog */
  title:string;
  /**description of the dialog */
  description:string;
  /**if halfday booking is checked*/
  halfday: boolean = false;
  /**if morning is selected to be booked */
  morning: boolean = false;
  /**if afternoon is selected to be booked */
  afternoon: boolean = false;
  /**optional note to attach to a booking */
  note: string ="";

  /**is empty */
  selected: string ="";

  /**
   * The constructor initilizes the attributes desription, title with the corresponding data.
   * @param dialogRef MatDialogRef<DialogComponent> to reference an open or closed dialog
   * @param data ConformDialogModel @Inject(MAT_DIALOG-DATA)
   */
  constructor(
      private dialogRef: MatDialogRef<DialogbookingComponent>,
      @Inject(MAT_DIALOG_DATA) public data:ConfirmDialogModel ) {
      
      this.description = data.description;
      this.title = data.title;
  }

  /**
   * The function is empty.
   */
  ngOnInit() {
      
  }

  /**
   * The function closes the dialog with the results morning, afternoon and note.
   */
  save() {
      //creating a object which will be returned
      if(!this.halfday){
        this.morning = true;
        this.afternoon = true;
      }

      let result: dialogbookingresult ={
        morning: this.morning,
        afternoon: this.afternoon,
        note: this.note
      }
      this.dialogRef.close(result);
  }

  /**
   * The function closes the dialog without results.
   */
  close() {
      this.dialogRef.close();
  }

  /**
   * This function checks which button is selected and sets the morning and afternoon variables
   * @param event is the value of the selected radio-button
   */
  changeRadio(event: string ){
    if(event === "nachmittags"){
      this.morning = false;
      this.afternoon = true;
    }
    else if(event ==="vormittags"){
      this.morning = true;
      this.afternoon = false;
    }

    // console.log("vormittags: " + this.morning +" nachmittags:  " + this.afternoon)
  }
}

/**This class is used as an interface for storing data of a Confirmdialog (title, description, showCancelButton) */
export class ConfirmDialogModel {

  /**
   * This constructor initilizes the class with the parameters below
   * @param title string, title of the dialog
   * @param description string, description of the dialog
   */
  constructor(public title: string, public description: string) {
  }
}