import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogbookingComponent } from './dialogbooking.component';

describe('DialogbookingComponent', () => {
  let component: DialogbookingComponent;
  let fixture: ComponentFixture<DialogbookingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogbookingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogbookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
