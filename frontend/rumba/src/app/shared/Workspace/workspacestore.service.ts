/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StoreService } from '../store-service.service';
import { CreateWorkspaceDto, UpdateWorkspaceDto } from './CreateWorkspaceDTO';
import { equipmentDTO } from './equipmentDTO';
import { Workspace } from './workspace';
import { Workspacecolor, createWorkspacecolorDTO, updateWorkspacecolorDTO } from './WorkspacecolorDTOs';

/**
 * This class provides the most recent absence data
 */
@Injectable({
  providedIn: 'root'
})
export class WorkspacestoreService {

  /**the base url for REST requests */
  api = this.ss.getBaseURL() + 'workspaces';
  /**the api but for equipment */
  equipmentapi = this.ss.getBaseURL() + 'equipment';
  /**the api but for workspacecolor */
  colorapi = this.ss.getBaseURL() + 'workspacecolors';
  /**the api to delete workspaces */
  deleteapi = this.ss.getBaseURL() + 'manage-workspaces';

  /**
   * This constructor initilizes the class with the parameters below
   * @param http HttpCient for REST requests
   * @param ss Service which contains the most recent common data (created by dependency injection)
   */
  constructor(private http: HttpClient, private ss: StoreService) { }

  /**
   * This function returns all workspaces for the specified department
   * @param department string, department name
   * @returns Workspace[], all workspaces of the department
   */
  getAll(department: string):Observable<Workspace[]>{
    return this.http.get<Workspace[]>(this.api + '/' + department);
  }

  /**
   * This function returns all workspaces for the specified department and map.
   * @param department string, department name
   * @param map_id number, id of the map
   * @returns Workspace[], all workspaces of the department
   */
  getAllForMap(department: string, map_id: number):Observable<Workspace[]>{
    return this.http.get<Workspace[]>(this.api + '/formap/' + department + "/" + map_id);
  }

  // getBookedWorkspacesForToday():Observable<Workspace[]>{
  //   return this.http.get<Workspace[]>(this.api + '/booked/' + this.ss.getNumberFromDate(new Date(Date.now())).toString());
  // }

  // getBookedWorkspacesForDay(date: Date):Observable<Workspace[]>{
  //   return this.http.get<Workspace[]>(this.api + '/booked/' + this.ss.getNumberFromDate(date));
  // }

  // getFreeWorkspacesForToday(department: string):Observable<Workspace[]>{
  //   return this.http.get<Workspace[]>(this.api + '/free/' + this.ss.getNumberFromDate(new Date(Date.now())).toString() + "/" + department);
  // }

  /**
   * The function returns all free workspaces for the specified date and department.
   * @param date Date
   * @param department string
   * @returns Workspace[]
   */
  getFreeWorkspacesForDay(date: Date, department: string):Observable<Workspace[]>{
    return this.http.get<Workspace[]>(this.api + '/free/' + this.ss.getNumberFromDate(date)+ "/" + department);
  }

  /**
   * The function returns all free workspaces for the specified date, department and map.
   * @param date Date
   * @param department string
   * @param map_id number, id of the map
   * @returns Workspace[]
   */
  getFreeWorkspacesForDayOnMap(date: Date, department: string, map_id: number):Observable<Workspace[]>{
    return this.http.get<Workspace[]>(this.api + '/free/' + this.ss.getNumberFromDate(date)+ "/formap/" + department + "/" + map_id);
  }
  // saveNewWorkspace(newWorkspace: CreateWorkspaceDTO):Observable<Workspace>{
  //   return this.http.post<Workspace>(this.api,newWorkspace);
  // }

  /**
   * This function updates the workspace of the workspace id
   * @param workspace_id number, id of the workspace to be updated
   * @param workspace UpdateWorkspaceDto, the updatable workspace data
   * @returns Observable<Workspace>, the updated workspace
   */
  updateWorkspace(workspace_id: number, workspace: UpdateWorkspaceDto):Observable<Workspace>{
    return this.http.patch<Workspace>(this.api + "/" + workspace_id, workspace);
  }

  /**
   * This function activates or deactivates the workspace
   * @param workspace_id number, id of the workspace to be updated
   * @param active {active: boolean}, if the workspace should be activated (true) or deactivated (false)
   * @returns Observable<Workspace>, the updated workspace
   */
  updateWorkspaceActive(workspace_id: number, active: {active: boolean}){
    this.http.patch<Workspace>(this.api + "/clear/" + workspace_id, active).subscribe(result => {console.log(result)});
    return this.http.patch<Workspace>(this.api + "/clear/" + workspace_id, active);
  }

  /**
   * This function creates a workspace.
   * @param workspace CreateWorkspaceDto, the settable workspace data
   * @returns Observable<Workspace>, the created workspace
   */
  createWorkspace(workspace: CreateWorkspaceDto):Observable<Workspace>{
    return this.http.post<Workspace>(this.api, workspace);
  }

  /**
   * This function deletes the specified workspace
   * @param workspace_id number, id of the workspace to be deleted
   * @returns any
   */
  deleteWorkspace(workspace_id: number){
    return this.http.delete<any>(this.deleteapi + "/" + workspace_id);
  }

  /**
   * This functions returns all available equipment.
   * @returns equipmentDTO[], allEquipment for the workspace to possibly have
   */
   allEquipment(){
    return this.http.get<equipmentDTO[]>(this.equipmentapi);
  }

  /**
   * This function returns all equipment the workspace already has.
   * @param employee_id number, id of the employee
   * @returns equipmentDTO[], all equipment of the workspace
   */
  allEquipmentForWorkspace(workspace_id: number){
    return this.http.get<equipmentDTO[]>(this.equipmentapi + "/forWorkspace/" + workspace_id);
  }

  /**
   * This functions adds an new option to the employee
   * @param option {equipment_id: number, employee_id: number}, information for the option
   * @returns Observable
   */
  addEquipmentForWorkspace(option: any):Observable<equipmentDTO>{
    return this.http.post<equipmentDTO>(this.equipmentapi + "/addEquipToWorkspace", option);
  }

  /**
   * This function deletes an option of the workspace.
   * @param employee_id number, id of the employee
   * @param id number, id of the option
   * @returns Observable
   */
  deleteEquipmentForWorkspace(workspace_id: number, id: number){
    return this.http.delete<equipmentDTO[]>(this.equipmentapi + "/forworkspace/" + workspace_id + "/" + id);
  }

  /**
   * This function returns the workspacecolor object of the department for the specified color if it exists.
   * @param department string, name of the department
   * @param color string, the rgb code of the color f.e. ffffff for white
   * @returns WOrkspacecolor, object of the workspacecolor of it exists
   */
  findWorkspacecolorByColorAndDepartment(department: string, color: string):Observable<Workspacecolor>{
    return this.http.get<Workspacecolor>(this.colorapi + "/departmentColor/" + department + "/" + color);
  }

  /**
   * This function returns the workspacecolor object of the specified id of it exists.
   * @param id number, the id of the worspacecolor
   * @returns Workspacecolor, the WOrkspacecolor object
   */
  findWorkspacecolorById(id: number){
    return this.http.get<Workspacecolor>(this.colorapi + "/" + id);
  }

  /**
   * This function deletes the workspacecolor object of the given id
   * @param id number, the id of the workspacecolor
   * @returns Observable
   */
  deleteWorkspacecolor(id: number){
    return this.http.delete<Workspacecolor>(this.colorapi + "/" + id);
  }

  /**
   * This function creates a new WOrkspacecolor object and returns it.
   * @param workspacecolor createWorkspacecolorDTO the data to create a new Workspacecolor object
   * @returns Workspacecolor, the created Workspacecolor objet
   */
  createWorkspacecolor(workspacecolor: createWorkspacecolorDTO){
    return this.http.post<Workspacecolor>(this.colorapi, workspacecolor);
  }

  /**
   * THis function update the a Workspacecolor.
   * @param workspacecolor updateWorkspacecolorDTO, the data to update the workspacecolor 
   * @param id number, the id of the WOrkspacecolor to be updated
   * @returns Observable
   */
  updateWorkspacecolor(workspacecolor: updateWorkspacecolorDTO, id: number){
    return this.http.patch<Workspacecolor>(this.colorapi + "/" + id, workspacecolor);
  }

  /**
   * THis function returns all Workspacecolor objects of the department
   * @param department string, name of the department
   * @returns Workspacecolor[]
   */
  getAllWorkspacecolorsForDepartment(department: string){
    return this.http.get<Workspacecolor[]>(this.colorapi + "/department/" + department)
  }
}
