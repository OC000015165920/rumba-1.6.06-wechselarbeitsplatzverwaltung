/**
 * This interface is used to store equipment data.
 */
export interface equipmentDTO{
    /** The Primary Key */
    equip_id: number;

    /** name of the option */
    name: string;
}