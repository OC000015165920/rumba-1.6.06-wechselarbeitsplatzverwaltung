/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Booking } from "../bookings/booking";
import { Room } from "../rooms/room";
import { equipmentDTO } from "./equipmentDTO";
import { Workspacecolor } from "./WorkspacecolorDTOs";

/**
 * This interface is used to store workspace data
 */
export interface Workspace {
  /**id of the workspace */
  workspace_id: number,
  /**name of the workspace */
  workspace_name: string,
  /**id of the room */
  room_id: number,
  /**room in which the workspace is */
  room: Room,
  /**x coordinate for point 1 of the workspace on the map */
  x: number,
  /**y coordinate for point 1 of the workspace on the map */
  y: number,
  /**scale of the workspace */
  scale: number;
  /**rotation of the workspace on the map */
  rotation: number;
  /**id of the employee which this workspace is assigned to */
  employee_id: number | undefined;
  /**if the workspace is bookable when the assigned employee is in homeoffice or absence */
  bookableWhenHO: boolean;
  /**all bookings for the workspace */
  bookings: Booking[];
  /**id of the department which the workspace belongs to */
  department_id: number;
  /**id of the map which the workspace is in */
  mapID: number;
  /**if the workspace is active */
  active: boolean;
  /**the color of the workspace */
  color: Workspacecolor;
  /**the equipment of the workspace */
  equipment: equipmentDTO[];
}