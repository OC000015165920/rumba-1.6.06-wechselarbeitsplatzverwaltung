/**This interface is used to store if a workspace is booked in the morning and or afternoon */
export interface workspaceState{
    /**if morning is booked */
    morningBooked: boolean,
    /**if afternoon is booked */
    afternoonBooked: boolean
}