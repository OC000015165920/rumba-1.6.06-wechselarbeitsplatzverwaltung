/**
 * THis interface is used to store Workspacecolor data
 */
export interface Workspacecolor{
    /**the id of the Workspacecolor */
    id: number;
    /**the color of the Workspacecolor */
    color: string;
    /**the description of the Workspacecolor */
    description: string;
    /**the department of the Workspacecolor */
    department: string;
}

/**
 * This interface is used to store data to create new Workspacecolor objects.
 */
export interface createWorkspacecolorDTO{
    /**the color of the Workspacecolor */
    color: string;
    /**the description of the Workspacecolor */
    description: string;
    /**the department of the Workspacecolor */
    department: string;
}

/**
 * This interface is used to store data to update workspacecolor objects.
 */
export interface updateWorkspacecolorDTO{
    /**the description of the Workspacecolor */
    description: string;
}