/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This interface is used to create new workspaces
 */
 export interface CreateWorkspaceDto{
    /**department which the workspace belongs to */
    department: string,
    /**name of the workspace */
    workspace_name: string,
    /**x coordinate for point 1 of the workspace*/
    x: string,
    /**y coordinate for point 1 of the workspace*/
    y: string,
    /**scale of the workspace */
    scale: number,
    /**rotation of the workspace */
    rotation: number,
    /**id of the map of the id */
    mapID: number,
    /**id of the room of the workspace */
    room_id: string,
    /**id of the workspacecolor */
    color_id: number,
    /**id of the employee who has the workspace reserved for him/her */
    employee_id?: string,
    /**if the workspace should be bookable when the employee is in HO or absent */
    bookableWhenHO: boolean,
}

/**
 * This interface is used to update workspaces
 */
export interface UpdateWorkspaceDto{
    /**name of the workspace */
    workspace_name: string,
    /**x coordinate for point 1 of the workspace*/
    x: string,
    /**y coordinate for point 1 of the workspace*/
    y: string,
    /**scale of the workspace */
    scale: number,
    /**rotation of the workspace */
    rotation: number,
    /**id of the map of the id */
    mapID: number,
    /**if the workspace is active */
    active: boolean,
    /**id of the workspacecolor */
    color_id: number,
    /**id of the employee who has the workspace reserved for him/her */
    employee_id?: string,
    /**if the workspace should be bookable when the employee is in HO or absent */
    bookableWhenHO: boolean,
}