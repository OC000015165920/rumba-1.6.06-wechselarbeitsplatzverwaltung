import { Component, Input, OnInit } from '@angular/core';

/**
 * This component is not used
 */
@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss']
})
export class StateComponent implements OnInit {
  /**not used*/
  @Input() morning?: boolean;
  /**not used */
  @Input() afternoon?: boolean;
  /**not used */
  constructor() { }

  /**not used */
  ngOnInit(): void {

  }

}
