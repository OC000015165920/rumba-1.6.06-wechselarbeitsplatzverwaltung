/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { User } from "../User/user";
import { Workspace } from "../Workspace/workspace";

/**
 * This interface is used to store bookings
 */
export interface Booking {
  /**id of the booking */
  booking_id: number,
  /**id of the workspace */
  workspace_id:number,
  /**day which the booking is for */
  booked_for_day: number;
  /**day when the booking was made */
  booked_at: Date;
  /**1:=workspace, 2:=homeoffice, 3:=absence */
  category: number;
  /**if the booking is for the morning */
  morning: boolean;
  /**if the booking is for the afternoon */
  afternoon: boolean;
  /**booking comment (optional "") */
  comment: string;
  /**id of the employee the booking was made for */
  employee_id: number;
  /**not used */
  active: boolean;
  /**User object of the employee */
  employee: User;
  /**who booked (employee/admin) */
  booked_by: string;
  /**Workspace object of the workspace */
  workspace: Workspace; // pk, 2021/09/23: server delivers type workspace, not Room anymore
}
