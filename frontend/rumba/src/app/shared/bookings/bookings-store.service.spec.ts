/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BookingsStoreService } from './bookings-store.service';
import { KeycloakService } from 'keycloak-angular';
import { AppConfigTestingService } from '../config/app-config-test.service';
import { AppConfigService } from '../config/app-config.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('BookingsStoreService', () => {
  let service: BookingsStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],

      providers: [
        BookingsStoreService,
        { provide: AppConfigService, useClass: AppConfigTestingService },
        { provide: KeycloakService, useValue: {} },
      ]
    });
    service = TestBed.inject(BookingsStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have getData function', () => {
    expect(service.getall).toBeTruthy();
});
});
