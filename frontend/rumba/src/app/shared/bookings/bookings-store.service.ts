/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ObservedValueOf } from 'rxjs';
import { Booking } from './booking';
import { CreateBookingDTO } from './CreateBookingDTO';
import { StoreService } from '../store-service.service';

/**
 * This class provides the recent booking data from the backend
 */
@Injectable({
  providedIn: 'root'
})
export class BookingsStoreService {

  /** pk, 2021/09/21: centralize definition for home-office "workplace" 
   * TODO: replace this by separate REST call in Sprint 6
  */
  HO_WORKSPACE_ID = 1;

  /** pk, 2021/09/01: centralize API definition in StoreService */
  api = this.ss.getBaseURL() + 'bookings';

  /**
   * This construsctor initilizes the component with the parameter below
   * @param http HttpClient for REST requests
   * @param ss StoreService Service which contains the most recent common data (created by dependency injection)
   */
  constructor(public http:HttpClient, public ss:StoreService) { 
  }

  /**
   * The function converts the internal date in format yyyyMMdd to the human form dd.MM.yyyy.
   * @param internalDate string, date in form yyyyMMdd
   * @returns string, date in form dd.MM.yyyy
   */
  convertInternalDateToHumanForm(internalDate:string): string {
    let humanForm =  internalDate.slice(6,8) + '.' + internalDate.slice(4,6) + '.' + internalDate.slice(0,4); 
       
    return humanForm;
  }

  /**
   * The function creates a new booking by requesting it with a post request.
   * @param booking CreateBookingDTO
   * @returns Observable<Booking>
   */
  create(booking: CreateBookingDTO): Observable<Booking> {
    console.log("create called for ", booking);
    return this.http.post<Booking>(this.api+"/user", booking);
  }

  /**
   * The function creates a new booking for a public workspace by requesting it with a post request.
   * @param booking CreateBookingDTO
   * @returns Observable<Booking>
   */
   createPublic(booking: CreateBookingDTO): Observable<Booking> {
    console.log("create called for ", booking);
    return this.http.post<Booking>(this.api+"/foralluser", booking);
  }

  /**
   * The function creates a new booking by requesting it with a post request as an admin.
   * @param booking CreateBookingDTO
   * @returns Observable<Booking>
   */
  createAsAdmin(booking: CreateBookingDTO): Observable<Booking> {
    console.log("create called for ", booking, " as admin");
    return this.http.post<Booking>(this.api+"/admin", booking);
  }

  /**
   * The function returns all bookings for the specified user.
   * @param id number, user id
   * @returns Observable<Booking>
   */
  getBookingsForUser(id: number):Observable<Booking[]>{
    // console.log("bookingstoreService: ",this.api +"/workspaces/" + id )
    return this.http.get<Booking[]>(this.api +"/user/" + id);
  }

  /**
   * The function returns all bookings for the specified user as a async function.
   * @param id number, user id
   * @returns Observable<Booking[]>
   */
  async getBookingsForUserAsPromise(id: number):Promise<Booking[]>{
    return await this.http.get<Booking[]>(this.api +"/user/" + id ).toPromise();
  }

  /**
   * The function  deletes the specified booking by requesting it with a delete request.
   * @param id number, user id
   * @returns Observable<Booking>
   */
  deleteBooking(id: number):Observable<string>{
    return this.http.delete(this.api + "/user/" + id, {responseType: 'text'});
  }

  /**
   * The function  deletes the specified booking by requesting it with a delete request as an admin.
   * @param id number, user id
   * @returns Observable<Booking>
   */
  deleteBookingAdmin(id: number):Observable<string>{
    return this.http.delete(this.api + "/admin/" + id, {responseType: 'text'});
  }

  /**
   * The function returns all bookings for the specified date and department.
   * @param id number, user id
   * @returns Observable<Booking[]>
   */
  getBookingsForDay(date: Date, department:string):Observable<Booking[]>{
    // pk, 2022/08/23: bug fix to ensure department is handled as a string
    return this.http.get<Booking[]>(this.api +"/forday/admin/"+ department+"/"+this.ss.getNumberFromDate(date));
  }

}

