/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This interface is used to store the app configuration data
 */
export interface AppConfig {
    /**base url for the app */
    apiUrl: string;
    /**url for keycloak */
    keycloak_url: string;
    /**the keycloak realm */
    keycloak_realm: string;
    /**id of the keycloak client */
    keycloak_clientId: string
}
