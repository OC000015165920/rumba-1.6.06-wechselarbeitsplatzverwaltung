/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from './app-config.data';

/**
 * This class handles the app configuration
 */
@Injectable()
export class AppConfigService {

    /**stores the app configurations */
    private appConfig: AppConfig | undefined;
    
    /**
     * The constructor initilizes the service with the parameters below
     * @param http HttpClient for REST requests
     */
    constructor(private http: HttpClient) { }

    /**This function loads the app configurations from "assets/config/config.json" to appConfig */
    loadAppConfig(): Promise<void> {
        return this.http.get<AppConfig>('/assets/config/config.json')
            .toPromise()
            .then(data => {
                this.appConfig = data;
            });
    }

    /**This function checks returns app Config or throws an Error if it is undefined */
    get(): AppConfig {
        if(this.appConfig === undefined) {
            throw new Error(`AppConfig not initialized.`);
        }
        return this.appConfig;
    }
}
