/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from './app-config.data';

/**
 * This class was a test
 */
@Injectable()
export class AppConfigTestingService {
    /**stores the app configurations */
    private appConfig: AppConfig = {
        apiUrl:                 '',
        keycloak_url:           '',
        keycloak_realm:         '',
        keycloak_clientId:      '',
    };

    /**
     * This constructor initilizes the class with the parameters below
     * @param http HttpClient for REST requests
     */
    constructor(private http: HttpClient) { }

    /**
     * This function returns the app configurations if they are stored in appConfig and raises an Error if not.
     * @returns AppConfig | undefined, app configurations if they exist
     */
    get(): AppConfig {
        if(this.appConfig === undefined) {
            throw new Error(`AppConfig not initialized.`);
        }
        return this.appConfig;
    }
}
