/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// pk, 2021/08/30: material layout
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { TokenInterceptorService } from './shared/Authentication/token-interceptor.service';
import { SharedModule } from './shared/shared.module';
import { DialogComponent } from './shared/Dialog/dialog.component';
import { ConfigdModule } from './config/config.module';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { AppConfigService } from './config/app-config.service';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field'


/** pk, 2022-05-13: new method to initialize the AppConfigService before KeycloakService */
/** */
/**
 * This Function initializes the AppConfigService.

 */
 function appInitializerFn(appConfig: AppConfigService) {
  return () => {
    return appConfig.loadAppConfig();
  };
};

/**
 * This Function initializes the keycloak Service.
 * The URl is the keycloak Server Adress
 * client ID is a client from keycloak
 * baererExcludedUrls are URLs where keycloak does not check for Authentication
 */
function initializeKeycloak(keycloak: KeycloakService, appConfigService: AppConfigService) {
  return () => new Promise<boolean> ((resolve) => {
    appConfigService.loadAppConfig().then(_ =>  {
      keycloak.init({
        config: {
          url: appConfigService.get().keycloak_url,
          realm: appConfigService.get().keycloak_realm,
          clientId: appConfigService.get().keycloak_clientId,
        },
        initOptions: {
          onLoad: 'check-sso',
          silentCheckSsoRedirectUri:
            window.location.origin + '/assets/silent-check-sso.html',
        },
        bearerExcludedUrls: ['/public', '/assets'],
        loadUserProfileAtStartUp: false,
      
      }).then(_ => {
        resolve(true);
      });
    });
  });
}
@NgModule({
    declarations: [
        AppComponent,
        DialogComponent,
    ],
    imports: [
        AngularMaterialModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        ConfigdModule.forRoot(),
        SharedModule,
        KeycloakAngularModule,
        MatFormFieldModule,
        MatInputModule
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: appInitializerFn,
            multi: true,
            deps: [AppConfigService],
        },
        {
            provide: APP_INITIALIZER,
            useFactory: initializeKeycloak,
            multi: true,
            deps: [KeycloakService, AppConfigService],
        },
    ],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
