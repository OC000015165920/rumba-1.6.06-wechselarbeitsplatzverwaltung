/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';
import { AppConfigService } from './config/app-config.service';
import { DepartmentstoreService } from './shared/Department/departmentstore.service';
import { mapperDepartmentOrgaunit } from './shared/Department/mapper_department_orgaunit.dto';
import { StoreService } from './shared/store-service.service';
import { createUserDTO } from './shared/User/createUserDTO';
import { UserStoreService } from './shared/User/user-store.service';

/**
 * This class handles the authentification
 */
@Injectable({
  providedIn: 'root',
})
export class AuthGuard extends KeycloakAuthGuard {

  /**
   * This constructor initilizes the AuthGuard with the parameters below and uses the constructor of KeycloakAuthGuard with router and keycloak.
   * @param router Router
   * @param keycloak Keycloakservice for Authentication
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   * @param userstoreservice Userstoreservice Service which contains the most recent user data (created by dependency injection)
   * @param configService AppConfigservice
   * @param departmentStoreService DepartmentstoreService Service which contains the most recent department data (created by dependency injection)
   */
  constructor(
    protected readonly router: Router,
    protected readonly keycloak: KeycloakService,
    private storeService: StoreService,
    private userstoreservice: UserStoreService,
    private configService: AppConfigService,
    private departmentStoreService: DepartmentstoreService
  ) {
    super(router, keycloak);
  }

  /**
   * This function checks if one of the given roles is user or admin. If it is, it will set that role as role in the storeService and return it.
   * @param roles string[] a list of roles to be checked
   * @returns string of a role
   */
  getUserAuthority(roles: string[]): string{
    for (const role of roles) {
      if(role == 'admin' || role == 'user'){
       return this.storeService.localuser.role = role;
      }
    }
    return ""
  } 

  /**
   * This function is always called when the user tries to access to a Module/URL which is protected by this authguard
   * @param route 
   * @param state 
   * @returns 
   */
  public async isAccessAllowed(route: ActivatedRouteSnapshot,state: RouterStateSnapshot) {

    // Force the user to log in if currently unauthenticated.
    if (!this.authenticated) {
      await this.keycloak.login({
        redirectUri: window.location.origin + state.url,
      });
    }

    
    //get Data from Keycloak
    let token = await this.keycloak.getToken();
    let username = await (await this.keycloak.loadUserProfile()).username;
    let forename = await (await this.keycloak.loadUserProfile()).firstName;
    let lastname = await (await this.keycloak.loadUserProfile()).lastName;
    let email = await (await this.keycloak.loadUserProfile()).email;
    let keycloak_id = await (await (await this.keycloak.loadUserProfile()).id);
    //Get all roles from the keycloak user
    const rolesFromKeycloak = this.keycloak.getUserRoles();
    
    //check if user is admin or user
    let role = this.getUserAuthority(rolesFromKeycloak); 
    

    //Check if forename has a value:
    if(forename == undefined || forename.length == 0){
      forename="unbekannt";
    }
  
    //Initialize localUser Object in the store service
    if(token && username && lastname && forename && email && keycloak_id){
      this.storeService.localuser.token = token;
      this.storeService.localuser.username = username;
      this.storeService.localuser.forename = forename;
      this.storeService.localuser.lastname = lastname;
      this.storeService.localuser.email_address = email;
      this.storeService.localuser.loggedIn = true;
      this.storeService.localuser.role = role;
      this.storeService.localuser.keycloak_id = keycloak_id;

      
    }
 


    //Send the recieved User Roles from Keycloak to the Backend to check to which Department the User belongs to
    await this.departmentStoreService.getDepartmentByRoles(rolesFromKeycloak).then(async result =>{
      this.storeService.localdepartment = result;
      this.storeService.localuser.department = result.name;
      /**
       * create a user object which will be sent to backend, to check if the user already exists or has to be created.
       * A complete object has to be send because if the user does not exist, the user can immediately be created and the backend returns the created user
       */
      let newUser: createUserDTO = {
        username: this.storeService.localuser.username,
        forename: this.storeService.localuser.forename,
        lastname: this.storeService.localuser.lastname,
        department: this.storeService.localuser.department,
        email_address: this.storeService.localuser.email_address,
        first_aid: false,
        keycloak_id: this.storeService.localuser.keycloak_id
      }
      console.log("usertocheck", newUser)
      let tmpUser = await this.userstoreservice.findOrCreateUser(newUser);
      if (tmpUser) {
        // console.log("isAccessAllowed(): setting employee_id to ",tmpUser.employee_id);
        this.storeService.localuser.employee_id = tmpUser.employee_id;
        this.storeService.localuser.default_mapID = tmpUser.default_mapID;
        //Start Logout Timer, so the User will be logged out automatlicly after 'secondsTillAutoLogout' Seconds
        this.storeService.startAutoLogoutTimer();
        this.storeService.switchMorningAfternoonSubscription.unsubscribe();
      }
      else {
        console.error("Could retrieve user ID");
      }
    });
    console.log("User Object: ", this.storeService.localuser)
    console.log("Department Object: ", this.storeService.localdepartment)
    
    // Get the public department if it exists
    await this.departmentStoreService.getDepartmentByName(this.storeService.publicDepartmentName).then(result => {
      this.storeService.publicdepartment = result;
    })
    
    // Get the roles required from the route.
    const requiredRoles = route.data.roles;

    // Allow the user to to proceed if no additional roles are required to access the route.
    if (!(requiredRoles instanceof Array) || requiredRoles.length === 0) {
      return true;
    }

    // Allow the user to proceed if all the required roles are present.
    return requiredRoles.every((role) => this.roles.includes(role));
  }  
}