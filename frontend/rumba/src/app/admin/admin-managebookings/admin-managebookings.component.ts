/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { filter } from 'rxjs/operators';
import { CATEGORY_AB, CATEGORY_HO, CATEGORY_NO_AB, CATEGORY_NO_HO, CATEGORY_NO_UN, CATEGORY_NO_WS, CATEGORY_UN, CATEGORY_WS } from 'src/app/bookings/bookingpage/bookingpage.component';
import { Booking } from 'src/app/shared/bookings/booking';
import { BookingsStoreService } from 'src/app/shared/bookings/bookings-store.service';
import { ConfirmDialogModel, DialogComponent } from 'src/app/shared/Dialog/dialog.component';
import { StoreService } from 'src/app/shared/store-service.service';
import { User } from 'src/app/shared/User/user';
import { UserStoreService } from 'src/app/shared/User/user-store.service';

/**This interface is used a datatype for the table to represent bookings */
export interface TableBookings {
  /**id if the booking */
  booking_id: number,
  /**for which day the booking is */
  booked_for_day: string,
  /**the booked workspace */
  workspace: string,
  /**if the booking is deletable */
  deletable: boolean,
  /**category of the booking (1/2/3 or workspace/homeoffice/absence) */
  category: string,
  /**if the morning is booked */
  morning: boolean,
  /**if the afternoon is booked */
  afternoon: boolean,
}

/**
 * This component handles bookings for the admin.
 */
@Component({
  selector: 'app-admin-managebookings',
  templateUrl: './admin-managebookings.component.html',
  styleUrls: ['./admin-managebookings.component.scss']
})


export class AdminManagebookingsComponent implements OnInit {

  /**the actual table */
  @ViewChild (MatTable) table!: MatTable<any>;
  /**manages the data for the table */
  dataSource: TableBookings[] = [];
  
  /**saves all bookings (from the department) */
  allBookings: Booking[] = [];
  /**store the future bookings for the selected user */
  futureBookings: Booking[] = [];
  /**saves all users (from the department) */
  allUsers: User[] = [];
  /**selected user */
  selectedUser?: User;
  /**if a booking was deleted */
  bookingDeleted = false;
  /**a string representative of the deleted booking (only date because you can't book multiple on the same day)*/
  deletedBooking = '';
  /**the columns displayed in the table */
  displayedColumns: string[] = [ 'booking_id', 'booked_for_day', 'workspace', 'category', 'occupied', 'deleteAction' ];
  /**special row colors */
  rowColors = [{category: "Arbeitsplatz", color:"red"}]

  /**
   * This constructor initilizes the parameters below
   * @param bookingStoreService BookingsStoreService Service which contains the most recent booking data (created by dependency injection)
   * @param userstoreService UserStoreService Service which contains the most recent user data (created by dependency injection)
   * @param dialog MatDialog for dialogs
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   */
  constructor(private bookingStoreService: BookingsStoreService, private userstoreService: UserStoreService, private dialog: MatDialog,
    public storeService: StoreService) { }

  
  /**
   * This function uses the function initialize
   */
  ngOnInit(): void {
    this.initialize();
  }

  /**
   * This function uses the function loadAllUsers
   */
  initialize(){
    this.loadAllUsers();
  }

  /**
   * This function loads all Bookings for the specified user and the table data
   * @param id number the id of the user for which the bookings should be loaded
   */
  laodAllBookingsForUser(id: number){
    this.allBookings =[];
    this.dataSource = [];
    this.bookingStoreService.getBookingsForUser(id).subscribe(result =>{
      this.allBookings = result;
      this.loadTableData();
    });
  }

  /**
   * This function loads all Users and saves them to the attribut allUsers
   */
  loadAllUsers(){
    if(this.storeService.localdepartment){
      this.userstoreService.getall(this.storeService.localdepartment.name).subscribe(result =>{
        this.allUsers = result.sort((a,b) => (a.lastname > b.lastname)?1:-1);
        this.allUsers = this.allUsers.filter(x => x.forename!="admin");
      });
    }

  }

  /**
   * This function gets the color of the category to be displayed
   * @param category string category (workspace/homeoffice/absence)
   * @return string color
   */
  getColor(category: string):string{
    console.log(category);
      return this.rowColors.filter(item => item.category === category)[0].color 
  }

  /**
   * The function changes the selectedUser to the one given by the event and loads all bookings for the user.
   * @param event any, any.value should be a User
   */
  onUserChanged(event:any){
    this.selectedUser= event.value;
    this.laodAllBookingsForUser(event.value.employee_id);
  }


  /**
   * This function loads the data for the table
   */
  loadTableData(){
      this.futureBookings = [];
      if(this.selectedUser){
        //allBookings will be filtered for the selected User and will be save in the new array filteredBookingsForUser
        let filteredBookingsForUser = this.allBookings.filter(x => x.employee_id == this.selectedUser?.employee_id);

        // show only bookings for today or newer
        filteredBookingsForUser.forEach(element => {

          if(element.booked_for_day >= this.storeService.getNumberFromDate(new Date(Date.now()))) {
            this.futureBookings.push(element);
          }
        });
    
        // sort the array to have the newest entry on top
        this.futureBookings.sort((a,b) => (a.booked_for_day>b.booked_for_day)||(a.booked_for_day==b.booked_for_day&&a.morning)?1:-1); 
        for (const booking of this.futureBookings) {
          let workspacename =""
          let category =""
          switch(booking.category){
            case CATEGORY_NO_UN: {
              workspacename = booking.workspace.workspace_name
              category = CATEGORY_UN;
            }break;
            case CATEGORY_NO_WS: {
              workspacename = booking.workspace.workspace_name
              category = CATEGORY_WS;
            }break;
            case CATEGORY_NO_HO : {
              category = CATEGORY_HO;
            }break;
            case CATEGORY_NO_AB: {
              category = CATEGORY_AB;
            }break;
          }
          let morning = booking.morning;
          let afternoon = booking.afternoon;
          let tableRow: TableBookings ={
            booking_id: booking.booking_id,
            booked_for_day:booking.booked_for_day.toString(),
            workspace: workspacename,
            deletable: true,
            category: category,
            morning: morning,
            afternoon: afternoon,
          }
          this.dataSource.push(tableRow);
        }
        this.dataSource.sort((a,b) => (a.booked_for_day>b.booked_for_day)||(a.booked_for_day==b.booked_for_day&&a.afternoon)?1:-1)
        this.table.renderRows();
      }

  }

  /**
   * This function sorts the data in dataSource asc by booked_for_day
   */
  sortData(){
    this.dataSource.sort((a,b) => (a.booked_for_day < b.booked_for_day)?1:-1);
  }

  /**
   * This function opens the dialog to delete specified booking
   * @param element TableBooking the booking to be deleted
   */
  onDeleteButton(element: TableBookings){
    console.log(`onDeleteButton() called for ${element.booking_id}`)
    this.openDeleteDialog(element.booking_id);
  }

  /**
   * This function opens the dialog to delete specified booking
   * @param booking_id number the booking to be deleted
   */
  openDeleteDialog(booking_id: number) {
    const dialogConfig = new MatDialogConfig();
    var date:string = '';
    this.allBookings.forEach(element => {
      if(element.booking_id == booking_id) {
        date = element.booked_for_day.toString(); //booked_for_day expected in Format YYYYMMDD
        date = date.slice(6,8) + '.' + date.slice(4,6) + '.' + date.slice(0,4)
      }
    })
    //content of Dialog
    const message = 'Soll die Buchung vom ' + date + ' wirklich gelöscht werden?';
    const dialogData = new ConfirmDialogModel('Bestätigung', message);

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = dialogData;
    
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.bookingDeleted = result; //for the html file to show deletion message
      
      if (result) {
        //actual deletion function
        this.deleteBooking(booking_id);
      }
    }); 
  } 

  /**
   * This function deletes specified booking
   * @param booking_id number the booking to be deleted
   */
  deleteBooking(booking_id: number){
    console.log(`deleteBooking called for booking ID ${booking_id}`);
     this.bookingStoreService.deleteBookingAdmin(booking_id).subscribe( result =>{
       this.allBookings.forEach(element => {
         if(element.booking_id == booking_id) {
           this.deletedBooking = element.booked_for_day.toString(); //booked_for_day expected in Format YYYYMMDD
           this.deletedBooking = this.deletedBooking.slice(6,8) + '.' + this.deletedBooking.slice(4,6) + '.' + this.deletedBooking.slice(0,4)
         }
       })
       console.log(`deleteBooking(${booking_id} returned: ${result}`);
       // reload the existing bookings to update the view
       if(this.selectedUser) {
         this.laodAllBookingsForUser(this.selectedUser.employee_id);
       }
     });
   }

}
