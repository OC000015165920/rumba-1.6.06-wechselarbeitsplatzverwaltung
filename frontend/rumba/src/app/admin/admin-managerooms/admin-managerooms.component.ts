/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { Room } from 'src/app/shared/rooms/room';
import { RoomStoreService } from 'src/app/shared/rooms/room-store.service';
import { StoreService } from 'src/app/shared/store-service.service';

/**
 * This component handels the rooms for admins.
 */
@Component({
  selector: 'app-admin-managerooms',
  templateUrl: './admin-managerooms.component.html',
  styleUrls: ['./admin-managerooms.component.scss']
})
export class AdminManageroomsComponent implements OnInit {

  /**array to store rooms */
  rooms: Room[] =[];
  /**FormGroup for storing room data */
  roomForm: UntypedFormGroup;

  /**
   * The constructor initilizes the roomForm with the empty variables room_name, orga_unit, desription, active, dateOfExpiry and validFrom.
   * @param roomStoreService RoomStoreService Service which contains the most recent rooms (created by dependency injection)
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   */
  constructor(private roomStoreService: RoomStoreService, private storeService: StoreService) { 
    this.roomForm = new UntypedFormGroup({
      room_name: new UntypedFormControl(''),
      orga_unit: new UntypedFormControl(''),
      description: new UntypedFormControl(''),
      active: new UntypedFormControl(''),
      dateOfExpiry: new UntypedFormControl(''),
      validFrom: new UntypedFormControl(''),
  })
  }

  /**
   * The function loads all rooms to the rooms attribute.
   */
  ngOnInit(): void {
    this.initialize();
  }

  /**
   * The function loads all rooms to the rooms attribute.
   */
  initialize(){
    this.loadAllRooms();
  }

  /**
   * The function loads all rooms to the rooms attribute.
   */
  loadAllRooms(){
    if(this.storeService.localdepartment)
    this.roomStoreService.getall(this.storeService.localdepartment.name).subscribe(result =>{
      this.rooms = result;
      console.log(result);
    })
  }

  /**
   * The function updates the attribute roomForm to match the values of the new selected Room
   * @param selectedRoom Room
   */
  roomSelected(selectedRoom: any){
    console.log(selectedRoom);
    // this.roomForm.patchValue({
    //   room_name: selectedRoom.room_name,
    //   orga_unit: selectedRoom.orga_unit,
    //   description: selectedRoom.description,
    //   active: selectedRoom.active,
    //   dateOfExpiry: selectedRoom.dateOfExpiry,
    //   validFrom: selectedRoom.validFrom
    // })
  }

}
