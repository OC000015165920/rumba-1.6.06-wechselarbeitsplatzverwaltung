/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Component, HostListener, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Room } from 'src/app/shared/rooms/room';
import { RoomStoreService } from 'src/app/shared/rooms/room-store.service';
import { StoreService } from 'src/app/shared/store-service.service';
import { Workspace } from 'src/app/shared/Workspace/workspace';
import { WorkspacestoreService } from 'src/app/shared/Workspace/workspacestore.service';
import { Map } from 'src/app/shared/map/Map'
import { CreateWorkspaceDto, UpdateWorkspaceDto } from 'src/app/shared/Workspace/CreateWorkspaceDTO';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmDialogModel, DialogComponent } from 'src/app/shared/Dialog/dialog.component';
import { equipmentDTO } from 'src/app/shared/Workspace/equipmentDTO';
import { Workspacecolor } from 'src/app/shared/Workspace/WorkspacecolorDTOs';
import { User } from 'src/app/shared/User/user';
import { UserStoreService } from 'src/app/shared/User/user-store.service';

/**
 * This component handels the workspace management for admins.
 */
@Component({
  selector: 'app-admin-manageworkspaces',
  templateUrl: './admin-manageworkspaces.component.html',
  styleUrls: ['./admin-manageworkspaces.component.scss']
})
export class AdminManageworkspacesComponent implements OnInit {

  /**saves all workspacecolors (from the department) */
  allColors: Workspacecolor[] = [];
  /**saves all maps (from the department) */
  allMaps: Map[] = [];
  /**saves all workspaces (from the department) */
  allWorkspaces: Workspace[] =[];
  /**saves all employees (from the department) */
  allEmployees: User[]=[];
  /**saves the workspaces displayed on the map */
  mapWorkspaces: Workspace[] = [];
  /**saves all rooms (from the department ) */
  allRooms: Room[] =[];
  /**form to save and validate data for a workspace */
  workspaceForm: UntypedFormGroup;
  /**a boolean to save if the admin is creating a new workspace */
  newWorkspaceCreating = false;
  /**the selected workspace */
  selectedWorkspace?: Workspace;

  /**offset left of the map in the html */
  mapOffsetLeft?: number;
  /**offset top of the map in the html */
  mapOffsetTop?: number;

  /**the x coordinate of the workspace */
  x: number=0;
  /**the x coordinate of the workspace */
  y: number=0;
  /**the room id of the room of the workspace */
  room: Room ={room_id: -1, room_name:"", orga_unit:"", description:"", active: true, dateOfExpiry: new Date(), validFrom: new Date()};
  /**scale of the workspace */
  scale: number=1;
  /**rotation of the worspace in degrees */
  rotation: number=0;
  /**the selected map */
  selectedMap: Map ={id:-1, name:"", imageName:""};
  /**the selected map */
  selectedColor: Workspacecolor ={id:-1, color:"", description:"", department:""};
  /**the selected employee */
  selectedEmployee: User = {employee_id: -1, username:"", forename: "", lastname:"", orga_unit:"", email_address:"", first_aid:false, keycloak_id:"", default_mapID:-1, anonymous:false};
  /**if the workspace should be bookable when the employee is in homeoffice */
  selectedBookableWhenHO: boolean= true;
  /**the name of the workspace to be created*/
  name: string="";
  /**if map should be shown */
  isMap: boolean = false;
  /**path of the imagefile of the selected map */
  ImagePath?: string;

  /**all possible equipment */
  allEquipment: equipmentDTO[]=[];
  /**the selected euipment */
  equipment: equipmentDTO[]=[];
  /**the euipment of the workspace before changes */
  equipmentbefore: equipmentDTO[]=[];
  /**formControl for the equipment */
  equipmentForm = new UntypedFormControl();


  /**width of the map in px */
  mapWidth=936;
  /**scale of the map */
  mapScale = 1;
  /**window screen width */
  windowWidth? = 0;

  /**listens for the window to be resized */
  @HostListener('window:resize', ['$event'])
  /**adjusts the hitboxes and map when the window is resized */
  onResize(){
    this.adjustHitboxes();
  }

  /**
   * The constructor initilizes the workspaceForm with the empty variables workspace_id, workspace_name, room_id, x1, x2, x3, x4, y1, y2, y3, y4, reserved, reserved_desription, active, dateOfExpiry and validFrom.
   * @param workspaceStoreService WorkspacestoreService Service which contains the most recent workspace data (created by dependency injection)
   * @param roomStoreService RoomStoreService Service which contains the most recent room data (created by dependency injection)
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   * @param _snackBar MatSnackbar for user feedback
   */
  constructor(private workspaceStoreService: WorkspacestoreService, private roomStoreService: RoomStoreService,
    private storeService: StoreService, private _snackBar: MatSnackBar, private dialog: MatDialog, private userStoreService: UserStoreService) {
    this.workspaceForm = new UntypedFormGroup({
      // pk, 2022/08/26: bug fix: force same length for name as the backend expects (5 chars)
      name: new UntypedFormControl(this.name, [Validators.required, Validators.minLength(5)]),
      x: new UntypedFormControl(this.x, [Validators.required, Validators.max(936), Validators.min(0)]),
      y: new UntypedFormControl(this.y, [Validators.required, Validators.max(936), Validators.min(0)]),
      scale: new UntypedFormControl(this.scale, [Validators.required, Validators.max(5), Validators.min(0.2)]),
      rotation:new UntypedFormControl(this.rotation, [Validators.required, Validators.min(0), Validators.max(359)]),
      selectedMap: new UntypedFormControl(this.selectedMap, [Validators.required]),
      selectedColor: new UntypedFormControl(this.selectedColor, [Validators.required]),
      selectedEmployee: new UntypedFormControl(this.selectedEmployee, [Validators.required]),
      // pk, 2022/08/26: bug fix: allow "false" setting in this property for new work spaces
      bookableWhenHO: new UntypedFormControl(Boolean(this.selectedBookableWhenHO), [Validators.required]),
      roomForm: new UntypedFormControl(this.room, [Validators.required]),
    });

  }

  /**
   * The function loads all workspaces to the allWorkspaces attribute and then all rooms to the allRooms attribute.
   */
  ngOnInit(): void {
    this.loadAllWorkspaces().then(() =>{
      this.loadAllRooms();
      this.loadAllMaps();
      this.loadAllColors();
      this.loadAllEmployees();
    });
  }

  /**adjusts the hitboxes for the workspaces when the map is resized */
  adjustHitboxes(){
    this.adjustMapSize();

    this.mapScale = this.mapWidth/936;
    this.getOffset();
  }

  /**
   * The function loads all workspaces to the allWorkspaces attribute.
   */
  async loadAllWorkspaces(){
    if(this.storeService.localdepartment){
      this.workspaceStoreService.getAll(this.storeService.localdepartment.name).subscribe(result =>{
        this.allWorkspaces = result.sort((a,b)=>{
          if(a.workspace_name < b.workspace_name) { return -1; }
          if(a.workspace_name > b.workspace_name) { return 1; }else{
            return 1;
          }
        });;
      });
    }
  }

  /**
   * The function loads all rooms to the allRooms attribute.
   */
  async loadAllRooms(){
    if(this.storeService.localdepartment){
      this.roomStoreService.getall(this.storeService.localdepartment.name).subscribe(result =>{
        this.allRooms = result.sort((a,b)=>{
          if(a.room_name < b.room_name) { return -1; }
          if(a.room_name > b.room_name) { return 1; }else{
            return 1;
          }
        });
      });
    }

  }

  /**
   * This function loads all maps of the department to the allMaps attribute.
   */
  loadAllMaps(){
    if(this.storeService.localdepartment){
      this.allMaps = this.storeService.localdepartment.maps;
    }
  }

  /**
   * This function loads all workspacecolors of the department to the allColors attribute.
   */
   loadAllColors(){
    if(this.workspaceStoreService&&this.storeService.localdepartment){
      this.workspaceStoreService.getAllWorkspacecolorsForDepartment(this.storeService.localdepartment.name).subscribe(result => {
        this.allColors = result;
      });
    }
  }

  /**
   * This function loads all Employees of the department into the attribute allEmployees.
   */
  loadAllEmployees(){
    if(this.storeService.localdepartment){
      this.userStoreService.getall(this.storeService.localdepartment.name).subscribe(result=>{
        this.allEmployees=result;
      });
    }
  }

  /**
   * The function updates the workspaceForm to match the selected workspace and sets selectedWorkspaceName to the name of the workspace selected.
   * @param event Workspace, which is selected
   */
  workspaceSelected(event:Workspace){
    console.log(event);
  }

  /**
   * The function is empty.
   */
  updateReserved(){

  }

  /**
   * The function resets the workspaceForm.
   */
  resetForm(){
    //this.workspaceForm.reset();
  }

  /**
   * The function tells the console to print room.room_name "was selected".
   * @param room Room
   */
  selectedRoom(room: Room){
    console.log(room.room_name + "was selected");
  }

  /**
   * The function is empty.
   */
  saveNewWorkspace(){

  }

  /**
   * This function sets the parameters for updating to the ones of the workspace and enables the workspace configuration
   * @param workspaceFromSelect Workspace, the selected workspace
   */
  setWorkspace(workspaceFromSelect: Workspace){
    this.selectedWorkspace = workspaceFromSelect;
    this.newWorkspaceCreating = false;
    this.name = this.selectedWorkspace.workspace_name;
    this.x = this.selectedWorkspace.x;
    this.y = this.selectedWorkspace.y;
    this.scale = this.selectedWorkspace.scale;
    this.rotation = this.selectedWorkspace.rotation;
    let map_id = this.selectedWorkspace.mapID;
    this.selectedColor = this.selectedWorkspace.color;
    for(let e of this.allEmployees){
      if(e.employee_id==this.selectedWorkspace.employee_id){
        this.selectedEmployee = e;
      }
    }
    if(this.selectedWorkspace.bookableWhenHO){
      this.selectedBookableWhenHO=true;
    }else{
      this.selectedBookableWhenHO=false;
    }
    //console.log(this.selectedColor, this.selectedBookableWhenHO);
    for(let e of this.allEmployees){
      if(e.employee_id==this.selectedWorkspace.employee_id){
        this.selectedEmployee=e;
        if(this.selectedWorkspace.bookableWhenHO){
          this.selectedBookableWhenHO=true;
        }
        else {
          this.selectedBookableWhenHO=false;
        }
      }
    }
    for(let color of this.allColors){
      if(color.id===this.selectedColor.id){
        this.selectedColor = color;
      }
    }
    for(let map of this.allMaps){
      if(map.id===map_id){
        this.selectedMap=map;
        this.ImagePath = "assets/" + this.selectedMap?.imageName;
        this.isMap=true;
        this.getMapWorkspaces(map_id);
        if(this.selectedWorkspace){
          this.workspaceStoreService.allEquipmentForWorkspace(this.selectedWorkspace.workspace_id).subscribe(result => {
            this.equipment = result;
            this.equipmentbefore = result;
          });
          this.workspaceStoreService.allEquipment().subscribe(result => {
            this.allEquipment = result;
          })
        }
      }
    }
    this.workspaceForm.controls.selectedMap.setValue(this.selectedMap);
      this.workspaceForm.controls.selectedEmployee.setValue(this.selectedEmployee);
      this.workspaceForm.controls.selectedColor.setValue(this.selectedColor);
      this.workspaceForm.controls.bookableWhenHO.setValue(this.selectedBookableWhenHO);
      
      console.log(this.workspaceForm.controls);
      console.log(this.selectedBookableWhenHO);
      console.log(this.selectedMap);
      console.log(this.allMaps[0]);
      console.log(this.allMaps[0]==this.selectedMap);
    this.adjustHitboxes();
  }

  /**
   * This function sets the parameters for creating a workspace to the default and enables the workspace creating.
   */
  openNewWorkspace(){
    this.newWorkspaceCreating=true;
    this.selectedWorkspace=undefined;
    this.name="";
    this.x=0;
    this.y=0;
    this.scale=1;
    this.rotation=0;
    // pk, 2023/01/13: improvement for departments with just 1 map: show it immediately
    if (this.allMaps.length != 1) {
      this.selectedMap={id:-1, name:"", imageName:""};
      this.isMap=false;
    }
    else {
      let map = this.allMaps[0];
      this.selectedMap={id: map.id, name: map.name, imageName: map.imageName};
      this.selectMap();
    }
    if (this.allColors.length == 1) {
      this.selectedColor=this.allColors[0]
      this.selectColor();
    }
  }
  /**
   * This function is triggered when a new colar is selected
   */
  selectColor(){
    if(this.selectedColor){
      this.adjustHitboxes();
    }
  }
  /**
   * This function is triggered when a new map is selected and changes the image.
   */
  selectMap(){
    if(this.selectedMap){
      this.ImagePath = "assets/" + this.selectedMap?.imageName + "?t=" + new Date().getTime();
      this.isMap = true;
      this.getMapWorkspaces(this.selectedMap.id);
      this.adjustHitboxes();
    }
  }

  /**
   * This function loads all workspaces of the specified map to mapWorkspaces.
   * @param map_id number, id of the map
   */
  getMapWorkspaces(map_id: number){
    let temp:Workspace[]=[]
    for(let w of this.allWorkspaces){
      if(w.mapID==map_id){
        temp.push(w);
      }
    }
    this.mapWorkspaces=temp;
  }

  /**
   * This function sets the varibles for offset for the displayed workspaces to the offset of the map
   */
  getOffset(){
    if(document.getElementById('map')){
      const rect = document.getElementById('img')?.getBoundingClientRect();

      //reset offset to 0
      this.mapOffsetLeft = 0;
      this.mapOffsetTop = 0;
      if(rect){
        //set offset to offset of the map
        this.mapOffsetLeft = rect?.x;
        this.mapOffsetTop = rect?.y;
      }
      //adjust the offset for the change of the worspaces scale
      this.mapOffsetLeft -= (1-this.mapScale)*25;
      this.mapOffsetTop -= (1-this.mapScale)*12.5;
    }
  }

  /**
   * This function updates the selected workspace if the settings are valid and then resets the page.
   */
  updateWorkspace(){
    if(this.selectedWorkspace){
      if(this.workspaceForm.valid&&this.selectedColor.id!=-1&&this.checkWorkspaceName(this.name, this.selectedMap.id, this.selectedWorkspace.workspace_id)){
        let tmp =false;
        if(this.selectedEmployee.employee_id>0){
          tmp = true;
        }
        let temp:UpdateWorkspaceDto;
        if(tmp){
          temp={
            workspace_name: this.name,
            x: this.x + "",
            y: this.y + "",
            scale: this.scale,
            rotation: this.rotation,
            mapID: this.selectedMap.id,
            active: this.selectedWorkspace.active,
            color_id: this.selectedColor.id,
            employee_id: this.selectedEmployee.employee_id+"",
            bookableWhenHO: Boolean(this.selectedBookableWhenHO),
          }
        }else{
          temp={
            workspace_name: this.name,
            x: this.x + "",
            y: this.y + "",
            scale: this.scale,
            rotation: this.rotation,
            mapID: this.selectedMap.id,
            active: this.selectedWorkspace.active,
            color_id: this.selectedColor.id,
            bookableWhenHO: true,
          }
        }


        this.workspaceStoreService.updateWorkspace(this.selectedWorkspace.workspace_id, temp).subscribe(result => {


          this.changeEquipment();

          this.openSnackBar("Der Arbeitsplatz " + this.selectedWorkspace?.workspace_name + " wurde bearbeitet.");
          this.selectedWorkspace=undefined;
          this.name="";
          this.x=0;
          this.y=0;
          this.scale=1;
          this.rotation=0;
          this.selectedMap={id:-1, name:"", imageName:""};
          this.selectedColor={id:-1, color:"", description:"", department:""};
          this.isMap=false;
          this.selectedBookableWhenHO=false;
          this.selectedEmployee= {employee_id: -1, username:"", forename: "", lastname:"", orga_unit:"", email_address:"", first_aid:false, keycloak_id:"", default_mapID:-1, anonymous:false};
          this.loadAllWorkspaces();
        });
      }else{
        if(!this.checkWorkspaceName(this.name, this.selectedMap.id, this.selectedWorkspace.workspace_id)){
          this.openSnackBar("Der Arbeitsplatzname ist auf dieser Karte schon belegt. Bitte wählen Sie einen anderen Namen!")
        }else{
          this.openSnackBar("Die Eingaben sind nicht gültig oder nicht vollständig.")
        }

      }

    }

  }

  /**
   * This function creates a workspace is the settings are valid and then resets the page.
   */
  createWorkspace(){
    //console.log("createWorkspace() called")
    if(this.newWorkspaceCreating&&this.storeService.localdepartment){
      console.log("checkWorkspace(): check whether the form is valid:"+this.workspaceForm.valid)
      if(this.workspaceForm.valid&&this.selectedColor.id!=-1&&this.selectedMap.id!=-1&&this.checkWorkspaceName(this.name, this.selectedMap.id, -1)){
        let tmp =false;
        if(this.selectedEmployee.employee_id>0){
          tmp = true;
        }
        let temp:CreateWorkspaceDto;
        if(tmp){
          temp={
            department: this.storeService.localdepartment?.name,
            workspace_name: this.name,
            x: this.x + "",
            y: this.y + "",
            scale: this.scale,
            rotation: this.rotation,
            mapID: this.selectedMap.id,
            room_id: this.room.room_id + "",
            color_id: this.selectedColor.id,
            employee_id: this.selectedEmployee.employee_id+"",
            bookableWhenHO: Boolean(this.selectedBookableWhenHO),
          }
        }else{
          temp={
            department: this.storeService.localdepartment?.name,
            workspace_name: this.name,
            x: this.x + "",
            y: this.y + "",
            scale: this.scale,
            rotation: this.rotation,
            mapID: this.selectedMap.id,
            room_id: this.room.room_id + "",
            color_id: this.selectedColor.id,
            bookableWhenHO: true,
          }
        }

        //console.log("createWorkspace(): temp="+ temp);
        this.workspaceStoreService.createWorkspace(temp).subscribe(result => {
          this.openSnackBar("Der Arbeitsplatz " + this.name + " wurde erstellt.");
          this.selectedWorkspace=undefined;
          this.name="";
          this.x=0;
          this.y=0;
          this.scale=1;
          this.rotation=0;
          this.selectedMap={id:-1, name:"", imageName:""};
          this.room={room_id: -1, room_name:"", orga_unit:"", description:"", active: true, dateOfExpiry: new Date(), validFrom: new Date()};
          this.isMap=false;
          this.selectedColor={id:-1, color:"", description:"", department:""};
          this.selectedBookableWhenHO=false;
          this.selectedEmployee= {employee_id: -1, username:"", forename: "", lastname:"", orga_unit:"", email_address:"", first_aid:false, keycloak_id:"", default_mapID:-1, anonymous:false};
          this.loadAllWorkspaces();
          this.newWorkspaceCreating=false;
        });
      }else{
        if(!this.checkWorkspaceName(this.name, this.selectedMap.id, -1)){
          this.openSnackBar("Der Arbeitsplatzname ist auf dieser Karte schon belegt. Bitte wählen Sie einen anderen Namen!")
        }else{
          this.openSnackBar("Die Eingaben sind nicht gültig oder nicht vollständig.")
        }
      }
    }
    //console.log("createWorkspace(): END")
  }

  /**
   * This function deletes a workspace if one is selected.
   */
  deleteWorkspace(){
    if(this.selectedWorkspace){

      this.workspaceStoreService.deleteWorkspace(this.selectedWorkspace.workspace_id).subscribe(result => {
        console.log(result);
        this.openSnackBar("Der Arbeitsplatz " + this.selectedWorkspace?.workspace_name + " wurde entfernt.");
        this.selectedWorkspace=undefined;
          this.name="";
          this.x=0;
          this.y=0;
          this.scale=1;
          this.rotation=0;
          this.selectedMap={id:-1, name:"", imageName:""};
          this.selectedEmployee={employee_id: -1, username:"", forename: "", lastname:"", orga_unit:"", email_address:"", first_aid:false, keycloak_id:"", default_mapID:-1, anonymous:false};
          this.isMap=false;
          this.selectedColor={id:-1, color:"", description:"", department:""};
          this.loadAllWorkspaces();
      });
    }
  }

  /**
   * Opens Dialog to Confirm Deletion of Workspace
   * @param booking_id Unique ID to indentify Workspace
   */
   openDeleteDialog() {
    if(this.selectedWorkspace){
      const dialogConfig = new MatDialogConfig();


      //content of Dialog
      const message = 'Soll der Arbeitsplatz ' + this.selectedWorkspace.workspace_name + ' wirklich gelöscht werden?';
      const dialogData = new ConfirmDialogModel('Bestätigung', message);

      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;

      dialogConfig.data = dialogData;

      const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {


        if (result) {
          //actual deletion function
          this.deleteWorkspace();
        }
      });
    }
  }

  /**
   * This function blocks the selected workspace.
   */
  blockWorkspace(){
    if(this.selectedWorkspace){
      this.workspaceStoreService.updateWorkspaceActive(this.selectedWorkspace.workspace_id, {active: false}).subscribe(result=> {
        this.openSnackBar("Der Arbeitsplatz " + this.selectedWorkspace?.workspace_name + " wurde gesperrt.");
        this.selectedWorkspace=undefined;
        this.name="";
        this.x=0;
        this.y=0;
        this.scale=1;
        this.rotation=0;
        this.selectedMap={id:-1, name:"", imageName:""};
        this.isMap=false;
        this.selectedEmployee={employee_id: -1, username:"", forename: "", lastname:"", orga_unit:"", email_address:"", first_aid:false, keycloak_id:"", default_mapID:-1, anonymous:false};
        this.loadAllWorkspaces();
      });


    }
  }

  /**
   * This function unblocks the selected workspace.
   */
  unblockWorkspace(){
    if(this.selectedWorkspace){
      this.workspaceStoreService.updateWorkspaceActive(this.selectedWorkspace.workspace_id, {active: true}).subscribe(result => {
        this.openSnackBar("Der Arbeitsplatz " + this.selectedWorkspace?.workspace_name + " wurde gesperrt.");
        this.selectedWorkspace=undefined;
        this.name="";
        this.x=0;
        this.y=0;
        this.scale=1;
        this.rotation=0;
        this.selectedMap={id:-1, name:"", imageName:""};
        this.isMap=false;
        this.selectedEmployee={employee_id: -1, username:"", forename: "", lastname:"", orga_unit:"", email_address:"", first_aid:false, keycloak_id:"", default_mapID:-1, anonymous:false};
        this.loadAllWorkspaces();

      });

    }
  }

  /**
   * This function checks if the given workspace is the selected one.
   * @param workspace Workspace, the workspace to check
   * @returns boolean, true it is the selected workspace
   */
  isSelectedWorkspace(workspace: Workspace):boolean{
    if(this.selectedWorkspace){
      if(this.selectedWorkspace.workspace_id==workspace.workspace_id){
        return true;
      }
    }
    return false;
  }

  /**
   * This function checks which equipment were added and which were removed and returns them.
   * @returns {equipmentDTO[], equipmentDTO[]}, the equipment added and the equipment removed
   */
  getEquipmentChanges(): {add: equipmentDTO[], todelete: equipmentDTO[]}{
    let add: equipmentDTO[]=[];
    let todelete: equipmentDTO[]=[];
      for(let e of this.equipment){
          let temp= false;
          for(let equipmentbefore of this.equipmentbefore){
            if(e.equip_id==equipmentbefore.equip_id){
              temp=true;
            }
          }
          if(!temp){
            add.push(e);
          }

      }
      for(let equipmentbefore of this.equipmentbefore){
          let temp= false;
          for(let e of this.equipment){
            if(e.equip_id==equipmentbefore.equip_id){
              temp=true;
            }
          }
          if(!temp){
            todelete.push(equipmentbefore);
          }
      }
    return {add: add, todelete: todelete};
  }

  /**
   * helper function to preselect the equipment the user already has.
   */
  compareEquipment(e1: equipmentDTO, e2: equipmentDTO): boolean {
    return e1 && e2 ? e1.equip_id === e2.equip_id : e1 === e2;
  }

  /**
   * This functions is used to send a toast message and disappears after 5 seconds.
   * @param input string, the message
   */
  openSnackBar(input: string) {
    this._snackBar.open(input, "" ,{
      duration: 5000
    });
  }

  /**
   * This function changes the equipment of the selected User.
   */
   changeEquipment(){
    if(this.selectedWorkspace){
      //get the changes
      let changes = this.getEquipmentChanges();

      //add equipment
      for(let add of changes.add){
        let equipment = {
          equipment_id: add.equip_id,
          workspace_id: this.selectedWorkspace.workspace_id,
        }
        console.log(add)
        this.workspaceStoreService.addEquipmentForWorkspace(equipment).subscribe(result => {
          console.log(result);
        });
      }

      //remove equipment
      for(let deletes of changes.todelete){
        this.workspaceStoreService.deleteEquipmentForWorkspace(this.selectedWorkspace.workspace_id, deletes.equip_id).subscribe(result => {
          //console.log(result);
          if(this.selectedWorkspace){
            this.workspaceStoreService.allEquipmentForWorkspace(this.selectedWorkspace.workspace_id).subscribe(result => {
              this.equipment = result;
              this.equipmentbefore = result;
            });
            this.workspaceStoreService.allEquipment().subscribe(result => {
              this.allEquipment = result;
            })
          }

        })
      }
      //userfeedback
      this.openSnackBar("Die Ausstattung vom Arbeitsplatz " + this.selectedWorkspace + " wurde geändert.");
    }
  }

  /**adjusts the map size to the window/screen height/width */
  adjustMapSize(){
    if(document.getElementById("map")){
      const rect=document.getElementById("map")?.getBoundingClientRect(); //div of the map and corresponding to available space
      if(rect){
        if(rect.width>document.documentElement.clientHeight-rect.y){
          //if the width of the rect is bigger than the height of the possible height to show the map adjust to the possible height
          this.mapWidth= document.documentElement.clientHeight-rect.y;
        }else{
          //if the width of the rect is smaller than the height of the possible height to show the map adjust to the rect width
          this.mapWidth= rect.width;
        }
      }
    }
  }


  /**
   * Checks if a Workspace name is used by another Workspace on the same map.
   * @param name string, name of the Workspace
   * @param map_id number, id of the map
   * @param workspace_id number, id of the workspace (in case of update), or -1 (in case of create)
   * @returns boolean, false if the same workspace name is used by another workspace on the same map - true if not
   */
  checkWorkspaceName(name: string, map_id: number, workspace_id: number){
    for(let w of this.allWorkspaces){
      if(w.workspace_name==name && w.mapID == map_id && workspace_id != w.workspace_id ){
        return false;
      }
    }
    return true;
  }
  
  stringToBool(string: string){
    if(string=="true"){
      return true;
    }else{
      return false;
    }
  }
}
