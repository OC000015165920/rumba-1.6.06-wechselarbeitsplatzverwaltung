import { Component, OnInit } from '@angular/core';
import { StoreService } from 'src/app/shared/store-service.service';
import { User } from 'src/app/shared/User/user';
import { UserStoreService } from 'src/app/shared/User/user-store.service';
import { Map } from 'src/app/shared/map/Map'
import { UpdateEmployeeDto } from 'src/app/shared/User/updateEmployeeDTO';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UntypedFormControl } from '@angular/forms';
import { optionDTO } from 'src/app/shared/User/optionDTO';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmDialogModel, DialogComponent } from 'src/app/shared/Dialog/dialog.component';

/**
 * This Component is used by admins to manage the users of their department.
 */
@Component({
  selector: 'app-admin-manageusers',
  templateUrl: './admin-manageusers.component.html',
  styleUrls: ['./admin-manageusers.component.scss']
})
export class AdminManageusersComponent implements OnInit {

  /**saves all users (from the department) */
  allUsers:User[] =[];
  /**selected user to manage */
  selectedUser?: User;
  /**selected attribute */
  selectedAttribute: string="Defaultkarte";
  /**all attributes */
  attributes: string[]=["Defaultkarte", 'Rollen', "Benutzer entfernen"];
  /**if the admin is changing the default map for a user */
  isChangingDefaultMap: boolean = false;
  /**if the admin is changing the anonymous attribute for a user */
  isChangingAnonymous: boolean = false;
  /**if the admin is changing the roles of the the user */
  isChangingRoles: boolean = false;
  /**if the admin wants to remove a user */
  isRemovingUser: boolean = false;
  /**all maps for the user */
  maps: Map[]=[];
  /**selected map */
  selectedMap?: Map={id:0, name:"", imageName:""};
  /**selected anonymous value */
  selectedAnonymous: boolean = false; 
  /**the roles the user had before changes */
  rolesbefore: optionDTO[]=[];
  /**the selected roles for the user */
  roles: optionDTO[]=[];
  /**formControl for the roles */
  roleForm = new UntypedFormControl();
  /**all possible roles for the user */
  allRoles: optionDTO[]=[];

  /**
   * This constructor sets maps and selected map.
   * @param storeService StorService
   * @param userStoreService UserStoreService
   * @param _snackBar MatSnackBar
   */
  constructor(private storeService: StoreService, private userStoreService: UserStoreService, private _snackBar: MatSnackBar, private dialog: MatDialog) {
    if(storeService.localdepartment){
      console.log(storeService.localdepartment);
      this.maps = storeService.localdepartment.maps;
      if(this.maps.length>0){
        this.selectedMap = this.maps[0];
      }
    }

  }

  /**
   * This function loads all Users of the department.
   */
  ngOnInit(): void {
    this.loadAllUsers();
  }

  /**
   * This function sets selectedUser to the selected user and enables isChangingDefaultMap if all the necessary parameters are set.
   * @param userFromSelect User, the user selected
   */
  setUser(userFromSelect: User){
    this.selectedUser = userFromSelect;
    if(this.selectedAttribute==="Defaultkarte"&&this.selectedUser){
      this.isChangingDefaultMap=true;
      this.isChangingAnonymous=false;
      this.isChangingRoles=false;
      this.isRemovingUser=false;
    }else if(this.selectedAttribute==="Anonym"&&this.selectedUser){
      this.isChangingAnonymous=true;
      this.isChangingDefaultMap=false;
      this.isChangingRoles=false;
      this.isRemovingUser=false;
    }else if(this.selectedAttribute==="Rollen"&&this.selectedUser){
      this.isChangingAnonymous=false;
      this.isChangingDefaultMap=false;
      this.isChangingRoles=true;
      this.isRemovingUser=false;
      this.userStoreService.allOptionsForEmployee(this.selectedUser.employee_id).subscribe(result => {
        this.roles = result;
        this.rolesbefore = result;
      });
      this.userStoreService.allOptions().subscribe(result => {
        this.allRoles = result;
      });
    }else if(this.selectedAttribute==="Benutzer entfernen"&&this.selectedUser){
      this.isChangingAnonymous=false;
      this.isChangingDefaultMap=false;
      this.isChangingRoles=false;
      this.isRemovingUser=true;
    }else{
      this.isChangingAnonymous=false;
      this.isChangingDefaultMap=false;
      this.isChangingRoles=false;
      this.isRemovingUser=false;
    }
  }

  /**
   * This function sets selectedAttribute to the selected attribute and enables isChangingDefaultMap if all the necessary parameters are set.
   * @param attributeSelect string, the attribute selected to be changed
   */
  setAttribute(attributeSelect: string){
    this.selectedAttribute = attributeSelect;
    if(this.selectedAttribute==="Defaultkarte"&&this.selectedUser){
      this.isChangingDefaultMap=true;
      this.isChangingAnonymous=false;
      this.isChangingRoles=false;
      this.isRemovingUser=false;
    }else if(this.selectedAttribute==="Anonym"&&this.selectedUser){
      this.isChangingAnonymous=true;
      this.isChangingDefaultMap=false;
      this.isChangingRoles=false;
      this.isRemovingUser=false;
    }else if(this.selectedAttribute==="Rollen"&&this.selectedUser){
      this.isChangingAnonymous=false;
      this.isChangingDefaultMap=false;
      this.isChangingRoles=true;
      this.isRemovingUser=false;
      this.userStoreService.allOptionsForEmployee(this.selectedUser.employee_id).subscribe(result => {
        this.roles = result;
        this.rolesbefore = result;
      });
      this.userStoreService.allOptions().subscribe(result => {
        this.allRoles = result;
      });
    }else if(this.selectedAttribute==="Benutzer entfernen"&&this.selectedUser){
      this.isChangingAnonymous=false;
      this.isChangingDefaultMap=false;
      this.isChangingRoles=false;
      this.isRemovingUser=true;
    }else{
      this.isChangingAnonymous=false;
      this.isChangingDefaultMap=false;
      this.isChangingRoles=false;
      this.isRemovingUser=false;
    }
  }

  /**
   * This function loads all Users of the department.
   */
  loadAllUsers(){
    if(this.storeService.localdepartment){
      this.userStoreService.getall(this.storeService.localdepartment.name).subscribe(result =>{
        this.allUsers = result.sort((a,b) => (a.lastname > b.lastname)?1:-1);;
        this.allUsers = this.allUsers.filter(x => x.forename!="admin");
        console.log(this.allUsers);
      });
    }
  }

  /**
   * This function changes the DefaultMap for the selectedUser.
   */
  changeDefaultMap(){
    if(this.selectedMap&&this.selectedUser&&this.storeService.localdepartment){
        if(this.selectedUser){
          
          if(this.selectedMap&&this.storeService.localdepartment){
            //updated employee data
            let temp: UpdateEmployeeDto = {
              forename: this.selectedUser.forename,
              lastname: this.selectedUser.lastname,
              department: this.storeService.localdepartment.name,
              default_mapID: this.selectedMap.id,
              email_address: this.selectedUser.email_address,
            }
            console.log("update User");
            console.log(this.selectedUser.employee_id);
            this.userStoreService.updateUserAsAdmin(this.selectedUser.employee_id, temp).subscribe(result => {
              console.log("User updated");
            });

            //gives the user a message with the changes
            if(this.selectedMap&&this.selectedUser){
              this.openSnackBarMap(this.selectedMap?.name, this.selectedUser.forename + " " + this.selectedUser.lastname);
            }
          }
        }
    }
  }

  /**
   * This function changes the roles of the selected User.
   */
  changeRoles(){
    if(this.selectedUser){
      //get the changes
      let changes = this.getRoleChanges();

      //add roles
      for(let add of changes.add){
        let option = {
          options_id: add.options_id + "",
          employee_id: this.selectedUser.employee_id + "",
        }
        this.userStoreService.addOptionForEmployeeAsAdmin(option).subscribe(result => {
          console.log(result);
        });
      }

      //remove roles
      for(let deletes of changes.todelete){
        this.userStoreService.deleteOptionForEmployeeAsAdmin(this.selectedUser.employee_id, deletes.options_id).subscribe(result => {
          console.log(result);
          if(this.selectedUser){
            this.userStoreService.allOptionsForEmployee(this.selectedUser.employee_id).subscribe(result => {
              this.roles = result;
              this.rolesbefore = result;
            });
            this.userStoreService.allOptions().subscribe(result => {
              this.allRoles = result;
            })
          }
        
        })
      }
      //userfeedback
      this.openSnackBarOptions(this.selectedUser.forename + " " + this.selectedUser.lastname);
    }
  }

  /**
   * This function removes the selected User from the database.
   */
  removeUser(){
    if(this.selectedUser){
      console.log(this.selectedUser)
      this.userStoreService.deleteEmployee(this.selectedUser.employee_id).subscribe(result => {
        console.log(result);
        this.ngOnInit();
      });
    }
  }

  /**
   * Opens Dialog to Confirm Deletion of User
   */
   openDeleteDialog() {
    if(this.selectedUser){ 
      const dialogConfig = new MatDialogConfig();
      
      
      //content of Dialog
      const message = 'Soll der User ' + this.selectedUser.forename + " " + this.selectedUser.lastname + ' wirklich gelöscht werden?';
      const dialogData = new ConfirmDialogModel('Bestätigung', message);

      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;

      dialogConfig.data = dialogData;
      
      const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        
        
        if (result) {
          //actual deletion function
          this.removeUser();
        }
      });
    } 
  }   

  /**
   * This functions is used to send a toast message for changing the default map and disappears after 5 seconds.
   * @param mapname string, name of the map
   * @param username string, name of the user
   */
  openSnackBarMap(mapname: string, username: string) {
    this._snackBar.open("Die Defaultkarte von " + username + " ist nun " + mapname, "" ,{
      duration: 5000
    });
  }

  /**
   * This functions is used to send a toast message for changing the options and disappears after 5 seconds.
   * @param username string, name of the user
   */
   openSnackBarOptions(username: string) {
    this._snackBar.open("Die Rollen von " + username + " wurden geändert.", "" ,{
      duration: 5000
    });
  }

  /**
   * This function checks which roles were added and which were removed and returns them.
   * @returns {optionDTO[], optionDTO[]}, the roles added and the roles removed
   */
  getRoleChanges(): {add: optionDTO[], todelete: optionDTO[]}{
    let add: optionDTO[]=[];
    let todelete: optionDTO[]=[];
      for(let role of this.roles){
          let temp= false;
          for(let rolebefore of this.rolesbefore){
            if(role.options_id==rolebefore.options_id){
              temp=true;
            }
          }
          if(!temp){
            add.push(role);
          }
        
      }
      for(let rolebefore of this.rolesbefore){
          let temp= false;
          for(let role of this.roles){
            if(role.options_id==rolebefore.options_id){
              temp=true;
            }
          }
          if(!temp){
            todelete.push(rolebefore);
          }
      }
    return {add: add, todelete: todelete};
  }

  /**
   * helper function to preselect the roles the user already has.
   */
  compareRoles(r1: optionDTO, r2: optionDTO): boolean { 
    return r1 && r2 ? r1.options_id === r2.options_id : r1 === r2;
  }
}
