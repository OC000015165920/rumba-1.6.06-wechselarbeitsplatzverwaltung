/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminManagebookingsComponent } from './admin-managebookings/admin-managebookings.component';
import { AdminManageorgaComponent } from './admin-manageorga/admin-manageorga.component';
import { AdminManageroomsComponent } from './admin-managerooms/admin-managerooms.component';
import { AdminManageusersComponent } from './admin-manageusers/admin-manageusers.component';
import { AdminManageworkspacesComponent } from './admin-manageworkspaces/admin-manageworkspaces.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NewbookingComponent } from './newbooking/newbooking.component';

const routes: Routes = [
  {path: '',component: DashboardComponent,
    children:[
      {path:'newbooking', component: NewbookingComponent},
      {path:'managebookings', component: AdminManagebookingsComponent},
      {path:'manageusers', component: AdminManageusersComponent},
      {path:'manageworkspaces', component: AdminManageworkspacesComponent},
      {path:'manageorga', component: AdminManageorgaComponent}
      //pk, 2022/05/27: currently not used
      /*
      {path:'managerooms', component: AdminManageroomsComponent},
      */
    ]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
