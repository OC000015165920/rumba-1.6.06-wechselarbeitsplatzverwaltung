/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Booking } from 'src/app/shared/bookings/booking';
import { bookingCategories } from 'src/app/shared/bookings/bookingCategories.enum';
import { BookingsStoreService } from 'src/app/shared/bookings/bookings-store.service';
import { CreateBookingDTO } from 'src/app/shared/bookings/CreateBookingDTO';
import { ConfirmDialogModel } from 'src/app/shared/Dialog/dialog.component';
import { ConfirmDialogModelOnlyHalfday } from 'src/app/shared/dialogbooking-onlyhalfday/dialogbooking-onlyhalfday.component';
import { ConfirmDialogModelMultiDay, DialogbookingWithMultiDayComponent } from 'src/app/shared/dialogbooking-with-multi-day/dialogbooking-with-multi-day.component';
import { dialogbookingresult } from 'src/app/shared/dialogbooking/dialogbookingresult';
import { occupationDTO } from 'src/app/shared/Occupation/occupation.dto';
import { OccupationService } from 'src/app/shared/Occupation/occupation.service';
import { StoreService } from 'src/app/shared/store-service.service';
import { User } from 'src/app/shared/User/user';
import { UserStoreService } from 'src/app/shared/User/user-store.service';
import { Workspace } from 'src/app/shared/Workspace/workspace';
import { WorkspacestoreService } from 'src/app/shared/Workspace/workspacestore.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CATEGORY_NO_AB, CATEGORY_NO_HO, CATEGORY_NO_WS } from 'src/app/bookings/bookingpage/bookingpage.component';
/**
 * This component handles the bookings for the admin.
 */
@Component({
  selector: 'app-newbooking',
  templateUrl: './newbooking.component.html',
  styleUrls: ['./newbooking.component.scss']
})
export class NewbookingComponent implements OnInit {
[x: string]: any;

  /**saves all users (from the department) */
  allUsers:User[] =[];
  /**saves all occupations (from the department) */
  occupations:occupationDTO[] =[];
  /**saves all unoccupied workspaces (from the department) */
  unoccupiedWorkspaces: Workspace[] =[];
  /**saves all unoccupied users (from the department) */
  unoccupiedUsers: User[] =[];
  /**saves all bookings (from the department) */
  allBookings?: Booking[];

  /**selected date for booking */
  selectedDate: Date = new Date(Date.now());
  /**selected user for booking*/
  selectedUser?: User;
  /**selected workspace for booking */
  selectedWorkspace?: Workspace;
  /**selected category for booking (workspace/homeoffice/absence) */
  selectedCategory?: number;

  /**if selected user has already booked for the morning */
  userBookedMorning = false;
  /**if selected user has already booked for the afternoon */
  userBookedAfternoon = false;

  /**selected category is workspace*/
  bookingWorkspace = false;

  /**
   * This constructor initilizes the parameters below
   * @param userStoreService UserStoreService Service which contains the most recent user data (created by dependency injection)
   * @param workspaceStoreService WorkspacestoreService Service which contains the most recent workspace data (created by dependency injection)
   * @param bookingStoreService BookingStoreService Service which contains the most recent booking data (created by dependency injection)
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   * @param dialog MatDialog for dialogs
   * @param occupationService OccupationService Service which contains the most recent occupation data (created by dependency injection)
   */
  constructor(private userStoreService:UserStoreService, private workspaceStoreService: WorkspacestoreService, private bookingStoreService: BookingsStoreService,
    private storeService: StoreService,private dialog: MatDialog, private occupationService: OccupationService, private _snackBar: MatSnackBar) {
   }

  /**
   * This function loads all necessary data
   */
  ngOnInit(): void {
    this.unoccupiedUsers = [];

    this.loadAllUsers();

    this.loadAllBookingsForDate(this.selectedDate);
    this.loadAllOccupations(this.selectedDate);
    // pk, 20240523: fix for 20240116-0046: this function has to be called after loadAllOccupations
    this.loadUnoccupiedWorkspaces();
  }

  /**
   * This function checks if booking button should be disabled by checking if all necessary selections have been made.
   * @returns a bool if the booking button is disabled or not (true if button is disabled)
   */
  BookingButtonDisabled(){
    if(this.selectedCategory){
      if(this.selectedCategory == CATEGORY_NO_WS){
        if(this.selectedUser && this.selectedWorkspace)
          return false;
      }
      else{
        if(this.selectedUser)
          return false;
      }
    }
    return true;
  }

  /**
   * This function chances selectedWorkspace to the given workspace
   * @param workspaceFromSelect Workspace to be selected
   */
  setWorkspace(workspaceFromSelect: Workspace){
    this.selectedWorkspace = workspaceFromSelect;
  }


  /**
   * This function sets selectedUser to the given user and updates the booking states
   * @param userFromSelect User to be selected
   */
  setUser(userFromSelect: User){
    this.selectedUser = userFromSelect;
    this.checkUserBookingState(userFromSelect);
  }

  /**
   * This function updates the booking states
   * @param user User to be selected
   */
  checkUserBookingState(user: User){
    this.userBookedMorning = false;
    this.userBookedAfternoon = false;
    if(this.allBookings){
      let result = this.allBookings.find(x => x.employee_id == user.employee_id);
      if(result){
        if(result.morning){
          this.userBookedMorning = true;
        }
        if(result.afternoon){
          this.userBookedAfternoon = true;
        }
      }
    }
  }

  /**
   * This Function loads all Users, to display them in the Combobox
   */
  loadAllUsers(){
    if(this.storeService.localdepartment){
      this.userStoreService.getall(this.storeService.localdepartment.name).subscribe(result =>{
        this.allUsers = result.sort((a,b) => (a.lastname > b.lastname)?1:-1);;
        this.getUsersWithoutOccupationForDay(this.allUsers,this.selectedDate);
        this.allUsers = this.allUsers.filter(x => x.forename!="admin"); //TODO
        //console.log("Newbooking.loadAllusers(): allUsers = ", this.allUsers);
        //console.log("Newbooking.loadAllusers(): unoccupiedUsers = ", this.unoccupiedUsers);
      });
    }
  }

  /**
   * This Function loads all bookings for the specified date to allBookings and update the userbookingstates
   * @param date Date to be loaded
   */
  loadAllBookingsForDate(date: Date){
    this.bookingStoreService.getBookingsForDay(date, this.storeService.localuser.department).subscribe(result => {
      this.allBookings = result;
      if(this.selectedUser){
        this.checkUserBookingState(this.selectedUser);
      }
    })
  }

  /**
   * This function loads all occupations for the specified day for the department of the localuser
   * @param date Date to be specified
   */
  loadAllOccupations(date: Date){
    this.occupationService.getOccupationsForDay(date, this.storeService.localuser.department).subscribe( result => {
      this.occupations = result;
    })
  }

  /**
   * This Function loads all unoccupied workspaces into the attribute unoccupiedWorkspaces
   */
  loadUnoccupiedWorkspaces(){
    if(this.storeService.localdepartment){
      this.workspaceStoreService.getFreeWorkspacesForDay(this.selectedDate, this.storeService.localdepartment.name).subscribe(result =>{

        //pk, 20240523, fix for 20240116-0046
        result.forEach(workspace => {
          let tempOccupations = this.occupations.filter ( o => o.workspace_id == workspace.workspace_id );
          if (tempOccupations.length > 1) {
            console.log("loadUnoccupiedWorkspaces(): set workspace to inactive: ", workspace.workspace_name)
            workspace.active = false;
          }
        });
        //pk, 20240517, offer active workdesks, only
        this.unoccupiedWorkspaces = result.filter(x => x.active == true);
      })
    }

  }

  /**
   * This Function sets a new date as selectedDate from the event and reloads the triggers nfOnInit
   * @param event from input
   */
  setDateFromEvent(event: any){
    // store the new date value (in milliseconds)
    this.selectedDate = new Date(event.target.value);
    this.ngOnInit();
  }

  /**
   * This Function sets selectedCategory to category selected and sets bookingWorkspace to true for workspace and to false for homeoffice or absence
   * @param event: string "1": workspace, "2": homeoffice, "3": absence
   */
  setCategory(event: any){
    this.selectedCategory = event;

    if(event == CATEGORY_NO_WS){
      this.bookingWorkspace = true;
    }
    else {
      this.bookingWorkspace = false;
    }
  }

  /**
   * This Function opens booking dialog if a category and user is selected. If selectedCategory is "1"(workspace) a workspace has to be selected as well.
   */
  booking_clicked(){
    if(this.selectedCategory){
      if(this.selectedCategory == CATEGORY_NO_WS){
        if(this.selectedUser && this.selectedWorkspace)
        {
          // this.createNewBooking(this.selectedUser.employee_id,this.selectedWorkspace.workspace_id);
          this.opendialog(bookingCategories.workspace, this.selectedWorkspace)
        }
      }
      else if(this.selectedCategory == CATEGORY_NO_HO){
        if(this.selectedUser)
        {
          this.opendialog(bookingCategories.homeoffice)
        }
      }
      else if(this.selectedCategory == CATEGORY_NO_AB){
        if(this.selectedUser)
        {
          this.opendialog(bookingCategories.absence);
        }
      }
    }
  }


    /**
   * A halper function to create a confirm dialog
   * @param message the message to be shown
   * @returns a reference to a MatDialogconfig instance which can be used to open the dialog
   */
     createConfirmDialog(message:string): MatDialogConfig {
      const dialogConfig = new MatDialogConfig();
      const dialogData = new ConfirmDialogModel('Bestätigung', message);
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;

      dialogConfig.data = dialogData;
      return dialogConfig;

    }

    /**
   * This Function opens a booking dialog for specified workspace and category
   * @param bookingcategory bookingCategories ("workspace"/"homeoffice"/"absence")
   * @param workspace Workspace to be booked
   */
    opendialog(bookingcategory: bookingCategories, workspace?: Workspace){
      let datePipe = new DatePipe('en-US');
      let selectedDay = datePipe.transform(this.selectedDate, 'dd.MM.yyyy');

      //content of Dialog
      let message = 'Soll der Arbeitsplatz ' +' für den '+ selectedDay +' gebucht werden?';
      if(this.selectedUser){
        let dialogConfig = this.createConfirmDialogMultiDay(message, this.userBookedMorning, this.userBookedAfternoon, this.filterForUserOccupations(this.selectedUser, this.occupations));
        const dialogRef = this.dialog.open(DialogbookingWithMultiDayComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.startBookingProcess(result,bookingcategory,false,workspace);
          }
        });
      }
    }
  /**
   *  Opens the dialog for a halfday booking
   * @param message the message shown to the user
   * @param morningBooked indicator for morning booking
   * @param afternoonBooked indicator for morning booking
   * @returns the reference to the dialogconfig data
   */
   createConfirmDialogHalfDay(message:string, morningBooked: boolean, afternoonBooked: boolean): MatDialogConfig {
    const dialogConfig = new MatDialogConfig();
    const dialogData = new ConfirmDialogModelOnlyHalfday('Bestätigung', message,morningBooked, afternoonBooked);
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = dialogData;
    return dialogConfig;

  }

  /**
   * This function filters the given occupations for occupations which are for the given user and returns them.
   * @param user the user for which you want the occupations
   * @param occupations the base for filtering
   * @returns returns the filtered list
   */
  filterForUserOccupations(user: User, occupations: occupationDTO[]){
    let userOccupations:occupationDTO[] = [];
    for(let i of occupations){
      if(i.employee_id==user.employee_id){
        userOccupations.push(i);
      }
      // pk, 20240527: #20240116-0046 - add occupations for the workspace, too
      if (i.workspace_id == this.selectedWorkspace?.workspace_id) {
        console.log("filterForUserOccupation(): add ", i);
        userOccupations.push(i);

      }
    }
    return userOccupations;
  }

  /**
   * This function creates a multi day booking dialog.
   * @param description description for the dialog
   * @param morningBooked if the user has booked for the morning of the day at where the dialog starts
   * @param afternoonBooked if the user has booked for the afternoon of the day at where the dialog starts
   * @param upCommingOccupations occupations for the days for multi day booking for the selected workspace
   * @returns dialog configurations
   */
  createConfirmDialogMultiDay(description: string, morningBooked: boolean, afternoonBooked: boolean, upCommingOccupations?: occupationDTO[], selectedUser?: User){
    const dialogConfig = new MatDialogConfig();
    const dialogData = new ConfirmDialogModelMultiDay('Bestätigung', description, this.selectedDate, morningBooked, afternoonBooked, upCommingOccupations, this.selectedWorkspace, this.selectedUser);
    console.log(dialogData)
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = dialogData;
    return dialogConfig;
  }
  /**
   * This functions is used to send a toast message for changing user attributes and disappears after 5 seconds.
   * @param input string, the message
   */
  openSnackBar(input: string) {
    this._snackBar.open(input, "" ,{
      duration: 5000
    });
  }

  /**
   * This Function creates the booking after the dialog confirmation
   * @param dialogResult dialogbookingresult
   * @param bookingcategory bookingCategories ("workspace"/"homeoffice"/"absence")
   * @param workspace Workspace to be booked
   */
  async createBooking(dialogResult:dialogbookingresult[], bookingcategory: bookingCategories, workspace?: Workspace, date?: Date){
      if(this.selectedUser){
        //TODO: rewrite this whole stuff to avoid redundand blocks of code
        for(let temp of dialogResult){

          if(temp.date){
            // used in multi-booking page for each booking
            let newBooking: CreateBookingDTO;
            newBooking ={
              workspace_id: workspace?.workspace_id, // TODO: use a unique identifier for HOMEOFFICE indication
              employee_id: this.selectedUser.employee_id,
              booked_at: new Date(Date.now()),
              booked_for_day: this.storeService.getNumberFromDate(temp.date),
              active: true,
              morning: temp.morning,
              afternoon: temp.afternoon,
              comment: temp.note,
              category: bookingcategory,
              department: this.storeService.localuser.department,
              booked_by: "admin"
            };
            console.log("newbooking 1: ", newBooking)
            await this.bookingStoreService.createAsAdmin(newBooking).subscribe(() =>{
              // alert("buchung wurde erstellt")
              //this.createdBooking = !this.createdBooking;
              // pk, 2023/03/28, initialize data store after booking
              this.openSnackBar("Die Buchung wurde erstellt!");
              this.ngOnInit();
            });
          }else if(temp.selectedDates){
            // seems not to be used (pk, 28.3.2023)
            for(let date of temp.selectedDates){
              let newBooking: CreateBookingDTO;
              newBooking ={
                workspace_id: workspace?.workspace_id, // TODO: use a unique identifier for HOMEOFFICE indication
                employee_id: this.selectedUser.employee_id,
                booked_at: new Date(Date.now()),
                booked_for_day: this.storeService.getNumberFromDate(date),
                active: true,
                morning: temp.morning,
                afternoon: temp.afternoon,
                comment: temp.note,
                category: bookingcategory,
                department: this.storeService.localuser.department,
                booked_by: "admin"
              };
              console.log("newbooking 2: ", newBooking)
              await this.bookingStoreService.createAsAdmin(newBooking).subscribe(() =>{
                // alert("buchung wurde erstellt")
                //this.createdBooking = !this.createdBooking;
                // pk, 2023/03/28, initialize data store after booking
                this.openSnackBar("Die Buchungen wurden erstellt!");
                this.ngOnInit();
              });
            }

          }else{
            // in case of single booking: no date is set, take the current date
            let newBooking: CreateBookingDTO;
            newBooking ={
              workspace_id: workspace?.workspace_id, // TODO: use a unique identifier for HOMEOFFICE indication
              employee_id: this.selectedUser.employee_id,
              booked_at: new Date(Date.now()),
              booked_for_day: this.storeService.getNumberFromDate(this.selectedDate),
              active: true,
              morning: temp.morning,
              afternoon: temp.afternoon,
              comment: temp.note,
              category: bookingcategory,
              department: this.storeService.localuser.department,
              booked_by: "admin"
            };
            console.log("newbooking 3: ", newBooking)
            await this.bookingStoreService.createAsAdmin(newBooking).subscribe(() =>{
              // pk, 2023/03/28, initialize data store after booking
              this.openSnackBar("Die Buchung wurde erstellt!");
              this.ngOnInit();
            });
          }
        }
      }

    }

  /**
   * The function executes the booking process for the selected workspace and date
   * @param workspace the workspace clicked in GUI
   */
   startBookingProcess(dialogResult: dialogbookingresult[], bookingCategory:bookingCategories, expanded: boolean,workspace?: Workspace){
    //console.log("starte bookingprocess", dialogResult)
    /**
     * If the Booking is for a workspace, then there has to be a validation if the workspace is already occupied
     * If there is no Workspace (like Bookings as Homeoffice and Absence) then there is no need to check if there is an occupation
     */
    if(workspace){
        this.createBooking(dialogResult,bookingCategory,workspace)

    }
    else{
      //Homeoffice or Absence
      this.createBooking(dialogResult,bookingCategory)
    }

  }

  /**
   * The function filters the given userlist for users without occupation for the specified date and loads the result into the attribute unoccupiedUsers.
   * @param userlist User[]
   * @param date Date for which to filter the users
   */
  getUsersWithoutOccupationForDay(userlist: User[], date: Date){
    if(this.storeService.localdepartment){

      this.bookingStoreService.getBookingsForDay(date,this.storeService.localdepartment.name).subscribe(result =>{

        let bookings = result;
        console.log("getUsersWithoutOccupationForDay() result = ", result);
        this.bookingStoreService.getBookingsForDay(date,this.storeService.publicDepartmentName).subscribe(publicResult =>{
          console.log("getUsersWithoutOccupationForDay() publicResult = ", publicResult);
          if ( publicResult ) {
            bookings = bookings.concat(publicResult);
            console.log("add public bookings")
          }
          for (const user of userlist) {
            let bookingsForUser = bookings.filter(x => x.employee_id == user.employee_id);
            let bookingForMorning = false;
            let bookingForAfternoon = false;
            // pk, 2022/05/27: improvement for halfday bookings
            for (const booking of bookingsForUser) {
              if (booking.morning) {
                bookingForMorning = true;
              }
              if (booking.afternoon) {
                bookingForAfternoon = true;
              }
            }
            if(!bookingForMorning || !bookingForAfternoon){
              if(user.forename!="admin"){  //TODO: why forename "admin" is filtered??
                //console.log("push user "+user+" to unoccupiedUsers")
                this.unoccupiedUsers.push(user);
              }
            }
          }
        });
      });
      }
    }


}
