import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { departmentDTO, updateDepartmentDTO } from 'src/app/shared/Department/Department.dto';
import { DepartmentstoreService } from 'src/app/shared/Department/departmentstore.service';
import { StoreService } from 'src/app/shared/store-service.service';
import { createUserDTO } from 'src/app/shared/User/createUserDTO';
import { createWorkspacecolorDTO } from 'src/app/shared/Workspace/WorkspacecolorDTOs';
import { WorkspacestoreService } from 'src/app/shared/Workspace/workspacestore.service';

/**
 * This component is used for admins to change department settings.
 */
@Component({
  selector: 'app-admin-manageorga',
  templateUrl: './admin-manageorga.component.html',
  styleUrls: ['./admin-manageorga.component.scss']
})
export class AdminManageorgaComponent implements OnInit {
  /**if the admin is editing the workspace colors */
  isColorEdit: boolean = false;
  /**description of the selected color */
  colordescription: string = "";
  /**name of the department */
  name: string= "";
  /**description of the department */
  description: string = "";
  /**how many days in advance the employees in the department can book */
  //days: number = 0;
  /**the pattern in which the names will be shown in public and booking page */
  namePattern: string = "";
  /**if the infocards should be rotated like the workspaces */
  rotateInfocards: boolean = false;
  /**the selected color */
  color: string = "";
  /**the form for the input */
  orgaForm: UntypedFormGroup;
  /**the form for the workspacecolor settings */
  colorForm: UntypedFormGroup;

  /**
   * This constructor initializes the attributes of the component.
   * @param storeService StoreService is used to store common data.
   * @param departmentStoreService DepartmentstoreService is used to store and manage department data
   * @param _snackBar MatSnackBar is used for toast messages
   */
  constructor(private storeService: StoreService, private departmentStoreService: DepartmentstoreService, private workspaceStoreService: WorkspacestoreService, private _snackBar: MatSnackBar) {
    if(storeService.localdepartment){
      this.name = storeService.localdepartment.name;
      this.description = storeService.localdepartment.description;
      //this.days = storeService.localdepartment.BookingInAdvanceDays;
      this.namePattern = storeService.localdepartment.NamePatternOnMap;
      this.rotateInfocards = storeService.localdepartment.RotateInfocards;
    }
    this.orgaForm = new UntypedFormGroup({
      //name: new FormControl(this.name, [Validators.required]),
      //days: new FormControl(this.days, [Validators.required, Validators.min(1)]),
      description: new UntypedFormControl(this.description, [Validators.required]),
      namePattern: new UntypedFormControl(this.namePattern, [Validators.required]),
      rotateInfocard: new UntypedFormControl(this.rotateInfocards, [Validators.required])
    });
    this.colorForm = new UntypedFormGroup({
      workspacecolorForm: new UntypedFormControl(this.color, [Validators.required, Validators.minLength(7), Validators.maxLength(7)]),
    })
  }

  /**
   * This function is empty
   */
  ngOnInit(): void {
  }

  /**
   * This function applies the changes to the department if the settings are valid.
   */
  change(){
    if(this.isNamePatternValid()){
      if(this.storeService.localdepartment){
        let temp: updateDepartmentDTO={
          name: this.name,
          description: this.description,
          NamePatternOnMap: this.namePattern,
          RotateInfocards: Boolean(this.rotateInfocards),
        }
        let id= this.storeService.localdepartment.id;
        this.departmentStoreService.updateDepartment(id, temp).subscribe(result => {
          if(this.storeService.localdepartment){
            //this.storeService.localdepartment.name = this.name;
            this.storeService.localdepartment.description = this.description;
            //this.storeService.localdepartment.BookingInAdvanceDays = this.days;
            this.storeService.localdepartment.NamePatternOnMap = this.namePattern;
          }
          this.openSnackBar("Die Organisationseinheitsdaten wurden geändert.")
        });
      }

    }
  }

  /**
   * This function checks if the namePattern is valid.
   * @returns boolean, true if the namePattern is a valid name pattern
   */
  isNamePatternValid():boolean{
    if(this.namePattern == "forename lastname"){
      return true
    }
    else if(this.namePattern == "forename, lastname"){
      return true;
    }
    // pk, 2021/12/16: new pattern added (forename without comma)
    else if(this.namePattern == "forename l."){
      return true;
    }
    else if(this.namePattern == "forename, l."){
      return true;
    }
    else if(this.namePattern == "f. lastname"){
      return true;
    }
    else if(this.namePattern == "lastname forename"){
      return true;
    }
    else if(this.namePattern == "lastname, forename"){
      return true;
    }
    else if(this.namePattern == "lastname, f."){
      return true;
    }else{
      return false;
    }
  }

  /**
   * This functions is used to send a toast message for changing user attributes and disappears after 5 seconds.
   * @param input string, the message
   */
   openSnackBar(input: string) {
    this._snackBar.open(input, "" ,{
      duration: 5000
    });
  }

  /**
   * This function sets selectedColor to the color selected and adjust other variables accordingly.
   */
  setColor(){
    if(this.color!=""&&this.storeService.localdepartment){
      this.isColorEdit=true;

      this.workspaceStoreService.findWorkspacecolorByColorAndDepartment(this.storeService.localdepartment.name, this.color).subscribe(result => {
        if(result){
          this.colordescription = result.description;
        }

      }, err => {
        this.colordescription = "";
      })
    }
  }

  /**
   * This function changes the workspacecolor object or creates it of it does not exist yet. Or it removes it if description is empty.
   */
  changeColor(){
    if(this.color&& this.storeService.localdepartment){
      this.workspaceStoreService.findWorkspacecolorByColorAndDepartment(this.storeService.localdepartment.name, this.color).subscribe(result => {
        if(this.colordescription==""){
          this.workspaceStoreService.deleteWorkspacecolor(result.id).subscribe(result => {
            console.log(result);
          });
        }else{
          this.workspaceStoreService.updateWorkspacecolor({description: this.colordescription}, result.id).subscribe(result =>{
            console.log(result);
          })
        }

      }, err => {
        if(this.storeService.localdepartment){
          let temp: createWorkspacecolorDTO = {
            color: this.color,
            description: this.colordescription,
            department: this.storeService.localdepartment.name,
          }
          this.workspaceStoreService.createWorkspacecolor(temp).subscribe(result => {
            console.log(result)
          });
        }
      })
    }
  }

  stringToBool(string: string){
    if(string=="true"){
      return true;
    }else{
      return false;
    }
  }
}
