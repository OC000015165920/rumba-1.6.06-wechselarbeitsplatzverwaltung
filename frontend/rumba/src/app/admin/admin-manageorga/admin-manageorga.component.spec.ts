import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminManageorgaComponent } from './admin-manageorga.component';

describe('AdminManageorgaComponent', () => {
  let component: AdminManageorgaComponent;
  let fixture: ComponentFixture<AdminManageorgaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminManageorgaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminManageorgaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
