/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicComponent } from './public/public.component';
import { PublicdashboardComponent } from './publicdashboard/publicdashboard.component';

const routes: Routes = [
  {path: '', component: PublicComponent},
  {path: 'terminal/:terminal', component: PublicComponent},
  {path: 'terminal/:terminal/:department', component: PublicdashboardComponent},
  {path: 'terminal/:terminal/:department/:map_id', component: PublicdashboardComponent},
  {path: ':department', component: PublicdashboardComponent},
  {path: ':department/:map_id', component: PublicdashboardComponent}
];

@NgModule({

  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicLandingpageRoutingModule { }
