/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicLandingpageRoutingModule } from './public-landingpage-routing.module';
import { PublicdashboardComponent } from './publicdashboard/publicdashboard.component';
import { BookingsModule } from '../bookings/bookings.module';
import { SharedModule } from '../shared/shared.module';
import { AngularMaterialModule } from '../angular-material.module';
import { PublicComponent } from './public/public.component';
import { ChooseMapDialogComponent } from './public/choose-map-dialog/choose-map-dialog.component';


@NgModule({
  declarations: [
    PublicdashboardComponent,
    PublicComponent,
    ChooseMapDialogComponent,
  ],
  imports: [
    CommonModule,
    PublicLandingpageRoutingModule,
    BookingsModule,
    SharedModule,
    AngularMaterialModule
  ]
})
export class PublicLandingpageModule { }
