/*
RUMBA Frontend
Copyright (C) 2021 Kreis Soest

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { StoreService } from 'src/app/shared/store-service.service';
import { occupationDTO } from 'src/app/shared/Occupation/occupation.dto';
import { OccupationService } from 'src/app/shared/Occupation/occupation.service';
import { HomeofficeStoreService } from 'src/app/shared/Homeoffice/homeoffice-store.service';
import { homeofficeDTO } from 'src/app/shared/Homeoffice/homeoffice.dto';
import { Workspace } from 'src/app/shared/Workspace/workspace';
import { WorkspacestoreService } from 'src/app/shared/Workspace/workspacestore.service';
import { ActivatedRoute} from '@angular/router';
import { departmentDTO } from 'src/app/shared/Department/Department.dto';
import { DepartmentstoreService } from 'src/app/shared/Department/departmentstore.service';
import { absenceDTO } from 'src/app/shared/absence/absence.dto';
import { AbsenceStoreService } from 'src/app/shared/absence/absence-store.service';
import { Map } from 'src/app/shared/map/Map'
import { tempUser } from 'src/app/shared/search/search.component';
import { Workspacecolor } from 'src/app/shared/Workspace/WorkspacecolorDTOs';

/**
 * The class to handle the overview page (shows the bookings before the user has logged in)
 */
@Component({
  selector: 'app-publicdashboard',
  templateUrl: './publicdashboard.component.html',
  styleUrls: ['./publicdashboard.component.scss']
})

export class PublicdashboardComponent implements OnInit, OnDestroy {

  /**selected date to show bookings */
  public selectedDate= new Date(Date.now());

  /**store for booked workspaces on all maps */
  occupationsForDay?:occupationDTO[];
  /**store for booked workspaces on the selected map */
  occupationsForDayOnMap?:occupationDTO[];
  /**reference to all active workspaces */
  allActiveWorkspaces?:Workspace[];
   /**the workspacecolors for the department */
  workspacecolors: Workspacecolor[]=[];
  /**list of users booked for HomeOffice */
  homeofficeUser?: homeofficeDTO[];
  /**list of users booked for absence */
  AbsenceUser?: absenceDTO[];

  /** contains the name of the image to be shown */
  floorplanName = '';
  /** don't allow bookings on the overview page */
  allowBookingOnMap = false;

  /**disables the decrement button on the html if true */
  disableDecrementButton = false;
  /**saves the date of today */
  today = new Date(Date.now());
  /**the last date to book for (28 days from today for now) */
  maxBookingDate = new Date();
  /**if the bookings should be shown as a table instead of the map */
  showTableInsteadOfMap = false;
  /**text for the toggle to change between morning and afternoon */
  toggleMorningAfternoonText = "Nachmittag anzeigen";

  /**if it is morning */
  isMorning: boolean=false;
  /**is not used */
  departmentFromURL ="";
  /**saves the department for which to show this page */
  public department?: departmentDTO

  /**all maps for the department */
  maps: Map[]=[];
  /**selected map */
  selected: Map={id:0, name:"", imageName:""};

  /**the occupation of the searched for user */
  searchedOccupation?: occupationDTO;
  /**the homeoffice entry of the searched for user */
  searchedHomeoffice?: homeofficeDTO;
  /**the absence entry of the searched for user */
  searchedAbsence?: absenceDTO;

  /**map name by url */
  public map_id?: string;

  /** Defines for scheduled data load */
  SCHEDULE_LOADDATA = 120000; // reload data every 2 minutes
  loadInterval?: NodeJS.Timeout;

  /** Defines for terminal mode */
  SCHEDULE_TERMINAL_BACKJUMP = 60000; // jump back to department view every 60 seconds
  terminalInterval?: NodeJS.Timeout;

  /**
   * This constructor initilizes the component with the parameters below
   * @param workspaceStoreService WorkspacestoreService Service which contains the most recent workspace data (created by dependency injection)
   * @param storeService StoreService Service which contains the most recent common data (created by dependency injection)
   * @param occupationService OccupatiomService Service which contains the most recent occupation data (created by dependency injection)
   * @param homeOfficeStoreService HomeofficeStoreService Service which contains the most recent homeoffice data (created by dependency injection)
   * @param route ActivatedRoute
   * @param departmentStoreService DepartmentstoreService Service which contains the most recent department data (created by dependency injection)
   * @param absenceStoreService AbsenceStoreService Service which contains the most recent absence data (created by dependency injection)
   */
  constructor(private workspaceService: WorkspacestoreService, public storeService: StoreService,
    private occupationService: OccupationService, private homeofficeStoreService: HomeofficeStoreService,
    private route: ActivatedRoute, private departmentStoreService: DepartmentstoreService, private absenceStoreService: AbsenceStoreService) {
      this.route.paramMap.subscribe(params => {
        this.storeService.terminal = params.get('terminal')=="terminal";
      });
      this.storeService.terminalpublic = false;
    }

  /**
   * the init method called by Angular
   */
  async ngOnInit() {

    await this.route.paramMap.subscribe(params => {
      let departmentid = params.get('department');
      let map_id = params.get('map_id');
      if(departmentid){

        this.departmentStoreService.getDepartmentByName(departmentid).then(result =>{
          this.department = result;

          if(this.department){

            this.maps = this.department.maps;
            if(this.maps.length>0){
              if(map_id){
                for(let map of this.maps){
                  if(map.id+""==map_id){
                    this.selected = map;
                  }
                }
              }
              if(this.selected.id==0){
                this.selected = this.maps[0];
              }

            }
            if(this.selected){
              this.storeService.selectedMap=this.selected;
            }
          }
          this.loadData();


          this.maxBookingDate =  this.storeService.addDaysOnDate(this.today, this.department.BookingInAdvanceDays);
        })
      }
    });

    // loads all data again in an 120 second interval
    this.loadInterval = setInterval(() => {
      //console.log("load new data")
      this.loadData();
    }, this.SCHEDULE_LOADDATA);

    if (this.storeService.terminal) {
      this.terminalInterval = setInterval(() => {
        //console.log("setInterval(): terminal="+this.storeService.terminal+", terminalpublic="+this.storeService.terminalpublic);
        if (this.storeService.terminal && !this.storeService.terminalpublic) {
          // we are in terminal mode, but the department page is not active ==> jump back after several seconds
          this.storeService.router.navigate(['public/terminal/terminal/']);

        }
      }, this.SCHEDULE_TERMINAL_BACKJUMP);
    }

  }
  /**
   * the destroy method called by Angular
   */
  async ngOnDestroy() {
    //console.log("ngOnDestroy() called");

    if (this.loadInterval) {
      //console.log("stop load interval")
      clearInterval(this.loadInterval)
    }
    if (this.terminalInterval) {
      //console.log("stop jump back interval")
      clearInterval(this.terminalInterval)
    }
  }


  /**
  * The function initialize all data fields
  */
  loadData(){

    this.loadAllWorkspaces();
    this.loadWorkspacecolordescriptions();
    this.loadoccupationsForDayOnMap();
    this.loadoccupationsForDay();
    this.loadHomeofficeForDay();
    this.loadAbsenceForDay();

    if(new Date().getHours()<13||(new Date().getHours()==12&&new Date().getMinutes()<30)){
      this.isMorning=true;
    }else{
      this.isMorning=false;
    }
  }

  /**
   * This function refreshes the map when the selected map changes.
   */
  refresh(){
    if(this.selected){
      this.storeService.selectedMap=this.selected;

    }
    this.loadAllWorkspaces();
    this.loadoccupationsForDayOnMap();
  }

  /**
   * This function is called if the user selects another date inside the date picker
   * @param event // the selected day
   */
  getDate(event: any){
    if(new Date(event.target.value) > this.today){
      this.disableDecrementButton = false
    }
    this.selectedDate = new Date(new Date(event.target.value).getTime());
    this.loadData();
  }

  /**
   * The function is called from '+' button and shall add one day to the current selection, then change the internal values accordingly
   */
  onIncrementCurrentDay(){
    this.selectedDate = new Date(this.selectedDate.setDate(this.selectedDate.getDate()+1));
    this.loadData();
    this.disableDecrementButton = false;
    this.storeService.showOnlyMorningBookings=true;
    this.storeService.showOnlyAfternoonBookings=false;
  }
 /**
   * The function is called from '-' button and shall subtract one day to the current selection, then change the internal values accordingly
   */
  onDecrementCurrentDay(){
    let date = this.selectedDate;
    let newDate = this.storeService.addDaysOnDate(date,-1);

    if(newDate.setHours(0,0,0,0) >= this.storeService.addDaysOnDate(new Date(Date.now()),-3).setHours(0,0,0,0)){
      this.disableDecrementButton = false;

      this.selectedDate = newDate;
      this.loadData();
    }
    else{
      this.disableDecrementButton = true;
    }
    this.storeService.showOnlyMorningBookings=true;
    this.storeService.showOnlyAfternoonBookings=false;

  }

   /**
   * The function loads the availalbe workspaces for the day specified in this.selectedDate, and stores prepares the infocards array
    */
  async loadAllWorkspaces(){
    if(this.department&&this.selected){
      this.workspaceService.getAllForMap(this.department?.name, this.selected?.id).subscribe(result =>{
        //pk, 20240517: show only active workdesks
        this.allActiveWorkspaces = result.filter(x => x.active ==true);
      })
    }

  }

  /**
   * This function loads all Workspacecolors for the department.
   */
  async loadWorkspacecolordescriptions(){
    //console.log("loadWorkspacecolordescriptions() called, department =", this.department);
    if(this.workspaceService&&this.department){
      await this.workspaceService.getAllWorkspacecolorsForDepartment(this.department.name).subscribe(result => {
        this.workspacecolors = result;
        //console.log("getAllWorkspacecolorsForDepartment() returned, count =", this.workspacecolors.length);
      });
    }
    //console.log("loadWorkspacecolordescriptions() done");
  }
  /**
   * The function loads the all occupations for the day into the attribute occupationsForDayOnMap.
   */
  loadoccupationsForDayOnMap(){
    if(this.department&&this.selected){
      this.occupationService.getOccupationsForDayOnMap(this.selectedDate, this.department.name, this.selected.id).subscribe(result =>{
        this.occupationsForDayOnMap = result;
        console.log("Occupatons: ",result)
      });
    }

  }

  /**
   * This function loads all the occupations for the day not only on the map.
   */
  loadoccupationsForDay(){
    if(this.department&&this.selected){
      this.occupationService.getOccupationsForDay(this.selectedDate, this.department.name).subscribe(result =>{
        this.occupationsForDay = result;
      });
    }

  }

  /**
   * The function loads the homeoffice bookings for the day into the attribute homeOfficeUser.
   */
  loadHomeofficeForDay(){
    if(this.department){
      this.homeofficeStoreService.getHomeofficeForDay(this.selectedDate, this.department.name).subscribe(result =>{
        this.homeofficeUser = result.sort((a,b) => (a.lastname > b.lastname)?1:-1);
      })
    }

  }

  /**
   * The function loads the absence bookings for the day into the attribute AbsenceUser.
   */
  loadAbsenceForDay(){
    if(this.department){
      this.absenceStoreService.getAbsenceForDate(this.selectedDate, this.department.name).subscribe(result =>{
        this.AbsenceUser = result.sort((a,b) => (a.lastname > b.lastname)?1:-1);
      })
    }

  }

  // loadAllRooms(){
  //   if(this.department){
  //     this.roomstoreservice.getall(this.department.name).subscribe(result =>{
  //       this.allRooms = result;
  //     })
  //   }
  //   }

  /**
   * The function checks if an entry in Homeoffice or Absence should be shown depending on halfdaybookings and if only morning or afternoon should be shown.
   */
  showEntry(homeofficeData?:homeofficeDTO, absenceData?: absenceDTO){
    let returnValue = false;
    if(homeofficeData){
      if(homeofficeData.morning == this.storeService.showOnlyMorningBookings || homeofficeData.afternoon == this.storeService.showOnlyAfternoonBookings ){
        returnValue = true;
      }
    }

    if(absenceData){
      if(absenceData.morning == this.storeService.showOnlyMorningBookings || absenceData.afternoon == this.storeService.showOnlyAfternoonBookings ){
        returnValue = true;
      }
    }

    return returnValue
  }

  /**
   * This function finds the occupation/homeoffice/absenc booking of the searched for user and highlights him/her and changes the map if necessary.
   * @param event the user which was searched for
   */
  searchUser(event: tempUser){
    console.log(event.employee_id);
    this.searchedHomeoffice=undefined;
    this.searchedAbsence=undefined;
    this.searchedOccupation=undefined;
    if(this.occupationsForDay&&this.AbsenceUser&&this.homeofficeUser){
      for(let occupation of this.occupationsForDay){
        //if the searched user is in occupations set the searchedOccupation on the searched one
        if(event.employee_id===occupation.employee_id){
          this.searchedOccupation = occupation;
          //change map
          for(let map of this.maps){
            if(map.id==occupation.workspace_mapID){
              this.storeService.selectedMap=map;
              this.selected=map;
              this.refresh();
            }
          }
        }
      }
      for(let absence of this.AbsenceUser){
        //if the searched user is in absence set the searchedAbsence on the searched one
        if(event.employee_id==absence.employee_id){
          this.searchedAbsence = absence;
        }
      }
      for(let homeoffice of this.homeofficeUser){
        //if the searched user is in homeoffice set the searchedHomeoffice on the searched one
        if(event.employee_id==homeoffice.employee_id){
          this.searchedHomeoffice = homeoffice;
        }
      }
    }
    //this.loadData();
  }

  /**
   * This function checks if the homeoffice booking is the one with the user searched for.
   * @param homeoffice the homeoffice to be checked
   * @returns boolean if the entry is the one searched
   */
  searchHomeoffice(homeoffice: homeofficeDTO){
    if(homeoffice===this.searchedHomeoffice){
      return true;
    }
    return false;
  }

  /**
   * This function checks if the absence booking is the one with the user searched for.
   * @param homeoffice the absence to be checked
   * @returns boolean if the entry is the one searched
   */
  searchAbsence(absence: absenceDTO){
    if(absence===this.searchedAbsence){
      return true;
    }
    return false;
  }


  /**
   * Called when the user switches between morning's and afternoon's view on the map
   */
   onToggleChangeMorningAfternoon(){
    this.storeService.showOnlyAfternoonBookings = !this.storeService.showOnlyAfternoonBookings;
    this.storeService.showOnlyMorningBookings =!this.storeService.showOnlyMorningBookings;

    if(this.storeService.showOnlyAfternoonBookings){
      this.toggleMorningAfternoonText = "Vormittag anzeigen"
    }
    else{
      this.toggleMorningAfternoonText = "Nachmittag anzeigen"
    }

    this.loadData();
  }

  /**
   * This function returns an Array of the workspaces (IDs) that are reserved but free due to bookings and when they are free.
   * @returns {workspace_id: number, morning: boolean, afternoon: boolean}[], an Array of the workspaces (IDs) that are reserved but free due to bookings and when they are free.
   */
  getFreeReservedWorkspaces():{workspace_id: number, morning: boolean, afternoon: boolean}[]{
    let list:{workspace_id: number, morning: boolean, afternoon: boolean}[] = [];
    if(this.allActiveWorkspaces){
      for(let w of this.allActiveWorkspaces){
        if(w.employee_id && w.bookableWhenHO){
          this.homeofficeUser?.find((e)=> {
            if(e.employee_id==w.employee_id){
              let temp = list.findIndex((e)=>{return e.workspace_id == w.workspace_id});
              if(temp>=0){
                if(e.morning){
                  list[temp].morning=true;
                }
                if(e.afternoon){
                  list[temp].afternoon=true;
                }
              }else{
                list.push({workspace_id: w.workspace_id, morning: e.morning, afternoon: e.afternoon})
              }
            }
          })
          this.AbsenceUser?.find((e)=> {
            if(e.employee_id==w.employee_id){
              let temp = list.findIndex((e)=>{return e.workspace_id == w.workspace_id});
              if(temp>=0){
                if(e.morning){
                  list[temp].morning=true;
                }
                if(e.afternoon){
                  list[temp].afternoon=true;
                }
              }else{
                list.push({workspace_id: w.workspace_id, morning: e.morning, afternoon: e.afternoon})
              }
            }
          })
          this.occupationsForDayOnMap?.find((e)=> {
            if(e.employee_id==w.employee_id){
              let temp = list.findIndex((e)=>{return e.workspace_id == w.workspace_id});
              if(temp>=0 && list[temp].workspace_id!=e.workspace_id){
                if(e.morning){
                  list[temp].morning=true;
                }
                if(e.afternoon){
                  list[temp].afternoon=true;
                }
              }else{
                list.push({workspace_id: w.workspace_id, morning: e.morning, afternoon: e.afternoon})
              }
            }
          })
        }
      }
    }
    return list;
  }
}
