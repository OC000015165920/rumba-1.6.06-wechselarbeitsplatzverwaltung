import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { departmentDTO } from 'src/app/shared/Department/Department.dto';
import { DepartmentstoreService } from 'src/app/shared/Department/departmentstore.service';
import { ConfirmDialogModel } from 'src/app/shared/Dialog/dialog.component';
import { Map } from 'src/app/shared/map/Map';
import { StoreService } from 'src/app/shared/store-service.service';
import { ChooseMapDialogComponent, ChooseMapDialogModel } from './choose-map-dialog/choose-map-dialog.component';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.scss']
})
export class PublicComponent implements OnInit {
  allDepartments: departmentDTO[] = [];

  constructor(public storeService: StoreService, private dialog: MatDialog, private router: Router, private route: ActivatedRoute, private departmentStoreService: DepartmentstoreService) {
    this.route.paramMap.subscribe(params => {
      this.storeService.terminal = params.get('terminal')=="terminal";
    });
    if(this.storeService.terminal){
      this.storeService.terminalpublic = true;
    }
  }

  ngOnInit(): void {
    this.loadAllDepartments();
  }

  loadAllDepartments(){
    this.departmentStoreService.getAllDepartments().subscribe(result => {
      this.allDepartments = result;
      this.allDepartments.sort((a,b)=> a.name<b.name ? -1:1)
    })
  }

  onButtonClick(department: departmentDTO, map: Map){
    if(this.storeService.terminal){
      this.router.navigate(['public/terminal/terminal/' + department.name + '/' + map.id])
    }else{
      this.router.navigate(['public/' + department.name + '/' + map.id])
    }
  }

  onCardClick(department: departmentDTO){
    let temp = "";
    if(this.storeService.terminal){
      temp = "terminal/terminal/"
    }
    if(department.maps.length<=1){
      this.router.navigate(['public/'+ temp + department.name]);
    }else{
      this.openDialog(department);
    }
  }
  

  openDialog(department: departmentDTO){
    let dialogConfig = this.createConfirmDialogMap("Wählen Sie die gewünschte Karte von Abteilung " + department.name + " aus.", department);
    const dialogRef = this.dialog.open(ChooseMapDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result: { id: number; }) => {
      let temp = "";
      if(this.storeService.terminal){
        temp = "terminal/terminal/"
      }
      if (result) {
        this.router.navigate(['public/' + temp + department.name + "/" + result.id]);
      }
    }); 
  }
  
  createConfirmDialogMap(message:string, department: departmentDTO): MatDialogConfig {
    const dialogConfig = new MatDialogConfig();
    const dialogData = new ChooseMapDialogModel("Karte", message, department);
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = dialogData;
    return dialogConfig;
    
  }
}
