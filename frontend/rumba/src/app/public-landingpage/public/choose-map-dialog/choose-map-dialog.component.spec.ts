import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseMapDialogComponent } from './choose-map-dialog.component';

describe('ChooseMapDialogComponent', () => {
  let component: ChooseMapDialogComponent;
  let fixture: ComponentFixture<ChooseMapDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChooseMapDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseMapDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
