import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { departmentDTO } from 'src/app/shared/Department/Department.dto';
import { Map } from 'src/app/shared/map/Map';

@Component({
  selector: 'app-choose-map-dialog',
  templateUrl: './choose-map-dialog.component.html',
  styleUrls: ['./choose-map-dialog.component.scss']
})
export class ChooseMapDialogComponent implements OnInit {
  title: string;
  description: string;
  department: departmentDTO;
  constructor(private dialogRef: MatDialogRef<ChooseMapDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:ChooseMapDialogModel ) {
    this.title = data.title;
    this.description = data.description;
    this.department = data.department;
  }

  ngOnInit(): void {
  }

  onButtonClick(map: Map){
    this.dialogRef.close(map);
  }

  /**
   * The function closes the dialog without results.
   */
  close() {
    this.dialogRef.close();
  }
}

export class ChooseMapDialogModel{
  
  /**
   * This constructor initilizes the class with the parameters below
   * @param title string, title of the dialog
   * @param description string, description of the dialog
   * @param department departmentDTO, department of the dialog
   */
   constructor(public title: string, public description: string, public department: departmentDTO) {
  }
}
