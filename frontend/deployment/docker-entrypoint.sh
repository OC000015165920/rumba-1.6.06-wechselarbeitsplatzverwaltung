#!/bin/sh
#
# RUMBA Frontend
# Copyright (C) 2021 Kreis Soest 

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
echo "Frontend is running against following backend url: ${API_URL}";
echo "{\"apiUrl\":\"${API_URL}\",
\"keycloak_url\": \"${KEYCLOAK_URL}\",
\"keycloak_realm\": \"${KEYCLOAK_REALM}\",
\"keycloak_clientId\": \"${KEYCLOAK_CLIENTID}\"}" > /app/assets/config/config.json;
