#
# RUMBA Frontend
# Copyright (C) 2021 Kreis Soest 

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
FROM node:18 As builder

#RUN npm install -g @angular/cli@latest

ARG APP_NAME
ENV APP_NAME=$APP_NAME

RUN npm install -g @angular/cli@latest

# Specify our working directory, this is in our container/in our image
WORKDIR /work/

# Copy the package.jsons from host to container
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY ./frontend/$APP_NAME/package*.json ./

# Here we install all the deps
RUN npm install

# Bundle app source / copy all other files
COPY ./frontend/$APP_NAME/ /work/

# Build the app to the /dist folder
RUN npm run build

FROM bitnami/nginx:latest

ENV API_URL="http://localhost:3000"

# Copy angular app
COPY --from=builder /work/dist/* /app

USER root
RUN chown -R 1001:1001 /app

COPY ./frontend/deployment/docker-entrypoint.sh /var
RUN chown -R 1001:1001 /var/docker-entrypoint.sh
RUN chmod +x /var/docker-entrypoint.sh

USER 1001

COPY ./frontend/deployment/angular.conf /opt/bitnami/nginx/conf/bitnami/

ENTRYPOINT /var/docker-entrypoint.sh && nginx -g 'daemon off;'
