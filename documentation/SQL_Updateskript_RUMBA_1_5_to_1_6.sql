/*
RUMBA SQL Updatescript
Copyright (C) 2022 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	This script is used to update the database for RUMBA from version 1.5 to version 1.6. You have to write your databases name into the databasename variable before using the script.
*/
SET NAMES utf8mb4;
SET databasename = '';

USE databasename;

CREATE TABLE `map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `imageName` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `mapping_department_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mapID` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `map` (`name`, `imageName`)
SELECT '', `FloorplanName` FROM `department`;

INSERT INTO `mapping_department_map` (`mapID`, `department`)
SELECT `map`.`id`, `department`.`name` FROM `department` INNER JOIN `map` ON `department`.`FloorplanName`=`map`.`imageName`;

ALTER TABLE `department`
ADD `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
ADD `modificationDate` timestamp NOT NULL DEFAULT current_timestamp(),
ADD `modifiedBy` varchar(255) NOT NULL DEFAULT '',
ADD `modifiedByUsername` varchar(255) NOT NULL DEFAULT '',
DROP COLUMN `FloorplanName`;

ALTER TABLE `employee`
ADD `default_mapID` int(11) NOT NULL DEFAULT -1,
ADD `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
ADD `modificationDate` timestamp NOT NULL DEFAULT current_timestamp(),
ADD `modifiedBy` varchar(255) NOT NULL DEFAULT '',
ADD `modifiedByUsername` varchar(255) NOT NULL DEFAULT '';

CREATE TABLE `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modificationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedBy` varchar(255) NOT NULL DEFAULT '',
  `modifiedByUsername` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `mapping_equipment_workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_id` int(11) NOT NULL,
  `workspace_id` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `mapping_options_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `options_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modificationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedBy` varchar(255) NOT NULL DEFAULT '',
  `modifiedByUsername` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `option` (`id`, `name`) VALUES
('1',	'anonymous'),
('2',   'Ersthelfer');

ALTER TABLE `room`
ADD `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
ADD `building_id` varchar(255) NOT NULL DEFAULT '0020';

DROP TABLE `style_room`;

ALTER TABLE `workspace`
RENAME COLUMN `x1` TO `x`,
RENAME COLUMN `y1` TO `y`,
DROP COLUMN `x2`,
DROP COLUMN `x3`,
DROP COLUMN `x4`,
DROP COLUMN `y2`,
DROP COLUMN `y3`,
DROP COLUMN `y4`,
DROP COLUMN `reserved`,
DROP COLUMN `reserved_ForOrgaUnit`,
DROP COLUMN `reserved_description`,
ADD `mapID` int(11) NOT NULL,
ADD `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
ADD `rotation` int(11) NOT NULL DEFAULT 0,
ADD `scale` double NOT NULL DEFAULT 1.0,
ADD `bookableWhenHO` tinyint(4) NOT NULL DEFAULT 1,
ADD `employee_id` int(11),
ADD `modificationDate` timestamp NOT NULL DEFAULT current_timestamp(),
ADD `modifiedBy` varchar(255) NOT NULL DEFAULT '',
ADD `color_id` int(11) NOT NULL DEFAULT 1;

/* mapID in workspace */
UPDATE `workspace` 
INNER JOIN `mapping_department_map` 
ON `workspace`.`department` = `mapping_department_map`.`department`
SET `workspace`.`mapID`=`mapping_department_map`.`mapID`;

CREATE TABLE `workspacecolor` (
  `color` varchar(255) NOT NULL DEFAULT '000000',
  `description` varchar(255) NOT NULL DEFAULT '',
  `department` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `workspacecolor` (`color`, `description`, `department`) 
SELECT 'd9d9d9', 'normaler Arbeitsplatz', `name` FROM `department`;

DROP VIEW IF EXISTS `absence_view`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `absence_view` AS select `booking`.`booking_id` AS `booking_id`,`booking`.`booked_for_day` AS `booked_for_day`,`booking`.`morning` AS `morning`,`booking`.`comment` AS `comment`,`booking`.`afternoon` AS `afternoon`,`booking`.`employee_id` AS `employee_id`,`employee`.`forename` AS `forename`,`employee`.`lastname` AS `lastname`,`employee`.`department` AS `department`,`employee`.`email_address` AS `email` from (`booking` join `employee` on(`employee`.`employee_id` = `booking`.`employee_id`)) where `booking`.`category` = 3;

DROP VIEW IF EXISTS `homeoffice_view`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `homeoffice_view` AS select `booking`.`booking_id` AS `booking_id`,`booking`.`booked_for_day` AS `booked_for_day`,`booking`.`morning` AS `morning`,`booking`.`comment` AS `comment`,`booking`.`afternoon` AS `afternoon`,`booking`.`employee_id` AS `employee_id`,`employee`.`forename` AS `forename`,`employee`.`lastname` AS `lastname`,`employee`.`department` AS `department`,`employee`.`email_address` AS `email` from (`booking` join `employee` on(`employee`.`employee_id` = `booking`.`employee_id`)) where `booking`.`category` = 2;

DROP VIEW IF EXISTS `occupied_workspace`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `occupied_workspace` AS select `booking`.`booking_id` AS `booking_id`,`booking`.`workspace_id` AS `workspace_id`,`booking`.`booked_for_day` AS `booked_for_day`,`booking`.`morning` AS `morning`,`booking`.`comment` AS `comment`,`booking`.`afternoon` AS `afternoon`,`booking`.`employee_id` AS `employee_id`,`workspace`.`workspace_name` AS `workspace_name`,`workspace`.`mapID` AS `workspace_mapID`,`workspace`.`x` AS `workspace_x`,`workspace`.`y` AS `workspace_y`,`workspace`.`rotation` AS `workspace_rotation`,`workspace`.`scale` AS `workspace_scale`,`workspace`.`bookableWhenHO` AS `workspace_bookableWhenHO`,`employee`.`forename` AS `forename`,`employee`.`lastname` AS `lastname`,`employee`.`department` AS `department`,`employee`.`email_address` AS `email` from ((`booking` join `workspace` on(`workspace`.`workspace_id` = `booking`.`workspace_id`)) join `employee` on(`employee`.`employee_id` = `booking`.`employee_id`)) where `booking`.`category` = 1;
