/*
RUMBA SQL Rolloutscript
Copyright (C) 2022 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	To use this script you need to set the attributes department and databasename. databasename is the name the database will be given.
	department is the name of the first department which will be made.
*/

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';
SET department = '';
SET databasename = '';

SET NAMES utf8mb4;

CREATE DATABASE databasename /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE databasename;



CREATE TABLE `booking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `workspace_id` int(11) DEFAULT NULL,
  `booked_for_day` int(11) NOT NULL,
  `booked_at` datetime NOT NULL,
  `category` int(11) NOT NULL,
  `department` varchar(255) NOT NULL DEFAULT '1',
  `morning` tinyint(4) NOT NULL DEFAULT 1,
  `comment` varchar(255) NOT NULL DEFAULT '',
  `afternoon` tinyint(4) NOT NULL DEFAULT 1,
  `booked_by` varchar(255) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`booking_id`),
  KEY `FK_ddeea2dd0dfa9414959dec60f80` (`employee_id`),
  KEY `FK_61da783c191bff56f00e2736dbb` (`workspace_id`),
  CONSTRAINT `FK_61da783c191bff56f00e2736dbb` FOREIGN KEY (`workspace_id`) REFERENCES `workspace` (`workspace_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ddeea2dd0dfa9414959dec60f80` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `booking_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `booking_category` (`id`, `category`) VALUES
(1,	'workspace'),
(2,	'homeoffice'),
(3,	'absence');

CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `BookingInAdvanceDays` int(11) NOT NULL,
  `NamePatternOnMap` varchar(255) NOT NULL,
  `RotateInfocards` tinyint(4) NOT NULL DEFAULT 0,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modificationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedBy` varchar(255) NOT NULL DEFAULT '',
  `modifiedByUsername` varchar(255) NOT NULL DEFAULT '',
  `RotateInfocards` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `keycloak_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `forename` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `default_mapID` int(11) NOT NULL DEFAULT -1,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modificationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedBy` varchar(255) NOT NULL DEFAULT '',
  `modifiedByUsername` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `employee` (`employee_id`, `keycloak_id`, `username`, `forename`, `lastname`, `department`, `email_address`) VALUES
(1,	'', 'dummyuser', 'dummy', 'user', 0, '');


CREATE TABLE `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modificationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedBy` varchar(255) NOT NULL DEFAULT '',
  `modifiedByUsername` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;




CREATE TABLE `map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `imageName` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `mapping_department_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mapID` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `mapping_department_orgaunit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(255) NOT NULL,
  `orgaunit` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `mapping_equipment_workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_id` int(11) NOT NULL,
  `workspace_id` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `mapping_options_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `options_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;




CREATE TABLE `option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modificationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedBy` varchar(255) NOT NULL DEFAULT '',
  `modifiedByUsername` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `building_id` varchar(255) NOT NULL DEFAULT '0020',
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `typeorm_metadata` (
  `type` varchar(255) NOT NULL,
  `database` varchar(255) DEFAULT NULL,
  `schema` varchar(255) DEFAULT NULL,
  `table` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `workspace` (
  `workspace_id` int(11) NOT NULL AUTO_INCREMENT,
  `workspace_name` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `room_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 1,
  `mapID` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `x` int(11) NOT NULL DEFAULT 0,
  `y` int(11) NOT NULL DEFAULT 0,
  `rotation` int(11) NOT NULL DEFAULT 0,
  `scale` double NOT NULL DEFAULT 1,
  `employee_id` int(11) DEFAULT NULL,
  `bookableWhenHO` tinyint(4) NOT NULL DEFAULT 1,
  `modificationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedBy` varchar(255) NOT NULL DEFAULT '',
  `color_id` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`workspace_id`),
  KEY `FK_a05a077e2712359069ec0be92d9` (`room_id`),
  CONSTRAINT `FK_a05a077e2712359069ec0be92d9` FOREIGN KEY (`room_id`) REFERENCES `room` (`room_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `workspacecolor` (
  `color` varchar(255) NOT NULL DEFAULT '000000',
  `description` varchar(255) NOT NULL DEFAULT '',
  `department` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `workspacecolor` (`color`, `description`, `department`, `id`) VALUES
('d9d9d9',	'normaler Arbeitsplatz',	department,	1);

INSERT INTO `option` (`id`, `name`) VALUES
('1',	'anonymous'),
('2',   'Ersthelfer');

INSERT INTO `department` (`name`, `description`, `BookingInAdvanceDays`, `NamePatternOnMap`) VALUES
(department, '', 28, 'forename lastname');

DROP VIEW IF EXISTS `absence_view`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `absence_view` AS select `booking`.`booking_id` AS `booking_id`,`booking`.`booked_for_day` AS `booked_for_day`,`booking`.`morning` AS `morning`,`booking`.`comment` AS `comment`,`booking`.`afternoon` AS `afternoon`,`booking`.`employee_id` AS `employee_id`,`employee`.`forename` AS `forename`,`employee`.`lastname` AS `lastname`,`employee`.`department` AS `department`,`employee`.`email_address` AS `email` from (`booking` join `employee` on(`employee`.`employee_id` = `booking`.`employee_id`)) where `booking`.`category` = 3;

DROP VIEW IF EXISTS `homeoffice_view`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `homeoffice_view` AS select `booking`.`booking_id` AS `booking_id`,`booking`.`booked_for_day` AS `booked_for_day`,`booking`.`morning` AS `morning`,`booking`.`comment` AS `comment`,`booking`.`afternoon` AS `afternoon`,`booking`.`employee_id` AS `employee_id`,`employee`.`forename` AS `forename`,`employee`.`lastname` AS `lastname`,`employee`.`department` AS `department`,`employee`.`email_address` AS `email` from (`booking` join `employee` on(`employee`.`employee_id` = `booking`.`employee_id`)) where `booking`.`category` = 2;

DROP VIEW IF EXISTS `occupied_workspace`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `occupied_workspace` AS select `booking`.`booking_id` AS `booking_id`,`booking`.`workspace_id` AS `workspace_id`,`booking`.`booked_for_day` AS `booked_for_day`,`booking`.`morning` AS `morning`,`booking`.`comment` AS `comment`,`booking`.`afternoon` AS `afternoon`,`booking`.`employee_id` AS `employee_id`,`workspace`.`workspace_name` AS `workspace_name`,`workspace`.`mapID` AS `workspace_mapID`,`workspace`.`x` AS `workspace_x`,`workspace`.`y` AS `workspace_y`,`workspace`.`rotation` AS `workspace_rotation`,`workspace`.`scale` AS `workspace_scale`,`workspace`.`bookableWhenHO` AS `workspace_bookableWhenHO`,`employee`.`forename` AS `forename`,`employee`.`lastname` AS `lastname`,`employee`.`department` AS `department`,`employee`.`email_address` AS `email` from ((`booking` join `workspace` on(`workspace`.`workspace_id` = `booking`.`workspace_id`)) join `employee` on(`employee`.`employee_id` = `booking`.`employee_id`)) where `booking`.`category` = 1;
