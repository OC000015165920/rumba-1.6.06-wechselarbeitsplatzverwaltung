/*
RUMBA SQL Updatescript
Copyright (C) 2022 Kreis Soest 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	This script is used to update the database for RUMBA from version 1.5 to version 1.6. You have to write your databases name into the databasename variable before using the script.
*/
SET NAMES utf8mb4;
SET databasename = '';

USE databasename;

ALTER TABLE `department`
ADD `RotateInfocards` tinyint(4) NOT NULL DEFAULT 0;